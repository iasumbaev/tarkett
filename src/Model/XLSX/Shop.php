<?php

namespace App\Model\XLSX;

class Shop
{
    public $city;

    public $name;

    public $address;

    public $phone;

    public $longitude;

    public $latitude;

    /**
     * @param $city
     * @param $name
     * @param $address
     * @param $phone
     */
    public function __construct($city, $name, $address, $phone)
    {
        $this->city = $city;
        $this->name = $name;
        $this->address = $address;
        $this->phone = $phone;
    }


}