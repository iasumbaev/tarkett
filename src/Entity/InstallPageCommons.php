<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstallPageCommonsRepository")
 */
class InstallPageCommons
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

  
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string")
     */
    private $caption;

    public function getCaption()
    {
        return $this->caption;
    }

    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="subcaption", type="string")
     */
    private $subcaption;

    public function getSubcaption()
    {
        return $this->subcaption;
    }

    public function setSubcaption($subcaption)
    {
        $this->subcaption = $subcaption;
    }





    /**
     * @var string
     *
     * @ORM\Column(name="metatitle", type="string",nullable=true)
     */
    private $metatitle;

    public function getMetatitle()
    {
        return $this->metatitle;
    }

    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="metakeys", type="string",nullable=true)
     */
    private $metakeys;

    public function getMetakeys()
    {
        return $this->metakeys;
    }

    public function setMetakeys($metakeys)
    {
        $this->metakeys = $metakeys;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="metadesc", type="string",nullable=true)
     */
    private $metadesc;

    public function getMetadesc()
    {
        return $this->metadesc;
    }

    public function setMetadesc($metadesc)
    {
        $this->metadesc = $metadesc;
    }




}

?>