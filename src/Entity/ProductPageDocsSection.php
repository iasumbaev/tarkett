<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductPageDocsSectionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ProductPageDocsSection
{

  public function sorter__hasparent(){
    return false;
  }

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

  
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * @ORM\OneToMany(targetEntity="ProductPageYoutube", mappedBy="section", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $youtube;
    // ...

    /**
     * @ORM\OneToMany(targetEntity="ProductPageDoc", mappedBy="section", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $doc;
    // ...


    public function __construct() {
        
        $this->youtube = new ArrayCollection();
        
        $this->doc = new ArrayCollection();

    }


    /**
     * @return Collection|ProductPageYoutube[]
     */
    public function getYoutube(): Collection
    {
        return $this->youtube;
    }

    public function addYoutube(ProductPageYoutube $youtube): self
    {
        if (!$this->youtube->contains($youtube)) {
            $this->youtube[] = $youtube;
            $youtube->setSection($this);
        }

        return $this;
    }

    public function removeYoutube(ProductPageYoutube $youtube): self
    {
        if ($this->youtube->contains($youtube)) {
            $this->youtube->removeElement($youtube);
            // set the owning side to null (unless already changed)
            if ($youtube->getSection() === $this) {
                $youtube->setSection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductPageDoc[]
     */
    public function getDoc(): Collection
    {
        return $this->doc;
    }

    public function addDoc(ProductPageDoc $doc): self
    {
        if (!$this->doc->contains($doc)) {
            $this->doc[] = $doc;
            $doc->setSection($this);
        }

        return $this;
    }

    public function removeDoc(ProductPageDoc $doc): self
    {
        if ($this->doc->contains($doc)) {
            $this->doc->removeElement($doc);
            // set the owning side to null (unless already changed)
            if ($doc->getSection() === $this) {
                $doc->setSection(null);
            }
        }

        return $this;
    }



    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
   }

   public function getPosition(): ?int
   {
       return $this->position;
   }

   public function setPosition(?int $position): self
   {
       $this->position = $position;

       return $this;
   }



}

?>