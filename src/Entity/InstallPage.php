<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstallPageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class InstallPage
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

  
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="reqcaption", type="string",nullable=true)
     */
    private $reqcaption;

    public function getReqcaption()
    {
        return $this->reqcaption;
    }

    public function setReqcaption($reqcaption)
    {
        $this->reqcaption = $reqcaption;
    }



    /**
     * @ORM\OneToMany(targetEntity="InstallPageReqs", mappedBy="install", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $reqs;
    // ...




    /**
     * @var string
     *
     * @ORM\Column(name="needcaption", type="string",nullable=true)
     */
    private $needcaption;

    public function getNeedcaption()
    {
        return $this->needcaption;
    }

    public function setNeedcaption($needcaption)
    {
        $this->needcaption = $needcaption;
    }



    /**
     * @ORM\OneToMany(targetEntity="InstallPageNeeds", mappedBy="install", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $needs;
    // ...




    /**
     * @var string
     *
     * @ORM\Column(name="compcaption", type="string",nullable=true)
     */
    private $compcaption;

    public function getCompcaption()
    {
        return $this->compcaption;
    }

    public function setCompcaption($compcaption)
    {
        $this->compcaption = $compcaption;
    }



    /**
     * @ORM\OneToMany(targetEntity="InstallPageComps", mappedBy="install", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $comps;
    // ...






    /**
     * @var string
     *
     * @ORM\Column(name="stagecaption", type="string",nullable=true)
     */
    private $stagecaption;

    public function getStagecaption()
    {
        return $this->stagecaption;
    }

    public function setStagecaption($stagecaption)
    {
        $this->stagecaption = $stagecaption;
    }



    /**
     * @ORM\OneToMany(targetEntity="InstallPageStages", mappedBy="install", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $stages;
    // ...



    /**
     * @ORM\OneToMany(targetEntity="InstallPageFAQ", mappedBy="install", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $faq;
    // ...


    /**
     * @ORM\OneToMany(targetEntity="InstallPageDocsSection", mappedBy="install", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $docs;
    // ...


    public function __construct() {

        $this->reqs = new ArrayCollection();

        $this->needs = new ArrayCollection();

        $this->comps = new ArrayCollection();

        $this->stages = new ArrayCollection();
        
        $this->faq = new ArrayCollection();
        
        $this->docs = new ArrayCollection();

    }




    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string")
     */
    private $icon;

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }


    private $iconFile;
    /**
     * @param UploadedFile $iconFile
     */
    public function setIconFile(UploadedFile $iconFile = null)
    {
        $this->iconFile = $iconFile;
    }
    /**
     * @return UploadedFile
     */
    public function getIconFile()
    {
        return $this->iconFile;
    }

    public function uploadIconFile()
    {


        if (null === $this->getIconFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getIconFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/install/top',$this->icon);

       $this->getIconFile()->move(
           __DIR__.'/../../public/install/top',
           $filename
       );

       $this->icon = $filename;

       $this->setIconFile(null);

   }



    /**
     * @var string
     *
     * @ORM\Column(name="activeicon", type="string")
     */
    private $activeicon;

    public function getActiveicon()
    {
        return $this->activeicon;
    }

    public function setActiveicon($activeicon)
    {
        $this->activeicon = $activeicon;
    }


    private $activeiconFile;
    /**
     * @param UploadedFile $activeiconFile
     */
    public function setActiveiconFile(UploadedFile $activeiconFile = null)
    {
        $this->activeiconFile = $activeiconFile;
    }
    /**
     * @return UploadedFile
     */
    public function getActiveiconFile()
    {
        return $this->activeiconFile;
    }

    public function uploadActiveiconFile()
    {


        if (null === $this->getActiveiconFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getActiveiconFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/install/top',$this->activeicon);

       $this->getActiveiconFile()->move(
           __DIR__.'/../../public/install/top',
           $filename
       );

       $this->activeicon = $filename;

       $this->setActiveiconFile(null);

   }




    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     */
    private $image;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }


    private $imageFile;
    /**
     * @param UploadedFile $imageFile
     */
    public function setImageFile(UploadedFile $imageFile = null)
    {
        $this->imageFile = $imageFile;
    }
    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function uploadImageFile()
    {


        if (null === $this->getImageFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getImageFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/install',$this->image);

       $this->getImageFile()->move(
           __DIR__.'/../../public/install',
           $filename
       );

       $this->image = $filename;

       $this->setImageFile(null);

   }

    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadIconFile();
       $this->uploadActiveiconFile();
       $this->uploadImageFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }

  /**
   * @return Collection|InstallPageReqs[]
   */
  public function getReqs(): Collection
  {
      return $this->reqs;
  }

  public function addReq(InstallPageReqs $req): self
  {
      if (!$this->reqs->contains($req)) {
          $this->reqs[] = $req;
          $req->setInstall($this);
      }

      return $this;
  }

  public function removeReq(InstallPageReqs $req): self
  {
      if ($this->reqs->contains($req)) {
          $this->reqs->removeElement($req);
          // set the owning side to null (unless already changed)
          if ($req->getInstall() === $this) {
              $req->setInstall(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|InstallPageNeeds[]
   */
  public function getNeeds(): Collection
  {
      return $this->needs;
  }

  public function addNeed(InstallPageNeeds $need): self
  {
      if (!$this->needs->contains($need)) {
          $this->needs[] = $need;
          $need->setInstall($this);
      }

      return $this;
  }

  public function removeNeed(InstallPageNeeds $need): self
  {
      if ($this->needs->contains($need)) {
          $this->needs->removeElement($need);
          // set the owning side to null (unless already changed)
          if ($need->getInstall() === $this) {
              $need->setInstall(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|InstallPageComps[]
   */
  public function getComps(): Collection
  {
      return $this->comps;
  }

  public function addComp(InstallPageComps $comp): self
  {
      if (!$this->comps->contains($comp)) {
          $this->comps[] = $comp;
          $comp->setInstall($this);
      }

      return $this;
  }

  public function removeComp(InstallPageComps $comp): self
  {
      if ($this->comps->contains($comp)) {
          $this->comps->removeElement($comp);
          // set the owning side to null (unless already changed)
          if ($comp->getInstall() === $this) {
              $comp->setInstall(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|InstallPageStages[]
   */
  public function getStages(): Collection
  {
      return $this->stages;
  }

  public function addStage(InstallPageStages $stage): self
  {
      if (!$this->stages->contains($stage)) {
          $this->stages[] = $stage;
          $stage->setInstall($this);
      }

      return $this;
  }

  public function removeStage(InstallPageStages $stage): self
  {
      if ($this->stages->contains($stage)) {
          $this->stages->removeElement($stage);
          // set the owning side to null (unless already changed)
          if ($stage->getInstall() === $this) {
              $stage->setInstall(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|InstallPageFAQ[]
   */
  public function getFaq(): Collection
  {
      return $this->faq;
  }

  public function addFaq(InstallPageFAQ $faq): self
  {
      if (!$this->faq->contains($faq)) {
          $this->faq[] = $faq;
          $faq->setInstall($this);
      }

      return $this;
  }

  public function removeFaq(InstallPageFAQ $faq): self
  {
      if ($this->faq->contains($faq)) {
          $this->faq->removeElement($faq);
          // set the owning side to null (unless already changed)
          if ($faq->getInstall() === $this) {
              $faq->setInstall(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|InstallPageDocsSection[]
   */
  public function getDocs(): Collection
  {
      return $this->docs;
  }

  public function addDoc(InstallPageDocsSection $doc): self
  {
      if (!$this->docs->contains($doc)) {
          $this->docs[] = $doc;
          $doc->setInstall($this);
      }

      return $this;
  }

  public function removeDoc(InstallPageDocsSection $doc): self
  {
      if ($this->docs->contains($doc)) {
          $this->docs->removeElement($doc);
          // set the owning side to null (unless already changed)
          if ($doc->getInstall() === $this) {
              $doc->setInstall(null);
          }
      }

      return $this;
  }



    /**
     * @var string
     *
     * @ORM\Column(name="designcaption", type="string",nullable=true)
     */
    private $designcaption;

    public function getDesigncaption()
    {
        return $this->designcaption;
    }

    public function setDesigncaption($designcaption)
    {
        $this->designcaption = $designcaption;
    }


    /**
     * @var text
     *
     * @ORM\Column(name="designtext", type="text",nullable=true)
     */
    private $designtext;

    public function getDesigntext()
    {
        return $this->designtext;
    }

    public function setDesigntext($designtext)
    {
        $this->designtext = $designtext;
    }



}

?>