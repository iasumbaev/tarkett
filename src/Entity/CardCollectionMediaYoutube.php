<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardCollectionMediaYoutubeRepository")
 */
class CardCollectionMediaYoutube
{

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * @var text
     *
     * @ORM\Column(name="href", type="text")
     */
    private $href;

    public function getHref()
    {
        return $this->href;
    }

    public function setHref($href)
    {
        $this->href = $href;
    }

  /**
     * @ORM\ManyToOne(targetEntity="CardCollection", inversedBy="mediayoutube")
     * @ORM\JoinColumn(name="collection_id", referencedColumnName="id")
     */
    private $collection;

    public function getCollection(): ?CardCollection
    {
        return $this->collection;
    }

    public function setCollection(?CardCollection $collection): self
    {
        $this->collection = $collection;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }



}

?>