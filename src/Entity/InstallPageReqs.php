<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstallPageReqsRepository")
 */
class InstallPageReqs
{


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

  
    public function getId()
    {
        return $this->id;
    }


  /**
     * @ORM\ManyToOne(targetEntity="InstallPage", inversedBy="reqs")
     * @ORM\JoinColumn(name="install_id", referencedColumnName="id")
     */
    private $install;



    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getInstall(): ?InstallPage
    {
        return $this->install;
    }

    public function setInstall(?InstallPage $install): self
    {
        $this->install = $install;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }


}

?>