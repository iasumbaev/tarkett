<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardCollectionTargetRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CardCollectionTarget
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string")
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="gicon", type="string")
     */
    private $gicon;

  
    public function getId()
    {
        return $this->id;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }




    private $iconFile;
    /**
     * @param UploadedFile $iconFile
     */
    public function setIconFile(UploadedFile $iconFile = null)
    {
        $this->iconFile = $iconFile;
    }
    /**
     * @return UploadedFile
     */
    public function getIconFile()
    {
        return $this->iconFile;
    }

    public function uploadIconFile()
    {


        if (null === $this->getIconFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getIconFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];

        Utils::RemoveFile(__DIR__.'/../../public/collections/icons',$this->icon);

       $this->getIconFile()->move(
           __DIR__.'/../../public/collections/icons',
           $filename
       );

       $this->icon = $filename;

       $this->setIconFile(null);

   }



    /**
     * @param UploadedFile $giconFile
     */

    public function getGicon()
    {
        return $this->gicon;
    }

    public function setGicon($gicon)
    {
        $this->gicon = $gicon;
    }

    private $giconFile;
    /**
     * @param UploadedFile $giconFile
     */
    public function setGiconFile(UploadedFile $giconFile = null)
    {
        $this->giconFile = $giconFile;
    }
    /**
     * @return UploadedFile
     */
    public function getGiconFile()
    {
        return $this->giconFile;
    }

    public function uploadGiconFile()
    {


        if (null === $this->getGiconFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getGiconFile()->getClientOriginalName());
        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/collections/icons',$this->gicon);

       $this->getGiconFile()->move(
           __DIR__.'/../../public/collections/icons',
           $filename
       );

       $this->gicon = $filename;

       $this->setGiconFile(null);

   }


    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadIconFile();
       $this->uploadGiconFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }


}

?>