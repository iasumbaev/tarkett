<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductPageSliderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ProductPageSlider
{

  public function sorter__hasparent(){
    return false;
  }

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;


  
    public function getId()
    {
        return $this->id;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

 /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductPageSliderInfo", mappedBy="slider", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $info;

    public function __construct()
    {
        $this->info = new ArrayCollection();
    }

  /**
     * @return Collection|ProductPageSliderInfo[]
     */
    public function getInfo(): Collection
    {
        return $this->info;
    }

    public function addInfo(ProductPageSliderInfo $info): self
    {
        if (!$this->info->contains($info)) {
            $this->info[] = $info;
            $info->setSlider($this);
        }

        return $this;
    }

    public function removeInfo(ProductPageSliderInfo $info): self
    {
        if ($this->info->contains($info)) {
            $this->info->removeElement($info);
            // set the owning side to null (unless already changed)
            if ($info->getSlider() === $this) {
                $info->setSlider(null);
            }
        }

        return $this;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     */
    private $image;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }


    private $imageFile;
    /**
     * @param UploadedFile $imageFile
     */
    public function setImageFile(UploadedFile $imageFile = null)
    {
        $this->imageFile = $imageFile;
    }
    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function uploadImageFile()
    {


        if (null === $this->getImageFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getImageFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/product/top',$this->image);

       $this->getImageFile()->move(
           __DIR__.'/../../public/product/top',
           $filename
       );

       $this->image = $filename;

       $this->setImageFile(null);

   }

    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadImageFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }

  public function getPosition(): ?int
  {
      return $this->position;
  }

  public function setPosition(?int $position): self
  {
      $this->position = $position;

      return $this;
  }



}

?>