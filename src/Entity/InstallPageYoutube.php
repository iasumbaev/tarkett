<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstallPageYoutubeRepository")
 */
class InstallPageYoutube
{


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

  
    public function getId()
    {
        return $this->id;
    }


  /**
     * @ORM\ManyToOne(targetEntity="InstallPageDocsSection", inversedBy="youtube")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id")
     */
    private $section;



    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @var text
     *
     * @ORM\Column(name="href", type="text")
     */
    private $href;

    public function getHref()
    {
        return $this->href;
    }

    public function setHref($href)
    {
        $this->href = $href;
    }

    public function getSection(): ?InstallPageDocsSection
    {
        return $this->section;
    }

    public function setSection(?InstallPageDocsSection $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }



}

?>