<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardCollectionInterestRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CardCollectionInterest
{

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", nullable=true)
     */
    private $image;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }


  /**
     * @ORM\ManyToOne(targetEntity="CardCollection", inversedBy="interest")
     * @ORM\JoinColumn(name="collection_id", referencedColumnName="id")
     */
    private $collection;

    public function getCollection(): ?CardCollection
    {
        return $this->collection;
    }

    public function setCollection(?CardCollection $collection): self
    {
        $this->collection = $collection;

        return $this;
    }



    private $imageFile;
    /**
     * @param UploadedFile $imageFile
     */
    public function setImageFile(UploadedFile $imageFile = null)
    {
        $this->imageFile = $imageFile;
    }
    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function uploadImageFile()
    {


        if (null === $this->getImageFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getImageFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];

        Utils::RemoveFile(__DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/interest',$this->image);

       $this->getImageFile()->move(
           __DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/interest',
           $filename
       );

       $this->image = $filename;

       $this->setImageFile(null);

   }

    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadImageFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }

  public function getPosition(): ?int
  {
      return $this->position;
  }

  public function setPosition(?int $position): self
  {
      $this->position = $position;

      return $this;
  }


  public function isSVG() {
       return strpos($this->image, '.svg') !== false;
  }

}