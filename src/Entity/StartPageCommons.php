<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StartPageCommonsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class StartPageCommons
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

  
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string")
     */
    private $caption;

    public function getCaption()
    {
        return $this->caption;
    }

    public function setCaption($caption)
    {
        $this->caption = $caption;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="designcaption", type="string",nullable=true)
     */
    private $designcaption;

    public function getDesigncaption()
    {
        return $this->designcaption;
    }

    public function setDesigncaption($designcaption)
    {
        $this->designcaption = $designcaption;
    }


    /**
     * @var text
     *
     * @ORM\Column(name="designtext", type="text",nullable=true)
     */
    private $designtext;

    public function getDesigntext()
    {
        return $this->designtext;
    }

    public function setDesigntext($designtext)
    {
        $this->designtext = $designtext;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="metatitle", type="string",nullable=true)
     */
    private $metatitle;

    public function getMetatitle()
    {
        return $this->metatitle;
    }

    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="metakeys", type="string",nullable=true)
     */
    private $metakeys;

    public function getMetakeys()
    {
        return $this->metakeys;
    }

    public function setMetakeys($metakeys)
    {
        $this->metakeys = $metakeys;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="metadesc", type="string",nullable=true)
     */
    private $metadesc;

    public function getMetadesc()
    {
        return $this->metadesc;
    }

    public function setMetadesc($metadesc)
    {
        $this->metadesc = $metadesc;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="captionaside", type="string")
     */
    private $captionaside;

    public function getCaptionaside()
    {
        return $this->captionaside;
    }

    public function setCaptionaside($captionaside)
    {
        $this->captionaside = $captionaside;
    }


    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }




}

?>