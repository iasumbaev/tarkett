<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardCollectionDocumentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CardCollectionDocument
{

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", nullable=true)
     */
    private $file;

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    private $size;

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type;

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }


  /**
     * @ORM\ManyToOne(targetEntity="CardCollection", inversedBy="docs")
     * @ORM\JoinColumn(name="collection_id", referencedColumnName="id")
     */
    private $collection;

    public function getCollection(): ?CardCollection
    {
        return $this->collection;
    }

    public function setCollection(?CardCollection $collection): self
    {
        $this->collection = $collection;

        return $this;
    }




    private $fileFile;
    /**
     * @param UploadedFile $fileFile
     */
    public function setFileFile(UploadedFile $fileFile = null)
    {
        $this->fileFile = $fileFile;
    }
    /**
     * @return UploadedFile
     */
    public function getFileFile()
    {
        return $this->fileFile;
    }

    public function uploadFileFile()
    {


        if (null === $this->getFileFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getFileFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];

        Utils::RemoveFile(__DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/docs',$this->file);

        $filepath = __DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/docs/'.$filename;
       
       $this->getFileFile()->move(
           __DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/docs',
           $filename
       );

       $this->size = filesize($filepath);
       
       $this->type = strtoupper($fileinfo['extension']);

       $this->file = $filename;

       $this->setFileFile(null);

   }

    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadFileFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }

  public function getPosition(): ?int
  {
      return $this->position;
  }

  public function setPosition(?int $position): self
  {
      $this->position = $position;

      return $this;
  }




}

?>