<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StartPageFactsInfoRepository")
 */
class StartPageFactsInfo
{


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string")
     */
    private $text;


  
    public function getId()
    {
        return $this->id;
    }


    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StartPageFacts", inversedBy="info")
     */
    private $fact;

    public function getFact(): ?StartPageFacts
    {
        return $this->fact;
    }

    public function setFact(?StartPageFacts $fact): self
    {
        $this->fact = $fact;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

}

?>