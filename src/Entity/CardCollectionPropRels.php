<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardCollectionPropRelsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CardCollectionPropRels
{

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardCollection", inversedBy="props")
     */
    private $collection;

    public function getCollection(): ?CardCollection
    {
        return $this->collection;
    }

  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardCollectionProp")
     * @ORM\JoinColumn(name="prop_id", referencedColumnName="id")
     */
    private $prop;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setCollection(?CardCollection $collection): self
    {
        $this->collection = $collection;

        return $this;
    }

    public function getProp(): ?CardCollectionProp
    {
        return $this->prop;
    }

    public function setProp(?CardCollectionProp $prop): self
    {
        $this->prop = $prop;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }


}

?>