<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Card
{

  public function sorter__hasparent(){
    return true;
  }

  public function sorter__getparent(){
    return $this->getCollection()->getId();
  }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

  
    public function getId()
    {
        return $this->id;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="factorwidth", type="integer",nullable=true)
     */
    private $factorwidth;

    public function getFactorwidth()
    {
        return $this->factorwidth;
    }

    public function setFactorwidth($factorwidth)
    {
        $this->factorwidth = $factorwidth;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="factorlength", type="integer",nullable=true)
     */
    private $factorlength;

    public function getFactorlength()
    {
        return $this->factorlength;
    }

    public function setFactorlength($factorlength)
    {
        $this->factorlength = $factorlength;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="factorcount", type="integer",nullable=true)
     */
    private $factorcount;

    public function getFactorcount()
    {
        return $this->factorcount;
    }

    public function setFactorcount($factorcount)
    {
        $this->factorcount = $factorcount;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="factorarea", type="integer",nullable=true)
     */
    private $factorarea;

    public function getFactorarea()
    {
        return $this->factorarea;
    }

    public function setFactorarea($factorarea)
    {
        $this->factorarea = $factorarea;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="metatitle", type="string",nullable=true)
     */
    private $metatitle;

    public function getMetatitle()
    {
        return $this->metatitle;
    }

    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="metakeys", type="string",nullable=true)
     */
    private $metakeys;

    public function getMetakeys()
    {
        return $this->metakeys;
    }

    public function setMetakeys($metakeys)
    {
        $this->metakeys = $metakeys;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="metadesc", type="string",nullable=true)
     */
    private $metadesc;

    public function getMetadesc()
    {
        return $this->metadesc;
    }

    public function setMetadesc($metadesc)
    {
        $this->metadesc = $metadesc;
    }





  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;


    /**
     * @ORM\OneToMany(targetEntity="CardImage", mappedBy="card", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $images;
    // ...


    /**
     * @ORM\OneToMany(targetEntity="CardValue", mappedBy="card", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $values;
    // ...

    public function __construct() {

        $this->images = new ArrayCollection();
        $this->values = new ArrayCollection();

    }



  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardCollection", inversedBy="cards")
     */
    private $collection;

    public function getCollection(): ?CardCollection
    {
        return $this->collection;
    }

    public function getType(): ?CardType
    {
        return $this->type;
    }

    public function setType(?CardType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|CardImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(CardImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setCard($this);
        }

        return $this;
    }

    public function removeImage(CardImage $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getCard() === $this) {
                $image->setCard(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CardValue[]
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    public function addValue(CardValue $value): self
    {
        if (!$this->values->contains($value)) {
            $this->values[] = $value;
            $value->setCard($this);
        }

        return $this;
    }

    public function removeValue(CardValue $value): self
    {
        if ($this->values->contains($value)) {
            $this->values->removeElement($value);
            // set the owning side to null (unless already changed)
            if ($value->getCard() === $this) {
                $value->setCard(null);
            }
        }

        return $this;
    }

    public function setCollection(?CardCollection $collection): self
    {
        $this->collection = $collection;

        return $this;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     */
    private $image;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }


    private $imageFile;
    /**
     * @param UploadedFile $imageFile
     */
    public function setImageFile(UploadedFile $imageFile = null)
    {
        $this->imageFile = $imageFile;
    }
    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function uploadImageFile()
    {


        if (null === $this->getImageFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getImageFile()->getClientOriginalName());
        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($this->title)),$this->image);

       $this->getImageFile()->move(
           __DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($this->title)),
           $filename
       );

       $this->image = $filename;

       $this->setImageFile(null);

   }




    /**
     * @var string
     *
     * @ORM\Column(name="texture", type="string", nullable=true)
     */
    private $texture;

    public function getTexture()
    {
        return $this->texture;
    }

    public function setTexture($texture)
    {
        $this->texture = $texture;
    }



    private $textureFile;
    /**
     * @param UploadedFile $textureFile
     */
    public function setTextureFile(UploadedFile $textureFile = null)
    {
        $this->textureFile = $textureFile;
    }
    /**
     * @return UploadedFile
     */
    public function getTextureFile()
    {
        return $this->textureFile;
    }

    public function uploadTextureFile()
    {


        if (null === $this->getTextureFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getTextureFile()->getClientOriginalName());
        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($this->title)),$this->texture);

       $this->getTextureFile()->move(
           __DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($this->title)),
           $filename
       );

       $this->texture = $filename;

       $this->setTextureFile(null);

   }




    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", nullable=true)
     */
    private $model;

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }




    private $modelFile;
    /**
     * @param UploadedFile $modelFile
     */
    public function setModelFile(UploadedFile $modelFile = null)
    {
        $this->modelFile = $modelFile;
    }
    /**
     * @return UploadedFile
     */
    public function getModelFile()
    {
        return $this->modelFile;
    }

    public function uploadModelFile()
    {


        if (null === $this->getModelFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getModelFile()->getClientOriginalName());
        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($this->title)),$this->model);

       $this->getModelFile()->move(
           __DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($this->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($this->title)),
           $filename
       );

       $this->model = $filename;

       $this->setModelFile(null);

   }


    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadImageFile();
       $this->uploadTextureFile();
       $this->uploadModelFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }

  public function getPosition(): ?int
  {
      return $this->position;
  }

  public function setPosition(int $position): self
  {
      $this->position = $position;

      return $this;
  }




}

?>