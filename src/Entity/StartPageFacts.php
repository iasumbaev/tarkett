<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StartPageFactsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class StartPageFacts
{

  public function sorter__hasparent(){
    return false;
  }

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;


  
    public function getId()
    {
        return $this->id;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

 /**
     * @ORM\OneToMany(targetEntity="App\Entity\StartPageFactsInfo", mappedBy="fact", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $info;

    public function __construct()
    {
        $this->info = new ArrayCollection();
    }

  /**
     * @return Collection|StartPageFactsInfo[]
     */
    public function getInfo(): Collection
    {
        return $this->info;
    }

    public function addInfo(StartPageFactsInfo $info): self
    {
        if (!$this->info->contains($info)) {
            $this->info[] = $info;
            $info->setFact($this);
        }

        return $this;
    }

    public function removeInfo(StartPageFactsInfo $info): self
    {
        if ($this->info->contains($info)) {
            $this->info->removeElement($info);
            // set the owning side to null (unless already changed)
            if ($info->getFact() === $this) {
                $info->setFact(null);
            }
        }

        return $this;
    }


   /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string")
     */
    private $icon;

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }




    private $iconFile;
    /**
     * @param UploadedFile $iconFile
     */
    public function setIconFile(UploadedFile $iconFile = null)
    {
        $this->iconFile = $iconFile;
    }
    /**
     * @return UploadedFile
     */
    public function getIconFile()
    {
        return $this->iconFile;
    }

    public function uploadIconFile()
    {


        if (null === $this->getIconFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getIconFile()->getClientOriginalName());


        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/start/facts'.$this->icon);

       $this->getIconFile()->move(
           __DIR__.'/../../public/start/facts',
           $filename
       );

       $this->icon = $filename;

       $this->setIconFile(null);

   }

    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadIconFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }

  public function getPosition(): ?int
  {
      return $this->position;
  }

  public function setPosition(?int $position): self
  {
      $this->position = $position;

      return $this;
  }


}

?>