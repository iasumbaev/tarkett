<?php

namespace App\Entity;

use App\Utils\Utils;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleVideoRepository")
 */
class ArticleVideo
{
    use TimestampableEntity;

    public const PATH = '/uploads/article_videos/';
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $originalFilename = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mimeType = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $extension = '';

    /**
     * @ORM\Column(type="bigint")
     */
    private $size = 0;

    /**
     * @var UploadedFile $videoFile
     * для сонаты
     */
    private $videoFile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getOriginalFilename(): ?string
    {
        return $this->originalFilename;
    }

    public function setOriginalFilename(string $originalFilename): self
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getFilePath(): string
    {
        return self::PATH . $this->getFilename();
    }

    /**
     * @return UploadedFile
     */
    public function getVideoFile(): ?UploadedFile
    {
        return $this->videoFile;
    }

    /**
     * @param UploadedFile $videoFile
     * @return ArticleVideo
     */
    public function setVideoFile(?UploadedFile $videoFile): self
    {
        $this->videoFile = $videoFile;

        return $this;
    }

    public function initialize() {
        if (null === $this->videoFile) {
            return;
        }
        $this->filename = $this->videoFile->getClientOriginalName();
        $this->extension = $this->videoFile->guessExtension();
        $this->mimeType = $this->videoFile->getClientMimeType();
        $this->size = $this->videoFile->getSize();
    }

    // todo: вынести в сервис
    public function uploadVideoFile()
    {
        if (null === $this->videoFile) {
            return;
        }

        $newFilename = Urlizer::urlize(pathinfo($this->videoFile->getClientOriginalName(), PATHINFO_FILENAME)) . '-' . uniqid('', true) . '.' . $this->videoFile->guessExtension();

        Utils::RemoveFile(__DIR__ . '/../../public/uploads/article_videos/', $this->filename);

        $this->videoFile->move(
            __DIR__ . '/../../public/uploads/article_videos/',
            $newFilename
        );

        $this->filename = $newFilename;

        $this->setVideoFile(null);

    }
}
