<?php


namespace App\Entity;

use App\Model\XLSX\Shop;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactsPageLocationsRepository")
 */
class ContactsPageLocations
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;


    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="float")
     */
    private $lng;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float")
     */
    private $lat;


    //Создание объекта на основе импортированного магазина из таблицы
    public function __construct(Shop $shop)
    {
        $this->title = $shop->name;
        $this->address = $shop->address;
        $this->phone = $shop->phone;
        $this->lng = $shop->longitude;
        $this->lat = $shop->latitude;
    }


    public function getId()
    {
        return $this->id;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }


    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }


    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


    public function getLat()
    {
        return $this->lat;
    }

    public function setLat($lat)
    {
        $this->lat = $lat;
    }


    public function getLng()
    {
        return $this->lng;
    }

    public function setLng($lng)
    {
        $this->lng = $lng;
    }


  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ContactsPageCities", inversedBy="locations")
     */
    private $city;

    public function getCity(): ?ContactsPageCities
    {
        return $this->city;
    }

    public function setCity(?ContactsPageCities $city): self
    {
        $this->city = $city;

        return $this;
    }


}
