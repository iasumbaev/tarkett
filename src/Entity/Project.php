<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use App\Utils\Utils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Sluggable\Util\Urlizer;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{

    use TimestampableEntity;
    use MetaTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"title"})
     */
    private $slug;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $square;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $designer;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $imageFileName;

    /**
     * @var UploadedFile $imageFile
     * для сонаты
     */
    private $imageFile;

    /**
     * @ORM\Column(name="alt_image",type="string", length=255, nullable=true)
     */
    private $altImage;

    /**
     * @ORM\ManyToMany(targetEntity=CardCollection::class, inversedBy="projects")
     */
    private $collections;

    public function __construct()
    {
        $this->collections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSquare(): ?float
    {
        return $this->square;
    }

    public function setSquare(?float $square): self
    {
        $this->square = $square;

        return $this;
    }

    public function getDesigner(): ?string
    {
        return $this->designer;
    }

    public function setDesigner(?string $designer): self
    {
        $this->designer = $designer;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getImageFileName(): ?string
    {
        return $this->imageFileName;
    }

    public function setImageFileName(?string $imageFileName): self
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    /**
     * @param UploadedFile $imageFile
     */
    public function setImageFile(UploadedFile $imageFile = null)
    {
        $this->imageFile = $imageFile;
    }

    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    // todo: вынести в сервис
    public function uploadImageFile()
    {
        if (null === $this->imageFile) {
            return;
        }

        $newFilename = Urlizer::urlize(pathinfo($this->imageFile->getClientOriginalName(), PATHINFO_FILENAME)) . '-' . uniqid('', true) . '.' . $this->imageFile->guessExtension();

        Utils::RemoveFile(__DIR__ . '/../../public/uploads/project_preview/', $this->imageFileName);

        $this->getImageFile()->move(
            __DIR__ . '/../../public/uploads/project_preview/',
            $newFilename
        );

        $this->imageFileName = $newFilename;

        $this->setImageFile(null);

    }

    public function getAltImage(): ?string
    {
        return $this->altImage;
    }

    public function setAltImage(?string $altImage): self
    {
        $this->altImage = $altImage;
        return $this;
    }

    /**
     * @return Collection|CardCollection[]
     */
    public function getCollections(): Collection
    {
        return $this->collections;
    }

    public function addCollection(CardCollection $collection): self
    {
        if (!$this->collections->contains($collection)) {
            $this->collections[] = $collection;
        }

        return $this;
    }

    public function removeCollection(CardCollection $collection): self
    {
        if ($this->collections->contains($collection)) {
            $this->collections->removeElement($collection);
        }

        return $this;
    }
}
