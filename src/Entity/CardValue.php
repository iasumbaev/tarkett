<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardValueRepository")
 */
class CardValue
{


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  

    public function getId()
    {
        return $this->id;
    }


  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardChar")
     * @ORM\JoinColumn(name="char_id", referencedColumnName="id")
     */
    private $char;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string")
     */
    private $value;


    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }


  /**
     * @ORM\ManyToOne(targetEntity="Card", inversedBy="values")
     * @ORM\JoinColumn(name="card_id", referencedColumnName="id")
     */
    private $card;

    public function getCard(): ?Card
    {
        return $this->card;
    }

    public function getChar(): ?CardChar
    {
        return $this->char;
    }

    public function setChar(?CardChar $char): self
    {
        $this->char = $char;

        return $this;
    }

    public function setCard(?Card $card): self
    {
        $this->card = $card;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }



}

?>