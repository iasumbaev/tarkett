<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstallPageCompsRepository")
 * @ORM\HasLifecycleCallbacks()
*/
class InstallPageComps
{


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

  
    public function getId()
    {
        return $this->id;
    }


  /**
     * @ORM\ManyToOne(targetEntity="InstallPage", inversedBy="comps")
     * @ORM\JoinColumn(name="install_id", referencedColumnName="id")
     */
    private $install;



    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getInstall(): ?InstallPage
    {
        return $this->install;
    }

    public function setInstall(?InstallPage $install): self
    {
        $this->install = $install;

        return $this;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string")
     */
    private $icon;

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }


    private $iconFile;
    /**
     * @param UploadedFile $iconFile
     */
    public function setIconFile(UploadedFile $iconFile = null)
    {
        $this->iconFile = $iconFile;
    }
    /**
     * @return UploadedFile
     */
    public function getIconFile()
    {
        return $this->iconFile;
    }

    public function uploadIconFile()
    {


        if (null === $this->getIconFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getIconFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/install/need',$this->icon);

       $this->getIconFile()->move(
           __DIR__.'/../../public/install/need',
           $filename
       );

       $this->icon = $filename;

       $this->setIconFile(null);

   }


    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadIconFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }

  public function getPosition(): ?int
  {
      return $this->position;
  }

  public function setPosition(?int $position): self
  {
      $this->position = $position;

      return $this;
  }


}

?>