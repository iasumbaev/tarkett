<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductPageSliderInfoRepository")
 */
class ProductPageSliderInfo
{


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string")
     */
    private $text;


  
    public function getId()
    {
        return $this->id;
    }


    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductPageSlider", inversedBy="info")
     */
    private $slider;

    public function getSlider(): ?ProductPageSlider
    {
        return $this->slider;
    }

    public function setSlider(?ProductPageSlider $slider): self
    {
        $this->slider = $slider;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

}

?>