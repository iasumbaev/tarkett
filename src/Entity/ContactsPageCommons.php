<?php

// src/Entity/Category.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactsPageCommonsRepository")
 */
class ContactsPageCommons
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

  
    public function getId()
    {
        return $this->id;
    }


    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }




    /**
     * @var string
     *
     * @ORM\Column(name="metatitle", type="string",nullable=true)
     */
    private $metatitle;

    public function getMetatitle()
    {
        return $this->metatitle;
    }

    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="metakeys", type="string",nullable=true)
     */
    private $metakeys;

    public function getMetakeys()
    {
        return $this->metakeys;
    }

    public function setMetakeys($metakeys)
    {
        $this->metakeys = $metakeys;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="metadesc", type="string",nullable=true)
     */
    private $metadesc;

    public function getMetadesc()
    {
        return $this->metadesc;
    }

    public function setMetadesc($metadesc)
    {
        $this->metadesc = $metadesc;
    }




}

?>