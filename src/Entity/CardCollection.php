<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardCollectionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CardCollection
{

  public function sorter__hasparent(){
    return false;
  }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

  
    public function getId()
    {
        return $this->id;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }



    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer",nullable=true)
     */
    private $type;

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="designtitle", type="string",nullable=true)
     */
    private $designtitle;

    public function getDesigntitle()
    {
        return $this->designtitle;
    }

    public function setDesigntitle($designtitle)
    {
        $this->designtitle = $designtitle;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="metatitle", type="string",nullable=true)
     */
    private $metatitle;

    public function getMetatitle()
    {
        return $this->metatitle;
    }

    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="metakeys", type="string",nullable=true)
     */
    private $metakeys;

    public function getMetakeys()
    {
        return $this->metakeys;
    }

    public function setMetakeys($metakeys)
    {
        $this->metakeys = $metakeys;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="metadesc", type="string",nullable=true)
     */
    private $metadesc;

    public function getMetadesc()
    {
        return $this->metadesc;
    }

    public function setMetadesc($metadesc)
    {
        $this->metadesc = $metadesc;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", nullable=true)
     */
    private $video;

    public function getVideo()
    {
        return $this->video;
    }

    public function setVideo($video)
    {
        $this->video = $video;
    }



  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardCollectionClass")
     * @ORM\JoinColumn(name="class_id", referencedColumnName="id", nullable=true)
     */
    private $class;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CardCollectionTargetRels", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $targets;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CardCollectionPropRels", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $props;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CardCollectionLayout", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $layout;


    /**
     * @var string
     *
     * @ORM\Column(name="texttop", type="text",nullable=true)
     */
    private $texttop;

  
    public function getTexttop()
    {
        return $this->texttop;
    }

    public function getTexttopRaw()
    {
        return nl2br($this->texttop);
    }

    public function setTexttop($texttop)
    {
        $this->texttop = $texttop;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="textbottom", type="text",nullable=true)
     */
    private $textbottom;

  
    public function getTextbottom()
    {
        return $this->textbottom;
    }

    public function getTextbottomRaw()
    {
        return nl2br($this->textbottom);
    }

    public function setTextbottom($textbottom)
    {
        $this->textbottom = $textbottom;
    }


    /**
     * @ORM\OneToMany(targetEntity="CardCollectionInterest", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $interest;
    // ...

    /**
     * @ORM\OneToMany(targetEntity="CardCollectionFAQ", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $faq;
    // ...


    /**
     * @ORM\OneToMany(targetEntity="CardCollectionInterior", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $interiors;
    // ...

    /**
     * @ORM\OneToMany(targetEntity="CardCollectionDocument", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $docs;
    // ...

    /**
     * @ORM\OneToMany(targetEntity="CardCollectionMediaDocument", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $mediadocs;
    // ...

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Card", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $cards;

    /**
     * @ORM\OneToMany(targetEntity="CardCollectionYoutube", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $youtube;

    /**
     * @ORM\OneToMany(targetEntity="CardCollectionMediaYoutube", mappedBy="collection", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $mediayoutube;

 
    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }


    public function __construct() {
        $this->targets = new ArrayCollection();
        $this->props = new ArrayCollection();
        $this->interest = new ArrayCollection();
        $this->interiors = new ArrayCollection();
        $this->docs = new ArrayCollection();
        $this->mediadocs = new ArrayCollection();
        $this->cards = new ArrayCollection();
        $this->faq = new ArrayCollection();
        $this->youtube = new ArrayCollection();
        $this->mediayoutube = new ArrayCollection();
        $this->layout = new ArrayCollection();
        $this->projects = new ArrayCollection();
  }

    public function getClass(): ?CardCollectionClass
    {
        return $this->class;
    }

    public function setClass(?CardCollectionClass $class): self
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return Collection|CardCollectionTargetRels[]
     */
    public function getTargets(): Collection
    {
        return $this->targets;
    }

    /**
     * @return Collection|CardCollectionProp[]
     */
    public function getProps(): Collection
    {
        return $this->props;
    }

    /**
     * @return Collection|CardCollectionInterest[]
     */
    public function getInterest(): Collection
    {
        return $this->interest;
    }

    public function addInterest(CardCollectionInterest $interest): self
    {
        if (!$this->interest->contains($interest)) {
            $this->interest[] = $interest;
            $interest->setCollection($this);
        }

        return $this;
    }

    public function removeInterest(CardCollectionInterest $interest): self
    {
        if ($this->interest->contains($interest)) {
            $this->interest->removeElement($interest);
            // set the owning side to null (unless already changed)
            if ($interest->getCollection() === $this) {
                $interest->setCollection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CardCollectionInterior[]
     */
    public function getInteriors(): Collection
    {
        return $this->interiors;
    }

    public function addInterior(CardCollectionInterior $interior): self
    {
        if (!$this->interiors->contains($interior)) {
            $this->interiors[] = $interior;
            $interior->setCollection($this);
        }

        return $this;
    }

    public function removeInterior(CardCollectionInterior $interior): self
    {
        if ($this->interiors->contains($interior)) {
            $this->interiors->removeElement($interior);
            // set the owning side to null (unless already changed)
            if ($interior->getCollection() === $this) {
                $interior->setCollection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CardCollectionDocument[]
     */
    public function getDocs(): Collection
    {
        return $this->docs;
    }

    public function addDoc(CardCollectionDocument $doc): self
    {
        if (!$this->docs->contains($doc)) {
            $this->docs[] = $doc;
            $doc->setCollection($this);
        }

        return $this;
    }

    public function removeDoc(CardCollectionDocument $doc): self
    {
        if ($this->docs->contains($doc)) {
            $this->docs->removeElement($doc);
            // set the owning side to null (unless already changed)
            if ($doc->getCollection() === $this) {
                $doc->setCollection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Card[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Card $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->setCollection($this);
        }

        return $this;
    }

    public function removeCard(Card $card): self
    {
        if ($this->cards->contains($card)) {
            $this->cards->removeElement($card);
            // set the owning side to null (unless already changed)
            if ($card->getCollection() === $this) {
                $card->setCollection(null);
            }
        }

        return $this;
    }

    public function addTarget(CardCollectionTargetRels $target): self
    {
        if (!$this->targets->contains($target)) {
            $this->targets[] = $target;
            $target->setCollection($this);
        }

        return $this;
    }

    public function removeTarget(CardCollectionTargetRels $target): self
    {
        if ($this->targets->contains($target)) {
            $this->targets->removeElement($target);
            // set the owning side to null (unless already changed)
            if ($target->getCollection() === $this) {
                $target->setCollection(null);
            }
        }

        return $this;
    }

    public function addProp(CardCollectionPropRels $prop): self
    {
        if (!$this->props->contains($prop)) {
            $this->props[] = $prop;
            $prop->setCollection($this);
        }

        return $this;
    }

    public function removeProp(CardCollectionPropRels $prop): self
    {
        if ($this->props->contains($prop)) {
            $this->props->removeElement($prop);
            // set the owning side to null (unless already changed)
            if ($prop->getCollection() === $this) {
                $prop->setCollection(null);
            }
        }

        return $this;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     */
    private $image;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }


    private $imageFile;
    /**
     * @param UploadedFile $imageFile
     */
    public function setImageFile(UploadedFile $imageFile = null)
    {
        $this->imageFile = $imageFile;
    }
    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function uploadImageFile()
    {


        if (null === $this->getImageFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getImageFile()->getClientOriginalName());


        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($this->title)),$this->image);

       $this->getImageFile()->move(
           __DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($this->title)),
           $filename
       );

       $this->image = $filename;

       $this->setImageFile(null);

   }






    /**
     * @var string
     *
     * @ORM\Column(name="designimage", type="string", nullable=true)
     */
    private $designimage;

    public function getDesignimage()
    {
        return $this->designimage;
    }

    public function setDesignimage($designimage)
    {
        $this->designimage = $designimage;
    }


    private $designimageFile;
    /**
     * @param UploadedFile $designimageFile
     */
    public function setDesignimageFile(UploadedFile $designimageFile = null)
    {
        $this->designimageFile = $designimageFile;
    }
    /**
     * @return UploadedFile
     */
    public function getDesignimageFile()
    {
        return $this->designimageFile;
    }

    public function uploadDesignimageFile()
    {


        if (null === $this->getDesignimageFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getDesignimageFile()->getClientOriginalName());


        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($this->title)),$this->designimage);

       $this->getDesignimageFile()->move(
           __DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($this->title)),
           $filename
       );

       $this->designimage = $filename;

       $this->setDesignimageFile(null);

   }





    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    /**
     * @ORM\ManyToMany(targetEntity=Project::class, mappedBy="collections")
     */
    private $projects;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadImageFile();
       $this->uploadDesignimageFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }

  /**
   * @return Collection|CardCollectionFAQ[]
   */
  public function getFaq(): Collection
  {
      return $this->faq;
  }

  public function addFaq(CardCollectionFAQ $faq): self
  {
      if (!$this->faq->contains($faq)) {
          $this->faq[] = $faq;
          $faq->setCollection($this);
      }

      return $this;
  }

  public function removeFaq(CardCollectionFAQ $faq): self
  {
      if ($this->faq->contains($faq)) {
          $this->faq->removeElement($faq);
          // set the owning side to null (unless already changed)
          if ($faq->getCollection() === $this) {
              $faq->setCollection(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|CardCollectionYoutube[]
   */
  public function getYoutube(): Collection
  {
      return $this->youtube;
  }

  public function addYoutube(CardCollectionYoutube $youtube): self
  {
      if (!$this->youtube->contains($youtube)) {
          $this->youtube[] = $youtube;
          $youtube->setCollection($this);
      }

      return $this;
  }

  public function removeYoutube(CardCollectionYoutube $youtube): self
  {
      if ($this->youtube->contains($youtube)) {
          $this->youtube->removeElement($youtube);
          // set the owning side to null (unless already changed)
          if ($youtube->getCollection() === $this) {
              $youtube->setCollection(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|CardCollectionMediaDocument[]
   */
  public function getMediadocs(): Collection
  {
      return $this->mediadocs;
  }

  public function addMediadoc(CardCollectionMediaDocument $mediadoc): self
  {
      if (!$this->mediadocs->contains($mediadoc)) {
          $this->mediadocs[] = $mediadoc;
          $mediadoc->setCollection($this);
      }

      return $this;
  }

  public function removeMediadoc(CardCollectionMediaDocument $mediadoc): self
  {
      if ($this->mediadocs->contains($mediadoc)) {
          $this->mediadocs->removeElement($mediadoc);
          // set the owning side to null (unless already changed)
          if ($mediadoc->getCollection() === $this) {
              $mediadoc->setCollection(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|CardCollectionMediaYoutube[]
   */
  public function getMediayoutube(): Collection
  {
      return $this->mediayoutube;
  }

  public function addMediayoutube(CardCollectionMediaYoutube $mediayoutube): self
  {
      if (!$this->mediayoutube->contains($mediayoutube)) {
          $this->mediayoutube[] = $mediayoutube;
          $mediayoutube->setCollection($this);
      }

      return $this;
  }

  public function removeMediayoutube(CardCollectionMediaYoutube $mediayoutube): self
  {
      if ($this->mediayoutube->contains($mediayoutube)) {
          $this->mediayoutube->removeElement($mediayoutube);
          // set the owning side to null (unless already changed)
          if ($mediayoutube->getCollection() === $this) {
              $mediayoutube->setCollection(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|CardCollectionLayout[]
   */
  public function getLayout(): Collection
  {
      return $this->layout;
  }

  public function addLayout(CardCollectionLayout $layout): self
  {
      if (!$this->layout->contains($layout)) {
          $this->layout[] = $layout;
          $layout->setCollection($this);
      }

      return $this;
  }

  public function removeLayout(CardCollectionLayout $layout): self
  {
      if ($this->layout->contains($layout)) {
          $this->layout->removeElement($layout);
          // set the owning side to null (unless already changed)
          if ($layout->getCollection() === $this) {
              $layout->setCollection(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|Project[]
   */
  public function getProjects(): Collection
  {
      return $this->projects;
  }

  public function addProject(Project $project): self
  {
      if (!$this->projects->contains($project)) {
          $this->projects[] = $project;
          $project->addCollection($this);
      }

      return $this;
  }

  public function removeProject(Project $project): self
  {
      if ($this->projects->contains($project)) {
          $this->projects->removeElement($project);
          $project->removeCollection($this);
      }

      return $this;
  }




}

?>