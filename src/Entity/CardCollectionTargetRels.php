<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardCollectionTargetRelsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CardCollectionTargetRels
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardCollection", inversedBy="targets")
     */
    private $collection;

    public function getCollection(): ?CardCollection
    {
        return $this->collection;
    }

  /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardCollectionTarget")
     * @ORM\JoinColumn(name="target_id", referencedColumnName="id")
     */
    private $target;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setCollection(?CardCollection $collection): self
    {
        $this->collection = $collection;

        return $this;
    }

    public function getTarget(): ?CardCollectionTarget
    {
        return $this->target;
    }

    public function setTarget(?CardCollectionTarget $target): self
    {
        $this->target = $target;

        return $this;
    }



    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }



}

?>