<?php

namespace App\Entity;

use App\Utils\Utils;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Sluggable\Util\Urlizer;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
    use TimestampableEntity;
    use MetaTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"title"})
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $previewText;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $timeOfReading;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ArticleCategory", inversedBy="articles")
     */
    private $articleCategory;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $imageFileName;

    /**
     * @var UploadedFile $imageFile
     * для сонаты
     */
    private $imageFile;

    /**
     * @ORM\Column(name="alt_image",type="string", length=255, nullable=true)
     */
    private $altImage;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }


    public function getPreviewText(): ?string
    {
        return $this->previewText;
    }

    public function setPreviewText(?string $previewText): self
    {
        $this->previewText = $previewText;

        return $this;
    }

    public function getTimeOfReading(): ?float
    {
        return $this->timeOfReading;
    }

    public function setTimeOfReading(?float $timeOfReading): self
    {
        $this->timeOfReading = $timeOfReading;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getArticleCategory(): ?ArticleCategory
    {
        return $this->articleCategory;
    }

    public function setArticleCategory(?ArticleCategory $articleCategory): self
    {
        $this->articleCategory = $articleCategory;

        return $this;
    }

    public function getImageFileName(): ?string
    {
        return $this->imageFileName;
    }

    public function setImageFileName(?string $imageFileName): self
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    /**
     * @param UploadedFile $imageFile
     */
    public function setImageFile(UploadedFile $imageFile = null)
    {
        $this->imageFile = $imageFile;
    }

    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    // todo: вынести в сервис
    public function uploadImageFile()
    {
        if (null === $this->imageFile) {
            return;
        }

        $newFilename = Urlizer::urlize(pathinfo($this->imageFile->getClientOriginalName(), PATHINFO_FILENAME)) . '-' . uniqid('', true) . '.' . $this->imageFile->guessExtension();

        Utils::RemoveFile(__DIR__ . '/../../public/uploads/article_preview/', $this->imageFileName);

        $this->getImageFile()->move(
            __DIR__ . '/../../public/uploads/article_preview/',
            $newFilename
        );

        $this->imageFileName = $newFilename;

        $this->setImageFile(null);

    }

    public function getAltImage(): ?string
    {
        return $this->altImage;
    }

    public function setAltImage(?string $altImage): self
    {
        $this->altImage = $altImage;
        return $this;
    }
}
