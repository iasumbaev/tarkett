<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardCollectionClassRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CardCollectionClass
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

  
    public function getId()
    {
        return $this->id;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string")
     */
    private $icon;

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }


    private $iconFile;
    /**
     * @param UploadedFile $iconFile
     */
    public function setIconFile(UploadedFile $iconFile = null)
    {
        $this->iconFile = $iconFile;
    }
    /**
     * @return UploadedFile
     */
    public function getIconFile()
    {
        return $this->iconFile;
    }

    public function uploadIconFile()
    {


        if (null === $this->getIconFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getIconFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/collections/class',$this->icon);

       $this->getIconFile()->move(
           __DIR__.'/../../public/collections/class',
           $filename
       );

       $this->icon = $filename;

       $this->setIconFile(null);

   }

    
    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
   public function lifecycleFileUpload()
   {
       $this->uploadIconFile();
       $this->uploadMenuIconFile();
   }

  public function refreshUpdated()
   {
      $this->setUpdated(new \DateTime());
   }


    /**
     * @var string
     *
     * @ORM\Column(name="menuicon", type="string")
     */
    private $menuicon;

    public function getMenuicon()
    {
        return $this->menuicon;
    }

    public function setMenuicon($menuicon)
    {
        $this->menuicon = $menuicon;
    }




    private $menuIconFile;
    /**
     * @param UploadedFile $menuIconFile
     */
    public function setMenuIconFile(UploadedFile $menuIconFile = null)
    {
        $this->menuIconFile = $menuIconFile;
    }
    /**
     * @return UploadedFile
     */
    public function getMenuIconFile()
    {
        return $this->menuIconFile;
    }

    public function uploadMenuIconFile()
    {


        if (null === $this->getMenuIconFile()) {
            return;
        }

        $fileinfo = pathinfo( $this->getMenuIconFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000,9999999999);

        $filename = $basename.'.'.$fileinfo['extension'];


        Utils::RemoveFile(__DIR__.'/../../public/collections/class',$this->menuicon);

       $this->getMenuIconFile()->move(
           __DIR__.'/../../public/collections/class',
           $filename
       );

       $this->menuicon = $filename;

       $this->setMenuIconFile(null);

   }


}

?>