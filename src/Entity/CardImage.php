<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\Utils;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CardImage
{


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */

    private $position;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     */
    private $image;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }


    /**
     * @ORM\ManyToOne(targetEntity="Card", inversedBy="images")
     * @ORM\JoinColumn(name="card_id", referencedColumnName="id")
     */
    private $card;

    public function getCard(): ?Card
    {
        return $this->card;
    }

    public function setCard(?Card $card): self
    {
        $this->card = $card;

        return $this;
    }


    private $imageFile;

    /**
     * @param UploadedFile $imageFile
     */
    public function setImageFile(UploadedFile $imageFile = null)
    {
        $this->imageFile = $imageFile;
    }

    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function uploadImageFile()
    {


        if (null === $this->getImageFile()) {
            return;
        }

        $fileinfo = pathinfo($this->getImageFile()->getClientOriginalName());

        $basename = Utils::RandomInteger(1000000000, 9999999999);

        $filename = $basename . '.' . $fileinfo['extension'];

        Utils::RemoveFile(__DIR__ . '/../../public/card/' . preg_replace('/\s+/', '-', strtolower($this->getCard()->getCollection()->getTitle())) . '/' . preg_replace('/\s+/', '-', strtolower($this->getCard()->getTitle())) . '/img', $this->image);

        $this->getImageFile()->move(
            __DIR__ . '/../../public/card/' . preg_replace('/\s+/', '-', strtolower($this->getCard()->getCollection()->getTitle())) . '/' . preg_replace('/\s+/', '-', strtolower($this->getCard()->getTitle())) . '/img',
            $filename
        );

        $this->image = $filename;

        $this->setImageFile(null);

    }


    /** @ORM\Column(type="datetime",nullable=true) */
    private $updated;

    /**
     * @ORM\Column(name="youtube_link", type="text", length=1000, nullable=true)
     */
    private $youtubeLink;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $video;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $videoLink;

    public function getUpdated($updated)
    {
        return $this->updated;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function lifecycleFileUpload()
    {
        $this->uploadImageFile();
    }

    public function refreshUpdated()
    {

        $this->setUpdated(new \DateTime());
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getYoutubeLink(): ?string
    {
        return $this->youtubeLink;
    }

    public function setYoutubeLink(?string $youtubeLink): self
    {
        $this->youtubeLink = $youtubeLink;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getVideoLink(): ?string
    {
        return $this->videoLink;
    }

    public function setVideoLink(?string $videoLink): self
    {
        $this->videoLink = $videoLink;

        return $this;
    }


}