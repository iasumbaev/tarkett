<?php

namespace App\Command;

use App\Entity\ContactsPageCities;
use App\Entity\ContactsPageLocations;
use App\Repository\ContactsPageCitiesRepository;
use App\Repository\ContactsPageLocationsRepository;
use App\Service\XLSX\Helper;
use App\Service\YandexMap\YandexMap;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RefreshShopCommand extends Command
{
    protected static $defaultName = 'app:refresh-shops';
    /**
     * @var YandexMap
     */
    private $yandexMapApi;
    /**
     * @var Helper
     */
    private $helper;
    /**
     * @var ContactsPageCitiesRepository
     */
    private $citiesRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ContactsPageLocationsRepository
     */
    private $locationsRepository;

    public function __construct(
        YandexMap                       $yandexMapApi,
        Helper                          $helper,
        ContactsPageCitiesRepository    $citiesRepository,
        EntityManagerInterface          $entityManager,
        ContactsPageLocationsRepository $locationsRepository,
        string                          $name = null)
    {
        parent::__construct($name);
        $this->yandexMapApi = $yandexMapApi;
        $this->helper = $helper;
        $this->citiesRepository = $citiesRepository;
        $this->entityManager = $entityManager;
        $this->locationsRepository = $locationsRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    private function getCityFromRepository(string $cityName): ContactsPageCities
    {
        $city = $this->citiesRepository->findOneBy(['title' => $cityName]);

        if (!$city) {
            $city = (new ContactsPageCities())->setTitle($cityName);
            $this->entityManager->persist($city);
            $this->entityManager->flush();
        }
        return $city;
    }

    private function removeAllLocations()
    {
        $locations = $this->locationsRepository->findAll();
        foreach ($locations as $index => $location) {
            $this->entityManager->remove($location);
        }
        $this->entityManager->flush();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }

        //Удалим все магазины
        $this->removeAllLocations();

        //Тут будем хранить магазины, для которых не получилось найти координаты
        $fails = [];
        $shops = $this->helper->getShopInfo();
        //Найдём первый город

        $city = $this->getCityFromRepository($shops[0]->city);
        foreach ($shops as $index => $shop) {
            $io->text('Work with ' . $index . ' ' . $shop->city . ', ' . $shop->address);
            $coordinates = $this->yandexMapApi->getCoordinatesFromAddress($shop->city . ', ' . $shop->address);

            if ($coordinates) {
                $shop->longitude = $coordinates['lng'];
                $shop->latitude = $coordinates['lat'];
            } else {
                $fails[] = $shop;
                continue;
            }

            //Если город предыдущего магазина равен текущему, то нет смысла по новой ходить в базу
            //В ином случае, поищем там
            //Для случая, когда в таблице упорядочены строки по городам, иначе - складывать в массив, с ключом - город
            if ($city !== $shop->city) {
                //Сохраним все изменения для города
                $this->entityManager->flush();
                $city = $this->getCityFromRepository($shop->city);
            }

            //Создадим ContactsPageLocations на основе Shop
            $location = new ContactsPageLocations($shop);
            $this->entityManager->persist($location);
            //Добавим к городу
            $city->addLocation($location);

        }

        $this->entityManager->flush();

        var_dump($fails);

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
