<?php

namespace App\Command;

use App\Repository\ContactsPageLocationsRepository;
use App\Service\XLSX\Helper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdatePhoneNumbersCommand extends Command
{
    protected static $defaultName = 'app:update-phone-numbers';
    /**
     * @var Helper
     */
    private $helper;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ContactsPageLocationsRepository
     */
    private $locationsRepository;

    public function __construct(
        Helper                          $helper,
        EntityManagerInterface          $entityManager,
        ContactsPageLocationsRepository $locationsRepository,
        string                          $name = null)
    {
        parent::__construct($name);
        $this->helper = $helper;
        $this->entityManager = $entityManager;
        $this->locationsRepository = $locationsRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //Тут будем хранить магазины, которые не получилось найти
        $fails = [];
        $shops = $this->helper->getShopInfo();

        foreach ($shops as $index => $shop) {
            $io->text('Work with ' . $index . ' ' . $shop->city . ', ' . $shop->address);
            $location = $this->locationsRepository->findOneBy([
                'title' => $shop->name,
                'address' => $shop->address
            ]);
            if(!$location) {
                $fails[] = $shop;
                continue;
            }
            $io->text('Location was found');

            if(empty($location->getPhone()) || $location->getPhone() === ' ') {
                $location->setPhone($shop->phone);
                $this->entityManager->persist($location);
                $io->text('Phone: ' . $location->getPhone());
            }
        }

        $this->entityManager->flush();

        var_dump($fails);

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
