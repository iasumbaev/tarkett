<?php

namespace App\Service\YandexMap;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class API
{
    const API_URL = 'https://geocode-maps.yandex.ru/1.x/';
    const API_KEY_PARAMETER = 'apikey';
    const GEOCODE_PARAM = 'geocode';
    //TODO: to .env
    const API_KEY = '28ba27e6-8179-45dd-8a90-a542e1883e67';

    /**
     * @var HttpClientInterface
     */
    private $httpClient;


    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    private function getCoordinatesFromApiResponse(string $response): ?array
    {
        $result = json_decode($response, true);
        //TODO:check all fields
        if(isset($result['response']['GeoObjectCollection']['featureMember'][0])) {
            $position = $result['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
            $coordinates = explode(' ', $position);
            return [
                'lng' => $coordinates[0],
                'lat' => $coordinates[1]
            ];
        }
       return null;
    }

    public function getCoordinatesByAddress(string $address): ?array
    {
        try {
            $response = $this->httpClient->request('GET', self::API_URL, [
                'query' => [
                    self::API_KEY_PARAMETER => self::API_KEY,
                    self::GEOCODE_PARAM => str_replace(' ', '+', $address),
                    'format' => 'json'
                ]
            ]);

            return $this->getCoordinatesFromApiResponse($response->getContent());
        } catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface $e) {
            //Ничего не поделаешь
        }
        return null;
    }
}