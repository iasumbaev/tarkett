<?php

namespace App\Service\YandexMap;

class YandexMap
{
    /**
     * @var API
     */
    private $api;

    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function getCoordinatesFromAddress(string $addresses): ?array
    {
        return $this->api->getCoordinatesByAddress($addresses);
    }
}