<?php

namespace App\Service\XLSX;

use App\Model\XLSX\Shop;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Helper
{
    const CITY_COLUMN = 'C';
    const NAME_COLUMN = 'E';
    const ADDRESS_COLUMN = 'F';
    const PHONE_COLUMN = 'G';


    private function removeCityFromAddress($city, $address)
    {
        return str_replace($city . ', ', '', $address);
    }

    /**
     * @param string $phone
     * @return array|string|string[]
     */
    private function preparePhoneNumber(string $phone) {
        return str_replace([' ', '-', '(', ')', '+'], "", $phone);
    }

    /**
     * @return Shop[]
     */
    public function getShopInfo(): array
    {
        //TODO: path
        $spreadsheet = IOFactory::load("var/xlsx/1.xlsx");
        try {
            $spreadsheet->setActiveSheetIndex(0);
        } catch (Exception $e) {
        }
        $sheet = $spreadsheet->getActiveSheet();

        $result = [];
        $row = 2;
        while (!empty($sheet->getCell(self::CITY_COLUMN . $row)->getValue())) {
            $phoneValue = $sheet->getCell(self::PHONE_COLUMN . $row)->getValue();
            $phoneValue = ($phoneValue == '0' || $phoneValue == '-' || $phoneValue == '') ? ' ' : $this->preparePhoneNumber($phoneValue);
            $cityValue = $sheet->getCell(self::CITY_COLUMN . $row)->getValue();
            $address = $this->removeCityFromAddress($cityValue, $sheet->getCell(self::ADDRESS_COLUMN . $row)->getValue());
            $result[] = new Shop(
                $cityValue,
                $sheet->getCell(self::NAME_COLUMN . $row)->getValue(),
                $address,
                $phoneValue
            );
            $row++;
        }
        return $result;
    }
}