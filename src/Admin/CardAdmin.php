<?php

namespace App\Admin;

// use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\Form\Type\CollectionType;


use App\Entity\CardCollection;
use App\Entity\Card;
use App\Entity\CardType;


use App\Utils\Utils;


final class CardAdmin extends AbstractAdmin
{

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->getSubject();

        $formMapper->add('title', TextType::class,[
            'label'=>'Название'
        ]);

        $formMapper
            ->add('type', EntityType::class, [
                'label' => 'Тип',
                'class' => CardType::class,
                'choice_label' => 'title',
            ])
        ;


        $formMapper->add('factorwidth', NumberType::class,[
            'label'=>'ширина',
            'required'=>false
        ]);

        $formMapper->add('factorlength', NumberType::class,[
            'label'=>'длина',
            'required'=>false
        ]);

        $formMapper->add('factorcount', NumberType::class,[
            'label'=>'количество в упаковке',
            'required'=>false
        ]);

        $formMapper->add('factorarea', NumberType::class,[
            'label'=>'общая площадь в упаковке',
            'required'=>false
        ]);


        $formMapper->add('metatitle', TextType::class,[
            'label'=>'meta-тег title',
            'required'=>false
        ]);

        $formMapper->add('metakeys', TextType::class,[
            'label'=>'meta-тег keyswords',
            'required'=>false
        ]);

        $formMapper->add('metadesc', TextType::class,[
            'label'=>'meta-тег description',
            'required'=>false
        ]);

        $formMapper
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label'=>'Изображение',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/card/'.preg_replace('/\s+/','-',strtolower($object->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($object->getTitle())).'/'.$object->getImage().'" class="admin-preview"/>':'')
            ])
        ;

        $formMapper
            ->add('collection', EntityType::class, [
                'label' => 'Коллекция',
                'class' => CardCollection::class,
                'choice_label' => 'title',
            ])
        ;

        $formMapper
            ->add('images', CollectionType::class, [

                'label'=>'Изображения',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'natural',
                'sortable' => 'position',
            ])
        ;

        $formMapper
            ->add('values', CollectionType::class, [

                'label'=>'Характеристики',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;

        $formMapper
            ->add('textureFile', FileType::class, [
                'required' => false,
                'label'=>'текстура',
            ])
        ;

        $formMapper
            ->add('modelFile', FileType::class, [
                'required' => false,
                'label'=>'модель',
            ])
        ;

    }


    private $logger;

    public function setLogger($logger){
        $this->logger = $logger;
    }



    public function prePersist($object){
        $this->updateObject($object,true);
    }

    public function preUpdate($object){
        $this->updateObject($object,false);
    }

    public function preRemove($object){

        Utils::RemoveFile(
            __DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($object->getCollection()->getTitle())),
            preg_replace('/\s+/','-',strtolower($object->getTitle()))
        );

    }


    public function updateObject($object,$creation)
    {

        // $this->logger->info('updateCard');

        $em = $this->getModelManager()->getEntityManager($this->getClass());
        $original = $em->getUnitOfWork()->getOriginalEntityData($object);


        $formData = $this->getRequest()->request->get($this->getRequest()->query->get('uniqid'));


        $order = [

            'images'=>[''=>[]],

            'values'=>[''=>[]],

        ];


        $orderIndex = 0;
        if (!empty($formData['images'])) {
            foreach ($formData['images'] as $item) {
                $id = $item['id'];
                if (empty($id)) {
                    $order['images'][''][] = ++$orderIndex;
                } else {
                    $order['images'][$id] = ++$orderIndex;
                }
            }
        }

        $orderIndex = 0;

        if(!empty($formData['values'])) foreach($formData['values'] as $item){
            $id = $item['id'];
            if(empty($id)) $order['values'][''][] = ++$orderIndex; else $order['values'][$id] = ++$orderIndex;
        }



        if($creation){

            $dir =  __DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($object->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($object->getTitle()));

            Utils::MakeDir($dir);

            Utils::MakeDir($dir.'/img');

        }else{


            $oldCard = [

                'title'=>$original['title'],

                'collection'=>[

                    'title'=>$original['collection']->getTitle()

                ]

            ];

            $oldCard['latin'] = preg_replace('/\s+/','-',strtolower($oldCard['title']));
            $oldCard['collection']['latin'] = preg_replace('/\s+/','-',strtolower($oldCard['collection']['title']));


            $newCard = [

                'title'=>$object->getTitle(),

                'collection'=>[

                    'title'=>$object->getCollection()->getTitle()

                ]

            ];

            $newCard['latin'] = preg_replace('/\s+/','-',strtolower($newCard['title']));
            $newCard['collection']['latin'] = preg_replace('/\s+/','-',strtolower($newCard['collection']['title']));


            $oldDir = __DIR__.'/../../public/card/'.$oldCard['collection']['latin'].'/'.$oldCard['latin'];
            $newDir = __DIR__.'/../../public/card/'.$newCard['collection']['latin'].'/'.$newCard['latin'];


            if($oldDir!=$newDir) rename($oldDir,$newDir);


        }


        foreach($object->getImages() as $index => $item){

            $item->setCard($object);

            if ($item->getImageFile()){

                $item->refreshUpdated();

            }

            $id = $item->getId();

            if(empty($id)){

                $orderIndex = array_shift($order['images']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['images'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }

        foreach($object->getValues() as $item){

            $item->setCard($object);

            $id = $item->getId();

            if(empty($id)){

                $orderIndex = array_shift($order['values']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['values'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }

        if ($object->getImageFile()||$object->getTextureFile()||$object->getModelFile()) {

            $object->refreshUpdated();

        }



    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('collection.title')
        ;
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('moveprev', $this->getRouterIdParameter().'/moveprev');
        $collection->add('movenext', $this->getRouterIdParameter().'/movenext');
    }


    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->addIdentifier('title',null,['label'=>'Продукт'])
            ->addIdentifier('collection.title',null,['label'=>'Коллекция'])
        ;


        $listMapper
            ->add('_action', null, [
                'actions' => [
                    'moveprev' => [
                        'template' => '@AppTemplates/CRUD/moveprev_action.html.twig',
                    ],
                    'movenext' => [
                        'template' => '@AppTemplates/CRUD/movenext_action.html.twig',
                    ],
                ]
            ]);

    }

}


?>