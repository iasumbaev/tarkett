<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\Form\Type\CollectionType;


use App\Entity\ProductPageDocsSection;


final class ProductPageDocsSectionAdmin extends AbstractAdmin
{

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        

        $object = $this->getSubject();


        $formMapper
            ->add('title', TextType::class, [
                'label' => 'Название',
            ])
        ;
        
        $formMapper
            ->add('youtube', CollectionType::class, [
                
                'label'=>'Видео',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   

        $formMapper
            ->add('doc', CollectionType::class, [
                
                'label'=>'Файлы',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   

    }


    public function prePersist($object){
        $this->updateObject($object,true);
    }
    
    public function preUpdate($object){
        $this->updateObject($object,false);
    }

    public function updateObject($object,$creation)
    {

       

        $formData = $this->getRequest()->request->get($this->getRequest()->query->get('uniqid'));


        $order = [

            'doc'=>[''=>[]],
            
            'youtube'=>[''=>[]],

        ];


        $orderIndex = 0;

        if(!empty($formData['doc'])) foreach($formData['doc'] as $item){
            $id = $item['id'];
            if(empty($id)) $order['doc'][''][] = ++$orderIndex; else $order['doc'][$id] = ++$orderIndex;
        }


        $orderIndex = 0;

        if(!empty($formData['youtube'])) foreach($formData['youtube'] as $item){
            $id = $item['id'];
            if(empty($id)) $order['youtube'][''][] = ++$orderIndex; else $order['youtube'][$id] = ++$orderIndex;
        }


        foreach($object->getYoutube() as $item){

            $item->setSection($object);

            $id = $item->getId();

            if(empty($id)){

                $orderIndex = array_shift($order['youtube']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['youtube'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }

        foreach($object->getDoc() as $item){

            $item->setSection($object);

            if($item->getFileFile()){

                $item->refreshUpdated();

            }

            $id = $item->getId();

            if(empty($id)){

                $orderIndex = array_shift($order['doc']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['doc'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }

    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('moveprev', $this->getRouterIdParameter().'/moveprev');
        $collection->add('movenext', $this->getRouterIdParameter().'/movenext');
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    
        $listMapper
        ->add('_action', null, [
            'actions' => [
                'moveprev' => [
                    'template' => '@AppTemplates/CRUD/moveprev_action.html.twig',
                ],
                'movenext' => [
                    'template' => '@AppTemplates/CRUD/movenext_action.html.twig',
                ],
            ]
        ]);

    }

}


?>