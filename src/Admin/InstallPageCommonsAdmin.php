<?php

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;



final class InstallPageCommonsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->getSubject();

        $formMapper->add('caption', TextType::class,[
            'label'=>'Заголовок'
        ]);

        $formMapper->add('subcaption', TextType::class,[
            'label'=>'Подзаголовок'
        ]);

        $formMapper->add('metatitle', TextType::class,[
            'label'=>'meta-тег title',
            'required'=>false
        ]);

        $formMapper->add('metakeys', TextType::class,[
            'label'=>'meta-тег keywords',
            'required'=>false
        ]);

        $formMapper->add('metadesc', TextType::class,[
            'label'=>'meta-тег description',
            'required'=>false
        ]);

    }    

    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
    
    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();

        unset($actions['create']);

        return $actions;
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('caption',null,['label'=>'Заголовок']);
    }
}


?>