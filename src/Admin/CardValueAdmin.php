<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\Form\Type\CollectionType;


use App\Entity\CardChar;
use App\Entity\CardValue;


final class CardValueAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        

        $formMapper->add('id',null,[
            'attr' => ['style' => 'display: none;','tabindex'=>'-1'],
            'label_attr' => ['style' => 'display: none;'],
        ]);

        $formMapper
            ->add('char', EntityType::class, [
                'label' => 'Характеристика',
                'class' => CardChar::class,
                'choice_label' => 'title',
            ])
        ;

        $formMapper
            ->add('value', TextType::class, [
                'label' => 'Значение',
            ])
        ;

    }


    protected function configureListFields(ListMapper $listMapper)
    {
        // $listMapper->addIdentifier('title');
    }

}


?>