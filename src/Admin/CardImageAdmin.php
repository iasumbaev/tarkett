<?php

namespace App\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\Form\Type\CollectionType;



final class CardImageAdmin extends AbstractAdmin
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct($code, $class, $baseControllerName, EntityManagerInterface $entityManager)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->entityManager = $entityManager;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {


        $formMapper->add('id',null,[
            'attr' => ['style' => 'display: none;','tabindex'=>'-1'],
            'label_attr' => ['style' => 'display: none;'],
        ]);

        $object = $this->getSubject();

        $formMapper
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label'=>'Изображение',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/card/'.preg_replace('/\s+/','-',strtolower($object->getCard()->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($object->getCard()->getTitle())).'/img/'.$object->getImage().'" class="admin-preview"/>':'')
            ])
            ->add('videoLink', TextareaType::class, [
                'required' => false,
                'label' => 'Ссылка на видео'
            ]);

    }


    public function prePersist($object)
    {
        $this->manageFileUpload($object);

    }

    public function preUpdate($object)
    {

        $this->manageFileUpload($object);

    }

    private function manageFileUpload($object)
    {

        if ($object->getImageFile()){

            $object->refreshUpdated();

        }
    }

    public function postUpdate($object)
    {
        $this->entityManager->persist($object);
        $this->entityManager->flush();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        // $listMapper->addIdentifier('title');
    }

}


?>