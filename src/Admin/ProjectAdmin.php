<?php


namespace App\Admin;

use App\Entity\CardCollection;
use App\Entity\Project;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProjectAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        /**
         * @var Project $project
         */
        $project = $this->getSubject();
        $formMapper
            ->add('title', null, [
                'label' => 'Название',
            ])
            ->add('square', null, [
                'label' => 'Площадь, м2',
            ])
            ->add('designer', null, [
                'label' => 'Дизайнер'
            ])
            ->add('link', null, [
                'label' => 'Ссылка'
            ])
            ->add('text', CKEditorType::class, array(
                'label' => 'Текст',
                'config' => array(
                    'toolbarGroups' => [
                        ['name' => 'document', 'groups' => ['mode', 'document', 'doctools']],
                        ['name' => 'clipboard', 'groups' => ['clipboard', 'undo']],
                        ['name' => 'editing', 'groups' => ['find', 'selection', 'spellchecker', 'editing']],
                        ['name' => 'forms', 'groups' => ['forms']],
                        '/',
                        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                        ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']],
                        ['name' => 'links', 'groups' => ['links']],
                        ['name' => 'insert', 'groups' => ['insert']],
                        '/',
                        ['name' => 'styles', 'groups' => ['styles']],
                        ['name' => 'colors', 'groups' => ['colors']],
                        ['name' => 'tools', 'groups' => ['tools']],
                        ['name' => 'others', 'groups' => ['others']],
                        ['name' => 'about', 'groups' => ['about']],
                    ],

                    'removeButtons' => 'Save,Templates,Cut,Find,SelectAll,Scayt,Form,Superscript,Subscript,Strike,Underline,Italic,Blockquote,CreateDiv,NewPage,Preview,Print,Copy,Paste,PasteText,PasteFromWord,Replace,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,BidiLtr,BidiRtl,Language,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Font,Maximize,ShowBlocks,About',
                    'extraPlugins' => 'image2,format',
                    'removePlugins' => 'image',
                    'filebrowserUploadUrl' => '/projects/project/upload_image',
                    'filebrowserUploadMethod' => 'form',
                    'uiColor' => '#ffffff',
                ),

            ))
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label' => 'Изображение',
                'help' => ($project && $project->getId() ? '<img style="max-width: 200px;max-height: 200px;" src="/uploads/project_preview/' . $project->getImageFileName() . '" class="admin-preview"/>' : '')
            ])
            ->add('collections', EntityType::class, [
                'label' => 'Коллекции',
                'class' => CardCollection::class,
                'choice_label' => 'title',
                'multiple' => true,
                'required' => false
            ])
//            ->add('altImage', TextType::class, [
//                'data' => $project->getAltImage(),
//                'label' => 'Alt-текст для изображения',
//                'mapped' => true,
//                'required' => false
//            ])
            ->add('metaTitle', null, [
                'label' => 'meta-тег title',
            ])
            ->add('metaKeywords', null, [
                'label' => 'meta-тег keywords',
            ])
            ->add('metaDescription', null, [
                'label' => 'meta-тег description',
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }

    public function prePersist($project)
    {
        /** @var Project $project */
        $project->uploadImageFile();
    }


    public function preUpdate($project)
    {
        /** @var Project $project */
        $project->uploadImageFile();
    }


}