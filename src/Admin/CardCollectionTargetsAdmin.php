<?php

namespace App\Admin;

// use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\Form\Type\CollectionType;


use App\Entity\CardCollectionTarget;


final class CardCollectionTargetsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        

        $formMapper
            ->add('class', EntityType::class, [
                'label' => ' ',
                'class' => CardCollectionTarget::class,
                'choice_label' => 'title',
            ])
        ;

    }


    protected function configureListFields(ListMapper $listMapper)
    {
        // $listMapper->addIdentifier('title');
    }

}


?>