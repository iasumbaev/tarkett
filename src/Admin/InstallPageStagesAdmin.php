<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\Form\Type\CollectionType;


use App\Entity\InstallPageStages;


final class InstallPageStagesAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->getSubject();

        $formMapper->add('id',null,[
            'attr' => ['style' => 'display: none;','tabindex'=>'-1'],
            'label_attr' => ['style' => 'display: none;'],
        ]);
        

        $object = $this->getSubject();


        $formMapper
            ->add('title', TextType::class, [
                'label' => 'Текст',
            ])
        ;
        

        $formMapper
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label'=>'Изображение',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/install/stages/'.$object->getImage().'" class="admin-preview"/>':'')
            ])
        ;

    }


    protected function configureListFields(ListMapper $listMapper)
    {
        // $listMapper->addIdentifier('title');
    }

}


?>