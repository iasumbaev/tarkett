<?php

namespace App\Admin;

// use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;


final class ContactsPageCommonsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper->add('metatitle', TextType::class,[
            'label'=>'meta-тег title',
            'required'=>false
        ]);

        $formMapper->add('metakeys', TextType::class,[
            'label'=>'meta-тег keyswords',
            'required'=>false
        ]);

        $formMapper->add('metadesc', TextType::class,[
            'label'=>'meta-тег description',
            'required'=>false
        ]);

        $formMapper->add('phone', TextType::class,[
            'label'=>'Телефон'
        ]);
        
    }    

    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
    
    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();

        unset($actions['create']);

        return $actions;
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('phone',null,['label'=>'Телефон']);
    }
}


?>