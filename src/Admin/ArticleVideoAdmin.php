<?php


namespace App\Admin;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use App\Entity\ArticleVideo;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ArticleVideoAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        /**
         * @var ArticleVideo $video
         */
        $video = $this->getSubject();
        $formMapper->add('videoFile', FileType::class, [
            'required' => false,
            'label' => 'Видео',
            'help' => ($video && $video->getId() ? '<video><source src="' . $video->getFilePath() . '" type="' . $video->getMimeType() . '"/></video>' : '')
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('filename');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('filename')
        ->addIdentifier('filePath');
    }

    public function prePersist($video)
    {
        /** @var ArticleVideo $video */
        $video->initialize();
        $video->uploadVideoFile();
    }

    public function preUpdate($video)
    {
        /** @var ArticleVideo $video */
        $video->initialize();
        $video->uploadVideoFile();
    }


}