<?php

namespace App\Admin;

// use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use App\Entity\ContactsPageCities;

final class ContactsPageLocationsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper->add('title', TextType::class,[
            'label'=>'Название'
        ]);


        $formMapper
            ->add('city', EntityType::class, [
                'label' => 'Тип',
                'class' => ContactsPageCities::class,
                'choice_label' => 'title',
            ])
        ;
        
        $formMapper->add('address', TextType::class,[
            'label'=>'Адрес'
        ]);

        $formMapper->add('phone', NumberType::class,[
            'label'=>'Телефон',
            'help'=>'только цифры'
        ]);
        
        $formMapper->add('lng', NumberType::class,[
            'label'=>'Долгота',
        ]);
        
        $formMapper->add('lat', NumberType::class,[
            'label'=>'Широта',
        ]);

    }    

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }
}


?>