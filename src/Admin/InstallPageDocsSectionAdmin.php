<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\Form\Type\CollectionType;


use App\Entity\InstallPageDocsSection;


final class InstallPageDocsSectionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        

        $object = $this->getSubject();


        $formMapper
            ->add('title', TextType::class, [
                'label' => 'Название',
            ])
        ;
        
        $formMapper
            ->add('youtube', CollectionType::class, [
                
                'label'=>'Видео',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
        ;   

        $formMapper
            ->add('doc', CollectionType::class, [
                
                'label'=>'Файлы',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
        ;   

    }


    public function prePersist($object){
        $this->updateObject($object,true);
    }
    
    public function preUpdate($object){
        $this->updateObject($object,false);
    }

    public function updateObject($object,$creation)
    {

       
        foreach($object->getYoutube() as $item){

            $item->setSection($object);

        }

        foreach($object->getDoc() as $item){

            $item->setSection($object);

            if($item->getFileFile()){

                $item->refreshUpdated();

            }

        }

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        // $listMapper->addIdentifier('title');
    }

}


?>