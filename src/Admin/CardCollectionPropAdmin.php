<?php

namespace App\Admin;

// use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\CoreBundle\Model\Metadata;


final class CardCollectionPropAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $dbObject = $this->getSubject();
        
        $formMapper->add('title', TextType::class,[
            'label'=>'Название'
        ]);
        

        $formMapper
            ->add('iconFile', FileType::class, [
                'required' => false,
                'label'=>'Иконка',
                'help'=>(!is_null($dbObject)&&!empty($dbObject->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/collections/advantages/'.$dbObject->getIcon().'" class="admin-preview"/>':'')
            ])
        ;

    }


    public function prePersist($dbObject)
    {
        $this->manageFileUpload($dbObject);
    }

    public function preUpdate($dbObject)
    {

        $this->manageFileUpload($dbObject);

    }

    private function manageFileUpload($dbObject)
    {


        if ($dbObject->getIconFile()){

            $dbObject->refreshUpdated();

        }
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }

    public function getObjectMetadata($dbObject): Metadata
    {

        $thumb = $dbObject->getIcon();

        return new Metadata($dbObject->getTitle(),'',(empty($thumb)?null:'/collections/advantages/'.$thumb));
    }

}


?>