<?php

namespace App\Admin;

use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\Form\Type\CollectionType;
// use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;


use App\Entity\CardCollectionClass;
use App\Entity\Card;

use App\Utils\Utils;


final class CardCollectionAdmin extends AbstractAdmin
{

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {


        $object = $this->getSubject();
        
        $formMapper->add('title', TextType::class,[
            'label'=>'Название',
        ]);

        $formMapper
            ->add('type', ChoiceType::class, [
                'label'=>'Тип коллекции',
                'choices' => [
                    'Обычная' => 1,
                    'Новинка' => 2,
                ],
            ])
        ;

        $formMapper->add('designtitle', TextType::class,[
            'label'=>'текст для блоков с галереями',
            'required'=>false
        ]);

        $formMapper->add('metatitle', TextType::class,[
            'label'=>'meta-тег title',
            'required'=>false
        ]);

        $formMapper->add('metakeys', TextType::class,[
            'label'=>'meta-тег keywords',
            'required'=>false
        ]);

        $formMapper->add('metadesc', TextType::class,[
            'label'=>'meta-тег description',
            'required'=>false
        ]);

        $formMapper
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label'=>'Изображение',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/collection/'.preg_replace('/\s+/','-',strtolower($object->getTitle())).'/'.$object->getImage().'" class="admin-preview"/>':'')
            ])
        ;

        $formMapper
            ->add('designimageFile', FileType::class, [
                'required' => false,
                'label'=>'Изображение для блоков с галереями',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/collection/'.preg_replace('/\s+/','-',strtolower($object->getTitle())).'/'.$object->getDesignimage().'" class="admin-preview"/>':'')
            ])
        ;
           
        $formMapper
            ->add('class', EntityType::class, [
                'label' => 'Тип',
                'class' => CardCollectionClass::class,
                'choice_label' => 'title',
            ])
        ;


        $formMapper
            ->add('layout', CollectionType::class, [
                
                'label'=>'Варианты раскладок',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'natural',
                'sortable' => 'position',
            ])
        ;   



        $formMapper
            ->add('targets', CollectionType::class, [
                
                'label'=>'Назначения',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
                // 'order' => 'ASC'
            ])
        ;   
        

        $formMapper
            ->add('props', CollectionType::class, [
                
                'label'=>'Преимущества',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   
        
        $formMapper
            ->add('interest', CollectionType::class, [
                
                'label'=>'Преимущества (галерея)',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'natural',
                'sortable' => 'position',
            ])
        ;   
        
/*        $formMapper
            ->add('faq', CollectionType::class, [
                
                'label'=>'FAQ',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'natural',
            ])
        ;   
*/        
        $formMapper
            ->add('interiors', CollectionType::class, [
                
                'label'=>'Интерьеры',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'natural',
                'sortable' => 'position',
            ])
        ;   
        
        $formMapper
            ->add('docs', CollectionType::class, [
                
                'label'=>'Документы',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   
        
        $formMapper
            ->add('youtube', CollectionType::class, [
                
                'label'=>'Youtube-видео',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   
                
        $formMapper
            ->add('mediadocs', CollectionType::class, [
                
                'label'=>'Документы (материалы по теме)',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   
        
        $formMapper
            ->add('mediayoutube', CollectionType::class, [
                
                'label'=>'Youtube-видео (материалы по теме)',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   
        
        $formMapper->add('text_top', TextareaType::class,[
            'label'=>'Описание',
            'required'=>false
        ]);
        
        $formMapper->add('text_bottom', TextareaType::class,[
            'label'=>'Описание (2я часть)',
            'help'=>'текст светлосерого цвета',
            'required'=>false
        ]);
        
        $formMapper
            ->add('video', TextType::class, [
                'required' => false,
                'label'=>'Видео',
            ])
        ;
          
          
    }


    private $logger;

    public function setLogger($logger){
        $this->logger = $logger;
    }

    public function prePersist($object){
        $this->updateObject($object,true);
    }
    
    public function preUpdate($object){
        $this->updateObject($object,false);
    }

    public function preRemove($object){

        Utils::RemoveFile(__DIR__.'/../../public/collection',preg_replace('/\s+/','-',strtolower($object->getTitle())));
        Utils::RemoveFile(__DIR__.'/../../public/card',preg_replace('/\s+/','-',strtolower($object->getTitle())));

    }


    public function updateObject($object,$creation)
    {


        $em = $this->getModelManager()->getEntityManager($this->getClass());
        $original = $em->getUnitOfWork()->getOriginalEntityData($object);
        

        $this->logger->info('preUpdate');

        $formData = $this->getRequest()->request->get($this->getRequest()->query->get('uniqid'));

        $this->logger->info(json_encode($formData));


        $order = [

            'targets'=>[''=>[]],

            'props'=>[''=>[]],

            'interest'=>[''=>[]],

            'interiors'=>[''=>[]],

            'docs'=>[''=>[]],

            'mediadocs'=>[''=>[]],

            'youtube'=>[''=>[]],

            'mediayoutube'=>[''=>[]],
            
            'layout'=>[''=>[]],

        ];

        if(!is_null($formData)){

            $orderIndex = 0;

            if(!empty($formData['targets'])) foreach($formData['targets'] as $item){
                $id = $item['id'];
                if(empty($id)) $order['targets'][''][] = ++$orderIndex; else $order['targets'][$id] = ++$orderIndex;
            }

            $orderIndex = 0;

            if(!empty($formData['props'])) foreach($formData['props'] as $item){
                $id = $item['id'];
                if(empty($id)) $order['props'][''][] = ++$orderIndex; else $order['props'][$id] = ++$orderIndex;
            }


            $orderIndex = 0;

            if(!empty($formData['interest'])) foreach($formData['interest'] as $item){
                $id = $item['id'];
                if(empty($id)) $order['interest'][''][] = ++$orderIndex; else $order['interest'][$id] = ++$orderIndex;
            }


            $orderIndex = 0;

            if(!empty($formData['interiors'])) foreach($formData['interiors'] as $item){
                $id = $item['id'];
                if(empty($id)) $order['interiors'][''][] = ++$orderIndex; else $order['interiors'][$id] = ++$orderIndex;
            }


            $orderIndex = 0;

            if(!empty($formData['docs'])) foreach($formData['docs'] as $item){
                $id = $item['id'];
                if(empty($id)) $order['docs'][''][] = ++$orderIndex; else $order['docs'][$id] = ++$orderIndex;
            }


            $orderIndex = 0;

            if(!empty($formData['mediadocs'])) foreach($formData['mediadocs'] as $item){
                $id = $item['id'];
                if(empty($id)) $order['mediadocs'][''][] = ++$orderIndex; else $order['mediadocs'][$id] = ++$orderIndex;
            }


            $orderIndex = 0;

            if(!empty($formData['youtube'])) foreach($formData['youtube'] as $item){
                $id = $item['id'];
                if(empty($id)) $order['youtube'][''][] = ++$orderIndex; else $order['youtube'][$id] = ++$orderIndex;
            }


            $orderIndex = 0;

            if(!empty($formData['mediayoutube'])) foreach($formData['mediayoutube'] as $item){
                $id = $item['id'];
                if(empty($id)) $order['mediayoutube'][''][] = ++$orderIndex; else $order['mediayoutube'][$id] = ++$orderIndex;
            }

            $orderIndex = 0;

            if(!empty($formData['layout'])) foreach($formData['layout'] as $item){
                $id = $item['id'];
                if(empty($id)) $order['layout'][''][] = ++$orderIndex; else $order['layout'][$id] = ++$orderIndex;
            }

        }


        if($creation){

            $dir = __DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($object->getTitle()));

            Utils::MakeDir( __DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($object->getTitle())));
            Utils::MakeDir($dir);
            Utils::MakeDir($dir.'/interest');
            Utils::MakeDir($dir.'/layout');
            Utils::MakeDir($dir.'/interiors');
            Utils::MakeDir($dir.'/docs');
            Utils::MakeDir($dir.'/video');

        }else if($original['title']!=$object->getTitle()){

            rename(
                __DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($original['title'])),
                __DIR__.'/../../public/collection/'.preg_replace('/\s+/','-',strtolower($object->getTitle()))
            );

            rename(
                __DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($original['title'])),
                __DIR__.'/../../public/card/'.preg_replace('/\s+/','-',strtolower($object->getTitle()))
            );

        }
        
        // $this->logger->info(json_encode($original));

        foreach ($object->getTargets() as $rel) {

            $rel->setCollection($object);

            if(!is_null($formData)){

                $id = $rel->getId();

                if(empty($id)){

                    $orderIndex = array_shift($order['targets']['']);

                    if(!empty($orderIndex)) $rel->setPosition($orderIndex);

                }else{

                    $orderIndex = $order['targets'][$id];

                    if(!empty($orderIndex)) $rel->setPosition($orderIndex);

                }

            }

        }

        foreach ($object->getProps() as $rel) {

            $rel->setCollection($object);

            if(!is_null($formData)){

                $id = $rel->getId();

                if(empty($id)){

                    $orderIndex = array_shift($order['props']['']);

                    if(!empty($orderIndex)) $rel->setPosition($orderIndex);

                }else{

                    $orderIndex = $order['props'][$id];

                    if(!empty($orderIndex)) $rel->setPosition($orderIndex);

                }

            }

        }

/*        foreach ($object->getFaq() as $item) {
            $item->setCollection($object);
        }
*/

        foreach($object->getInterest() as $item){

            $item->setCollection($object);

            if ($item->getImageFile()){

                $item->refreshUpdated();

            }

            if(!is_null($formData)){

                $id = $item->getId();

                if(empty($id)){

                    $orderIndex = array_shift($order['interest']['']);

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }else{

                    $orderIndex = $order['interest'][$id];

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }

            }

        }


        foreach($object->getInteriors() as $item){

            $item->setCollection($object);

            if ($item->getImageFile()){

                $item->refreshUpdated();

            }

            if(!is_null($formData)){

                $id = $item->getId();

                if(empty($id)){

                    $orderIndex = array_shift($order['interiors']['']);

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }else{

                    $orderIndex = $order['interiors'][$id];

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }

            }

        }


        foreach($object->getDocs() as $item){

            $item->setCollection($object);

            if ($item->getFileFile()){

                $item->refreshUpdated();

            }

            if(!is_null($formData)){

                $id = $item->getId();

                if(empty($id)){

                    $orderIndex = array_shift($order['docs']['']);

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }else{

                    $orderIndex = $order['docs'][$id];

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }

            }

        }

        foreach($object->getMediadocs() as $item){

            $item->setCollection($object);

            if ($item->getFileFile()){

                $item->refreshUpdated();

            }

            if(!is_null($formData)){

                $id = $item->getId();

                if(empty($id)){

                    $orderIndex = array_shift($order['mediadocs']['']);

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }else{

                    $orderIndex = $order['mediadocs'][$id];

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }

            }

        }


        foreach($object->getYoutube() as $item){

            $item->setCollection($object);

            if(!is_null($formData)){

                $id = $item->getId();

                if(empty($id)){

                    $orderIndex = array_shift($order['youtube']['']);

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }else{

                    $orderIndex = $order['youtube'][$id];

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }

            }

        }

        foreach($object->getMediayoutube() as $item){

            $item->setCollection($object);

            if(!is_null($formData)){

                $id = $item->getId();

                if(empty($id)){

                    $orderIndex = array_shift($order['mediayoutube']['']);

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }else{

                    $orderIndex = $order['mediayoutube'][$id];

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }

            }

        }

        foreach($object->getLayout() as $item){

            $item->setCollection($object);

            if ($item->getImageFile()){

                $item->refreshUpdated();

            }

            if(!is_null($formData)){

                $id = $item->getId();

                if(empty($id)){

                    $orderIndex = array_shift($order['layout']['']);

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }else{

                    $orderIndex = $order['layout'][$id];

                    if(!empty($orderIndex)) $item->setPosition($orderIndex);

                }

            }

        }


        if ($object->getImageFile()||$object->getDesignimageFile()) {

            $object->refreshUpdated();

        }


    }



    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('moveprev', $this->getRouterIdParameter().'/moveprev');
        $collection->add('movenext', $this->getRouterIdParameter().'/movenext');
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');

        $listMapper
        ->add('_action', null, [
            'actions' => [
                'moveprev' => [
                    'template' => '@AppTemplates/CRUD/moveprev_action.html.twig',
                ],
                'movenext' => [
                    'template' => '@AppTemplates/CRUD/movenext_action.html.twig',
                ],
            ]
        ]);
    }


}


?>