<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\Form\Type\CollectionType;


final class StartPageFactsAdmin extends AbstractAdmin
{


    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->getSubject();
        
        $formMapper->add('title', TextType::class,[
            'label'=>'Заголовок'
        ]);


        $formMapper
            ->add('info', CollectionType::class, [
                
                'label'=>'Преимущества',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   


        $formMapper
            ->add('iconFile', FileType::class, [
                'required' => false,
                'label'=>'Иконка',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/start/facts/'.$object->getIcon().'" class="admin-preview"/>':'')
            ])
        ;
                
    }    

    public function prePersist($object){
        $this->updateObject($object,true);
    }
    
    public function preUpdate($object){
        $this->updateObject($object,false);
    }

    public function updateObject($object,$creation)
    {


        $formData = $this->getRequest()->request->get($this->getRequest()->query->get('uniqid'));


        $order = [

            'info'=>[''=>[]],

        ];


        $orderIndex = 0;

        if(!empty($formData['info'])) foreach($formData['info'] as $item){
            $id = $item['id'];
            if(empty($id)) $order['info'][''][] = ++$orderIndex; else $order['info'][$id] = ++$orderIndex;
        }


        foreach($object->getInfo() as $item){

            $item->setFact($object);

            $id = $item->getId();

            if(empty($id)){

                $orderIndex = array_shift($order['info']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['info'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }


        if ($object->getIconFile()) {

            $object->refreshUpdated();

        }

    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('moveprev', $this->getRouterIdParameter().'/moveprev');
        $collection->add('movenext', $this->getRouterIdParameter().'/movenext');
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');

        $listMapper
        ->add('_action', null, [
            'actions' => [
                'moveprev' => [
                    'template' => '@AppTemplates/CRUD/moveprev_action.html.twig',
                ],
                'movenext' => [
                    'template' => '@AppTemplates/CRUD/movenext_action.html.twig',
                ],
            ]
        ]);

    }
}


?>