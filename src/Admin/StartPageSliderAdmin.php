<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;


final class StartPageSliderAdmin extends AbstractAdmin
{


    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->getSubject();
        
        $formMapper->add('title', TextType::class,[
            'label'=>'Название'
        ]);
        
        $formMapper->add('text', TextareaType::class,[
            'required' => false,
            'label'=>'Описание'
        ]);
        

        $formMapper
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label'=>'Изображение',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/start/about/'.$object->getImage().'" class="admin-preview"/>':'')
            ])
        ;
        


    }


    public function prePersist($object)
    {
        $this->manageFileUpload($object);
    }

    public function preUpdate($object)
    {

        $this->manageFileUpload($object);

    }

    private function manageFileUpload($object)
    {


        if ($object->getImage()){

            $object->refreshUpdated();

        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('moveprev', $this->getRouterIdParameter().'/moveprev');
        $collection->add('movenext', $this->getRouterIdParameter().'/movenext');
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        
        $listMapper
            ->addIdentifier('title',null,['label'=>'Название'])
        ;


        $listMapper
        ->add('_action', null, [
            'actions' => [
                'moveprev' => [
                    'template' => '@AppTemplates/CRUD/moveprev_action.html.twig',
                ],
                'movenext' => [
                    'template' => '@AppTemplates/CRUD/movenext_action.html.twig',
                ],
            ]
        ]);

    }


}


?>