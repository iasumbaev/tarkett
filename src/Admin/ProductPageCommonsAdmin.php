<?php

namespace App\Admin;

// use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


use App\Entity\CardCollection;


final class ProductPageCommonsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->getSubject();

        $formMapper->add('caption', TextType::class,[
            'label'=>'Заголовок'
        ]);

        $formMapper->add('metatitle', TextType::class,[
            'label'=>'meta-тег title',
            'required'=>false
        ]);

        $formMapper->add('metakeys', TextType::class,[
            'label'=>'meta-тег keywords',
            'required'=>false
        ]);

        $formMapper->add('metadesc', TextType::class,[
            'label'=>'meta-тег description',
            'required'=>false
        ]);

        $formMapper->add('text', TextType::class,[
            'label'=>'Текст'
        ]);
        
        $formMapper
            ->add('video', TextType::class, [
                'required' => false,
                'label'=>'Видео'
            ])
        ;
           
    }    

    public function prePersist($object){
        $this->updateObject($object,true);
    }
    
    public function preUpdate($object){
        $this->updateObject($object,false);
    }

    public function updateObject($object,$creation)
    {

/*        if ($object->getVideoFile()||$object->getVideoposterFile()) {

            $object->refreshUpdated();

        }
*/
    }

    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
    
    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();

        unset($actions['create']);

        return $actions;
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('caption',null,['label'=>'Заголовок']);
    }
}


?>