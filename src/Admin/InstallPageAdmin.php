<?php

namespace App\Admin;

// use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use FOS\CKEditorBundle\Form\Type\CKEditorType;

use Sonata\Form\Type\CollectionType;

use App\Entity\InstallPageComps;


final class InstallPageAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->getSubject();

        $formMapper->add('title', TextType::class,[
            'label'=>'Название'
        ]);

        $formMapper->add('designcaption', TextType::class,[
            'label'=>'Заголовок для блока с галереей'
        ]);


        $formMapper->add('designtext', CKEditorType::class, array(

            'label'=>'Текст для блока с галереей',

            'config' => array(

                'toolbarGroups'=>[

                    ['name'=> 'document', 'groups'=> [ 'mode', 'document', 'doctools' ] ],

                    ['name'=> 'clipboard', 'groups'=> [ 'clipboard', 'undo' ] ],

                    ['name'=> 'editing', 'groups'=> [ 'find', 'selection', 'spellchecker', 'editing' ] ],

                    ['name'=> 'forms', 'groups'=> [ 'forms' ] ],

                    '/',

                    ['name'=> 'basicstyles', 'groups'=> [ 'basicstyles', 'cleanup' ] ],

                    ['name'=> 'paragraph', 'groups'=> [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] ],

                    ['name'=> 'links', 'groups'=> [ 'links' ] ],

                    ['name'=> 'insert', 'groups'=> [ 'insert' ] ],

                    '/',

                    ['name'=> 'styles', 'groups'=> [ 'styles' ] ],

                    ['name'=> 'colors', 'groups'=> [ 'colors' ] ],

                    ['name'=> 'tools', 'groups'=> [ 'tools' ] ],

                    ['name'=> 'others', 'groups'=> [ 'others' ] ],

                    ['name'=> 'about', 'groups'=> [ 'about' ]],

                ],

                "removeButtons"=> 'Save,Templates,Cut,Find,SelectAll,Scayt,Form,Superscript,Subscript,Strike,Underline,Italic,Blockquote,CreateDiv,NewPage,Preview,Print,Copy,Paste,PasteText,PasteFromWord,Replace,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,BidiLtr,BidiRtl,Language,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Font,Maximize,ShowBlocks,About',

                'uiColor' => '#ffffff',

            ),

        ));


        $formMapper
            ->add('iconFile', FileType::class, [
                'required' => false,
                'label'=>'Иконка',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;background: #000;" src="/install/top/'.$object->getIcon().'" class="admin-preview"/>':'')
            ])
        ;

        $formMapper
            ->add('activeiconFile', FileType::class, [
                'required' => false,
                'label'=>'Активная иконка',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/install/top/'.$object->getActiveicon().'" class="admin-preview"/>':'')
            ])
        ;
           
        $formMapper
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label'=>'Фоновое изображение',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/install/'.$object->getImage().'" class="admin-preview"/>':'')
            ])
        ;

        $formMapper->add('compcaption', TextType::class,[
            'label'=>'Что понадобится для укладки?'
        ]);

        $formMapper
            ->add('comps', CollectionType::class, [
                
                'label'=>'Список',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'natural',
                'sortable' => 'position',
            ])
        ;   


        $formMapper->add('needcaption', TextType::class,[
            'label'=>'Нельзя укладывать'
        ]);

        $formMapper
            ->add('needs', CollectionType::class, [
                
                'label'=>'Список',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   


        $formMapper->add('reqcaption', TextType::class,[
            'label'=>'Требования'
        ]);

        $formMapper
            ->add('reqs', CollectionType::class, [
                
                'label'=>'Список',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   


        $formMapper->add('stagecaption', TextType::class,[
            'label'=>'Этапы укладки'
        ]);

        $formMapper
            ->add('stages', CollectionType::class, [
                
                'label'=>'Список',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'natural',
                'sortable' => 'position',
            ])
        ;   


        $formMapper
            ->add('faq', CollectionType::class, [
                
                'label'=>'FAQ',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
        ;   



        $formMapper
            ->add('docs', CollectionType::class, [
                
                'label'=>'Документы',
                'required'=>false,
                'by_reference'=>true
            ], [
                'edit' => 'inline',
                'inline' => 'natural',
            ])
        ;   



    }    

    public function prePersist($object){
        $this->updateObject($object,true);
    }
    
    public function preUpdate($object){
        $this->updateObject($object,false);
    }

    public function updateObject($object,$creation)
    {




        $formData = $this->getRequest()->request->get($this->getRequest()->query->get('uniqid'));


        $order = [

            'comps'=>[''=>[]],

            'needs'=>[''=>[]],

            'reqs'=>[''=>[]],

            'stages'=>[''=>[]],
            
            'faq'=>[''=>[]],

        ];


        $orderIndex = 0;

        if(!empty($formData['comps'])) foreach($formData['comps'] as $item){
            $id = $item['id'];
            if(empty($id)) $order['comps'][''][] = ++$orderIndex; else $order['comps'][$id] = ++$orderIndex;
        }


        $orderIndex = 0;

        if(!empty($formData['needs'])) foreach($formData['needs'] as $item){
            $id = $item['id'];
            if(empty($id)) $order['needs'][''][] = ++$orderIndex; else $order['needs'][$id] = ++$orderIndex;
        }


        $orderIndex = 0;

        if(!empty($formData['reqs'])) foreach($formData['reqs'] as $item){
            $id = $item['id'];
            if(empty($id)) $order['reqs'][''][] = ++$orderIndex; else $order['reqs'][$id] = ++$orderIndex;
        }


        $orderIndex = 0;

        if(!empty($formData['stages'])) foreach($formData['stages'] as $item){
            $id = $item['id'];
            if(empty($id)) $order['stages'][''][] = ++$orderIndex; else $order['stages'][$id] = ++$orderIndex;
        }


        $orderIndex = 0;

        if(!empty($formData['faq'])) foreach($formData['faq'] as $item){
            $id = $item['id'];
            if(empty($id)) $order['faq'][''][] = ++$orderIndex; else $order['faq'][$id] = ++$orderIndex;
        }



        if ($object->getIconFile()||$object->getActiveiconFile()||$object->getImage()) {

            $object->refreshUpdated();

        }

        foreach($object->getComps() as $item){

            $item->setInstall($object);

            if ($item->getIconFile()){

                $item->refreshUpdated();

            }

            $id = $item->getId();

            if(empty($id)){

                $orderIndex = array_shift($order['comps']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['comps'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }

        foreach($object->getReqs() as $item){

            $item->setInstall($object);

            $id = $item->getId();

            if(empty($id)){

                $orderIndex = array_shift($order['reqs']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['reqs'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }

        foreach($object->getNeeds() as $item){

            $item->setInstall($object);

            $id = $item->getId();

            if(empty($id)){

                $orderIndex = array_shift($order['needs']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['needs'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }

        foreach($object->getStages() as $item){

            $item->setInstall($object);

            $id = $item->getId();

            if ($item->getImageFile()){

                $item->refreshUpdated();

            }

            if(empty($id)){

                $orderIndex = array_shift($order['stages']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['stages'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }


        foreach($object->getFaq() as $item){

            $item->setInstall($object);

            $id = $item->getId();

            if(empty($id)){

                $orderIndex = array_shift($order['faq']['']);

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }else{

                $orderIndex = $order['faq'][$id];

                if(!empty($orderIndex)) $item->setPosition($orderIndex);

            }

        }


        foreach($object->getDocs() as $item){

            $item->setInstall($object);

            foreach($item->getYoutube() as $sub_item){

                $sub_item->setSection($item);

            }

            foreach($item->getDoc() as $sub_item){

                $sub_item->setSection($item);

                if ($sub_item->getFileFile()){

                    $sub_item->refreshUpdated();

                }

            }

        }


    }

    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
    
    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();

        unset($actions['create']);

        return $actions;
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title',null,['label'=>'Название']);
    }
}


?>