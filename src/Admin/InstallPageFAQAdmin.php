<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use FOS\CKEditorBundle\Form\Type\CKEditorType;


final class InstallPageFAQAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title', TextType::class,[
            'label'=>'Заголовок'
        ]);


        $formMapper->add('text', CKEditorType::class, array(

            'label'=>'Текст',

            'config' => array(

                'toolbarGroups'=>[

                    ['name'=> 'document', 'groups'=> [ 'mode', 'document', 'doctools' ] ],

                    ['name'=> 'clipboard', 'groups'=> [ 'clipboard', 'undo' ] ],

                    ['name'=> 'editing', 'groups'=> [ 'find', 'selection', 'spellchecker', 'editing' ] ],

                    ['name'=> 'forms', 'groups'=> [ 'forms' ] ],

                    '/',

                    ['name'=> 'basicstyles', 'groups'=> [ 'basicstyles', 'cleanup' ] ],

                    ['name'=> 'paragraph', 'groups'=> [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] ],

                    ['name'=> 'links', 'groups'=> [ 'links' ] ],

                    ['name'=> 'insert', 'groups'=> [ 'insert' ] ],

                    '/',

                    ['name'=> 'styles', 'groups'=> [ 'styles' ] ],

                    ['name'=> 'colors', 'groups'=> [ 'colors' ] ],

                    ['name'=> 'tools', 'groups'=> [ 'tools' ] ],

                    ['name'=> 'others', 'groups'=> [ 'others' ] ],

                    ['name'=> 'about', 'groups'=> [ 'about' ]],

                ],

                "removeButtons"=> 'Save,Templates,Cut,Find,SelectAll,Scayt,Form,Superscript,Subscript,Strike,Underline,Italic,Blockquote,CreateDiv,NewPage,Preview,Print,Copy,Paste,PasteText,PasteFromWord,Replace,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,BidiLtr,BidiRtl,Language,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Font,Maximize,ShowBlocks,About',

                'uiColor' => '#ffffff',

            ),

        ));


    }    


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }

}