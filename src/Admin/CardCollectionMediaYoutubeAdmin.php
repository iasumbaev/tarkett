<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\Form\Type\CollectionType;


use App\Entity\CardCollectionMediaYoutube;


final class CardCollectionMediaYoutubeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
                
        $formMapper->add('id',null,[
            'attr' => ['style' => 'display: none;','tabindex'=>'-1'],
            'label_attr' => ['style' => 'display: none;'],
        ]);


        $formMapper->add('title', TextType::class, [
            'label' => 'Название файла',
        ]);

        $formMapper->add('href', TextType::class, [
            'label' => 'Гиперссылка',
        ]);

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }

}


?>