<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\Form\Type\CollectionType;


final class ProductPageIconAdmin extends AbstractAdmin
{

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->getSubject();
        
        $formMapper->add('caption', TextType::class,[
            'label'=>'Текст'
        ]);
        

        $formMapper
            ->add('iconFile', FileType::class, [
                'required' => false,
                'label'=>'иконка',
                'help'=>(!is_null($object)&&!empty($object->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/product/top/icons/'.$object->getIcon().'" class="admin-preview"/>':'')
            ])
        ;
            
    }    

    public function prePersist($object){
        $this->updateObject($object,true);
    }
    
    public function preUpdate($object){
        $this->updateObject($object,false);
    }



    public function updateObject($object,$creation)
    {

        if ($object->getIconFile()) {

            $object->refreshUpdated();

        }

    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('moveprev', $this->getRouterIdParameter().'/moveprev');
        $collection->add('movenext', $this->getRouterIdParameter().'/movenext');
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('caption');

        $listMapper
        ->add('_action', null, [
            'actions' => [
                'moveprev' => [
                    'template' => '@AppTemplates/CRUD/moveprev_action.html.twig',
                ],
                'movenext' => [
                    'template' => '@AppTemplates/CRUD/movenext_action.html.twig',
                ],
            ]
        ]);

    }

}


?>