<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\Form\Type\CollectionType;


use App\Entity\InstallPageDoc;


final class InstallPageDocAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        

        $object = $this->getSubject();


        $formMapper
            ->add('title', TextType::class, [
                'label' => 'Название',
            ])
        ;

        $formMapper
            ->add('fileFile', FileType::class, [
                'required' => false,
                'label'=>'Файл'
            ])
        ;
        
    }

    public function prePersist($object)
    {
        $this->manageFileUpload($object);

    }

    public function preUpdate($object)
    {

        $this->manageFileUpload($object);

    }

    private function manageFileUpload($object)
    {


        if ($object->getFileFile()){

            $object->refreshUpdated();

        }
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        // $listMapper->addIdentifier('title');
    }

}


?>