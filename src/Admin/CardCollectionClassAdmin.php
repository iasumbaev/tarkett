<?php

namespace App\Admin;

// use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;


final class CardCollectionClassAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $cardClass = $this->getSubject();
        
        $formMapper->add('title', TextType::class,[
            'label'=>'Название'
        ]);
        

        $formMapper
            ->add('iconFile', FileType::class, [
                'required' => false,
                'label'=>'Иконка',
                'help'=>(!empty($cardClass->getId())?'<div style="display:inline-block;background: #000;"><img style="max-width: 200px;max-height: 200px;" src="/collections/class/'.$cardClass->getIcon().'" class="admin-preview"/></div>':'')
            ])
        ;
        

        $formMapper
            ->add('menuIconFile', FileType::class, [
                'required' => false,
                'label'=>'Иконка в меню коллекций',
                'help'=>(!empty($cardClass->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/collections/class/'.$cardClass->getMenuIcon().'" class="admin-preview"/>':'')
            ])
        ;

    }


    public function prePersist($cardClass)
    {
        $this->manageFileUpload($cardClass);
    }

    public function preUpdate($cardClass)
    {

        $this->manageFileUpload($cardClass);

    }

    private function manageFileUpload($cardClass)
    {


        if ($cardClass->getIconFile()||$cardClass->getMenuIconFile()) {

            $cardClass->refreshUpdated();

        }
    }

    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
    
    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();

        unset($actions['create']);

        return $actions;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }
}


?>