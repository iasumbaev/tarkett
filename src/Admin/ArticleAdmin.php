<?php


namespace App\Admin;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        /**
         * @var Article $article
         */
        $article = $this->getSubject();
        $formMapper->add('title', null, [
            'label' => 'Название',
        ])
            ->add('previewText', null, [
                'label' => 'Текст для превью',
            ])
            ->add('timeOfReading', NumberType::class, [
                'label' => 'Время прочтения',
            ])
            ->add('text', CKEditorType::class, array(
                'label' => 'Текст',
                'config' => array(
                    'toolbarGroups' => [
                        ['name' => 'document', 'groups' => ['mode', 'document', 'doctools']],
                        ['name' => 'clipboard', 'groups' => ['clipboard', 'undo']],
                        ['name' => 'editing', 'groups' => ['find', 'selection', 'spellchecker', 'editing']],
                        ['name' => 'forms', 'groups' => ['forms']],
                        '/',
                        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                        ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']],
                        ['name' => 'links', 'groups' => ['links']],
                        ['name' => 'insert', 'groups' => ['insert']],
                        '/',
                        ['name' => 'styles', 'groups' => ['styles']],
                        ['name' => 'colors', 'groups' => ['colors']],
                        ['name' => 'tools', 'groups' => ['tools']],
                        ['name' => 'others', 'groups' => ['others']],
                        ['name' => 'about', 'groups' => ['about']],
                    ],

                    'removeButtons' => 'Save,Templates,Cut,Find,SelectAll,Scayt,Form,Superscript,Subscript,Strike,Underline,Italic,Blockquote,CreateDiv,NewPage,Preview,Print,Copy,Paste,PasteText,PasteFromWord,Replace,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,BidiLtr,BidiRtl,Language,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Font,Maximize,ShowBlocks,About',
                    'extraPlugins' => 'image2,format,video',
                    'removePlugins' => 'image',
                    'filebrowserUploadUrl' => '/blog/article/upload_image',
                    'filebrowserUploadMethod' => 'form',
                    'uiColor' => '#ffffff',
                ),

            ))
            ->add('articleCategory', EntityType::class, [
                'label' => 'Категория',
                'class' => ArticleCategory::class,
                'choice_label' => 'title'
            ])
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label' => 'Изображение',
                'help' => ($article && $article->getId() ? '<img style="max-width: 200px;max-height: 200px;" src="/uploads/article_preview/' . $article->getImageFileName() . '" class="admin-preview"/>' : '')
            ])
//            ->add('altImage', TextType::class, [
//                'data' => $article->getAltImage(),
//                'label' => 'Alt-текст для изображения',
//                'mapped' => true,
//                'required' => false
//            ])
            ->add('metaTitle', null, [
                'label' => 'meta-тег title',
            ])
            ->add('metaKeywords', null, [
                'label' => 'meta-тег keywords',
            ])
            ->add('metaDescription', null, [
                'label' => 'meta-тег description',
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }

    public function prePersist($article)
    {
        /** @var Article $article */
        $article->uploadImageFile();
    }


    public function preUpdate($article)
    {
        /** @var Article $article */
        $article->uploadImageFile();
    }


}