<?php

namespace App\Admin;

// use Symfony\Bridge\Monolog\Logger;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Console\Output\OutputInterface;

use Sonata\CoreBundle\Model\Metadata;


final class CardCollectionTargetAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $dbObject = $this->getSubject();
        
        $formMapper->add('title', TextType::class,[
            'label'=>'Название'
        ]);
        

        $formMapper
            ->add('iconFile', FileType::class, [
                'required' => false,
                'label'=>'Иконка',
                'help'=>(!is_null($dbObject)&&!empty($dbObject->getId())?'<div style="display:inline-block;background: #000;padding: 10px;"><img style="max-width: 200px;max-height: 200px;" src="/collections/icons/'.$dbObject->getIcon().'" class="admin-preview"/></div>':'')
            ])
        ;

        $formMapper
            ->add('giconFile', FileType::class, [
                'required' => false,
                'label'=>'Иконка в карточку товара',
                'help'=>(!is_null($dbObject)&&!empty($dbObject->getId())?'<img style="max-width: 200px;max-height: 200px;" src="/collections/icons/'.$dbObject->getGicon().'" class="admin-preview"/>':'')
            ])
        ;

    }


    public function prePersist($dbObject)
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($dbObject)
    {

        $this->manageFileUpload($dbObject);

    }

    private function manageFileUpload($dbObject)
    {


        if ($dbObject->getIconFile()||$dbObject->getGiconFile()){

            $dbObject->refreshUpdated();

        }
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }

    public function getObjectMetadata($dbObject): Metadata
    {

        $thumb = $dbObject->getIcon();

        return new Metadata($dbObject->getTitle(),'',(empty($thumb)?null:'/collections/icons/'.$thumb));
    }

}


?>