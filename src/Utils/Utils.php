<?php

namespace App\Utils;



function delTree($dir) { 

    $files = array_diff(scandir($dir), array('.','..')); 

    foreach ($files as $file) { 

        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 

    } 

    return rmdir($dir); 

} 


function make_seed()
{
    list($usec, $sec) = explode(' ', microtime());
    return $sec + $usec * 1000000;
}

srand(make_seed());


class Utils{

	static function MakeDir($dir){
		if(!file_exists($dir)) mkdir($dir);
	}

	static function RemoveFile($parent='',$file=''){

		if(!$parent) return;
		
		if(!$file) return;

		$path = $parent.'/'.$file;

		if(!file_exists($path)) return;

		if(is_file($path)){
			unlink($path);
			return;
		}

		if(is_dir($path)) delTree($path);

	}

	static function RandomInteger($from,$to){
		
		return rand($from,$to);
		
	}


}


?>