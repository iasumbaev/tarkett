<?php

namespace App\Repository;

use App\Entity\ProductPageSliderInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductPageSliderInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPageSliderInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPageSliderInfo[]    findAll()
 * @method ProductPageSliderInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPageSliderInfoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductPageSliderInfo::class);
    }

    // /**
    //  * @return ProductPageSliderInfo[] Returns an array of ProductPageSliderInfo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPageSliderInfo
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
