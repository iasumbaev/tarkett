<?php

namespace App\Repository;

use App\Entity\ContactsPageCommons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ContactsPageCommons|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactsPageCommons|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactsPageCommons[]    findAll()
 * @method ContactsPageCommons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactsPageCommonsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ContactsPageCommons::class);
    }

    // /**
    //  * @return ContactsPageCommons[] Returns an array of ContactsPageCommons objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactsPageCommons
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
