<?php

namespace App\Repository;

use App\Entity\ProductPageFAQ;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductPageFAQ|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPageFAQ|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPageFAQ[]    findAll()
 * @method ProductPageFAQ[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPageFAQRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductPageFAQ::class);
    }

    // /**
    //  * @return ProductPageFAQ[] Returns an array of ProductPageFAQ objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPageFAQ
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
