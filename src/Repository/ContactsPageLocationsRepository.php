<?php

namespace App\Repository;

use App\Entity\ContactsPageLocations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ContactsPageLocations|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactsPageLocations|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactsPageLocations[]    findAll()
 * @method ContactsPageLocations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactsPageLocationsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ContactsPageLocations::class);
    }

    // /**
    //  * @return ContactsPageLocations[] Returns an array of ContactsPageLocations objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactsPageLocations
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
