<?php

namespace App\Repository;

use App\Entity\CardCollectionTargetRels;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardCollectionTargetRels|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardCollectionTargetRels|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardCollectionTargetRels[]    findAll()
 * @method CardCollectionTargetRels[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardCollectionTargetRelsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardCollectionTargetRels::class);
    }

    // /**
    //  * @return CardCollectionTargetRels[] Returns an array of CardCollectionTargetRels objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardCollectionTargetRels
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
