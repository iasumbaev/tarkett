<?php

namespace App\Repository;

use App\Entity\InstallPageReqs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPageReqs|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPageReqs|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPageReqs[]    findAll()
 * @method InstallPageReqs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageReqsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPageReqs::class);
    }

    // /**
    //  * @return InstallPageReqs[] Returns an array of InstallPageReqs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPageReqs
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
