<?php

namespace App\Repository;

use App\Entity\StartPageFacts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StartPageFacts|null find($id, $lockMode = null, $lockVersion = null)
 * @method StartPageFacts|null findOneBy(array $criteria, array $orderBy = null)
 * @method StartPageFacts[]    findAll()
 * @method StartPageFacts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StartPageFactsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StartPageFacts::class);
    }

    // /**
    //  * @return StartPageFacts[] Returns an array of StartPageFacts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StartPageFacts
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
