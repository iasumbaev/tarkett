<?php

namespace App\Repository;

use App\Entity\CardCollectionMediaYoutube;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardCollectionMediaYoutube|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardCollectionMediaYoutube|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardCollectionMediaYoutube[]    findAll()
 * @method CardCollectionMediaYoutube[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardCollectionMediaYoutubeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardCollectionMediaYoutube::class);
    }

    // /**
    //  * @return CardCollectionMediaYoutube[] Returns an array of CardCollectionMediaYoutube objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardCollectionMediaYoutube
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
