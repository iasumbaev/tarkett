<?php

namespace App\Repository;

use App\Entity\CardCollectionMediaDocument;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardCollectionMediaDocument|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardCollectionMediaDocument|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardCollectionMediaDocument[]    findAll()
 * @method CardCollectionMediaDocument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardCollectionMediaDocumentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardCollectionMediaDocument::class);
    }

    // /**
    //  * @return CardCollectionMediaDocument[] Returns an array of CardCollectionMediaDocument objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardCollectionMediaDocument
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
