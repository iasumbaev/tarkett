<?php

namespace App\Repository;

use App\Entity\InstallPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPage[]    findAll()
 * @method InstallPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPage::class);
    }

    // /**
    //  * @return InstallPage[] Returns an array of InstallPage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPage
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
