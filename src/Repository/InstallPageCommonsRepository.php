<?php

namespace App\Repository;

use App\Entity\InstallPageCommons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPageCommons|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPageCommons|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPageCommons[]    findAll()
 * @method InstallPageCommons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageCommonsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPageCommons::class);
    }

    // /**
    //  * @return InstallPageCommons[] Returns an array of InstallPageCommons objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPageCommons
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
