<?php

namespace App\Repository;

use App\Entity\CardCollectionTarget;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardCollectionTarget|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardCollectionTarget|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardCollectionTarget[]    findAll()
 * @method CardCollectionTarget[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardCollectionTargetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardCollectionTarget::class);
    }

    // /**
    //  * @return CardCollectionTarget[] Returns an array of CardCollectionTarget objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardCollectionTarget
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
