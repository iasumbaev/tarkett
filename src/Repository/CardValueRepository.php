<?php

namespace App\Repository;

use App\Entity\CardValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardValue[]    findAll()
 * @method CardValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardValueRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardValue::class);
    }

    // /**
    //  * @return CardValue[] Returns an array of CardValue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardValue
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
