<?php

namespace App\Repository;

use App\Entity\InstallPageComps;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPageComps|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPageComps|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPageComps[]    findAll()
 * @method InstallPageComps[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageCompsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPageComps::class);
    }

    // /**
    //  * @return InstallPageComps[] Returns an array of InstallPageComps objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPageComps
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
