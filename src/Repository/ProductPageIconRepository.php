<?php

namespace App\Repository;

use App\Entity\ProductPageIcon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductPageIcon|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPageIcon|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPageIcon[]    findAll()
 * @method ProductPageIcon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPageIconRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductPageIcon::class);
    }

    // /**
    //  * @return ProductPageIcon[] Returns an array of ProductPageIcon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPageIcon
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
