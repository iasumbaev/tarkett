<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function getPreviouslyArticle($articleId) {
        $qb = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.id < :articleId')
            ->setParameter(':articleId', $articleId)
            ->orderBy('a.id', 'DESC')
            ->setMaxResults(1)
        ;

        $result = $qb->getQuery()->getOneOrNullResult();

        if($result) {
            return $result;
        }

        //Для первой статьи, предыдущая - последняя
        return $this->createQueryBuilder('a')
            ->select('a')
            ->setMaxResults(1)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getNextArticle($articleId) {
        $qb = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.id > :articleId')
            ->setParameter(':articleId', $articleId)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(1)
        ;

        $result = $qb->getQuery()->getOneOrNullResult();

        if($result) {
            return $result;
        }

        //Для последней статьи, следующя - первая
        return $this->createQueryBuilder('a')
            ->select('a')
            ->setMaxResults(1)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getArticlesSameCategory($articleId, $categoryId) {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->andWhere('a.id != :articleId')
            ->andWhere('a.articleCategory = :categoryId')
            ->setParameter(':articleId', $articleId)
            ->setParameter(':categoryId', $categoryId)
            ->orderBy('a.id', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
