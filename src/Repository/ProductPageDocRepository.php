<?php

namespace App\Repository;

use App\Entity\ProductPageDoc;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductPageDoc|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPageDoc|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPageDoc[]    findAll()
 * @method ProductPageDoc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPageDocRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductPageDoc::class);
    }

    // /**
    //  * @return ProductPageDoc[] Returns an array of ProductPageDoc objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPageDoc
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
