<?php

namespace App\Repository;

use App\Entity\ContactsPageCities;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ContactsPageCities|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactsPageCities|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactsPageCities[]    findAll()
 * @method ContactsPageCities[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactsPageCitiesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ContactsPageCities::class);
    }

    // /**
    //  * @return ContactsPageCities[] Returns an array of ContactsPageCities objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactsPageCities
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
