<?php

namespace App\Repository;

use App\Entity\InstallPageDocsSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPageDocsSection|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPageDocsSection|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPageDocsSection[]    findAll()
 * @method InstallPageDocsSection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageDocsSectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPageDocsSection::class);
    }

    // /**
    //  * @return InstallPageDocsSection[] Returns an array of InstallPageDocsSection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPageDocsSection
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
