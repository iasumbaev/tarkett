<?php

namespace App\Repository;

use App\Entity\CardCollectionPropRels;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardCollectionPropRels|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardCollectionPropRels|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardCollectionPropRels[]    findAll()
 * @method CardCollectionPropRels[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardCollectionPropRelsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardCollectionPropRels::class);
    }

    // /**
    //  * @return CardCollectionPropRels[] Returns an array of CardCollectionPropRels objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardCollectionPropRels
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
