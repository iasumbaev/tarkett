<?php

namespace App\Repository;

use App\Entity\InstallPageDoc;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPageDoc|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPageDoc|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPageDoc[]    findAll()
 * @method InstallPageDoc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageDocRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPageDoc::class);
    }

    // /**
    //  * @return InstallPageDoc[] Returns an array of InstallPageDoc objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPageDoc
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
