<?php

namespace App\Repository;

use App\Entity\ProductPageDocsSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductPageDocsSection|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPageDocsSection|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPageDocsSection[]    findAll()
 * @method ProductPageDocsSection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPageDocsSectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductPageDocsSection::class);
    }

    // /**
    //  * @return ProductPageDocsSection[] Returns an array of ProductPageDocsSection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPageDocsSection
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
