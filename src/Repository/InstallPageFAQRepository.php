<?php

namespace App\Repository;

use App\Entity\InstallPageFAQ;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPageFAQ|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPageFAQ|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPageFAQ[]    findAll()
 * @method InstallPageFAQ[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageFAQRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPageFAQ::class);
    }

    // /**
    //  * @return InstallPageFAQ[] Returns an array of InstallPageFAQ objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPageFAQ
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
