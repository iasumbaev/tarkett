<?php

namespace App\Repository;

use App\Entity\StartPageSlider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StartPageSlider|null find($id, $lockMode = null, $lockVersion = null)
 * @method StartPageSlider|null findOneBy(array $criteria, array $orderBy = null)
 * @method StartPageSlider[]    findAll()
 * @method StartPageSlider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StartPageSliderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StartPageSlider::class);
    }

    // /**
    //  * @return StartPageSlider[] Returns an array of StartPageSlider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StartPageSlider
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
