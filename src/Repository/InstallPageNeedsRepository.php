<?php

namespace App\Repository;

use App\Entity\InstallPageNeeds;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPageNeeds|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPageNeeds|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPageNeeds[]    findAll()
 * @method InstallPageNeeds[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageNeedsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPageNeeds::class);
    }

    // /**
    //  * @return InstallPageNeeds[] Returns an array of InstallPageNeeds objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPageNeeds
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
