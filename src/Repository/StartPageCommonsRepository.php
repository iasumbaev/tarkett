<?php

namespace App\Repository;

use App\Entity\StartPageCommons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StartPageCommons|null find($id, $lockMode = null, $lockVersion = null)
 * @method StartPageCommons|null findOneBy(array $criteria, array $orderBy = null)
 * @method StartPageCommons[]    findAll()
 * @method StartPageCommons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StartPageCommonsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StartPageCommons::class);
    }

    // /**
    //  * @return StartPageCommons[] Returns an array of StartPageCommons objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StartPageCommons
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
