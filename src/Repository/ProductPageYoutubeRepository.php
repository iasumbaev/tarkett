<?php

namespace App\Repository;

use App\Entity\ProductPageYoutube;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductPageYoutube|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPageYoutube|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPageYoutube[]    findAll()
 * @method ProductPageYoutube[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPageYoutubeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductPageYoutube::class);
    }

    // /**
    //  * @return ProductPageYoutube[] Returns an array of ProductPageYoutube objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPageYoutube
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
