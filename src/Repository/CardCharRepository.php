<?php

namespace App\Repository;

use App\Entity\CardChar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardChar|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardChar|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardChar[]    findAll()
 * @method CardChar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardCharRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardChar::class);
    }

    // /**
    //  * @return CardChar[] Returns an array of CardChar objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardChar
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
