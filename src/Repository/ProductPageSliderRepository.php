<?php

namespace App\Repository;

use App\Entity\ProductPageSlider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductPageSlider|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPageSlider|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPageSlider[]    findAll()
 * @method ProductPageSlider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPageSliderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductPageSlider::class);
    }

    // /**
    //  * @return ProductPageSlider[] Returns an array of ProductPageSlider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPageSlider
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
