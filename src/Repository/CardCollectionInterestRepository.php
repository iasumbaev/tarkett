<?php

namespace App\Repository;

use App\Entity\CardCollectionInterest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardCollectionInterest|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardCollectionInterest|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardCollectionInterest[]    findAll()
 * @method CardCollectionInterest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardCollectionInterestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardCollectionInterest::class);
    }

    // /**
    //  * @return CardCollectionInterest[] Returns an array of CardCollectionInterest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardCollectionInterest
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
