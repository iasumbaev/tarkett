<?php

namespace App\Repository;

use App\Entity\ProductPageCommons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductPageCommons|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPageCommons|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPageCommons[]    findAll()
 * @method ProductPageCommons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPageCommonsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductPageCommons::class);
    }

    // /**
    //  * @return ProductPageCommons[] Returns an array of ProductPageCommons objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPageCommons
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
