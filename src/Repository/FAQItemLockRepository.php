<?php

namespace App\Repository;

use App\Entity\FAQItemLock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FAQItemLock|null find($id, $lockMode = null, $lockVersion = null)
 * @method FAQItemLock|null findOneBy(array $criteria, array $orderBy = null)
 * @method FAQItemLock[]    findAll()
 * @method FAQItemLock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FAQItemLockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FAQItemLock::class);
    }

    // /**
    //  * @return FAQItemLock[] Returns an array of FAQItemLock objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FAQItemLock
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
