<?php

namespace App\Repository;

use App\Entity\CardCollectionInterior;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardCollectionInterior|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardCollectionInterior|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardCollectionInterior[]    findAll()
 * @method CardCollectionInterior[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardCollectionInteriorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardCollectionInterior::class);
    }

    // /**
    //  * @return CardCollectionInterior[] Returns an array of CardCollectionInterior objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardCollectionInterior
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
