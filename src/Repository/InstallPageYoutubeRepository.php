<?php

namespace App\Repository;

use App\Entity\InstallPageYoutube;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPageYoutube|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPageYoutube|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPageYoutube[]    findAll()
 * @method InstallPageYoutube[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageYoutubeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPageYoutube::class);
    }

    // /**
    //  * @return InstallPageYoutube[] Returns an array of InstallPageYoutube objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPageYoutube
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
