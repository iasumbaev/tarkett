<?php

namespace App\Repository;

use App\Entity\InstallPageStages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstallPageStages|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstallPageStages|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstallPageStages[]    findAll()
 * @method InstallPageStages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstallPageStagesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstallPageStages::class);
    }

    // /**
    //  * @return InstallPageStages[] Returns an array of InstallPageStages objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstallPageStages
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
