<?php

namespace App\Repository;

use App\Entity\FAQItemGlue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FAQItemGlue|null find($id, $lockMode = null, $lockVersion = null)
 * @method FAQItemGlue|null findOneBy(array $criteria, array $orderBy = null)
 * @method FAQItemGlue[]    findAll()
 * @method FAQItemGlue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FAQItemGlueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FAQItemGlue::class);
    }

    // /**
    //  * @return FAQItemGlue[] Returns an array of FAQItemGlue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FAQItemGlue
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
