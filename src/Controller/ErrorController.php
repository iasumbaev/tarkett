<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class ErrorController extends Controller
 {
     public function route()
     {

		$pageData = [
			'type' => 'notfound',
		];


		$context = $this->getDoctrine();

		CommonsPageData::process($context,$pageData);

		 return new Response(
		 	$this->renderView('index.html.twig', $pageData),
		 	404
		 );
     }
}

