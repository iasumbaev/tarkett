<?php

namespace App\Controller;

use App\Repository\ProjectRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\ProductPageSlider;
use App\Entity\ProductPageSliderInfo;
use App\Entity\ProductPageCommons;
use App\Entity\ProductPageIcon;
use App\Entity\ProductPageFAQ;
use App\Entity\ProductPageDocsSection;

use Symfony\Component\HttpFoundation\Request;


class ProductPageController extends Controller
 {
     /**
      * @Route("/art-vinyl/")
      */
     public function route(Request $request, ProjectRepository $projectRepository)
     {

		$pageData = [
			'type' => 'product',
            'istest'=> $request->query->has('test'),
			'slider' => []
		];


		$context = $this->getDoctrine();

		CommonsPageData::process($context,$pageData);


		$sliderRep = $this->getDoctrine()->getRepository(ProductPageSlider::class);

		$slider = $sliderRep->findBy([],['position'=>'ASC']);

		foreach($slider as $slider){

			$sliderData = [

				'title'=>$slider->getTitle(),

				'info'=>[],
				
				'image'=>$slider->getImage(),

			];

			$info = $slider->getInfo();

			foreach($info as $item){

				$sliderData['info'][] = [
					'text'=>$item->getText()
				];

			}

			$pageData['slider'][] = $sliderData;

		}
  

		$productCommonsRep = $context->getRepository(ProductPageCommons::class);

		$productCommons = ($productCommonsRep->findAll())[0];


		$pageData['caption'] = $productCommons->getCaption();
		$pageData['text'] = $productCommons->getText();


		$pageData['video'] = $productCommons->getVideo();



		$metatitle = $productCommons->getMetatitle();

		if(!empty($metatitle)) $pageData['metatitle'] = $metatitle;

		$metakeys = $productCommons->getMetakeys();

		if(!empty($metakeys)) $pageData['metakeys'] = $metakeys;

		$metadesc = $productCommons->getMetadesc();

		if(!empty($metadesc)) $pageData['metadesc'] = $metadesc;





		$productIconsRep = $context->getRepository(ProductPageIcon::class);

		$productIcons = $productIconsRep->findBy([],['position'=>'ASC']);

		$pageData['icons'] = [];

		foreach($productIcons as $item){

			$pageData['icons'][] = [
				
				'caption'=>$item->getCaption(),
				
				'icon'=>$item->getIcon(),
				
			];

		}




		$ProductPageFAQRep = $context->getRepository(ProductPageFAQ::class);

		$productFAQ = $ProductPageFAQRep->findBy([],['position'=>'ASC']);

		$pageData['faq'] = [];

		foreach($productFAQ as $item){

			$pageData['faq'][] = [
				
				'caption'=>$item->getTitle(),
				
				'text'=>$item->getText(),
				
			];

		}




		$pageData['docs'] = [];

		$productPageDocsRep = $context->getRepository(ProductPageDocsSection::class);

		foreach($productPageDocsRep->findBy([],['position'=>'ASC']) as $item){

			$section = [

				'title'=>$item->getTitle(),

				'videos'=>[],

				'files'=>[]

			];

			foreach($item->getYoutube() as $video){

				$section['videos'][] = [

					'title'=>$video->getTitle(),

					'href'=>$video->getHref()

				];

			}

			foreach($item->getDoc() as $file){

				$section['files'][] = [

					'title'=>$file->getTitle(),

					'name'=>$file->getFile(),
					
					'size'=>$file->getSize(),
					
					'type'=>$file->getType()

				];

			}

			$pageData['docs'][] = $section;

		}


		$pageData['projects'] = $projectRepository->findAll();
        return $this->render('index.html.twig',$pageData);

     }

    /**
     * @Route("/product/")
     */
    public function redirecting() {
        return $this->redirectToRoute('productpage');
    }

}