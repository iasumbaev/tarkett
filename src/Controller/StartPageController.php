<?php

namespace App\Controller;

use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\StartPageSlider;
use App\Entity\StartPageFacts;
use App\Entity\Card;
use App\Entity\StartPageCommons;
use App\Entity\CardCollection;

use Symfony\Component\HttpFoundation\Request;

use App\Utils\Utils;


class StartPageController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function route(Request $request, ProjectRepository $projectRepository)
    {

        $pageData = [
            'type' => 'start',
            'slider' => [],
            'facts' => [],
            'istest'=> $request->query->has('test')
        ];


        $context = $this->getDoctrine();

        CommonsPageData::process($context,$pageData);


        $sliderRep = $this->getDoctrine()->getRepository(StartPageSlider::class);

        $slider = $sliderRep->findBy([],['position'=>'ASC']);

        foreach($slider as $slide){

            $pageData['slider'][] = [

                'title'=>$slide->getTitle(),

                'text'=>$slide->getText(),

                'image'=>$slide->getImage(),

            ];

        }


        $factsRep = $this->getDoctrine()->getRepository(StartPageFacts::class);

        $facts = $factsRep->findBy([],['position'=>'ASC']);

        foreach($facts as $fact){

            $factData = [

                'title'=>$fact->getTitle(),

                'icon'=>$fact->getIcon(),

                'info'=>[],

            ];

            $info = $fact->getInfo();

            foreach($info as $item){

                $factData['info'][] = [
                    'text'=>$item->getText()
                ];

            }

            $pageData['facts'][] = $factData;

        }


        $cardsRep = $this->getDoctrine()->getRepository(Card::class);

        $cards = $cardsRep->findAll();

        $usedCards = [];

        $pageData['cards1'] = [];
        $pageData['cards2'] = [];
        $pageData['cards3'] = [];

        do{

            $index = Utils::RandomInteger(0,count($cards)-1);



            if(!in_array($index, $usedCards)){

                $usedCards[] = $index;

                $card = $cards[$index];

                if($card->getCollection()->getClass()->getId()==1){

                    $type = ['tile','plate'][$card->getType()->getId()-1];

                    if($type=='plate'){

                        $collection = $card->getCollection()->getTitle();

                        $pageData['cards1'][] = [

                            'title'=>$card->getTitle(),

                            'image'=>$card->getImage(),

                            'collection'=>$collection,

                            'href'=>'/collections/'.preg_replace('/\s+/','-',strtolower($collection)).'/'.preg_replace('/\s+/','-',strtolower($card->getTitle())).'/',
                            'href_image'=>'/card/'.preg_replace('/\s+/','-',strtolower($collection)).'/'.preg_replace('/\s+/','-',strtolower($card->getTitle())).'/'

                        ];

                    }

                }

            }

        }while(count($pageData['cards1'])<5);

        $usedCards = [];

        do{

            $index = Utils::RandomInteger(0,count($cards)-1);

            if(!in_array($index, $usedCards)){

                $usedCards[] = $index;

                $card = $cards[$index];

                if($card->getCollection()->getClass()->getId()==2){

                    $type = ['tile','plate'][$card->getType()->getId()-1];

                    if($type=='plate'){

                        $collection = $card->getCollection()->getTitle();

                        $pageData['cards2'][] = [

                            'title'=>$card->getTitle(),

                            'image'=>$card->getImage(),

                            'collection'=>$collection,

                            'href'=>'/collections/'.preg_replace('/\s+/','-',strtolower($card->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($card->getTitle())).'/',
                            'href_image'=>'/card/'.preg_replace('/\s+/','-',strtolower($collection)).'/'.preg_replace('/\s+/','-',strtolower($card->getTitle())).'/'
                        ];

                    }

                }

            }

        }while(count($pageData['cards2'])<4);

        $usedCards = [];

        do{

            $index = Utils::RandomInteger(0,count($cards)-1);

            if(!in_array($index, $usedCards)){

                $usedCards[] = $index;

                $card = $cards[$index];

                $type = ['tile','plate'][$card->getType()->getId()-1];

                if($type=='tile'){

                    $collection = $card->getCollection()->getTitle();

                    $pageData['cards3'][] = [

                        'title'=>$card->getTitle(),

                        'image'=>$card->getImage(),

                        'collection'=>$collection,

                        'href'=>'/collections/'.preg_replace('/\s+/','-',strtolower($card->getCollection()->getTitle())).'/'.preg_replace('/\s+/','-',strtolower($card->getTitle())).'/',
                        'href_image'=>'/card/'.preg_replace('/\s+/','-',strtolower($collection)).'/'.preg_replace('/\s+/','-',strtolower($card->getTitle())).'/'

                    ];

                }

            }

        }while(count($pageData['cards3'])<9);


        $startCommonsRep = $context->getRepository(StartPageCommons::class);

        $startCommons = ($startCommonsRep->findAll())[0];


        $pageData['caption'] = $startCommons->getCaption();
        $pageData['captionAside'] = $startCommons->getCaptionaside();


        $pageData['designcaption'] = $startCommons->getDesigncaption();
        $pageData['designtext'] = $startCommons->getDesigntext();

        $pageData['designlist'] = [];


        foreach($context->getRepository(CardCollection::class)->findBy([],['position'=>'ASC']) as $collection){

            $count = count($collection->getCards());

            $f = floor($count/10); $s = $count%10;

            $pageData['designlist'][] = [

                'title'=>$collection->getTitle(),

                'aside'=>$collection->getDesigntitle(),

                'href' => '/collections/'.preg_replace('/\s+/','-',strtolower($collection->getTitle())).'/',

                'image' => '/collection/'.preg_replace('/\s+/','-',strtolower($collection->getTitle())).'/'.$collection->getImage(),

                'count' => $count,

                'unit' => 'дизайн'.($f==1?'ов':($s==1?'':($s>=2&&$s<=4?'а':'ов')))

            ];

        }

        $pageData['projects'] = $projectRepository->findAll();

        return $this->render('index.html.twig',$pageData);

    }

}