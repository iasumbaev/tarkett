<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// use App\Entity\ContactsPageCities;
// use App\Entity\ContactsPageLocations;


use App\Entity\InstallPage;
use App\Entity\InstallPageCommons;
use App\Entity\InstallPageComps;
use App\Entity\InstallPageFAQ;
use App\Entity\InstallPageNeeds;
use App\Entity\InstallPageReqs;
use App\Entity\InstallPageStages;

use App\Entity\CardCollection;

use App\Utils\Utils;


class InstallPageController extends Controller{

    /**
     * @Route("/installation/", name="installpage")
     */
    public function route(){

        $pageData = [

            'type' => 'install',

            'variants' => []

        ];


        $context = $this->getDoctrine();

        CommonsPageData::process($context,$pageData);

        $commonsRep = $this->getDoctrine()->getRepository(InstallPageCommons::class);

        $commons = ($commonsRep->findAll())[0];

        $pageData['caption'] = $commons->getCaption();

        $pageData['subcaption'] = $commons->getSubcaption();


        $metatitle = $commons->getMetatitle();

        if(!empty($metatitle)) $pageData['metatitle'] = $metatitle;

        $metakeys = $commons->getMetakeys();

        if(!empty($metakeys)) $pageData['metakeys'] = $metakeys;

        $metadesc = $commons->getMetadesc();

        if(!empty($metadesc)) $pageData['metadesc'] = $metadesc;


        foreach($this->getDoctrine()->getRepository(InstallPage::class)->findAll() as $variant){

            $variantData = [

                'title' => $variant->getTitle(),

                'icon' => $variant->getIcon(),

                'activeicon' => $variant->getActiveicon(),

                'image' => $variant->getImage(),

                'reqcaption' => $variant->getReqcaption(),

                'needcaption' => $variant->getNeedcaption(),

                'compcaption' => $variant->getCompcaption(),

                'stagecaption' => $variant->getStagecaption(),

                'reqs' => [],

                'needs' => [],

                'comps' => [],

                'stages' => [],

                'faq' => [],

                'docs' => []

            ];


            foreach($variant->getComps() as $item){

                $variantData['comps'][] = [

                    'title'=>$item->getTitle(),

                    'icon'=>$item->getIcon()

                ];

            }


            foreach($this->getDoctrine()->getRepository(InstallPageFAQ::class)->findAll() as $item){

                $variantData['faq'][] = [

                    'title'=>$item->getTitle(),

                    'text'=>$item->getText()

                ];

            }


            foreach($variant->getNeeds() as $item){

                $variantData['needs'][] = [

                    'title'=>$item->getTitle()

                ];

            }


            foreach($variant->getReqs() as $item){

                $variantData['reqs'][] = [

                    'title'=>$item->getTitle()

                ];

            }


            foreach($variant->getStages() as $item){

                $variantData['stages'][] = [

                    'title'=>$item->getTitle(),

                    'image'=>$item->getImage()

                ];

            }


            foreach($variant->getDocs() as $item){

                $section = [

                    'title'=>$item->getTitle(),

                    'videos'=>[],

                    'files'=>[]

                ];

                foreach($item->getYoutube() as $video){

                    $section['videos'][] = [

                        'title'=>$video->getTitle(),

                        'href'=>$video->getHref()

                    ];

                }

                foreach($item->getDoc() as $file){

                    $section['files'][] = [

                        'title'=>$file->getTitle(),

                        'name'=>$file->getFile(),

                        'size'=>$file->getSize(),

                        'type'=>$file->getType()

                    ];

                }

                $variantData['docs'][] = $section;

            }



            $variantData['design'] = [

                'title' => $variant->getDesigncaption(),

                'text' => $variant->getDesigntext(),

                'images'=>[]

            ];


            $layoutCollections = [];

            foreach($context->getRepository(CardCollection::class)->findBy([],['position'=>'ASC']) as $collection){

                if($collection->getClass()->getId()==(3-$variant->getId())) continue;

                $layout = $collection->getLayout();

                if(count($layout)>0) $layoutCollections[] = $collection;

            }

            if(count($layoutCollections)>0){

                $collection = $layoutCollections[Utils::RandomInteger(0,count($layoutCollections)-1)];

                $variantData['design']['collection'] = $collection->getTitle();

                $layout = $collection->getLayout();

                foreach($layout as $image) $variantData['design']['images'][] = '/collection/'.preg_replace('/\s+/','-',strtolower($collection->getTitle())).'/layout/'.$image->getImage();

                $count = count($layout);

                $f = floor($count/10); $s = $count%10;

                $variantData['design']['count'] = $count;

                $variantData['design']['unit'] = 'вариант'.($f==1?'ов':($s==1?'':($s>=2&&$s<=4?'а':'ов'))).' раскладки';

            }



            $pageData['variants'][] = $variantData;


        }



        return $this->render('index.html.twig',$pageData);

    }


    /**
     * @Route("/install/")
     */
    public function redirecting()
    {
        return $this->redirectToRoute('installpage');
    }
}

