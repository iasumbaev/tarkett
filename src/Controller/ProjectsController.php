<?php

namespace App\Controller;

use App\Repository\ProjectRepository;
use App\Repository\CardCollectionClassRepository;
use App\Repository\CardCollectionRepository;
use App\Repository\ContactsPageCommonsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ProjectsController extends AbstractController
{

    /**
     * @Route("/projects/{slug}", name="project")
     * @param ProjectRepository $projectRepository
     * @param CardCollectionClassRepository $cardCollectionClassRepository
     * @param CardCollectionRepository $cardCollectionRepository
     * @param ContactsPageCommonsRepository $contactsPageCommonsRepository
     * @param $slug
     * @return Response
     */
    public function project(ProjectRepository $projectRepository, CardCollectionClassRepository $cardCollectionClassRepository,
                            CardCollectionRepository $cardCollectionRepository,
                            ContactsPageCommonsRepository $contactsPageCommonsRepository, $slug)
    {

        $project = $projectRepository->findOneBy(['slug' => $slug]);
        if (!$project) {
            throw new NotFoundHttpException('Project not found!');
        }

        $pageData['collections'] = [];
        foreach ($cardCollectionClassRepository->findAll() as $class) {
            $classData = [
                'caption' => $class->getTitle(),
                'icon' => $class->getMenuicon(),
                'list' => []
            ];

            $collections = $cardCollectionRepository->findBy(['class' => $class->getId()], ['position' => 'ASC']);
            foreach ($collections as $collection) {
                $classData['list'][] = [
                    'title' => $collection->getTitle(),
                    'href' => '/collections/' . preg_replace('/\s+/', '-', strtolower($collection->getTitle())) . '/',
                    'collectionType' => ['regular', 'new'][$collection->getType() - 1]

                ];
            }
            $pageData['collections'][] = $classData;
        }

        $collections = $project->getCollections();
        $collectionsData = [];
        foreach ($collections as $collection) {
            $collectionsData[] = [
                'title' => $collection->getTitle(),
                'href' => '/collections/' . preg_replace('/\s+/', '-', strtolower($collection->getTitle())) . '/',
            ];
        }

        $contactsCommons = $contactsPageCommonsRepository->findAll();
        if (!$contactsCommons) {
            throw new NotFoundHttpException('Not found contacts data. Check your database.');
        }
        $contactsCommons[0]->getPhone();

        return $this->render('index.html.twig', [
            'metatitle' => $project->getMetaTitle(),
            'metakeys' => $project->getMetaKeywords(),
            'metadesc' => $project->getMetaDescription(),
            'type' => 'project',
            'collections' => $pageData['collections'],
            'collections_data' => $collectionsData,
            'phone' => $contactsCommons[0]->getPhone(),
            'currentYear' => date('Y'),
            'project' => $project,
        ]);
    }

    /**
     * @Route("/projects/project/upload_image")
     * @IsGranted("ROLE_ADMIN")
     */
    public function uploadImage()
    {
        if (isset($_FILES['upload'])) {

            $file = $_FILES['upload']['tmp_name'];
            $file_name = $_FILES['upload']['name'];
            $file_name_array = explode('.', $file_name);
            $extension = end($file_name_array);
            $new_image_name = time() . mt_rand() . '.' . $extension;

            move_uploaded_file($file, $this->getParameter('projects_directory') . $new_image_name);

            $function_number = $_GET['CKEditorFuncNum'];
            $url = '/' . $this->getParameter('projects_directory') . $new_image_name;
            $message = '';

            return new Response("<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '$url', '$message');</script>");

        }
        return new Response("<script type='text/javascript'>alert(1);</script>");
    }

}
