<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\ContactsPageCities;
use App\Entity\ContactsPageLocations;
use App\Entity\ContactsPageCommons;


class ContactsPageController extends Controller
 {
     /**
      * @Route("/contacts/")
      */
     public function route()
     {

		$pageData = [
			'type' => 'contacts',
		];


		$context = $this->getDoctrine();

		CommonsPageData::process($context,$pageData);



		$contactsCommonsRep = $context->getRepository(ContactsPageCommons::class);

		$contactsCommons = ($contactsCommonsRep->findAll())[0];


		$metatitle = $contactsCommons->getMetatitle();

		if(!empty($metatitle)) $pageData['metatitle'] = $metatitle;

		$metakeys = $contactsCommons->getMetakeys();

		if(!empty($metakeys)) $pageData['metakeys'] = $metakeys;

		$metadesc = $contactsCommons->getMetadesc();

		if(!empty($metadesc)) $pageData['metadesc'] = $metadesc;




        return $this->render('index.html.twig',$pageData);

     }


    /**
      * @Route("/contacts/cities")
      */
      public function locations()
     {

		$locations = [];

		$citiesRep = $this->getDoctrine()->getRepository(ContactsPageCities::class);

		$cities = $citiesRep->findAll();

		foreach($cities as $city){

			$cityData = [

				'title'=>$city->getTitle(),

				'locations'=>[],

			];

			foreach($city->getLocations() as $location){

				$cityData['locations'][] = [

					'title'=>$location->getTitle(),

					'address'=>$location->getAddress(),

					'phone'=>$location->getPhone(),

					'coords'=>[

						'lat'=>$location->getLat(),

						'lng'=>$location->getLng()

					]

				];

			}

			$locations[] = $cityData;

		}

        return new Response(
		    json_encode($locations),
		    Response::HTTP_OK,
		    ['content-type' => 'text/json;charset=utf-8']
		);

     }

    /**
      * @Route("/contacts/current")
      */
    public function currentCoords(Request $request){
    	
    	$IP = $request->headers->get('x-forwarded-for');
    	if(empty($IP)) $IP = $request->server->get('REMOTE_ADDR');
    	// $IP = '37.193.30.95';

		$data = null;

		try{

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, 'http://api.ipstack.com/'.$IP.'?access_key=bc7dac1becc3e78d0b2ddd15f0d11d4a');

			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-type'=>'application/json']);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


			$response = curl_exec($ch);

			$coords = json_decode($response);

			$data = [$coords->latitude,$coords->longitude];

		}catch(Exception $e){

			$data = null;

		}


    	return new Response(
		    json_encode($data),
		    Response::HTTP_OK,
		    ['content-type' => 'text/json;charset=utf-8']
		);

    }


}

?>