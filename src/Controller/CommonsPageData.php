<?php

namespace App\Controller;


use App\Entity\CardCollectionClass;
use App\Entity\CardCollection;

use App\Entity\ContactsPageCommons;
use App\Entity\StartPageCommons;

class CommonsPageData
 {

 	static public function process(&$context,&$pageData){

		$pageData['collections'] = []; 
 		
		$classRep = $context->getRepository(CardCollectionClass::class);

		$collectionRep = $context->getRepository(CardCollection::class);

		$collections = $collectionRep->findBy([],['position'=>'ASC']);

		foreach($classRep->findAll() as $class){

			$classData = [

				'icon' => $class->getMenuicon(),

				'caption' => $class->getTitle(),

				'list' => []

			];

			foreach($collections as $collection){

				if($collection->getClass()->getId()!=$class->getId()) continue;

				$classData['list'][] = [

					'title' => $collection->getTitle(),
					
					'href' => '/collections/'.preg_replace('/\s+/','-',strtolower($collection->getTitle())).'/',

					'collectionType'=>['regular','new'][$collection->getType()-1]

				];

			}

			$pageData['collections'][] = $classData;

		}
 		
		$contactsCommonsRep = $context->getRepository(ContactsPageCommons::class);

		$contactsCommons = $contactsCommonsRep->findAll();


		$pageData['phone'] = $contactsCommons[0]->getPhone();


 		
		$startCommonsRep = $context->getRepository(StartPageCommons::class);

		$startCommons = $startCommonsRep->findAll();


		$pageData['metatitle'] = $startCommons[0]->getMetatitle();
		$pageData['metakeys'] = $startCommons[0]->getMetakeys();
		$pageData['metadesc'] = $startCommons[0]->getMetadesc();

		$pageData['currentYear'] = date("Y");


 	}


}

?>