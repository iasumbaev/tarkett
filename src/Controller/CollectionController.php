<?php

namespace App\Controller;

use App\Entity\FAQItemGlue;
use App\Entity\FAQItemLock;
use App\Repository\ProjectRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\CardCollection;

class CollectionController extends Controller
{
    /**
     * @Route("/collections/{collectionname}/", name="collectionpage")
     */
    public function route(Request $request, $collectionname, ProjectRepository $projectRepository)
    {



        $pageData = [
            
            'type' => 'collection',

            'istest'=> $request->query->has('test')

        ];


        $context = $this->getDoctrine();

        CommonsPageData::process($context,$pageData);


        $collectionRep = $this->getDoctrine()->getRepository(CardCollection::class);

        $collection = null;

        foreach($collectionRep->findAll() as $candidate){

            $candidateName = preg_replace('/\s+/','-',strtolower($candidate->getTitle()));

            if($candidateName==$collectionname){
                $collection = $candidate;
                break;
            }

        }

        if(is_null($collection)) throw $this->createNotFoundException();



        $metatitle = $collection->getMetatitle();

        if(!empty($metatitle)) $pageData['metatitle'] = $metatitle;

        $metakeys = $collection->getMetakeys();

        if(!empty($metakeys)) $pageData['metakeys'] = $metakeys;

        $metadesc = $collection->getMetadesc();

        if(!empty($metadesc)) $pageData['metadesc'] = $metadesc;



        $pageData['collectionType'] = ['regular','new'][$collection->getType()-1];


        $pageData['title'] = $collection->getTitle();

        $pageData['image'] = $collection->getImage();

        $pageData['video'] = $collection->getVideo();

        $class = $collection->getClass();

        $pageData['class'] = [

            'title'=>$class->getTitle(),

            'icon'=>$class->getIcon()

        ];


        $targets = $collection->getTargets();

        $pageData['targets'] = [];

        foreach($targets as $target){

            $pageData['targets'][] = [

                'title'=>$target->getTarget()->getTitle(),

                'icon'=>$target->getTarget()->getIcon()

            ];

        }


        $props = $collection->getProps();

        $pageData['props'] = [];

        foreach($props as $prop){

            $pageData['props'][] = [

                'title'=>$prop->getProp()->getTitle(),

                'icon'=>$prop->getProp()->getIcon()

            ];

        }

        $pageData['texttop'] = $collection->getTexttop();
        $pageData['texttopRaw'] = $collection->getTexttopRaw();
        $pageData['textbottom'] = $collection->getTextbottom();
        $pageData['textbottomRaw'] = $collection->getTextbottomRaw();



        $interest = $collection->getInterest();

        $pageData['interest'] = [];

        foreach($interest as $item){

            $pageData['interest'][] = [

                'title'=>$item->getTitle(),

                'text'=>preg_replace('/\n/','<br/>',htmlspecialchars($item->getText())),

                'image'=>$item->getImage(),
                'svg' => $item->isSVG()

            ];

        }


        $interiors = $collection->getInteriors();

        $pageData['interiors'] = [];

        foreach($interiors as $interior){

            $pageData['interiors'][] = [

                'title'=>$interior->getTitle(),

                'image'=>$interior->getImage()

            ];

        }



        $pageData['href_image'] = '/collection/'.preg_replace('/\s+/','-',strtolower($collection->getTitle())).'/';
        $pageData['href'] = '/collections/'.preg_replace('/\s+/','-',strtolower($collection->getTitle())).'/';

        $pageData['cards'] = [];

        foreach ($collection->getCards() as $card){

            $pageData['cards'][] = [

                'title'=>$card->getTitle(),

                'image'=>$card->getImage(),

                'type'=>['tile','plate'][$card->getType()->getId()-1],

                'href'=>'/collections/'.$collectionname.'/'.preg_replace('/\s+/','-',strtolower($card->getTitle())).'/',
                'href_image'=>'/card/'.$collectionname.'/'.preg_replace('/\s+/','-',strtolower($card->getTitle())).'/'

            ];

        }


        $pageData['data'] = json_encode($pageData);

        if ($collection->getClass()->getTitle() === 'Клеевое соединение') {
            $pageData['faqItems'] = $this->getDoctrine()->getRepository(FAQItemGlue::class)->findAll();
        } elseif ($collection->getClass()->getTitle() === 'Замковое соединение') {
            $pageData['faqItems'] = $this->getDoctrine()->getRepository(FAQItemLock::class)->findAll();
        } else {
            $pageData['faqItems'] = null;
        }

        $pageData['projects'] = $projectRepository->findAll();

        return $this->render('index.html.twig',$pageData);

    }

    /**
     * @Route("/collection/{collectionname}/")
     */
    public function redirecting($collectionname) {

        return $this->redirectToRoute('collectionpage', ['collectionname' => $collectionname]);
    }

}
