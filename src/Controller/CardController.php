<?php

namespace App\Controller;

use App\Entity\FAQItemGlue;
use App\Entity\FAQItemLock;
use App\Repository\ProjectRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;

use App\Entity\CardCollection;

class CardController extends Controller
{
    /**
     * @Route("/collections/{collectionname}/{cardname}/")
     */
    public function route(Request $request, $collectionname,$cardname, ProjectRepository $projectRepository)
    {

        $pageData = [
            'type' => 'card',

            'istest'=> $request->query->has('test')
        ];


        $context = $this->getDoctrine();

        CommonsPageData::process($context,$pageData);


        $collectionRep = $this->getDoctrine()->getRepository(CardCollection::class);

        $collection = null;

        foreach($collectionRep->findAll() as $candidate){

            $candidateName = preg_replace('/\s+/','-',strtolower($candidate->getTitle()));

            if($candidateName==$collectionname){
                $collection = $candidate;
                break;
            }

        }


        if(is_null($collection)) throw $this->createNotFoundException();


        $cards = $collection->getCards();

        $card = null;

        foreach($cards as $candidate){

            $candidateName = preg_replace('/\s+/','-',strtolower($candidate->getTitle()));

            if($candidateName==$cardname){
                $card = $candidate;
                break;
            }

        }

        if(is_null($card)) throw $this->createNotFoundException();


        $pageData['class']=$card->getType()->getId();


        $pageData['factor']=[

            'width'=>$card->getFactorwidth(),

            'length'=>$card->getFactorlength(),

            'count'=>$card->getFactorcount(),

            'area'=>$card->getFactorarea()

        ];


        $metatitle = $card->getMetatitle();

        if(!empty($metatitle)) $pageData['metatitle'] = $metatitle;

        $metakeys = $card->getMetakeys();

        if(!empty($metakeys)) $pageData['metakeys'] = $metakeys;

        $metadesc = $card->getMetadesc();

        if(!empty($metadesc)) $pageData['metadesc'] = $metadesc;



        $pageData['texture'] = $card->getTexture();

        $pageData['textureExt'] = (!empty($pageData['texture'])?'.'.(pathinfo($pageData['texture']))['extension']:'');

        $pageData['model'] = $card->getModel();

        $pageData['modelExt'] = (!empty($pageData['model'])?'.'.(pathinfo($pageData['model']))['extension']:'');


        $pageData['title'] = $card->getTitle();

        $pageData['image'] = $card->getImage();

        $pageData['collectionTitle'] = $collection->getTitle();

        $pageData['collectionHref'] = $href = '/collections/'.preg_replace('/\s+/','-',strtolower($collection->getTitle())).'/';

        $pageData['others'] = [];

        foreach ($collection->getCards() as $item){

            $active = ($item->getId()==$card->getId());

            $href = '/collections/'.$collectionname.'/'.preg_replace('/\s+/','-',strtolower($item->getTitle())).'/';

            if($active){

                $pageData['href'] = $href;
                $pageData['href_image'] =  '/card/'.$collectionname.'/'.preg_replace('/\s+/','-',strtolower($item->getTitle())).'/';

            }

            $info = [
                'title' => $item->getTitle(),
                'image' => $item->getImage(),
                'active' => $active,
                'isPlate' => $item->getType()->getId()==1,
                'href'=>$href,
                'href_image'=>'/card/'.$collectionname.'/'.preg_replace('/\s+/','-',strtolower($item->getTitle())).'/'
            ];

            $pageData['others'][] = $info;

        }




        $pageData['props'] = [];

        foreach($collection->getProps() as $prop){

            $pageData['props'][] = [

                'title'=>$prop->getProp()->getTitle(),

                'icon'=>$prop->getProp()->getIcon()

            ];

        }



        $pageData['targets'] = [];

        foreach($collection->getTargets() as $target){

            $pageData['targets'][] = [

                'title'=>$target->getTarget()->getTitle(),

                'icon'=>$target->getTarget()->getIcon(),

                'gicon'=>$target->getTarget()->getGicon()

            ];

        }

        $pageData['images'] = [];
        $pageData['youtubeLinks'] = [];

        foreach($card->getImages() as $image){

            $pageData['images'][] = $image->getImage();
            $pageData['youtubeLinks'][] = $image->getVideo();

        }



        $pageData['chars'] = [];

        foreach($card->getValues() as $value){

            $pageData['chars'][] = [

                'title'=>$value->getChar()->getTitle(),

                'value'=>$value->getValue()

            ];

        }

        $pageData['youtube'] = [];

        foreach($collection->getYoutube() as $item){

            $pageData['youtube'][] = [

                'title'=>$item->getTitle(),

                'href'=>$item->getHref()

            ];

        }

        $pageData['mediaYoutube'] = [];

        foreach($collection->getMediayoutube() as $item){

            $pageData['mediaYoutube'][] = [

                'title'=>$item->getTitle(),

                'href'=>$item->getHref()

            ];

        }


        $pageData['docs'] = [];

        foreach($collection->getDocs() as $doc){

            $size = $doc->getSize();

            if($size>990*1024*1024){

                $size = round($size/1024/1024/1024*10);

                $frac = $size%10;

                $size = floor($size/10).($frac>0?'.'.$frac:'').' Гб';

            }else if($size>990*1024){

                $size = round($size/1024/1024*10);

                $frac = $size%10;

                $size = floor($size/10).($frac>0?'.'.$frac:'').' Мб';

            }else if($size>990){

                $size = round($size/1024*10);

                $frac = $size%10;

                $size = floor($size/10).($frac>0?'.'.$frac:'').' Кб';

            }else $size = $size.' б';

            $pageData['docs'][] = [

                'title'=>$doc->getTitle(),

                'href'=>'/collection/'.$collectionname.'/docs/'.$doc->getFile(),

                'size'=>$size,

                'type'=>$doc->getType()

            ];

        }

        $pageData['mediaDocs'] = [];

        foreach($collection->getMediadocs() as $doc){

            $size = $doc->getSize();

            if($size>990*1024*1024){

                $size = round($size/1024/1024/1024*10);

                $frac = $size%10;

                $size = floor($size/10).($frac>0?'.'.$frac:'').' Гб';

            }else if($size>990*1024){

                $size = round($size/1024/1024*10);

                $frac = $size%10;

                $size = floor($size/10).($frac>0?'.'.$frac:'').' Мб';

            }else if($size>990){

                $size = round($size/1024*10);

                $frac = $size%10;

                $size = floor($size/10).($frac>0?'.'.$frac:'').' Кб';

            }else $size = $size.' б';

            $pageData['mediaDocs'][] = [

                'title'=>$doc->getTitle(),

                'href'=>'/collections/'.$collectionname.'/docs/'.$doc->getFile(),

                'size'=>$size,

                'type'=>$doc->getType()

            ];

        }

        if ($collection->getClass()->getTitle() === 'Клеевое соединение') {
            $pageData['faqItems'] = $this->getDoctrine()->getRepository(FAQItemGlue::class)->findAll();
        } elseif ($collection->getClass()->getTitle() === 'Замковое соединение') {
            $pageData['faqItems'] = $this->getDoctrine()->getRepository(FAQItemLock::class)->findAll();
        } else {
            $pageData['faqItems'] = null;
        }

        // $pageData['data'] = json_encode($pageData);

        $pageData['projects'] = $projectRepository->findAll();

        $interiors = $collection->getInteriors();

        $pageData['interiors'] = [];

        foreach($interiors as $interior){

            $pageData['interiors'][] = [

                'title'=>$interior->getTitle(),

                'image'=>$interior->getImage()

            ];

        }
        $pageData['interiors_href_image'] = '/collection/'.preg_replace('/\s+/','-',strtolower($collection->getTitle())).'/';

        return $this->render('index.html.twig',$pageData);

    }

    /**
     * @Route("/card/{collectionname}/{cardname}/")
     */
    public function redirecting($collectionname, $cardname) {

        return $this->redirectToRoute('cardpage', ['collectionname' => $collectionname, 'cardname' => $cardname]);
    }

}