<?php

namespace App\Controller;

use App\Repository\ArticleCategoryRepository;
use App\Repository\ArticleRepository;
use App\Repository\BlogPageRepository;
use App\Repository\CardCollectionClassRepository;
use App\Repository\CardCollectionRepository;
use App\Repository\ContactsPageCommonsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     * @param BlogPageRepository $blogPageRepository
     * @param CardCollectionClassRepository $cardCollectionClassRepository
     * @param CardCollectionRepository $cardCollectionRepository
     * @param ContactsPageCommonsRepository $contactsPageCommonsRepository
     * @param ArticleCategoryRepository $articleCategoryRepository
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function blog(BlogPageRepository $blogPageRepository,
                         CardCollectionClassRepository $cardCollectionClassRepository,
                         CardCollectionRepository $cardCollectionRepository,
                         ContactsPageCommonsRepository $contactsPageCommonsRepository,
                         ArticleCategoryRepository $articleCategoryRepository,
                         ArticleRepository $articleRepository)
    {

        $page = $blogPageRepository->find(1);

        if (!$page) {
            throw new NotFoundHttpException('Not found blog page data. Check your database.');
        }

        $pageData['collections'] = [];
        foreach ($cardCollectionClassRepository->findAll() as $class) {
            $classData = [
                'caption' => $class->getTitle(),
                'icon' => $class->getMenuicon(),
                'list' => []
            ];

            $collections = $cardCollectionRepository->findBy(['class' => $class->getId()], ['position' => 'ASC']);
            foreach ($collections as $collection) {
                $classData['list'][] = [
                    'title' => $collection->getTitle(),
                    'href' => '/collections/' . preg_replace('/\s+/', '-', strtolower($collection->getTitle())) . '/',
                    'collectionType' => ['regular', 'new'][$collection->getType() - 1]

                ];
            }
            $pageData['collections'][] = $classData;
        }

        $contactsCommons = $contactsPageCommonsRepository->findAll();
        if (!$contactsCommons) {
            throw new NotFoundHttpException('Not found contacts data. Check your database.');
        }
        $contactsCommons[0]->getPhone();


        return $this->render('index.html.twig', [
            'metatitle' => $page->getMetaTitle(),
            'metakeys' => $page->getMetaKeywords(),
            'metadesc' => $page->getMetaDescription(),
            'type' => 'blog',
            'collections' => $pageData['collections'],
            'phone' => $contactsCommons[0]->getPhone(),
            'currentYear' => date('Y'),
            'categories' => $articleCategoryRepository->findAll(),
            'page' => $page,
            'firstArticle' => $articleRepository->findOneBy([], ['id' => 'ASC']),
            'articles' => $articleRepository->findBy([], ['id' => 'ASC'], null, 1)
        ]);
    }


    /**
     * @Route("/blog/{slug}", name="article")
     * @param ArticleRepository $articleRepository
     * @param CardCollectionClassRepository $cardCollectionClassRepository
     * @param CardCollectionRepository $cardCollectionRepository
     * @param ContactsPageCommonsRepository $contactsPageCommonsRepository
     * @param $slug
     * @return Response
     */
    public function article(ArticleRepository $articleRepository, CardCollectionClassRepository $cardCollectionClassRepository,
                            CardCollectionRepository $cardCollectionRepository,
                            ContactsPageCommonsRepository $contactsPageCommonsRepository, $slug)
    {

        $article = $articleRepository->findOneBy(['slug' => $slug]);
        if (!$article) {
            throw new NotFoundHttpException('Article not found!');
        }

        $pageData['collections'] = [];
        foreach ($cardCollectionClassRepository->findAll() as $class) {
            $classData = [
                'caption' => $class->getTitle(),
                'icon' => $class->getMenuicon(),
                'list' => []
            ];

            $collections = $cardCollectionRepository->findBy(['class' => $class->getId()], ['position' => 'ASC']);
            foreach ($collections as $collection) {
                $classData['list'][] = [
                    'title' => $collection->getTitle(),
                    'href' => '/collections/' . preg_replace('/\s+/', '-', strtolower($collection->getTitle())) . '/',
                    'collectionType' => ['regular', 'new'][$collection->getType() - 1]

                ];
            }
            $pageData['collections'][] = $classData;
        }

        $contactsCommons = $contactsPageCommonsRepository->findAll();
        if (!$contactsCommons) {
            throw new NotFoundHttpException('Not found contacts data. Check your database.');
        }
        $contactsCommons[0]->getPhone();

        $previouslyArticle =  $articleRepository->getPreviouslyArticle($article->getId());
        $nextArticle =  $articleRepository->getNextArticle($article->getId());

        $articles = $articleRepository->getArticlesSameCategory($article->getId(), $article->getArticleCategory()->getId());

        return $this->render('index.html.twig', [
            'metatitle' => $article->getMetaTitle(),
            'metakeys' => $article->getMetaKeywords(),
            'metadesc' => $article->getMetaDescription(),
            'type' => 'article',
            'collections' => $pageData['collections'],
            'phone' => $contactsCommons[0]->getPhone(),
            'currentYear' => date('Y'),
            'article' => $article,
            'prevArticle' => $previouslyArticle,
            'nextArticle' => $nextArticle,
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/blog/api/get_articles_by_category/{id}", name="get_articles")
     * @param $id
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function getArticlesByCategory($id, ArticleRepository $articleRepository): Response
    {
        $articlesOnPage = 6;

        if ($id === '-1') {
            return $this->render('_articles.html.twig', [
                'firstArticle' => $articleRepository->findOneBy([], ['id' => 'DESC']),
                'articles' => $articleRepository->findBy([], ['id' => 'DESC'], null, 1)
            ]);
        }

        $articleCategory = $articleRepository->find($id);
        if ($articleCategory) {
            return $this->render('_articles.html.twig', [
                'firstArticle' => $articleRepository->findOneBy(['articleCategory' => $articleCategory], ['id' => 'ASC']),
                'articles' => $articleRepository->findBy(['articleCategory' => $articleCategory], ['id' => 'ASC'], null, 1)
            ]);
        }

        throw new NotFoundHttpException('Article category not found!');
    }

    /**
     * @Route("/blog/api/get_articles_with_offset", name="get_articles_with_offset", methods={"POST"})
     * @param ArticleRepository $articleRepository
     * @param Request $request
     * @return Response
     */
    public function getArticlesByCategoryWithOffset(ArticleRepository $articleRepository, Request $request): Response
    {
        $id = $request->request->get('id');
        $offset = $request->request->get('offset') ?: 0;
        $articlesOnPage = 6;

        $articles = $articleRepository->findBy([], ['id' => 'ASC'], $articlesOnPage, 1 + $articlesOnPage * $offset);
        if (!$articles) {
            return new Response(null, 204);
        }

        if ($id === '-1') {
            return $this->render('_articles_list.html.twig', [
                'articles' => $articles
            ]);
        }

        $articleCategory = $articleRepository->find($id);

        if (!$articles) {
            return new Response(null, 204);
        }

        $articles = $articleRepository->findBy(['articleCategory' => $articleCategory], ['id' => 'ASC'], $articlesOnPage, 1 + $articlesOnPage * $offset);

        if ($articleCategory) {
            return $this->render('_articles_list.html.twig', [
                'articles' => $articles
            ]);
        }
        throw new NotFoundHttpException('Article category not found!');
    }


    /**
     * @Route("/blog/article/upload_image")
     * @IsGranted("ROLE_ADMIN")
     */
    public function uploadImage()
    {
        if (isset($_FILES['upload'])) {

            $file = $_FILES['upload']['tmp_name'];
            $file_name = $_FILES['upload']['name'];
            $file_name_array = explode('.', $file_name);
            $extension = end($file_name_array);
            $new_image_name = time() . mt_rand() . '.' . $extension;
            move_uploaded_file($file, $this->getParameter('article_directory') . $new_image_name);

            $function_number = $_GET['CKEditorFuncNum'];
            $url = '/' . $this->getParameter('article_directory') . $new_image_name;
            $message = '';

            return new Response("<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '$url', '$message');</script>");

        }
        return new Response("<script type='text/javascript'>alert(1);</script>");
    }

//    /**
//     * @Route("/blog/article/new", name="article_new")
//     * @param Article $article
//     * @param CardCollectionClassRepository $cardCollectionClassRepository
//     * @param CardCollectionRepository $cardCollectionRepository
//     * @param ContactsPageCommonsRepository $contactsPageCommonsRepository
//     * @return Response
//     */
//    public function new(EntityManagerInterface $entityManager)
//    {
//        $article = new Article();
//        $article->setTitle('Как выбрать ламинат');
//        $entityManager->persist($article);
//        $entityManager->flush();
//        return new Response(123);
//    }
}
