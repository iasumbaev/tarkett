<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DesignerPageController extends Controller
 {
     /**
      * @Route("/designer/")
      */
     public function route()
     {

		$pageData = [
			'type' => 'designer',
			'slider' => []
		];


		$context = $this->getDoctrine();

		CommonsPageData::process($context,$pageData);



        return $this->render('index.html.twig',$pageData);

     }

}

?>