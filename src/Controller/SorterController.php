<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Controller\CRUDController;

use App\Entity\CardCollection;


class SorterController extends CRUDController
{

    /**
     * Move element
     *
     * @param string $id
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function moveprevAction()
    {

        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException();
        }

        
        $counter = 0;

        $classpath = explode('\\',get_class($object));
        $classname = end($classpath);

        $list = $this->getDoctrine()->getRepository('App:'.$classname)->findBy([],['position'=>'ASC']);

        $prev = null;

        $hasparent = $object->sorter__hasparent();

        $currentparent = null;

        if($hasparent){
            $currentparent = $object->sorter__getparent();
        }
        
        foreach($list as $item){

            if($hasparent){

                if($item->sorter__getparent()!=$currentparent) continue;

            }

            $counter++;

            $position = $counter;

            if($item->getId()==$object->getId()){

                if($position>1){

                    $prev->setPosition($position);

                    $position--;

                }

            }

            if($item->getPosition()!==$position){

                $item->setPosition($position);

                $this->admin->update($item);

            }

            $prev = $item;

        }

        // return new RedirectResponse($this->admin->generateUrl('list',['filter' => $this->admin->getFilterParameters()]));
        return new RedirectResponse($this->getRequest()->headers->get('referer'));

    }

    /**
     * Move element
     *
     * @param string $parent
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function movenextAction($parent = '')
    {

        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException();
        }

        
        $classpath = explode('\\',get_class($object));
        $classname = end($classpath);

        $list = $this->getDoctrine()->getRepository('App:'.$classname)->findBy([],['position'=>'DESC']);

        $count = 0;

        $hasparent = $object->sorter__hasparent();

        $currentparent = null;

        if($hasparent){
            $currentparent = $object->sorter__getparent();
        }
        
        foreach($list as $item){

            if($hasparent){

                if($item->sorter__getparent()!=$currentparent) continue;

            }

            $count++;

        }

        $counter = $count+1;

        $next = null;
        
        foreach($list as $item){

            if($hasparent){

                if($item->sorter__getparent()!=$currentparent) continue;

            }

            $counter--;

            $position = $counter;

            if($item->getId()==$object->getId()){

                if($position<$count){

                    $next->setPosition($position);

                    $position++;

                }

            }

            if($item->getPosition()!==$position){

                $item->setPosition($position);

                $this->admin->update($item);

            }

            $next = $item;

        }

        // return new RedirectResponse($this->admin->generateUrl('list',['filter' => $this->admin->getFilterParameters()]));
        return new RedirectResponse($this->getRequest()->headers->get('referer'));

    }


}


?>