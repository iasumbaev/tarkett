(function (window) {
    "use strict";
    var Engine = window.Engine, Codevia = window.Codevia;
    var let_string = Codevia.let_string, clean_spaces = Codevia.clean_spaces;

    const $body = $('body:first');
    if (!$body.hasClass('article-page')) return;
    (() => {
        let els = $('.section-text .contents>*').toArray().map(el => $(el));
        const swipers = [];
        let swiper = null;
        for (const $el of els) {
            if ($el.is('figure') && $el.children().filter('img').length == 1) {
                if (swiper) {
                    swiper.push($el)
                } else {
                    swiper = [$el]
                }
            } else if ($el.is('video,table,figure')) {
                if (swiper && swiper.length > 1) {
                    swipers.push(swiper)
                }
                swiper = null
            } else {
                if ($el.text().replace(/[ \s\n\t]+/g, '')) {
                    if (swiper && swiper.length > 1) {
                        swipers.push(swiper)
                    }
                    swiper = null
                }
            }
        }
        if (swiper && swiper.length > 1) {
            swipers.push(swiper);
            swiper = null
        }
        for (const images of swipers) {
            const
                first = images[0][0], last = images[images.length - 1][0];
            const slice = [];
            for (const el of els) {
                if (slice.length == 0) {
                    if (!first.compareDocumentPosition(el[0])) slice.push(el)
                } else {
                    slice.push(el);
                    if (!last.compareDocumentPosition(el[0])) break
                }
            }
            for (const el of slice.slice(1)) $(el).detach();
            const $swiper = $(`<div class="slider"><div class="nav"><div class="prev disabled"></div><div class="next"></div></div><div class="slides swiper-container"><div class="swiper-wrapper"></div></div><div class="nav-v2"><div class="active"></div>${'<div></div>'.repeat(images.length - 1)}</div></div>`),
                $slides = $swiper.find('.swiper-wrapper');
            for (const img of images) {
                $slides.append($(`<div class="swiper-slide"><div class="lazyload" data-bg="/media/cache/resolve/article_image${$(img).find('img:first').attr('data-src')}"></div></div>`))
            }
            $(first).replaceWith($swiper);
            const swiper = new Swiper($swiper.find('.swiper-container:first')[0], {
                on: {
                    slideChange() {
                        $swiper.find('.nav-v2:first').children().eq(this.realIndex).addClass('active').siblings().removeClass('active');
                        if (this.isBeginning) {
                            $swiper.find('.nav:first').find('.prev:first').addClass('disabled')
                        } else {
                            $swiper.find('.nav:first').find('.prev:first').removeClass('disabled')
                        }
                        if (this.isEnd) {
                            $swiper.find('.nav:first').find('.next:first').addClass('disabled')
                        } else {
                            $swiper.find('.nav:first').find('.next:first').removeClass('disabled')
                        }
                    }
                }
            });
            $swiper.find('.nav:first').find('.prev:first').click(e => {
                swiper.slidePrev()
            });
            $swiper.find('.nav:first').find('.next:first').click(e => {
                swiper.slideNext()
            });
            $swiper.find('.nav-v2:first').children().click(function () {
                swiper.slideTo($(this).prevAll().length)
            })
        }
        els = $('.section-text .contents>*').toArray().map(el => $(el));
        let style = '';
        for (const $el of els) {
            if ($el.is('figure') && $el.children().filter('img').length == 1) {
                const
                    img = $el.find('img:first').attr('data-src'), alt = $el.find('img:first').attr('alt'),
                    caption = clean_spaces(let_string($el.find('figcaption:first').html()));
                $el.attr('class', 'image').empty().append($('<img />').attr('data-src', '/media/cache/resolve/article_image' + img).attr('alt', alt).attr('class', 'lazyload'));
                if (caption) $el.append($('<figcaption></figcaption>').html(caption))
            } else if ($el.is('video')) {
                const $video = $('<div class="video"></video>').append($el[0].outerHTML);
                $el.replaceWith($video);
                const video = $video.find('video:first')[0];
                video.setAttribute('preload', 'metadata');
                video.setAttribute('muted', '');
                video.setAttribute('width', '100%');
                video.setAttribute('height', '100%');
                $video.click(function () {
                    if (video.paused) video.play(); else video.pause()
                });
                $(video).on('pause', () => {
                    $video.removeClass('played')
                }).on('play', () => {
                    $video.addClass('played')
                })
            } else if ($el.is('p') && $el.children().filter('video').length == 1) {
                const $video = $('<div class="video"></video>').append($el[0].outerHTML);
                $el.replaceWith($video);
                const video = $video.find('video:first')[0];
                video.setAttribute('preload', 'metadata');
                video.setAttribute('muted', '');
                video.setAttribute('width', '100%');
                video.setAttribute('height', '100%');
                $video.click(function () {
                    if (video.paused) video.play(); else video.pause()
                });
                $(video).on('pause', () => {
                    $video.removeClass('played')
                }).on('play', () => {
                    $video.addClass('played')
                })
            } else if ($el.is('table')) {
                const header = $el.find('thead:first').find('th').toArray().map(el => clean_spaces(let_string($(el).text())));
                const widths = $el.find('thead:first').find('th').toArray().map(el => {
                    const value = (el.style && el.style.width ? let_string(el.style.width) : '');
                    if (!value) return 1;
                    const num = parseFloat(value.replace(/^\D+/, ''));
                    if (!isFinite(num) || num < 1e-6) return 1;
                    if (value.indexOf('%') >= 0) return num / 100;
                    return num / 870
                });
                const maxWidth = widths.reduce((max, width) => Math.max(max, width), -Infinity);
                const id = `_${Math.round(Math.random() * 1e9)}${Date.now()}${Math.round(Math.random() * 1e9)}`;
                style += `@media (min-width: 601px){ ${widths.map((width, index) => `#${id}.header>*:nth-child(${index + 1}),#${id}.rows.row>*:nth-child(${index + 1}){flex-grow:${(width / maxWidth).toFixed(3).replace(/0+$/, '').replace(/\.$/, '')}}`).join(' ')} } `;
                const $table = $(`<div id="${id}" class="table"></div>`),
                    $thead = $('<div class="header"></div>').appendTo($table),
                    $rows = $('<div class="rows"></div>').appendTo($table);
                for (const caption of header) $thead.append($('<div></div>').text(caption));
                let lastFirstCell = '', lastFirstCellOrigin = '';
                for (const row_el of $el.find('tbody:first tr')) {
                    const cells = $(row_el).children().filter('td').toArray().map(el => clean_spaces(let_string($(el).text()))).slice(0, header.length);
                    if (cells.length == 0) continue;
                    const $row = $('<div class="row"></div>').appendTo($rows);
                    while (cells.length < header.let_string) cells.unshift('');
                    const firstCell = cells[0].toLowerCase().replace(/[\s\n \t]+/g, '');
                    if (lastFirstCell == firstCell || !firstCell) {
                        cells[0] = ''
                    } else if (lastFirstCell != firstCell) {
                        lastFirstCell = firstCell;
                        lastFirstCellOrigin = cells[0];
                        if ($row.prevAll().length != 0) $row.before('<div class="separator"></div>')
                    }
                    const hideFirst = !cells[0] && lastFirstCell;
                    if (hideFirst) cells[0] = lastFirstCellOrigin;
                    for (let index = 0; index < header.length; index++) {
                        $row.append($('<div></div>').append($('<div class="title"></div>').text(header[index])).append($('<div class="value"></div>').text(cells[index])))
                    }
                    if (hideFirst) $row.children().first().find('.value').addClass('hidden')
                }
                $el.replaceWith($table)
            }
        }
        $('<style type="text/css"></style>').appendTo('head:first').text(style);
    })();

    $(".contents img").each(function () {
        $(this).attr('data-src', $(this).attr('src')).removeAttr('src');
    });


})(window)