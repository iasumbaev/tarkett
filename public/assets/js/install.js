(function (window) {
    "use strict";
    var Engine = window.Engine, Codevia = window.Codevia;
    var onWndResize = Codevia.onWndResize;

    const $body = $('body:first');
    if (!$body.hasClass('install-page')) return;
    (() => {
        $('.install-page .section-top .nav>*').click(function () {
            if ($(this).hasClass('active')) return;
            $('.install-page .section-top .nav>*').toggleClass('active');
            $('.install-page .section-top>.section-inner .image').toggleClass('hidden');
            $('.page-variant').toggleClass('hidden');
            Codevia.onScreenScroll()
        })
    })();
    $('.page-variant').each(function (variantIndex) {
        const $variant = $(this);
        (() => {
            const $style = $('<style type="text/css"></style>').appendTo('head:first');

            function resize() {
                $style.text($variant.find('.section-manual .list>*').toArray().map(function (item) {
                    const $item = $(item);
                    let id = $item.attr('id');
                    if (!id) $item.attr('id', id = '_' + Math.round(Math.random() * 1e9) + Date.now() + Math.round(Math.random() * 1e9));
                    return `.install-page .section-manual .list>*.active#${id} .text{height:${$item.find('.text:first>*').outerHeight()}px;}`
                }).join('\n\n'));
            }

            onWndResize(resize);
            resize();
            $variant.find('.section-manual .list .caption').click(function () {
                $(this).parent().toggleClass('active')
            })
        })();
        (() => {
            const $style = $('<style type="text/css"></style>').appendTo('head:first');

            function resize() {
                $style.text($variant.find('.section-faq .list>*').toArray().map(function (item) {
                    const $item = $(item);
                    let id = $item.attr('id');
                    if (!id) $item.attr('id', id = '_' + Math.round(Math.random() * 1e9) + Date.now() + Math.round(Math.random() * 1e9));
                    return `.install-page .section-faq .list>*.active#${id} .text{height:${$item.find('.text:first>*').outerHeight()}px;}`
                }).join('\n\n'))
            }

            onWndResize(resize);
            resize();
            $variant.find('.section-faq .list .caption').click(function () {
                $(this).parent().toggleClass('active')
            })
        })();


        (() => {

            const $window = $(window);

            const swiper = new Swiper($variant.find('.section-variants .images .slider')[0], {

                slidesPerView: 'auto', spaceBetween: 45, freeMode: !0, on: {

                    slideChangeTransitionStart() {

                        if (!swiper) return;

                        if (swiper.isBeginning) {

                            $variant.find('.section-variants .images .slide-to.prev').addClass('disabled')

                        } else {

                            $variant.find('.section-variants .images .slide-to.prev').removeClass('disabled')

                        }

                        if (swiper.isEnd) {

                            $variant.find('.section-variants .images .slide-to.next').addClass('disabled')

                        } else {

                            $variant.find('.section-variants .images .slide-to.next').removeClass('disabled')

                        }

                    }

                }

            });

            $variant.find('.section-variants .slide-to.prev').click(function () {

                if (!swiper) return;

                swiper.slidePrev()

            });

            $variant.find('.section-variants .slide-to.next').click(function () {

                if (!swiper) return;

                swiper.slideNext()

            });

            const
                $swiper = $variant.find('.section-variants .images .slider'),
                $swiperWrapper = $swiper.children('.swiper-wrapper');

            function resize(){

                if(

                    $variant.find('.section-variants .images').outerWidth()>=

                    ($(swiper.slides[swiper.slides.length-1]).offset().left + $(swiper.slides[swiper.slides.length-1]).outerWidth() - $(swiper.slides[0]).offset().left)

                ){

                    $variant.find('.section-variants .slide-to').addClass('hidden');

                    swiper.detachEvents();

                }else{
                    
                    $variant.find('.section-variants .slide-to').removeClass('hidden');
                
                    swiper.attachEvents();

                }

                swiper.update();

            }

            Codevia.onValueChange('tarkett-install-page-section-variants-slider-contents-resize-'+variantIndex,()=>{

                return [$window.outerWidth(),$swiper.outerWidth(),$swiperWrapper.outerWidth()];

            })(resize);

            resize();

            $('.install-page .section-top .nav>*').click(function () {
                swiper.update();
                resize();
            });

        })();



    });


})(window)