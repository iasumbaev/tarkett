(function (window) {
    "use strict";


    /*************************************************************************


     P   R   E   P   A   R   I   N   G       S   C   R   I   P   T


     *************************************************************************/


    /*************************************************************************

     B A S I C S

     *************************************************************************/


    var Engine = window.Engine,
        Codevia = window.Codevia,
        $document = $(window.document),
        $window = $(window),
        query = Codevia.queryServer;


    var onWndResize = Codevia.onWndResize,
        onScreenScroll = Codevia.onScreenScroll;


    var requestAnimationFrame = Codevia.requestAnimationFrame;


    var scrollTop = Codevia.scrollTop;


    var animate = Codevia.animate;


    var absolute_offset = Codevia.absoluteOffset;


    var afterEvents = Codevia.afterEvents;


    var let_string = Codevia.let_string,
        clean_spaces = Codevia.clean_spaces;



    const $body = $('body:first');

    if (!$body.hasClass('card-page')) return;


    /*************************************************************************

     S L I D E R

     *************************************************************************/


    (() => {


        const initialSlide = $('.card-page .section-nav .list .slider .swiper-slide.active').prevAll().length;


        const swiper = new Swiper($('.card-page .section-nav .list .slider')[0], {

            spaceBetween: 54,

            slidesPerView: 6,

            initialSlide,

            on: {

                slideChange(...p) {

                    if (this.isBeginning) {

                        $('.card-page .section-nav .list .slide-to.prev').addClass('hidden');

                    } else {

                        $('.card-page .section-nav .list .slide-to.prev').removeClass('hidden');

                    }

                    if (this.isEnd) {

                        $('.card-page .section-nav .list .slide-to.next').addClass('hidden');

                    } else {

                        $('.card-page .section-nav .list .slide-to.next').removeClass('hidden');

                    }

                }

            },

            breakpoints: {

                1280: {

                    spaceBetween: 25,

                    slidesPerView: 6

                },

                980: {

                    spaceBetween: 35,

                    slidesPerView: 'auto'

                }

            }

        });


        $('.card-page .section-nav .list .slider .swiper-slide').click(function () {

            $(this).addClass('active').siblings().removeClass('active');

        });


        $('.card-page .section-nav .list .slide-to.prev').click(function () {

            if (!swiper) return;

            swiper.slidePrev();

        });


        $('.card-page .section-nav .list .slide-to.next').click(function () {

            if (!swiper) return;

            swiper.slideNext();

        });


    })();

    /*************************************************************************

     F A Q

     *************************************************************************/


    (() => {


        const $style = $('<style type="text/css"></style>').appendTo('head:first');


        function resize() {

            $style.text($('.card-page .section-faq .list>*').toArray().map(function (item) {

                const $item = $(item);

                let id = $item.attr('id');

                if (!id) $item.attr('id', id = '_' + Math.round(Math.random() * 1e9) + Date.now() + Math.round(Math.random() * 1e9));

                return `.card-page .section-faq .list>*.active#${id} .text{height:${$item.find('.text:first>*').outerHeight()}px;}`

            }).join('\n\n'));

            // console.log($style[0])

        }


        onWndResize(resize);

        resize();


        $('.card-page .section-faq .list .caption').click(function () {

            $(this).parent().toggleClass('active');

        });


    })();

    /*************************************************************************

     I N F O

     *************************************************************************/


    (() => {


        let
            swiper = null,
            navSwiper = null;


        swiper = new Swiper($('.card-page .section-about .right .slider')[0], {

            on: {

                slideChange() {

                    if (!swiper) return;

                    if (!navSwiper) return;

                    navSwiper.slideTo(swiper.realIndex);

                    $('.card-page .section-about .right .nav .swiper-slide').eq(swiper.realIndex).addClass('active').siblings().removeClass('active');

                }

            }

        });


        navSwiper = new Swiper($('.card-page .section-about .right .nav>*')[0], {

            slidesPerView: 'auto',

            spaceBetween: 20,

            freeMode: true,

            breakpoints: {

                600: {

                    spaceBetween: 8

                }

            }

        });


        $('.card-page .section-about .right .nav .swiper-slide').click(function () {

            if (!swiper) return;

            $(this).addClass('active').siblings().removeClass('active');

            swiper.slideTo($(this).prevAll().length);

        });


    })();



    
    if($('.card-page .section-projects .list .slides').length) (()=>{


        const $window = $(window);


        let swiper = null;


        function resize(){

            const ww = $window.outerWidth();

            if(swiper) swiper.destroy();

            swiper = new Swiper($('.card-page .section-projects .list .slides')[0],{

                slidesPerView: (ww>980?3:ww>600?2:1),

                spaceBetween: (ww>1280?40:24),

                loop: true,

            });

        }


        resize();


        Codevia.onValueChange('section-projects-resized',()=>{

            const ww = $window.outerWidth();

            return [ww<=600,ww<=980,ww<=1280];

        })(resize);


        $('.card-page .section-projects .list .slide-to .prev').click(e=>{

            swiper.slidePrev();

        });

        $('.card-page .section-projects .list .slide-to .next').click(e=>{

            swiper.slideNext();

        });


    })();



    /************************************************************************/



})(window);