(function (window) {
    "use strict";


    /*************************************************************************


     P   R   E   P   A   R   I   N   G       S   C   R   I   P   T


     *************************************************************************/


    /*************************************************************************

     B A S I C S

     *************************************************************************/


    var Engine = window.Engine,
        Codevia = window.Codevia,
        $document = $(window.document),
        $window = $(window),
        query = Codevia.queryServer;


    var onWndResize = Codevia.onWndResize,
        onScreenScroll = Codevia.onScreenScroll;


    var requestAnimationFrame = Codevia.requestAnimationFrame;


    var scrollTop = Codevia.scrollTop;


    var animate = Codevia.animate;


    var absolute_offset = Codevia.absoluteOffset;


    var afterEvents = Codevia.afterEvents;


    var let_string = Codevia.let_string,
        clean_spaces = Codevia.clean_spaces;


    const $body = $('body:first');

    if (!$body.hasClass('blog-page')) return;


    /*************************************************************************

     C O L L E C T I O N S

     *************************************************************************/

    $('.js-change-category').click(function () {
        $.ajax({
                url: $(this).data('url'),
                success: function (data) {
                    $('.js-articles').empty().append(data);
                }
            }
        );
        $('.js-change-category').removeClass('current');
        $(this).addClass('current');
        $('.js-load-more-articles').css('display', 'block');
    });

    $('.js-load-more-articles').click(function () {
        $.ajax({
            url: $(this).data('url'),
            method: 'POST',
            data: {
                id: $('.js-change-category.current').data('id'),
                offset: $(this).attr('data-offset')
            },
            success: function (data) {
                if(!data) {
                    $('.js-load-more-articles').css('display', 'none');
                    return;
                }
                $('.js-articles .list').append(data);
                $('.js-load-more-articles').attr('data-offset', parseInt($('.js-load-more-articles').attr('data-offset')) + 1)
            }

        });
    });

    
    /************************************************************************/



})(window);