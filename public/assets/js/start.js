(function (window) {
    "use strict";
    var Engine = window.Engine, Codevia = window.Codevia;
    var onWndResize = Codevia.onWndResize;

    const $body = $('body:first');
    if (!$body.hasClass('start-page')) return;
    (() => {
        $('.start-page .section-collections .section-inner>*>*').each(function (index) {
            const
                $this = $(this), initialInner = $(this).html(), id = '_' + Math.round(Math.random() * 1e9);
            $this.attr('id', id);
            const $range = [$this.children().first(), $this.children().last()];
            for (let index = 0; index < 4; index++) $this.append(initialInner);
            const $style = $('<style type="text/css"></style>').appendTo('head:first');

            function resize() {
                const w = ($range[1].offset().left - $range[0].offset().left + $range[1].outerWidth()) * 2;
                $style.text(`
		@keyframes ${id}-anim{
			from {
				transform: translate(0,0);
			}
			to {
				transform: translate(${index == 2 ? '' : '-'}${w}px,0);
			}
		}
		#${id}{
			animation: ${id}-anim ${7 * (index == 1 ? 100 : 40)}s linear infinite;
		}
	`)
            }

            onWndResize(resize);
            resize()
        })
    })();
    $('.start-page .section-collections .section-inner>*>*>*').hover(function () {
        $(this).parent().css({
            '-webkit-animation-play-state': 'paused',
            '-moz-animation-play-state': 'paused',
            '-ms-animation-play-state': 'paused',
            'animation-play-state': 'paused',
        })
    }, function () {
        $(this).parent().removeAttr('style')
    });
    (() => {
        const swiper = new Swiper($('.start-page .section-about .image .slider')[0], {
            direction: 'vertical', breakpoints: {600: {direction: 'horizontal'}}, on: {
                slideChange() {
                    if (!swiper) return;
                    $('.start-page .section-about .image .nav>*').eq(swiper.realIndex).addClass('active').siblings().removeClass('active');
                    if (swiper.isBeginning) {
                        $('.start-page .section-about .image .prev').addClass('hidden')
                    } else {
                        $('.start-page .section-about .image .prev').removeClass('hidden')
                    }
                    if (swiper.isEnd) {
                        $('.start-page .section-about .image .next').addClass('hidden')
                    } else {
                        $('.start-page .section-about .image .next').removeClass('hidden')
                    }
                }
            }
        });
        $('.start-page .section-about .image .nav>*').click(function () {
            if (!swiper) return;
            swiper.slideTo($(this).prevAll().length)
        });
        $('.start-page .section-about .image .prev').click(function () {
            if (!swiper) return;
            swiper.slidePrev()
        });
        $('.start-page .section-about .image .next').click(function () {
            if (!swiper) return;
            swiper.slideNext()
        })
    })();
    (() => {
        let swiper = null, bottomSwiper = null;
        bottomSwiper = new Swiper($('.start-page .section-facts .bottom')[0], {
            loop: !0,
            allowTouchMove: !1,
            autoHeight: !0
        });
        swiper = new Swiper($('.start-page .section-facts .top .slider')[0], {
            loop: !0, on: {
                slideChange() {
                    if (!swiper || !bottomSwiper) return;
                    bottomSwiper.slideToLoop(swiper.realIndex)
                }
            }
        });
        $('.start-page .section-facts .top .prev').click(function () {
            if (!swiper) return;
            swiper.slidePrev()
        });
        $('.start-page .section-facts .top .next').click(function () {
            if (!swiper) return;
            swiper.slideNext()
        })
    })();
    (() => {
        const swiper = new Swiper($('.start-page .section-unique .images .slider')[0], {
            loop: !0, on: {
                click(e) {
                    const $slide = $(e.target).is('.swiper-slide') ? $(e.target) : $(e.target).parents('.swiper-slide:first');
                    if ($slide.length == 0) return;
                    window.location.href = $slide.find('.img').data('href')
                }, slideChange() {
                }
            }
        });
        $('.start-page .section-unique .slide-to.prev').click(function () {
            if (!swiper) return;
            swiper.slidePrev()
        });
        $('.start-page .section-unique .slide-to.next').click(function () {
            if (!swiper) return;
            swiper.slideNext()
        })
    })();

    
    if($('.start-page .section-projects .list .slides').length) (()=>{


        const $window = $(window);


        let swiper = null;


        function resize(){

            const ww = $window.outerWidth();

            if(swiper) swiper.destroy();

            swiper = new Swiper($('.start-page .section-projects .list .slides')[0],{

                slidesPerView: (ww>980?3:ww>600?2:1),

                spaceBetween: (ww>1280?40:24),

                loop: true,

            });

        }


        resize();


        Codevia.onValueChange('section-projects-resized',()=>{

            const ww = $window.outerWidth();

            return [ww<=600,ww<=980,ww<=1280];

        })(resize);


        $('.start-page .section-projects .list .slide-to .prev').click(e=>{

            swiper.slidePrev();

        });

        $('.start-page .section-projects .list .slide-to .next').click(e=>{

            swiper.slideNext();

        });


    })();


})(window)