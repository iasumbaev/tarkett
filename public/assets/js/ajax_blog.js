$(document).ready(function () {
        $('.js-change-category').click(function () {
            $.ajax({
                    url: $(this).data('url'),
                    success: function (data) {
                        $('.js-articles').empty().append(data);
                    }
                }
            );
            $('.js-change-category').removeClass('current');
            $(this).addClass('current');
            $('.js-load-more-articles').css('display', 'block');
        });

        $('.js-load-more-articles').click(function () {
            $.ajax({
                url: $(this).data('url'),
                method: 'POST',
                data: {
                    id: $('.js-change-category.current').data('id'),
                    offset: $(this).attr('data-offset')
                },
                success: function (data) {
                    if(!data) {
                        $('.js-load-more-articles').css('display', 'none');
                        return;
                    }
                    $('.js-articles .list').append(data);
                    $('.js-load-more-articles').attr('data-offset', parseInt($('.js-load-more-articles').attr('data-offset')) + 1)
                }

            });
        });
    }
);