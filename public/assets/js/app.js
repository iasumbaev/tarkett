(function (window) {
    "use strict";
    var Engine = window.Engine = {}, Codevia = window.Codevia, $document = $(window.document), $window = $(window);
    var onScreenScroll = Codevia.onScreenScroll;
    var scrollTop = Codevia.scrollTop;
    var documentComplete = Engine.documentComplete = $.Deferred();

    (function () {
        var onDocumentStateChanged = Codevia.onValueChange('ondocumentstatechanged', function () {
            return [(document.readyState || '') + '']
        });
        onDocumentStateChanged(function () {
            if (document.readyState == 'complete') {
                documentComplete.resolve();
                $document.trigger('Engine-document-completed')
            }
        })
    })();

    const $body = $('body:first');
    (function () {
        $document.on('focus', '.search-input:not(.focus) input, .input:not(.focus) input, .input-commit:not(.focus) input, .input-calc:not(.focus) input, .textarea:not(.focus) textarea', function (e) {
            $('.search-input.focus, .input.focus, .input-commit.focus, .input-calc.focus, .textarea.focus').removeClass('focus');
            $(this).parent().addClass('focus')
        });
        $document.on('blur', '.search-input.focus input, .input.focus input, .input-commit.focus input, .input-calc.focus input, .textarea.focus textarea', function (e) {
            $(this).parent().removeClass('focus')
        });
        $document.on('click', '.search-input,.input,.input-commit,.textarea', function (e) {
            var $this = $(this), $input = $(this).find('input,textarea');
            if (e.target == $input[0]) return;
            $input.focus();
            $input[0].selectionStart = $input[0].selectionEnd = $input.val().length
        });

        function changed($input) {
            var $inputObject = $input.parent();
            if ($input.val().replace(/\s+/g, '').length > 0) {
                $inputObject.addClass('filled')
            } else {
                $inputObject.removeClass('filled')
            }
            $inputObject.trigger('value-changed')
        }

        $document.on('keyup', '.search-input input, .input input, .input-commit input, .textarea textarea', function () {
            var $this = $(this);
            $this.parents('.textarea,.input').removeClass('failed');
            changed($this)
        });
        $document.on('drop', '.search-input input,.input input,.input-commit input,.textarea textarea', function (e) {
            var $input = $(this);
            setTimeout(function () {
                $('.search-input input,.input input,.input-commit input,.textarea textarea').each(function () {
                    changed($(this))
                })
            }, 30)
        });
        $document.on('click', '.search-input .clear', function (e) {
            var $input = $(this).siblings('input');
            $input.val('');
            changed($input.parent().removeClass('filled'))
        });
        $document.on('click', '.input-calc-wrapper .btn-inc', function (e) {
            var $input = $(this).parents('.input-calc-wrapper').find('.input-calc input'), val = $input.val();
            $input.val(Number(val) + 1)
        });
        $document.on('click', '.input-calc-wrapper .btn-dec', function (e) {
            var $input = $(this).parents('.input-calc-wrapper').find('.input-calc input'), val = $input.val();
            if (val <= 1) return;
            $input.val(Number(val) - 1)
        })
    })();
    (() => {
        $('.nav-button').click(function () {
            $('.nav-overlay').toggleClass('shown');
            $(this).toggleClass('active');
            $body.removeClass('show-header-subnav')
        });
        $('.nav-overlay .collections .header').click(function () {
            $body.removeClass('show-header-subnav')
            if($('.nav-overlay .root .nav > *.active').length>1) $('.collections-link').removeClass('active');
        });

        $('.collections-link').click(function () {

            $('.collections-link').toggleClass('active');

            console.log(':::collections-link');

            if ($body.hasClass('show-header-subnav')) {

                $('.nav-overlay').removeClass('shown');

                $('.nav-button').removeClass('active')

            }else if($window.outerWidth()<=1280){

                $('.nav-overlay').addClass('shown');
                $('.nav-button').addClass('active');

            }

            $body.toggleClass('show-header-subnav')

        });

        $document.click(function (e) {
            if ($(e.target).is('.show-header-subnav .page-header .last-level, .show-header-subnav .page-header .last-level *, .page-header .first-level .nav>*.collections-link, .page-header .first-level .nav>*.collections-link *, .article-page .collections-link, .article-page .collections-link *,  .project-page .collections-link, .project-page .collections-link *, .page-footer .collections-link')) return;
            if ($window.outerWidth() <= 1280) return;
            $('.collections-link').removeClass('active');
            $('.nav-overlay').removeClass('shown');
            $('.nav-button').removeClass('active');
            $body.removeClass('show-header-subnav')
        })
    })();
    (() => {
        (new Promise((resolve, reject) => {
            documentComplete.then(resolve);
            setTimeout(resolve, 10)
        })).then(() => {
            setTimeout(() => {
                $('body:first').addClass('appeared');
                setTimeout(() => {
                    const $appear = $('.appear').toArray().map(el => $(el));
                    const SPACE = 150;

                    function scroll() {
                        const
                            st = scrollTop.get(), sb = st + $window.outerHeight();
                        for (const $el of $appear) {
                            const offs = $el.offset();
                            offs.bottom = offs.top + $el.outerHeight();
                            let space = Math.min(250, $window.outerHeight() / 3);
                            let h = offs.bottom - offs.top;
                            if (h < 1.2 * space) {
                                space *= 2 / 3;
                                if (h < 1.2 * space) {
                                    space *= 1 / 2;
                                    if (h < 1.2 * space) {
                                        space = 0
                                    }
                                }
                            }
                            if (sb >= (offs.top + space) && st <= (offs.bottom - space)) {
                                $el.addClass('appeared')
                            }
                        }
                    }

                    onScreenScroll(scroll)
                    scroll()
                }, 2000)
            }, 1000)
        })
    })()

})(window)