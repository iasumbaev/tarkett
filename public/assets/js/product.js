(function (window) {
    "use strict";
    var Engine = window.Engine, Codevia = window.Codevia, $window = $(window);
    var onWndResize = Codevia.onWndResize;

    const $body = $('body:first');
    if (!$body.hasClass('product-page')) return;

    (() => {
        let nav_swiper = null;
        const swiper = new Swiper($('.product-page .section-slider .slider .slides')[0], {
            direction: 'vertical', on: {
                slideChange() {
                    if (!swiper) return;
                    $('.product-page .section-slider .nav>*>*').eq(swiper.realIndex).addClass('active').siblings().removeClass('active');
                    if (nav_swiper) nav_swiper.slideTo(swiper.realIndex);
                    if (swiper.isBeginning) {
                        $('.product-page .section-slider .slide-to.prev').addClass('hidden')
                    } else {
                        $('.product-page .section-slider .slide-to.prev').removeClass('hidden')
                    }
                    if (swiper.isEnd) {
                        $('.product-page .section-slider .slide-to.next').addClass('hidden')
                    } else {
                        $('.product-page .section-slider .slide-to.next').removeClass('hidden')
                    }
                }
            }, breakpoints: {600: {direction: 'horizontal',autoHeight:true}}
        });

        Engine.documentComplete.then(()=>swiper.update());

        function resize() {
            if ($window.outerWidth() > 1280) {
                if (!nav_swiper) return;
                nav_swiper.destroy();
                nav_swiper = null
            } else {
                if (nav_swiper) return;
                nav_swiper = new Swiper($('.product-page .section-slider .nav')[0], {
                    slidesPerView: 'auto',
                    freeMode: !0
                })
            }
        }

        onWndResize(resize);
        resize();
        $('.product-page .section-slider .nav>*>*').click(function () {
            if (!swiper) return;
            swiper.slideTo($(this).prevAll().length)
        });
        $('.product-page .section-slider .slide-to.prev').click(function () {
            if (!swiper) return;
            swiper.slidePrev()
        });
        $('.product-page .section-slider .slide-to.next').click(function () {
            if (!swiper) return;
            swiper.slideNext()
        })
    })();
    (() => {
        const $style = $('<style type="text/css"></style>').appendTo('head:first');

        function resize() {
            $style.text($('.product-page .section-faq .list>*').toArray().map(function (item) {
                const $item = $(item);
                let id = $item.attr('id');
                if (!id) $item.attr('id', id = '_' + Math.round(Math.random() * 1e9) + Date.now() + Math.round(Math.random() * 1e9));
                return `.product-page .section-faq .list>*.active#${id} .text{height:${$item.find('.text:first>*').outerHeight()}px;}`
            }).join('\n\n'))
        }

        onWndResize(resize);
        resize();
        $('.product-page .section-faq .list .caption').click(function () {
            $(this).parent().toggleClass('active')
        })
    })();
    (() => {
        const $style = $('<style type="text/css"></style>').appendTo('head:first');

        function resize() {
            $style.text($('.product-page .section-docs .list>*').toArray().map(function (item) {
                const $item = $(item);
                let id = $item.attr('id');
                if (!id) $item.attr('id', id = '_' + Math.round(Math.random() * 1e9) + Date.now() + Math.round(Math.random() * 1e9));
                return `.product-page .section-docs .list>*.active#${id} .text{height:${$item.find('.text:first>*').outerHeight()}px;}`
            }).join('\n\n'));
            console.log($style[0])
        }

        onWndResize(resize);
        resize();
        $('.product-page .section-docs .list .caption').click(function () {
            $(this).parent().toggleClass('active')
        })
    })();

    
    
    if($('.product-page .section-projects .list .slides').length) (()=>{


        const $window = $(window);


        let swiper = null;


        function resize(){

            const ww = $window.outerWidth();

            if(swiper) swiper.destroy();

            swiper = new Swiper($('.product-page .section-projects .list .slides')[0],{

                slidesPerView: (ww>980?3:ww>600?2:1),

                spaceBetween: (ww>1280?40:24),

                loop: true,

            });

        }


        resize();


        Codevia.onValueChange('section-projects-resized',()=>{

            const ww = $window.outerWidth();

            return [ww<=600,ww<=980,ww<=1280];

        })(resize);


        $('.product-page .section-projects .list .slide-to .prev').click(e=>{

            swiper.slidePrev();

        });

        $('.product-page .section-projects .list .slide-to .next').click(e=>{

            swiper.slideNext();

        });


    })();

})(window)