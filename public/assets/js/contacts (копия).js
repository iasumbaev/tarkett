(function(window){
"use strict";


/*************************************************************************


      P   R   E   P   A   R   I   N   G       S   C   R   I   P   T


*************************************************************************/


/*************************************************************************

                              B A S I C S

*************************************************************************/


var Engine = window.Engine,
	Codevia = window.Codevia,
	$document = $(window.document),
	$window = $(window),
	query = Codevia.queryServer;


var onWndResize = Codevia.onWndResize,
	onScreenScroll = Codevia.onScreenScroll;


var requestAnimationFrame = Codevia.requestAnimationFrame;


var scrollTop = Codevia.scrollTop;


var animate = Codevia.animate;


var absolute_offset = Codevia.absoluteOffset;


var afterEvents = Codevia.afterEvents;


var let_string = Codevia.let_string,
	clean_spaces = Codevia.clean_spaces;



/*************************************************************************
          
                           P R O M I S E S

*************************************************************************/


var documentReady = Engine.documentReady,
	documentComplete = Engine.documentComplete;


var GoogleMapsAPIReady = Engine.GoogleMapsAPIReady;


/*************************************************************************


          D   O   C   U   M   E   N   T       R   E   A   D   Y


*************************************************************************/


documentReady.then(function(){


const $body =  $('body:first');

if(!$body.hasClass('contacts-page')) return;



/*************************************************************************

                               S L I D E R

*************************************************************************/


(()=>{


$document.on('click','.contacts-page .section-map .left .list .item',function(){

	$(this).addClass('active').siblings('.item').removeClass('active');

});


})();


$.when(

	$.get('/contacts/cities'),

	$.get('/contacts/current'),

).then((citiesResponse,currentResponse)=>{

	const cities = citiesResponse[0];

	const current = currentResponse[0];

	const locations = cities.reduce((locations,city)=>{

		return locations.concat(city.locations.map(item=>{

			item.city = city;

			item.distance = (Math.abs(item.coords.lat - current[0])**2+Math.abs(item.coords.lng - current[1])**2)**.5

			return item;

		}));

	},[]);


	const instances = [];

	function instance(){

		if(instances.length==0){

			return $(`<div class="item">

				<div class="marker"><div></div></div>

				<div class="name"></div>

				<div class="info">

					<!--<div class="status">открыто</div>-->

					<div class="address"></div>

					<!--<div class="link"></div>-->

					<div class="phone"></div>

				</div>

			</div>`);

		}

		const $el = $(instances.splice(0,1)[0]);

		$el.find('.marker>*:first').text('');

		$el.find('.name:first').text('');

		$el.find('.address:first').text('');

		$el.find('.phone:first').text('');

		$el.removeClass('active');

		return $el;

	}

	function revoke(item){

		if(item instanceof jQuery) item = item.toArray(); else if(!(item instanceof Array)) item = [item];


		for(const el of item){

			$(el).detach();

			instances.push(el);

		}

	}

	function revokeAll(){
		
		revoke($('.contacts-page .section-map .left .list>*>*.item'));

	}


	ymaps.ready(()=>{ 

		const map = new ymaps.Map($('.contacts-page .section-map .right>*')[0], {

			center: current,

			zoom: 12

		});


		let activeMarker = null;


		for(const item of locations){

			const marker = new ymaps.Placemark([item.coords.lat,item.coords.lng],{},{
            	preset: 'islands#icon',
				iconColor: '#0000ff'
			});

			map.geoObjects.add(marker);
			
			marker.events.add('click',function () {

				if(marker==activeMarker) return;

				$('.contacts-page .section-map .left .tools .region input').val('');

				if(activeMarker){

					activeMarker.options.set({
						iconColor: '#0000ff'
					})

				}

				activeMarker = marker;

				marker.options.set({
					iconColor: '#ff0000'
				});

				map.setCenter([item.coords.lat,item.coords.lng]);

   			});


		}


		function search(bypattern){

			revokeAll();

			const mapCenter = map.getCenter();

			// console.log(mapCenter);

			for(const item of locations){

				item.distance = (Math.abs(item.coords.lat - mapCenter[0])**2+Math.abs(item.coords.lng - mapCenter[1])**2)**.5;

			}

			const pattern = clean_spaces(let_string($('.contacts-page .section-map .left .tools .region input').val())).toLowerCase();

			let list = locations.slice().sort((a,b)=>(a.distance - b.distance));

			if(pattern){

				list = list.filter(item=>{

					const value = clean_spaces(let_string(item.city+' '+item.address)).toLowerCase();

					return (value.indexOf(pattern)>=0);

				});

			}

			const city = list[0].city;

			list = list.filter(item=>item.city==city);

			$('.contacts-page .section-map .left .list .count').text(`Найдено ${list.length} магазин${(x=>{

				const f = x%10, s = Math.floor(x/10)%10;

				if(s==1) return 'ов';

				if(f==1) return '';

				if(f>=2&&f<=4) return 'а';

				return 'ов';

			})(list.length)}`);

			if(list.length==0) return;

			const center = {
				lat: list.reduce((s,item)=>(s+item.coords.lat),0)/list.length,
				lng: list.reduce((s,item)=>(s+item.coords.lng),0)/list.length
			};

			let index = 0;

			for(const item of list){

				index++;

				const $el = instance();

				// console.log($el[0])

				$el.find('.marker>*:first').text(index);

				$el.find('.name:first').text(item.title);

				$el.find('.address:first').text(item.city.title+', '+item.address);

				$el.find('.phone:first').text(item.phone);

				$('.contacts-page .section-map .left .list>*').append($el);

			}

			// map.setCenter([center.lat,center.lng]);

			// map.setZoom(12);

			$('.contacts-page .section-map .left .list>*>*.item').first().addClass('active');

			$('.contacts-page .section-map .left .list>*')[0].scrollTo(0,0);

		}

		map.events.add('boundschange',()=>{
			search();
		});

		$('.contacts-page .section-map .left .tools .search').click(()=>{
			map.setCenter(current);
		});

		search();


	});

});


// 

/************************************************************************/



});



})(window);