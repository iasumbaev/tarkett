(function (window) {
    "use strict";
    var Engine = window.Engine, Codevia = window.Codevia;
    var onWndResize = Codevia.onWndResize;
    var documentReady = Engine.documentReady;

    const $body = $('body:first');
    if (!$body.hasClass('collection-page')) return;


    (() => {

        const swiper = new Swiper($('.collection-page .section-advantages .slider')[0], {

            spaceBetween: 120, loop:true, on: {

                slideChange() {

                    $('.collection-page .section-advantages .nav .inner>*').eq(this.realIndex).addClass('active').siblings().removeClass('active');
/*
                    if (swiper.isBeginning) {

                        $('.collection-page .section-advantages .prev').addClass('hidden')

                    } else {

                        $('.collection-page .section-advantages .prev').removeClass('hidden')

                    }

                    if (swiper.isEnd) {

                        $('.collection-page .section-advantages .next').addClass('hidden')

                    } else {

                        $('.collection-page .section-advantages .next').removeClass('hidden')

                    }
*/
                }

            }

        });

    
        $('.collection-page .section-advantages .nav .inner>*').click(function () {

            if (!swiper) return;

            swiper.slideToLoop($(this).prevAll().length)

        });

        $('.collection-page .section-advantages .prev').click(function () {

            if (!swiper) return;

            swiper.slidePrev()

        });

        $('.collection-page .section-advantages .next').click(function () {

            if (!swiper) return;

            swiper.slideNext()

        })

    })();

    (() => {
        const $style = $('<style type="text/css"></style>').appendTo('head:first');

        function resize() {
            $style.text($('.collection-page .section-faq .list>*').toArray().map(function (item) {
                const $item = $(item);
                let id = $item.attr('id');
                if (!id) $item.attr('id', id = '_' + Math.round(Math.random() * 1e9) + Date.now() + Math.round(Math.random() * 1e9));
                return `.collection-page .section-faq .list>*.active#${id} .text{height:${$item.find('.text:first>*').outerHeight()}px;}`
            }).join('\n\n'))
        }

        onWndResize(resize);
        resize();
        $('.collection-page .section-faq .list .caption').click(function () {
            $(this).parent().toggleClass('active')
        })
    })();
    (() => {
        let swiper = null;
        swiper = new Swiper($('.collection-page .section-slider .slider')[0], {
            loop: !0,
            spaceBetween: 120,
            breakpoints: {1280: {spaceBetween: 20}},
            on: {
                slideChange() {
                    if (!swiper) return;
                    $('.collection-page .section-slider .nav>*').eq(swiper.realIndex).addClass('active').siblings().removeClass('active')
                }
            }
        });
        $('.collection-page .section-slider .prev').click(function () {
            if (!swiper) return;
            swiper.slidePrev()
        });
        $('.collection-page .section-slider .next').click(function () {
            if (!swiper) return;
            swiper.slideNext()
        });
        $('.collection-page .section-slider .nav>*').click(function () {
            if (!swiper) return;
            swiper.slideToLoop($(this).prevAll().length)
        })
    })();


    if($('.collection-page .section-projects .list .slides').length) (()=>{


        const $window = $(window);


        let swiper = null;


        function resize(){

            const ww = $window.outerWidth();

            if(swiper) swiper.destroy();

            swiper = new Swiper($('.collection-page .section-projects .list .slides')[0],{

                slidesPerView: (ww>980?3:ww>600?2:1),

                spaceBetween: (ww>1280?40:24),

                loop: true,

            });

        }


        resize();


        Codevia.onValueChange('section-projects-resized',()=>{

            const ww = $window.outerWidth();

            return [ww<=600,ww<=980,ww<=1280];

        })(resize);


        $('.collection-page .section-projects .list .slide-to .prev').click(e=>{

            swiper.slidePrev();

        });

        $('.collection-page .section-projects .list .slide-to .next').click(e=>{

            swiper.slideNext();

        });


    })();


})(window)