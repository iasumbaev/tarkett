-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: tarkett
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl_classes`
--

DROP TABLE IF EXISTS `acl_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_classes` (
                               `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                               `class_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_classes`
--

LOCK TABLES `acl_classes` WRITE;
/*!40000 ALTER TABLE `acl_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_entries`
--

DROP TABLE IF EXISTS `acl_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_entries` (
                               `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                               `class_id` int(10) unsigned NOT NULL,
                               `object_identity_id` int(10) unsigned DEFAULT NULL,
                               `security_identity_id` int(10) unsigned NOT NULL,
                               `field_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `ace_order` smallint(5) unsigned NOT NULL,
                               `mask` int(11) NOT NULL,
                               `granting` tinyint(1) NOT NULL,
                               `granting_strategy` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `audit_success` tinyint(1) NOT NULL,
                               `audit_failure` tinyint(1) NOT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
                               KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
                               KEY `IDX_46C8B806EA000B10` (`class_id`),
                               KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
                               KEY `IDX_46C8B806DF9183C9` (`security_identity_id`),
                               CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                               CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                               CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_entries`
--

LOCK TABLES `acl_entries` WRITE;
/*!40000 ALTER TABLE `acl_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_object_identities`
--

DROP TABLE IF EXISTS `acl_object_identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_object_identities` (
                                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                         `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
                                         `class_id` int(10) unsigned NOT NULL,
                                         `object_identifier` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `entries_inheriting` tinyint(1) NOT NULL,
                                         PRIMARY KEY (`id`),
                                         UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
                                         KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`),
                                         CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_object_identities`
--

LOCK TABLES `acl_object_identities` WRITE;
/*!40000 ALTER TABLE `acl_object_identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_object_identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_object_identity_ancestors`
--

DROP TABLE IF EXISTS `acl_object_identity_ancestors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_object_identity_ancestors` (
                                                 `object_identity_id` int(10) unsigned NOT NULL,
                                                 `ancestor_id` int(10) unsigned NOT NULL,
                                                 PRIMARY KEY (`object_identity_id`,`ancestor_id`),
                                                 KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
                                                 KEY `IDX_825DE299C671CEA1` (`ancestor_id`),
                                                 CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                                 CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_object_identity_ancestors`
--

LOCK TABLES `acl_object_identity_ancestors` WRITE;
/*!40000 ALTER TABLE `acl_object_identity_ancestors` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_object_identity_ancestors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_security_identities`
--

DROP TABLE IF EXISTS `acl_security_identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_security_identities` (
                                           `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                           `identifier` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
                                           `username` tinyint(1) NOT NULL,
                                           PRIMARY KEY (`id`),
                                           UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_security_identities`
--

LOCK TABLES `acl_security_identities` WRITE;
/*!40000 ALTER TABLE `acl_security_identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_security_identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
                           `id` int(11) NOT NULL AUTO_INCREMENT,
                           `article_category_id` int(11) DEFAULT NULL,
                           `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `preview_text` longtext COLLATE utf8mb4_unicode_ci,
                           `time_of_reading` double DEFAULT NULL,
                           `text` longtext COLLATE utf8mb4_unicode_ci,
                           `image_file_name` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `created_at` datetime NOT NULL,
                           `updated_at` datetime NOT NULL,
                           `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           `alt_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `UNIQ_23A0E66989D9B62` (`slug`),
                           KEY `IDX_23A0E6688C5F785` (`article_category_id`),
                           CONSTRAINT `FK_23A0E6688C5F785` FOREIGN KEY (`article_category_id`) REFERENCES `article_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,1,'Как выбрать ламинат','kak-vybrat-laminat','В результате чего — сменил пару команд разработчиков переделывая всё с чистого листа, а ещё получал угрозы от дочерней компании алроса и запугивания их службой безопасности.',10,'<figure class=\"image\"><img alt=\"\" height=\"856\" src=\"/uploads/article/15845986332001500521.jpg\" width=\"1520\" />\r\n<figcaption>Оригинал:&nbsp;<a href=\"https://automattic.design/\" target=\"__blank\">automattic.design</a></figcaption>\r\n</figure>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Критерии выбора ламинированных покрытий</h2>\r\n\r\n<p>Как минимум, вам нужно что-то, чем рисовать и на чем рисовать. Это могут быть карандаш и бумага, маркеры и картон, мел и доска или даже ручка и ваша рука.</p>\r\n\r\n<p>Если вы хотите сделать набросок в&nbsp;<a href=\"https://artvinyl.ru\">цифровом виде</a>, вам доступны несколько удивительных вариантов. Однако вместе с этим возникает искушение слишком усложнить свой эскиз. Просто предупреждение. Это случилось со мной.</p>\r\n\r\n<figure class=\"image\"><img alt=\"\" height=\"860\" src=\"/uploads/article/15846012361414165430.jpg\" width=\"1520\" />\r\n<figcaption>Название</figcaption>\r\n</figure>\r\n\r\n<figure class=\"image\"><img alt=\"\" height=\"856\" src=\"/uploads/article/1584601253205541686.jpg\" width=\"1520\" />\r\n<figcaption>Название</figcaption>\r\n</figure>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Ламинат 31 класса эксплуатации может использоваться в коммерческих помещениях со слабой нагрузкой от 2 до 3 лет.</li>\r\n	<li>Ламинат 32 класса в коммерческих помещениях со средней нагрузкой прослужит 3 &ndash; 5 лет, дома &mdash; до 15 лет.</li>\r\n	<li>33 класс подходит для коммерческих помещений с высокой нагрузкой, где срок его службы составит от 5 до 6 лет.</li>\r\n</ol>\r\n\r\n<h2>Критерии выбора ламинированных покрытий</h2>\r\n\r\n<p>Как минимум, вам нужно что-то, чем рисовать и на чем рисовать. Это могут быть карандаш и бумага, маркеры и картон, мел и доска или даже ручка и ваша рука.</p>\r\n\r\n<p>Если вы хотите сделать набросок в цифровом виде, вам доступны несколько удивительных вариантов. Однако вместе с этим возникает искушение слишком усложнить свой эскиз. Просто предупреждение. Это случилось со мной:</p>\r\n\r\n<ul>\r\n	<li>Ламинат 31 класса эксплуатации может использоваться в коммерческих помещениях со слабой нагрузкой от 2 до 3 лет.</li>\r\n	<li>Ламинат 32 класса в коммерческих помещениях со средней нагрузкой прослужит 3 &ndash; 5 лет, дома &mdash; до 15 лет.</li>\r\n	<li>33 класс подходит для коммерческих помещений с высокой нагрузкой, где срок его службы составит от 5 до 6 лет.</li>\r\n</ul>\r\n\r\n<h2>Критерии выбора ламинированных покрытий</h2>\r\n\r\n<p>\r\n<video controls=\"controls\" height=\"450\" preload=\"metadata\" src=\"/uploads/article_videos/1-5e730be4dd94e0.34826907.mp4\" width=\"800\">&nbsp;</video>\r\n</p>\r\n\r\n<p>Как минимум, вам нужно что-то, чем рисовать и на чем рисовать. Это могут быть карандаш и бумага, маркеры и картон, мел и доска или даже ручка и ваша рука.</p>\r\n\r\n<p>Если вы хотите сделать набросок в цифровом виде, вам доступны несколько удивительных вариантов. Однако вместе с этим возникает искушение слишком усложнить свой эскиз. Просто предупреждение. Это случилось со мной:</p>\r\n\r\n<h2>Таблица 5 звезд по выбору линолиума</h2>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<thead>\r\n		<tr>\r\n			<th scope=\"col\">Назначение</th>\r\n			<th scope=\"col\">Помещение</th>\r\n			<th scope=\"col\">Класс</th>\r\n			<th scope=\"col\">Продукт</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Дом</td>\r\n			<td>Спальня</td>\r\n			<td>21</td>\r\n			<td>Caprice Европа</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Гостинная</td>\r\n			<td>22</td>\r\n			<td><br />\r\n			Discovery, Magia viva, Фаворит</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Кухня, коридор</td>\r\n			<td>23</td>\r\n			<td>Idylle nova, Force, Diva, Moda</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Офис</td>\r\n			<td>Кабинет, офис</td>\r\n			<td>31</td>\r\n			<td>Grand, Admiral, Acczent pro, Idylle nova, Force, Diva, Moda</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Банкетный зал, бутик</td>\r\n			<td>32</td>\r\n			<td>Idylle nova, Force, Diva, Moda</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Школа, больница</td>\r\n			<td>33</td>\r\n			<td>Idylle nova, Force, Diva, Moda</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Магазин</td>\r\n			<td>34</td>\r\n			<td>Discovery, Magia viva, Фаворит</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Производство</td>\r\n			<td>Цех, завод точной механики</td>\r\n			<td>41</td>\r\n			<td>Idylle nova, Force, Diva, Moda</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Склад, цеха электротехнической сборки</td>\r\n			<td>43</td>\r\n			<td>Discovery, Magia viva, Фаворит</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Склады</td>\r\n			<td>43</td>\r\n			<td>Caprice Европа</td>\r\n		</tr>\r\n	</tbody>\r\n</table>','1-5e731830bd33f4.34367760.jpeg','2020-03-19 07:57:20','2020-03-19 08:00:58',NULL,NULL,NULL,NULL),(2,1,'Тест','test',NULL,15,NULL,'ivndrn0634-5e9d510d1e2432.90867215.jpeg','2020-04-20 09:36:45','2020-04-20 09:36:45',NULL,NULL,NULL,'выпыв');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_category`
--

DROP TABLE IF EXISTS `article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_category` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `created_at` datetime NOT NULL,
                                    `updated_at` datetime NOT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_category`
--

LOCK TABLES `article_category` WRITE;
/*!40000 ALTER TABLE `article_category` DISABLE KEYS */;
INSERT INTO `article_category` VALUES (1,'Укладка','2020-03-19 07:12:34','2020-03-19 07:12:34');
/*!40000 ALTER TABLE `article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_video`
--

DROP TABLE IF EXISTS `article_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_video` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `original_filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `size` bigint(20) NOT NULL,
                                 `created_at` datetime NOT NULL,
                                 `updated_at` datetime NOT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_video`
--

LOCK TABLES `article_video` WRITE;
/*!40000 ALTER TABLE `article_video` DISABLE KEYS */;
INSERT INTO `article_video` VALUES (1,'1-5e730be4dd94e0.34826907.mp4','','video/mp4','mp4',1416783,'2020-03-19 07:06:28','2020-03-19 07:06:28');
/*!40000 ALTER TABLE `article_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_page`
--

DROP TABLE IF EXISTS `blog_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_page` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `text` longtext COLLATE utf8mb4_unicode_ci,
                             `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_page`
--

LOCK TABLES `blog_page` WRITE;
/*!40000 ALTER TABLE `blog_page` DISABLE KEYS */;
INSERT INTO `blog_page` VALUES (1,'Статьи','Мы — эксперты по напольным покрытиям, что означает, что мы также проявляем интерес ко всему, что имеет отношение к напольным покрытиям и как это способствует формированию хороших условий жизни для всех.','Статьи',NULL,NULL);
/*!40000 ALTER TABLE `blog_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `type_id` int(11) DEFAULT NULL,
                        `collection_id` int(11) DEFAULT NULL,
                        `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `updated` datetime DEFAULT NULL,
                        `metatitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `metakeys` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `metadesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `texture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `factorwidth` int(11) DEFAULT NULL,
                        `factorlength` int(11) DEFAULT NULL,
                        `factorcount` int(11) DEFAULT NULL,
                        `factorarea` int(11) DEFAULT NULL,
                        `position` int(11) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        KEY `IDX_161498D3C54C8C93` (`type_id`),
                        KEY `IDX_161498D3514956FD` (`collection_id`),
                        CONSTRAINT `FK_161498D3514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`),
                        CONSTRAINT `FK_161498D3C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `card_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
INSERT INTO `card` VALUES (1,1,1,'Dingo','6041672289.jpg','2019-04-23 05:40:44','Дизайн Dingo / Коллекция Blues | Tarkett Art Vinyl','Dingo, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Dingo, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,1),(2,1,1,'Edmonton','view.jpg',NULL,'Дизайн Edmonton / Коллекция Blues | Tarkett Art Vinyl','Edmonton, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Edmonton, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,2),(3,1,1,'Essence','view.jpg',NULL,'Дизайн Essence / Коллекция Blues | Tarkett Art Vinyl','Essence, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Essence, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,3),(4,1,1,'Harvest','view.jpg',NULL,'Дизайн Harvest / Коллекция Blues | Tarkett Art Vinyl','Harvest, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Harvest, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,4),(5,1,1,'Helena','view.jpg',NULL,'Дизайн Helena / Коллекция Blues | Tarkett Art Vinyl','Helena, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Helena, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,5),(6,1,1,'Omaha','view.jpg',NULL,'Дизайн Omaha / Коллекция Blues | Tarkett Art Vinyl','Omaha, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Omaha, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,6),(7,1,1,'Portland','view.jpg',NULL,'Дизайн Portland / Коллекция Blues | Tarkett Art Vinyl','Portland, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Portland, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,7),(8,1,1,'Roots','view.jpg',NULL,'Дизайн Roots / Коллекция Blues | Tarkett Art Vinyl','Roots, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Roots, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,8),(9,1,1,'Victoria','view.jpg',NULL,'Дизайн Victoria / Коллекция Blues | Tarkett Art Vinyl','Victoria, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Victoria, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,9),(10,1,1,'Windsor','view.jpg',NULL,'Дизайн Windsor / Коллекция Blues | Tarkett Art Vinyl','Windsor, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Windsor, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,457,457,10,2,10),(11,2,1,'Highland','7878888314.jpg','2019-06-19 17:58:53','Дизайн Highland / Коллекция Blues | Tarkett Art Vinyl','Highland, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Highland, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,152,914,15,2,11),(12,2,1,'Lancaster','8252486128.jpg','2019-06-19 17:59:36','Дизайн Lancaster / Коллекция Blues | Tarkett Art Vinyl','Lancaster, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Lancaster, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,152,914,15,2,12),(13,2,1,'Stafford','4726976576.jpg','2019-06-19 17:58:39','Дизайн Stafford / Коллекция Blues | Tarkett Art Vinyl','stafford, blues, art vinyl, коллекция блюз, модульное покрытие','Дизайн Stafford, коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/34 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,152,914,15,2,13),(14,2,2,'Altair','3245227624.jpg','2019-04-18 17:16:41',NULL,NULL,NULL,NULL,NULL,152,914,15,2,NULL),(15,2,2,'Antares','9834027749.jpg','2019-04-23 13:19:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,2,2,'Atlas','5814650184.jpg','2019-04-18 17:06:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,2,2,'Hercules','7479043408.jpg','2019-04-18 17:07:45',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,2,2,'Jupiter','4459844285.jpg','2019-04-18 17:08:32',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,2,2,'Moon','5671875568.jpg','2019-04-18 17:09:43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,2,2,'Nemesis','9650643180.jpg','2019-04-18 17:11:17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,2,2,'Phoenix','2027282030.jpg','2019-04-18 17:12:18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,2,2,'Sirius','7090813712.jpg','2019-04-18 17:14:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,2,2,'Stardust','1715644553.jpg','2019-04-18 17:15:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,2,3,'Alan','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,2,3,'Craig','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,2,3,'Hans','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,2,3,'Howard','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,2,3,'James','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,2,3,'Joel','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,2,3,'Kevin','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,2,3,'Mark','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,2,3,'Ramin','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,2,3,'Rupert','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,2,4,'Anita','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,2,4,'Dave','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,2,4,'Frank','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,2,4,'Les','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(38,2,4,'Miles','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,2,4,'Ornette','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,2,4,'Ray','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(41,1,5,'Chill','3016919819.jpg','2019-04-23 13:44:04','Chill / Lounge | Tarkett Art Vinyl','чил, chill, лаунж коллекция, lounge, art vinyl,','Дизайн Chill, коллекция Lounge от Tarkett Art Vinyl - плитка ПВХ, напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала','1545442538.jpg','2202115722.zip',NULL,NULL,NULL,NULL,18),(42,1,5,'Cocktail','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,19),(43,1,5,'Concrete','1722423961.jpg','2019-04-19 11:25:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20),(44,1,5,'Delmar','8482315158.jpg','2019-04-23 14:09:45','Delmar / Lounge | Tarkett Art Vinyl','дельмар, delmar, лаунж коллекция, lounge, art vinyl,','Дизайн Delmar, коллекция Lounge от Tarkett Art Vinyl - плитка ПВХ, напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала','9919457626.jpg',NULL,NULL,NULL,NULL,NULL,21),(45,1,5,'Jaffa','8609484138.jpg','2019-04-23 13:32:30','Lounge / Jaffa  | Tarkett Art Vinyl','джафа, jaffa, лаунж коллекция, lounge, art vinyl,','Дизайн Jaffa, коллекция Lounge от Tarkett Art Vinyl - плитка ПВХ, напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала','2892992917.jpg','1776716136.zip',NULL,NULL,NULL,NULL,23),(46,1,5,'Sandy','8045531036.jpg','2019-04-23 13:51:53','Sandy / Lounge | Tarkett Art Vinyl','сенди, sandy, лаунж коллекция, lounge, art vinyl,','Дизайн Sandy, коллекция Lounge от Tarkett Art Vinyl - плитка ПВХ, напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала','3677885309.jpg','1346575654.zip',NULL,NULL,NULL,NULL,24),(47,1,5,'Skye','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25),(48,1,5,'Version','3215410472.jpg','2019-04-19 11:27:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,26),(49,2,5,'Bali','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(50,2,5,'Buddha','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2),(51,2,5,'Charango','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3),(52,2,5,'Costes','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4),(53,2,5,'Henry','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5),(54,2,5,'Husky','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6),(55,2,5,'Ibiza','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7),(56,2,5,'Lorenzo','8990344136.jpg','2019-04-23 13:48:53','Lorenzo / Lounge | Tarkett Art Vinyl','лоренцо, lorenzo, лаунж коллекция, lounge, art vinyl,','Дизайн Lorenzo, коллекция Lounge от Tarkett Art Vinyl - плитка ПВХ, напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала','5777381876.jpg','3161651460.zip',NULL,NULL,NULL,NULL,8),(57,2,5,'Moby','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9),(58,2,5,'Nordic','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10),(59,2,5,'Ramon','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,11),(60,2,5,'Relax','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,12),(61,2,5,'Serge','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13),(62,2,5,'Simple','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14),(63,2,5,'Studio','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15),(64,2,5,'Tribute','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16),(65,2,5,'Woody','view.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17),(66,1,6,'Amber','8958740667.jpg','2019-04-23 14:10:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(67,1,6,'Aquamarine','8385069241.jpg','2019-04-23 14:10:53',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(68,1,6,'Crystal','2104566166.jpg','2019-04-23 14:10:58',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(69,1,6,'Diamond','5451781575.jpg','2019-04-23 14:11:03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(70,1,6,'Emerald','8238246910.jpg','2019-04-23 14:11:08',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(71,1,6,'Onyx','9375448931.jpg','2019-04-23 14:11:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(72,1,6,'Opal','3728858473.jpg','2019-04-23 14:11:18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(73,1,6,'Ruby','3909921642.jpg','2019-04-23 14:06:21','Ruby / Murano | Tarkett Art Vinyl','руби, ruby, мурано коллекция, murano, art vinyl,','Дизайн Ruby, коллекция Murano от Tarkett Art Vinyl - плитка ПВХ, напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(74,1,6,'Topaz','3621379372.jpg','2019-04-23 14:05:36','Topaz / Murano | Tarkett Art Vinyl','топаз, topaz, мурано коллекция, murano, art vinyl,','Дизайн Topaz, коллекция Murano от Tarkett Art Vinyl - плитка ПВХ, напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала','9854965113.jpg',NULL,NULL,NULL,NULL,NULL,NULL),(75,2,7,'Ambient','9973902893.jpg','2019-05-22 09:01:59',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(76,2,7,'Ameno','1255810270.jpg','2019-05-22 09:03:39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2),(77,2,7,'Elysium','3473209714.jpg','2019-05-22 09:04:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3),(78,2,7,'Enigma','9153451083.jpg','2019-05-22 09:04:33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4),(79,2,7,'Equilibre','3155373831.jpg','2019-05-22 09:04:55',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5),(81,2,7,'Exotic','7107910276.jpg','2019-05-22 09:05:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6),(83,2,7,'Mistero','7770557276.jpg','2019-05-22 09:05:47',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,8),(84,2,7,'Misty','1808249566.jpg','2019-05-22 09:06:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9),(86,2,7,'Orient','2072354920.jpg','2019-05-22 09:06:56',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10),(87,2,7,'Orto','7540261306.jpg','2019-05-22 09:07:16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,11),(88,2,7,'Sense','9273959260.jpg','2019-05-22 09:07:37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,12),(89,2,7,'Serenity','8887525660.jpg','2019-05-22 09:07:55',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13),(90,2,7,'Soul','5774969121.jpg','2019-05-22 09:08:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14),(92,2,7,'Volo','7376365550.jpg','2019-05-22 09:08:37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15),(93,2,8,'Angel','6304246439.jpg','2019-06-19 18:22:18','Дизайн Angel / Коллекция Dream House | Tarkett Art Vinyl','Angel, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Angel, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(94,2,8,'Agostino','3823394512.jpg','2019-06-19 18:28:00','Дизайн Agostino/ Коллекция Dream House | Tarkett Art Vinyl','Agostino, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Agostino, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(95,2,8,'Dreamer','2324674854.jpg','2019-06-19 18:30:49','Дизайн Dreamer / Коллекция Dream House | Tarkett Art Vinyl','Dreamer, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Dreamer, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(96,2,8,'Groove','8793087527.jpg','2019-06-19 18:33:48','Дизайн Groove / Коллекция Dream House | Tarkett Art Vinyl','Groove, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Groove, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(97,2,8,'Jasper','7523164630.jpg','2019-06-19 18:34:27','Дизайн Jasper / Коллекция Dream House | Tarkett Art Vinyl','Jasper, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Jasper, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(98,2,8,'Lost','1911428595.jpg','2019-06-19 18:34:58','Дизайн Lost / Коллекция Dream House | Tarkett Art Vinyl','Lost, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Lost, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(99,2,8,'Nocturne','4658824987.jpg','2019-06-19 18:38:35','Дизайн Nocturne/ Коллекция Dream House | Tarkett Art Vinyl','Nocturne, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Nocturne, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(100,2,8,'Metropolis','9307228187.jpg','2019-06-19 18:39:24','Дизайн Metropolis/ Коллекция Dream House | Tarkett Art Vinyl','Metropolis, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Metropolis, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(101,2,8,'Robert','9858080727.jpg','2019-06-19 18:41:17','Дизайн Robert / Коллекция Dream House | Tarkett Art Vinyl','Robert, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Robert, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(102,2,8,'Timeless','6918364961.jpg','2019-06-20 09:54:23','Дизайн Timeless / Коллекция Dream House | Tarkett Art Vinyl','Timeless, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Timeless, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(103,2,8,'Trance','5260358282.jpg','2019-06-20 09:56:04','Дизайн Trance / Коллекция Dream House | Tarkett Art Vinyl','Trance, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Trance, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL),(104,2,9,'Darin','7756996451.jpg','2019-05-17 16:10:07','Дизайн Darin / Коллекция Progressive House | Tarkett Art Vinyl','Darin, Progressive House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Darin, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(105,2,9,'Eric','4510579719.jpg','2019-05-17 16:10:47','Дизайн Eric / Коллекция Progressive House | Tarkett Art Vinyl','Eric, Progressive House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Eric, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(106,2,9,'Gabriel','7970768555.jpg','2019-05-17 16:11:40','Дизайн Gabriel / Коллекция Progressive House | Tarkett Art Vinyl','Gabriel, Progressive House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Gabriel, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(107,2,9,'Jason','9236753595.jpg','2019-05-17 16:13:04','Дизайн Jason / Коллекция Progressive House | Tarkett Art Vinyl','Jason, Progressive House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Jason, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(108,2,9,'Jody','6839319443.jpg','2019-05-17 16:14:04','Дизайн Jody / Коллекция Progressive House | Tarkett Art Vinyl','Jody, Progressive House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Jody, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(109,2,9,'Mario','7154125458.jpg','2019-05-17 16:15:09','Дизайн Mario / Коллекция Progressive House | Tarkett Art Vinyl','Mario, Progressive House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Mario, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(110,2,9,'Max','8684829565.jpg','2019-05-17 16:15:54','Дизайн Max / Коллекция Progressive House | Tarkett Art Vinyl','Max, Progressive House, art vinyl, коллекция прогрессив хауз, модульное покрытие','Дизайн Max, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(111,2,9,'Michael','8133762133.jpg','2019-05-17 16:17:20','Дизайн Michael / Коллекция Progressive House | Tarkett Art Vinyl','Michael, Progressive House, art vinyl, коллекция прогрессив хауз, модульное покрытие','Дизайн Michael, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(112,2,9,'Paolo','8161909355.jpg','2019-05-17 16:18:10','Дизайн Paolo / Коллекция Progressive House | Tarkett Art Vinyl','Paolo, Progressive House, art vinyl, коллекция прогрессив хауз, модульное покрытие','Дизайн Paolo, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(113,2,9,'Roger','4805434018.jpg','2019-05-17 16:19:28','Дизайн Roger / Коллекция Progressive House | Tarkett Art Vinyl','Roger, Progressive House, art vinyl, коллекция прогрессив хауз, модульное покрытие','Дизайн Roger, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(114,2,9,'Sebastian','9285107918.jpg','2019-05-17 16:22:19','Дизайн Sebastian / Коллекция Progressive House | Tarkett Art Vinyl','Sebastian, Progressive House, art vinyl, коллекция прогрессив хауз, модульное покрытие','Дизайн Sebastian, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(115,2,9,'Silva','8403772129.jpg','2019-05-17 16:23:24','Дизайн Silva / Коллекция Progressive House | Tarkett Art Vinyl','Silva, Progressive House, art vinyl, коллекция прогрессив хауз, модульное покрытие','Дизайн Silva, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(116,2,9,'Steve','6433694708.jpg','2019-05-17 16:25:07','Дизайн Steve / Коллекция Progressive House | Tarkett Art Vinyl','Steve, Progressive House, art vinyl, коллекция прогрессив хауз, модульное покрытие','Дизайн Steve, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(117,2,9,'William','2273990021.jpg','2019-05-17 16:26:00','Дизайн William / Коллекция Progressive House | Tarkett Art Vinyl','William, Progressive House, art vinyl, коллекция прогрессив хауз, модульное покрытие','Дизайн William, коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 23/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(118,2,4,'Marcus','6593421507.jpg','2019-05-17 14:13:31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(119,2,4,'Nina','9402561381.jpg','2019-05-17 14:14:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(120,2,4,'Oscar','2599831526.jpg','2019-05-17 14:09:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(121,1,5,'Fabric','2886667787.jpg','2019-05-17 14:00:42',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,22),(124,2,7,'Flow','7476527365.jpg','2019-05-16 18:45:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7),(126,1,7,'Era','7456206954.jpg','2019-05-25 07:01:54',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16),(127,1,7,'Gravity','8179818994.jpg','2019-05-25 07:09:12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17),(128,1,7,'Noise','5174151004.jpg','2019-05-25 07:17:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,18),(130,1,7,'Space','9733229439.jpg','2019-05-25 07:40:47',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,19),(131,1,7,'Tempus','3804043173.jpg','2019-05-25 07:48:17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20),(132,1,7,'Vernum','6498998201.jpg','2019-05-25 08:08:47',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,21),(133,2,8,'Princess','1944837705.jpg','2019-06-20 10:00:54','Дизайн Princess / Коллекция Dream House | Tarkett Art Vinyl','Princess, Dream House, art vinyl, коллекция дрим хауз, модульное покрытие','Дизайн Princess, коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие с фаской 21/31 класса применения, KM5 класса пожарной опасности материала',NULL,NULL,183,1220,9,2,NULL);
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_char`
--

DROP TABLE IF EXISTS `card_char`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_char` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_char`
--

LOCK TABLES `card_char` WRITE;
/*!40000 ALTER TABLE `card_char` DISABLE KEYS */;
INSERT INTO `card_char` VALUES (1,'Тип'),(2,'Размер'),(3,'Количество штук в упаковке'),(4,'Вес упаковки'),(5,'Дополнительный защитный слой, лак'),(6,'Общая толщина'),(7,'Толщина рабочего слоя'),(8,'Способ укладки'),(9,'Класс пожарной опасности материала'),(10,'Классы применения'),(11,'Фаска');
/*!40000 ALTER TABLE `card_char` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection`
--

DROP TABLE IF EXISTS `card_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `class_id` int(11) DEFAULT NULL,
                                   `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `texttop` longtext COLLATE utf8mb4_unicode_ci,
                                   `textbottom` longtext COLLATE utf8mb4_unicode_ci,
                                   `position` int(11) DEFAULT NULL,
                                   `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `updated` datetime DEFAULT NULL,
                                   `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `metatitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `metakeys` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `metadesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `type` int(11) DEFAULT NULL,
                                   `designtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `designimage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   PRIMARY KEY (`id`),
                                   KEY `IDX_903FF83CEA000B10` (`class_id`),
                                   CONSTRAINT `FK_903FF83CEA000B10` FOREIGN KEY (`class_id`) REFERENCES `card_collection_class` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection`
--

LOCK TABLES `card_collection` WRITE;
/*!40000 ALTER TABLE `card_collection` DISABLE KEYS */;
INSERT INTO `card_collection` VALUES (1,1,'Blues','BLUES - это коллекция-импровизация, новые дизайны, солирующие и акцентирующие, идеально взаимодополняющие друг друга.','Сочетает в себе ключевые преимущества трех напольных покрытий: красоту паркета, практичность линолеума и модульность плитки.',1,'9927466312.jpg','2019-05-22 17:58:06','https://www.youtube.com/embed/k8soJlPUig8','Коллекция Blues | Tarkett Art Vinyl','блюз коллекция, blues, art vinyl, финишное напольное покрытие, модульный пол','Коллекция Blues от Tarkett Art Vinyl - модульное напольное покрытие 23/34 класса применения, KM2 класса пожарной опасности материала, сертификат экологичности',1,'Используются плитки Edmonton, Roots и планка Highland',NULL),(2,1,'Cosmic','COSMIC - это не только дизайнерские возможности, но и удобство укладки и ухода, долговечность и износостойкость, возможность применения в помещениях с высокой проходимостью, комфорт при эксплуатации, 100% влагостойкость и экологическая безопасность.',NULL,2,'bg.jpg','2019-05-15 05:45:42','https://www.youtube.com/embed/k8soJlPUig8','Коллекция Cosmic | Tarkett Art Vinyl','космик коллекция, Cosmic, art vinyl, финишное напольное покрытие, модульный пол','Коллекция Cosmic от Tarkett Art Vinyl - модульное напольное покрытие 23/34 класса применения, KM2 класса пожарной опасности материала, сертификат экологичности',1,'Используются плитки Edmonton, Roots и планка Highland',NULL),(3,1,'Epic','Дизайны коллекции названы в честь популярных композиторов жанра EPIC, «музыки героев», используемой наиболее широко в кинематографе и компьютерных играх.','Мы уверены, что дизайны EPIC станут блокбастерами Вашего интерьера, от которых невозможно отвести взгляд!',3,'bg.jpg','2019-05-15 05:46:15','https://www.youtube.com/embed/k8soJlPUig8','Коллекция Epic | Tarkett Art Vinyl','эпик коллекция, Epic, art vinyl, финишное напольное покрытие, модульный пол','Коллекция Epic от Tarkett Art Vinyl - модульное напольное покрытие 23/33/42 класса применения, KM2 класса пожарной опасности материала, сертификат экологичности',1,'Используются плитки Edmonton, Roots и планка Highland',NULL),(4,1,'Jazz','Динамичный ритм современной жизни предполагает материалы, в которых сочетаются функциональность, практичность, легкость воплощения и привлекательный дизайн.','В напольных покрытиях эти характеристики нашли свое отражение в коллекции ART VINYL JAZZ.',4,'1696630732.jpg','2019-05-22 18:07:59','https://www.youtube.com/embed/k8soJlPUig8','Коллекция Jazz | Tarkett Art Vinyl','джаз коллекция, Jazz, art vinyl, финишное напольное покрытие, модульный пол','Коллекция Jazz от Tarkett Art Vinyl - модульное напольное покрытие 23/34 класса применения, KM2 класса пожарной опасности материала, сертификат экологичности',1,'Используются плитки Edmonton, Roots и планка Highland',NULL),(5,1,'Lounge','Коллекция создана специально для тех, кто любит удобные и современные решения, кто ценит красоту, комфорт и возможности создания уникального дизайна. LOUNGE представлена широкой палитрой дизайнов, которые позволяют зонировать пространство, сочетая покрытия различных видов, и создавать оригинальные интерьеры.','Полное соответствие коллекции требованиям по пожарной безопасности КМ2, наличие всех необходимых сертификатов,  в том числе международный сертификат экомаркировки \"Листок жизни, а также высокая плотность и износостойкость - класс помещений 34/43 значительно расширяют зоны применения этого материала. А фаска с 4-х сторон не просто зрительно увеличивает пространство, а создает полное ощущение натурального напольного покрытия. Образовательные учреждения, офисы, бутики, рестораны, магазины выгодно подчеркнут свою индивидуальность благодаря модульности и широкому ассортименту цветов и фактур LOUNGE.',5,'bg.jpg','2019-05-22 18:14:25','https://www.youtube.com/embed/k8soJlPUig8','Коллекция Lounge | Tarkett Art Vinyl','лаунж коллекция, Lounge, art vinyl, финишное напольное покрытие, модульный пол','Коллекция Lounge от Tarkett Art Vinyl - модульное напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала',1,'Используются плитки Edmonton, Roots и планка Highland',NULL),(6,1,'Murano','Искусство стеклодувов с итальянского острова Мурано воплощено в практичном модульном ПВХ напольном покрытии.','Офисы, бутики, рестораны, магазины выгодно подчеркнут свою индивидуальность благодаря уникальному дизайну.',6,'bg.jpg','2019-05-22 18:18:15','https://www.youtube.com/embed/F8rvJEyAf6E','Murano | Tarkett Art Vinyl','мурано, коллекция, модульный пол, пвх, арт винил','Коллекция Murano от Tarkett Art Vinyl - модульное напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала',1,'Используются плитки Edmonton, Roots и планка Highland',NULL),(7,1,'New Age','Ключевая идея ART VINYL - модульность - подразумевает множество функций, которые смогут отразить личность и образ жизни владельца.','Динамичный ритм современной жизни предполагает материалы, в которых сочетаются функциональность, практичность, легкость воплощения идей.',7,'bg.jpg','2019-05-22 18:20:24','https://www.youtube.com/embed/k8soJlPUig8','Коллекция NEW AGE  | Tarkett Art Vinyl','new age, коллекция, модульное, напольное, покрытие','Коллекция NEW AGE от Tarkett Art Vinyl - модульное напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала',1,'Используются плитки Edmonton, Roots и планка Highland',NULL),(8,2,'Dream House','Главной особенностью музыкального жанра «Дрим-хаус» является мягкая, мечтательная мелодия, преимущественно играемая на фортепиано. Присутствуют мягкие мелодичные звуки, лёгкий бит, ненавязчивый фоновый звук, возможны женский вокал или даже хор.','Такова новая коллекция ART VINYL DREAM HOUSE, сочетающая в себе красоту и комфорт, легкость и теплоту.',8,'6967079428.jpg','2019-05-22 18:23:19','https://www.youtube.com/embed/Xyky7AyO-RU','Коллекция Dream House | Tarkett Art Vinyl','Dream House, замковый, модульный, пол, напольное покрытие','Коллекция Dream House от Tarkett Art Vinyl - модульное напольное покрытие 23/34/43 класса применения, KM2 класса пожарной опасности материала',1,'Используются плитки Edmonton, Roots и планка Highland',NULL),(9,2,'Progressive House','Progressive House – это стиль современного динамичного пространства.','Коллекция представлена широкой палитрой дизайнов c текстурой натурального дерева, которые позволяют создавать оригинальные и классические интерьеры. Это простое и доступное, но практичное и функциональное решение отлично впишется в любой интерьер.',9,'3377434907.jpg','2019-05-17 12:52:48','https://www.youtube.com/embed/Xyky7AyO-RU','Коллекция Progressive House  | Tarkett Art Vinyl','Progressive House, прогрессив хауз  коллекция, art vinyl,','Коллекция Progressive House от Tarkett Art Vinyl - модульное напольное покрытие 23/34 класса применения, KM2 класса пожарной опасности материала',2,'Используются плитки Edmonton, Roots и планка Highland',NULL);
/*!40000 ALTER TABLE `card_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_class`
--

DROP TABLE IF EXISTS `card_collection_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_class` (
                                         `id` int(11) NOT NULL AUTO_INCREMENT,
                                         `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `updated` datetime DEFAULT NULL,
                                         `menuicon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_class`
--

LOCK TABLES `card_collection_class` WRITE;
/*!40000 ALTER TABLE `card_collection_class` DISABLE KEYS */;
INSERT INTO `card_collection_class` VALUES (1,'Клеевое соединение','9703205341.png','2019-04-23 04:48:37','2310658921.svg'),(2,'Замковое соединение','lock.svg',NULL,'menu-lock.svg');
/*!40000 ALTER TABLE `card_collection_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_document`
--

DROP TABLE IF EXISTS `card_collection_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_document` (
                                            `id` int(11) NOT NULL AUTO_INCREMENT,
                                            `collection_id` int(11) DEFAULT NULL,
                                            `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                            `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                            `size` int(11) DEFAULT NULL,
                                            `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                            `updated` datetime DEFAULT NULL,
                                            `position` int(11) DEFAULT NULL,
                                            PRIMARY KEY (`id`),
                                            KEY `IDX_516F30D7514956FD` (`collection_id`),
                                            CONSTRAINT `FK_516F30D7514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_document`
--

LOCK TABLES `card_collection_document` WRITE;
/*!40000 ALTER TABLE `card_collection_document` DISABLE KEYS */;
INSERT INTO `card_collection_document` VALUES (1,1,'Инструкция по укладке','351798513716.pdf',188688,'pdf',NULL,1),(2,1,'Инструкция по уходу','445198996153.pdf',130078,'pdf',NULL,2),(3,1,'Пожарный сертификат','465209799859.pdf',1421256,'pdf',NULL,3),(4,1,'Сертификат соответствия','856666100177.pdf',773845,'pdf',NULL,4),(5,1,'Экспертное заключение','571947254202.pdf',1762607,'pdf',NULL,5),(6,2,'Инструкция по подготовке основания','702400564130.pdf',16363732,'pdf',NULL,1),(7,2,'Инструкция по укладке','779331716332.pdf',188688,'pdf',NULL,2),(8,2,'Инструкция по уходу','977606167233.pdf',130078,'pdf',NULL,3),(9,2,'Пожарный сертификат','66341297972.pdf',1069992,'pdf',NULL,4),(10,2,'Сертификат соответствия ГОСТ','523969584783.pdf',706605,'pdf',NULL,5),(11,2,'Экспертное заключение','643729187876.pdf',505575,'pdf',NULL,6),(12,3,'Инструкция по уходу','715773369796.pdf',130078,'pdf',NULL,1),(13,3,'Инстуркция по укладке','202347978519.pdf',188688,'pdf',NULL,2),(14,3,'Пожарный сертификат','975313844322.pdf',4006220,'pdf',NULL,3),(15,3,'Сертификат соответствия EPIC','318252508613.pdf',766407,'pdf',NULL,4),(16,3,'Экспертное заключение','487879380544.pdf',1205522,'pdf',NULL,5),(17,4,'Инструкция по укладке','244991785878.pdf',188688,'pdf',NULL,1),(18,4,'Инструкция по уходу','517720847611.pdf',130078,'pdf',NULL,2),(19,4,'Подготовка основания','353005043893.pdf',16363732,'pdf',NULL,3),(20,4,'Пожарный сертификат','86749999100.pdf',814163,'pdf',NULL,4),(21,4,'Сертификат соответствия','548437349057.pdf',699411,'pdf',NULL,5),(22,4,'Экспертное заключение','82879258867.pdf',651007,'pdf',NULL,6),(23,5,'Инструкция по подготовке основания','600597625716.pdf',16363732,'pdf',NULL,1),(24,5,'Инструкция по укладке','834494625990.pdf',188688,'pdf',NULL,2),(25,5,'Инструкция по уходу','721873102339.pdf',130078,'pdf',NULL,3),(26,5,'Пожарный сертификат','361617873619.pdf',1191879,'pdf',NULL,4),(27,5,'Сертификат соответствия ГОСТ','903535752526.pdf',879891,'pdf',NULL,5),(28,5,'Экспертное заключение','145038553829.pdf',654243,'pdf',NULL,6),(29,6,'Инструкция по укладке','352062594611.pdf',188688,'pdf',NULL,1),(30,6,'Инструкция по уходу','879615980968.pdf',130078,'pdf',NULL,2),(31,6,'Пожарный сертификат','564308669860.pdf',1142217,'pdf',NULL,3),(32,6,'Сертификат соответствия ГОСТ','758344309409.pdf',399738,'pdf',NULL,4),(33,6,'Экспертное заключение','68366392104.pdf',2360586,'pdf',NULL,5),(34,7,'Инструкция по укладке','749515244067.pdf',188688,'pdf',NULL,1),(35,7,'Инструкция по уходу','974072003198.pdf',130078,'pdf',NULL,2),(36,7,'Подготовка основания','682179507677.pdf',16363732,'pdf',NULL,3),(37,7,'Пожарный сертификат','425604421861.pdf',814163,'pdf',NULL,4),(38,7,'Сертификат соответствия ГОСТ','754440188897.pdf',879891,'pdf',NULL,5),(39,7,'Экспертное заключение','762524552380.pdf',704523,'pdf',NULL,6),(40,8,'Инструкция по подготовка основания','399018058236.pdf',16363732,'pdf',NULL,1),(41,8,'Инструкция по укладке и уходу','511230567435.pdf',407691,'pdf',NULL,2),(42,8,'Пожарный сертификат','156069751522.pdf',477142,'pdf',NULL,3),(43,8,'Сертификат соответствия ГОСТ','623728251197.pdf',504272,'pdf',NULL,4),(44,8,'Экспертное заключение','239216357058.pdf',447519,'pdf',NULL,5),(45,9,'Инструкция по укладке','7585913499.pdf',1045372,'PDF','2019-04-26 15:07:52',1),(46,9,'Инструкция по уходу','9844301733.pdf',2281456,'pdf','2019-04-19 16:37:50',2),(47,9,'Подготовка основания','866146426619.pdf',784959,'pdf',NULL,3),(48,9,'Пожарный сертификат','5893262251.pdf',1045372,'PDF','2019-05-17 13:10:56',4),(49,9,'Сертификат соответствия','6628991392.pdf',784959,'PDF','2019-05-17 13:10:56',5),(50,9,'Экспертное заключение',NULL,NULL,NULL,NULL,6),(52,1,'Экологический сертификат \"Листок Жизни\"','6932088344.pdf',1311808,'PDF','2019-05-21 13:22:38',6),(54,3,'Экологический сертификат \"Листок Жизни\"','9771530820.pdf',1358638,'PDF','2019-05-21 13:26:18',6),(55,4,'Экологический сертификат \"Листок Жизни\"','1556072493.pdf',927138,'PDF','2019-05-21 13:25:55',7),(56,4,'Экологический сертификат \"Листок Жизни\" 1.3','1851470741.pdf',701096,'PDF','2019-05-21 13:25:55',8),(57,5,'Экологический сертификат \"Листок Жизни\"','2546805216.pdf',555047,'PDF','2019-05-21 13:26:36',7),(58,7,'Экологический сертификат \"Листок Жизни\"','9798974982.pdf',1220881,'PDF','2019-05-21 13:27:46',7),(59,9,'Экологический сертификат \"Листок Жизни\"','9851539250.pdf',2281456,'PDF','2019-05-21 13:28:01',7);
/*!40000 ALTER TABLE `card_collection_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_faq`
--

DROP TABLE IF EXISTS `card_collection_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_faq` (
                                       `id` int(11) NOT NULL AUTO_INCREMENT,
                                       `collection_id` int(11) DEFAULT NULL,
                                       `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                       PRIMARY KEY (`id`),
                                       KEY `IDX_C6FB03F514956FD` (`collection_id`),
                                       CONSTRAINT `FK_C6FB03F514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_faq`
--

LOCK TABLES `card_collection_faq` WRITE;
/*!40000 ALTER TABLE `card_collection_faq` DISABLE KEYS */;
INSERT INTO `card_collection_faq` VALUES (1,1,'Что делать если испортилась одна плитка?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(2,1,'Можно ли укладывать Art Vinyl на неровные покрытия?','<p>Основание &nbsp;для Art Vinyl должно быть абсолютно ровным.</p>'),(3,1,'Как правильно выбрать клей?','<p>Для Art Vinyl применяется акриловый водно-дисперсионный клей, предназначенный для ПВХ покрытий. Рекомендуемые марки клеев1: Uzin КЕ 2000 S (производитель Uzin Utz AG), Homakoll 164 prof (производитель Homa Chemical Engineering), Ultrabond Eco 375 (производитель Mapei S.p.A.), Ultrabond V4SP (производитель Mapei S.p.A.), EUROCOL Eurostar Allround 528 (производитель Forbo).</p>'),(4,1,'Где я могу посчитать точное количество необходимой плитки?','<p>Посчитать количество необходимой плитки просто: Нужно рассчитать площадь помещения и прибавить +10%. При сложных укладках из нескольких форматов и/или дизайнов &nbsp;можно воспользоваться функцией калькулятор в программе <a href=\"http://new.artvinyl.ru/designer/\">Art Designer</a></p>'),(5,1,'Как уложить Art Vinyl на потолок?','<p>Действительно многие дизайнеры используют Art Vinyl для стен и потолков. Мы не даем рекомендаций, так как для нас это прежде всего напольное покрытие.</p>'),(6,1,'Можно ли укладывать на теплый пол?','<p>Да можно использовать покрытие с теплым полом до 27℃.</p>'),(8,2,'Что делать если испортилась одна плитка?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(9,2,'Можно ли укладывать Art Vinyl на неровные покрытия?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(10,2,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(11,2,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(12,2,'Как уложить Art Vinyl на потолок?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(13,2,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(14,2,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(15,3,'Что делать если испортилась одна плитка?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(16,3,'Можно ли укладывать Art Vinyl на неровные покрытия?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(17,3,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(18,3,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(19,3,'Как уложить Art Vinyl на потолок?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(20,3,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(21,3,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(22,4,'Что делать если испортилась одна плитка?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(23,4,'Можно ли укладывать Art Vinyl на неровные покрытия?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(24,4,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(25,4,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(26,4,'Как уложить Art Vinyl на потолок?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(27,4,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(28,4,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(29,5,'Что делать если испортилась одна плитка?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(30,5,'Можно ли укладывать Art Vinyl на неровные покрытия?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(31,5,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(32,5,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(33,5,'Как уложить Art Vinyl на потолок?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(34,5,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(35,5,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(36,6,'Что делать если испортилась одна плитка?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(37,6,'Можно ли укладывать Art Vinyl на неровные покрытия?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(38,6,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(39,6,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(40,6,'Как уложить Art Vinyl на потолок?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(41,6,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(42,6,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(43,7,'Что делать если испортилась одна плитка?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(44,7,'Можно ли укладывать Art Vinyl на неровные покрытия?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(45,7,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(46,7,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(47,7,'Как уложить Art Vinyl на потолок?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(48,7,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(49,7,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(50,8,'Что делать если испортилась одна плитка?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(51,8,'Можно ли укладывать Art Vinyl на неровные покрытия?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(52,8,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(53,8,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(54,8,'Как уложить Art Vinyl на потолок?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(55,8,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(56,8,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(57,9,'Что делать если испортилась одна плитка?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(58,9,'Можно ли укладывать Art Vinyl на неровные покрытия?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(59,9,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(60,9,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(61,9,'Как уложить Art Vinyl на потолок?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(62,9,'Как правильно выбрать клей?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(63,9,'Где я могу посчитать точное количество необходимой плитки?','<p>С&nbsp;помощью ножа вырежьте по&nbsp;стыкам и&nbsp;удалите поврежденную планку. Возьмите новую планку и&nbsp;подрежьте замки.<br />\r\nПеред&nbsp;монтажом планки убедитесь, что&nbsp;соблюдается направление рисунка. Закрепите новую планку и&nbsp;окружающие ее&nbsp;планки<br />\r\nпри&nbsp;помощи двусторонней клейкой ленты. Тщательно прокатайте и&nbsp;придавите все&nbsp;планки, чтобы&nbsp;надёжно прикрепить их&nbsp;к&nbsp;полу.</p>'),(65,1,'Можно ли укладывать на фанеру?','<p>Нет, нужно укладывать только на ровное бетонное основание.</p>'),(66,1,'Можно ли укладывать на балконе?','<p>Нет, можно укладывать только в отапливаемых остекленных помещениях.</p>'),(67,1,'Насколько продукт безопасен?','<p>Продукт абсолютно безопасен. Он имеет экологический сертификат &laquo;Листок Жизни&raquo;.</p>');
/*!40000 ALTER TABLE `card_collection_faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_interest`
--

DROP TABLE IF EXISTS `card_collection_interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_interest` (
                                            `id` int(11) NOT NULL AUTO_INCREMENT,
                                            `collection_id` int(11) DEFAULT NULL,
                                            `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                            `text` longtext COLLATE utf8mb4_unicode_ci,
                                            `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                            `updated` datetime DEFAULT NULL,
                                            `position` int(11) DEFAULT NULL,
                                            PRIMARY KEY (`id`),
                                            KEY `IDX_E538A0C6514956FD` (`collection_id`),
                                            CONSTRAINT `FK_E538A0C6514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_interest`
--

LOCK TABLES `card_collection_interest` WRITE;
/*!40000 ALTER TABLE `card_collection_interest` DISABLE KEYS */;
INSERT INTO `card_collection_interest` VALUES (1,1,'100% влагостойкий материал','Можно использовать на кухне \r\nВлага не попадает  между стыками благодаря конструкции и составу материала\r\nТорцы не подвержены изменениям от влаги','8136283779.jpg','2019-05-17 11:29:18',1),(2,1,'Экологичность и безопасность','Имеет экологическую маркировку «Листок жизни» международного уровня\r\nНе выделяет вредных веществ\r\nНе содержит и не выделяет формальдегид\r\nВ производстве не используются ядовитые органические растворители','2.jpg',NULL,2),(7,1,'Комфорт при эксплуатации','Устойчивость к скольжению\r\nАкустический комфорт – меньше шума как в самом помещении, так для соседей снизу\r\nАнтистатические свойства – не «бьет током» при прикосновении к дверным ручкам\r\nТактильно теплый материал  – можно ходить босиком, комфортно сидеть и лежать на полу\r\nВозможность использовать поверх теплых полов','7.jpg',NULL,3),(8,2,'100% влагостойкий материал','Можно использовать на кухне \r\nВлага не попадает  между стыками благодаря конструкции и составу материала\r\nТорцы не подвержены изменениям от влаги','6965708773.jpg','2019-05-17 11:56:20',1),(9,2,'Долговечность и износостойкость','Возможность применения в помещениях с высокой проходимостью – в коридорах и прихожих\r\nПрочен, мало изнашивается - устойчив к царапинам и различным загрязнениям\r\nНе деформируется - не боится следов от кресел и каблуков\r\nНе боится сырости, не гниет\r\nНе хрупкий – не боится падения тяжелых предметов','6235338811.jpg','2019-05-17 11:56:20',2),(10,2,'Комфорт при эксплуатации','Устойчивость к скольжению\r\nАкустический комфорт – меньше шума как в самом помещении, так для соседей снизу\r\nАнтистатические свойства – не «бьет током» при прикосновении к дверным ручкам\r\nТактильно теплый материал (не пропускает холод от бетонной стяжки) – можно ходить босиком, комфортно сидеть и лежать на полу\r\nВозможность использовать поверх теплых полов','2090075870.jpg','2019-04-19 09:31:51',3),(14,2,'Удобная транспортировка и хранение','Вес упаковки не превышает 10 кг – не требуются услуги грузчика\r\nМаксимальная длина пачки 92 см - помещается в лифте (даже в пассажирском), в багажнике любого автомобиля\r\nЛегко и удобно складировать во время ремонта, например, если материал закуплен заранее','3537735071.jpg','2019-04-19 10:07:48',4),(15,3,'100% влагостойкий материал','Можно использовать на кухне \r\nВлага не попадает  между стыками благодаря конструкции и составу материала\r\nТорцы не подвержены изменениям от влаги','7136092989.jpg','2019-04-19 10:40:21',1),(16,3,'Экологичность и безопасность','Имеет экологическую маркировку «Листок жизни» международного уровня\r\nНе выделяет вредных веществ\r\nНе содержит и не выделяет формальдегид\r\nВ производстве не используются ядовитые органические растворители','9526900915.jpg','2019-04-19 10:42:47',2),(17,3,'Комфорт при эксплуатации','Устойчивость к скольжению\r\nАкустический комфорт – меньше шума как в самом помещении, так для соседей снизу\r\nАнтистатические свойства – не «бьет током» при прикосновении к дверным ручкам\r\nТактильно теплый материал (не пропускает холод от бетонной стяжки) – можно ходить босиком, комфортно сидеть и лежать на полу\r\nВозможность использовать поверх теплых полов','7936114063.jpg','2019-04-19 10:43:18',3),(21,3,'Удобная транспортировка и хранение','Вес упаковки не превышает 10 кг – не требуются услуги грузчика\r\nМаксимальная длина пачки 92 см - помещается в лифте (даже в пассажирском), в багажнике любого автомобиля\r\nЛегко и удобно складировать во время ремонта, например, если материал закуплен заранее','5488624301.jpg','2019-04-19 10:46:19',4),(22,4,'100% влагостойкий материал','Можно использовать на кухне \r\nВлага не попадает  между стыками благодаря конструкции и составу материала\r\nТорцы не подвержены изменениям от влаги','3774076116.jpg','2019-05-22 18:07:59',1),(23,4,'Дизайнерские возможности','Дизайнерские возможности\r\nБогатый ассортимент расцветок и фактур планок для любого стиля интерьера\r\nМногообразие схем укладок - Ваш неповторимый пол\r\nВозможность разработки собственного дизайн-проекта и создания уникального пространства\r\nМодули имеют точные размеры и форму, пол выглядит единым бесшовным массивом','9290328973.jpg','2019-05-22 18:07:59',2),(24,4,'Экологичность и безопасность','Имеет экологическую маркировку «Листок жизни» международного уровня\r\nНе выделяет вредных веществ\r\nНе содержит и не выделяет формальдегид\r\nВ производстве не используются ядовитые органические растворители','3217068912.jpg','2019-05-22 18:07:59',3),(28,4,'Удобная транспортировка и хранение','Вес упаковки не превышает 10 кг – не требуются услуги грузчика\r\nМаксимальная длина пачки 92 см - помещается в лифте (даже в пассажирском), в багажнике любого автомобиля\r\nЛегко и удобно складировать во время ремонта, например, если материал закуплен заранее','8412766076.jpg','2019-05-22 18:07:59',4),(29,5,'100% влагостойкий материал','Можно использовать на кухне \r\nВлага не попадает  между стыками благодаря конструкции и составу материала\r\nТорцы не подвержены изменениям от влаги','5412437493.jpg','2019-04-19 11:20:31',1),(30,5,'Долговечность и износостойкость','Возможность применения в помещениях с высокой проходимостью – в коридорах и прихожих\r\nПрочен, мало изнашивается - устойчив к царапинам и различным загрязнениям\r\nНе деформируется - не боится следов от кресел и каблуков\r\nНе боится сырости, не гниет\r\nНе хрупкий – не боится падения тяжелых предметов','8727751700.jpg','2019-05-17 12:17:22',2),(31,5,'Экологичность и безопасность','Имеет экологическую маркировку «Листок жизни» международного уровня\r\nНе выделяет вредных веществ\r\nНе содержит и не выделяет формальдегид\r\nВ производстве не используются ядовитые органические растворители','2685197847.jpg','2019-04-19 11:28:08',3),(32,5,'Комфорт при эксплуатации','Устойчивость к скольжению\r\nАкустический комфорт – меньше шума как в самом помещении, так для соседей снизу\r\nАнтистатические свойства – не «бьет током» при прикосновении к дверным ручкам\r\nТактильно теплый материал  – можно ходить босиком, комфортно сидеть и лежать на полу\r\nВозможность использовать поверх теплых полов','5818398995.jpg','2019-04-19 11:33:40',4),(33,5,'Удобство укладки и ухода','Легко обойти препятствия \r\nРемонтопригодность покрытия – можно заменить поврежденную планку\r\nМатериал можно резать обычным техническим ножом\r\nВозможность использования на больших площадях без переходных порожков – нет дополнительных затрат на порожки, не скапливается грязь\r\nЛегко ухаживать за покрытием – простая сухая и, если необходимо, влажная уборка','3083759344.jpg','2019-04-19 11:34:35',5),(35,5,'Удобная транспортировка и хранение','Вес упаковки не превышает 10 кг – не требуются услуги грузчика\r\nМаксимальная длина пачки 92 см - помещается в лифте, даже в пассажирском и в багажнике любого автомобиля\r\nЛегко и удобно складировать во время ремонта, например, если материал закуплен заранее','9296953623.jpg','2019-04-19 11:36:17',6),(36,6,'100% влагостойкий материал','Можно использовать на кухне \r\nВлага не попадает  между стыками благодаря конструкции и составу материала\r\nТорцы не подвержены изменениям от влаги','8243321812.jpg','2019-05-27 17:02:28',1),(37,6,'Долговечность и износостойкость','Возможность применения в помещениях с высокой проходимостью – в коридорах и прихожих\r\nПрочен, мало изнашивается - устойчив к царапинам и различным загрязнениям\r\nНе деформируется - не боится следов от кресел и каблуков\r\nНе боится сырости, не гниет\r\nНе хрупкий – не боится падения тяжелых предметов','5779298972.jpg','2019-05-27 17:03:34',2),(38,6,'Экологичность и безопасность','Имеет экологическую маркировку «Листок жизни» международного уровня\r\nНе выделяет вредных веществ\r\nНе содержит и не выделяет формальдегид\r\nВ производстве не используются ядовитые органические растворители','1032593992.jpg','2019-05-27 17:03:34',3),(39,6,'Дизайнерские возможности','Богатый ассортимент расцветок и фактур плиток для любого стиля интерьера\r\nМногообразие схем укладок - Ваш неповторимый пол\r\nВозможность разработки собственного дизайн-проекта и создания уникального пространства\r\nМодули имеют точные размеры и форму, пол выглядит единым бесшовным массивом','7618554596.jpg','2019-05-27 17:03:34',4),(40,6,'Комфорт при эксплуатации','Устойчивость к скольжению\r\nАкустический комфорт – меньше шума как в самом помещении, так для соседей снизу\r\nАнтистатические свойства – не «бьет током» при прикосновении к дверным ручкам\r\nТактильно теплый материал (не пропускает холод от бетонной стяжки) – можно ходить босиком, комфортно сидеть и лежать на полу\r\nВозможность использовать поверх теплых полов','3085346781.jpg','2019-05-27 17:03:34',5),(42,6,'Удобная транспортировка и хранение','Вес упаковки не превышает 10 кг – не требуются услуги грузчика\r\nМаксимальная длина пачки 92 см - помещается в лифте (даже в пассажирском), в багажнике любого автомобиля\r\nЛегко и удобно складировать во время ремонта, например, если материал закуплен заранее','2063430503.jpg','2019-05-27 17:03:34',6),(43,7,'100% влагостойкий материал','Можно использовать на кухне \r\nВлага не попадает  между стыками благодаря конструкции и составу материала\r\nТорцы не подвержены изменениям от влаги','1307885949.jpg','2019-04-19 14:46:22',1),(47,7,'Экологичность и безопасность','Имеет экологическую маркировку «Листок жизни» международного уровня\r\nНе выделяет вредных веществ\r\nНе содержит и не выделяет формальдегид\r\nВ производстве не используются ядовитые органические растворители','4780969322.jpg','2019-04-19 14:57:45',2),(48,7,'Комфорт при эксплуатации','Устойчивость к скольжению\r\nАкустический комфорт – меньше шума как в самом помещении, так для соседей снизу\r\nАнтистатические свойства – не «бьет током» при прикосновении к дверным ручкам\r\nТактильно теплый материал (не пропускает холод от бетонной стяжки) – можно ходить босиком, комфортно сидеть и лежать на полу\r\nВозможность использовать поверх теплых полов','8219801565.jpg','2019-04-19 14:58:50',3),(49,7,'Дизайнерские возможности','Богатый ассортимент расцветок и фактур планок и плиток для любого стиля интерьера\r\nМногообразие схем укладок - Ваш неповторимый пол\r\nВозможность разработки собственного дизайн-проекта и создания уникального пространства\r\nМодули имеют точные размеры и форму, пол выглядит единым бесшовным массивом','9146735577.jpg','2019-04-19 15:23:41',4),(50,8,'100% влагостойкий материал','Можно использовать на кухне \r\nВлага не попадает  между стыками благодаря конструкции и составу материала\r\nТорцы не подвержены изменениям от влаги','1.jpg',NULL,1),(51,8,'Долговечность и износостойкость','Прочное покрытие, с надежным замковым соединением. При падении тяжелых предметов не образуются сколы.','2.jpg',NULL,2),(52,8,'Легкий уход','Легко моется, не требует специального ухода.','3.jpg',NULL,3),(53,8,'Легкая укладка','Замковая система не требует специального инструмента и клея, а покрытие готово к эксплуатации сразу после монтажа. В случае необходимости, легко заменить поврежденную планку.','4.jpg',NULL,4),(57,9,'100% влагостойкий материал','Можно использовать на кухне \r\nВлага не попадает  между стыками благодаря конструкции и составу материала\r\nТорцы не подвержены изменениям от влаги','1942007069.jpg','2019-05-17 13:02:18',1),(58,9,'Долговечность и износостойкость','Специальное покрытие EXTREME PROTECTION обеспечивает его защиту на долгие годы\r\nНе требует специального ухода','9312052945.jpg','2019-05-17 13:02:18',2),(59,9,'Легкая укладка','Замковая система не требует специального инструмента и клея, а покрытие готово к эксплуатации сразу после монтажа\r\nВ случае необходимости, легко заменить\r\nповрежденную планку','6446517677.jpg','2019-05-17 13:02:18',3),(61,9,'Комфорт при эксплуатации','Более теплое и амортизирующее покрытие (по сравнению с керамической плиткой). Приглушает звук шагов.','4093585980.jpg','2019-05-17 13:02:18',4),(65,1,'Удобная транспортировка и хранение','Вес упаковки не превышает 10 кг – не требуются услуги грузчика\r\nМаксимальная длина пачки 92 см - помещается в лифте, даже в пассажирском и в багажнике любого автомобиля\r\nЛегко и удобно складировать во время ремонта, например, если материал закуплен заранее','6599633445.jpg','2019-05-17 11:47:42',4);
/*!40000 ALTER TABLE `card_collection_interest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_interior`
--

DROP TABLE IF EXISTS `card_collection_interior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_interior` (
                                            `id` int(11) NOT NULL AUTO_INCREMENT,
                                            `collection_id` int(11) DEFAULT NULL,
                                            `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                            `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                            `updated` datetime DEFAULT NULL,
                                            `position` int(11) DEFAULT NULL,
                                            PRIMARY KEY (`id`),
                                            KEY `IDX_E336A1CA514956FD` (`collection_id`),
                                            CONSTRAINT `FK_E336A1CA514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_interior`
--

LOCK TABLES `card_collection_interior` WRITE;
/*!40000 ALTER TABLE `card_collection_interior` DISABLE KEYS */;
INSERT INTO `card_collection_interior` VALUES (1,1,'Кухня Essence, Windsor, Victoria','1.jpg',NULL,1),(2,1,'Гостиная Lancaster, Stafford, Omaha','5.jpg',NULL,2),(3,1,'Прихожая Stafford, Essence, Dingo','8.jpg',NULL,3),(4,1,'Гостиная Highland, Edmonton, Roots','12.jpg',NULL,4),(5,2,'Гостиная NEMESIS, ALTAIR','1.jpg',NULL,1),(6,2,'Гостиная ANTARES','2.jpg',NULL,2),(7,2,'Ванная ATLAS','1908466555.jpg','2019-04-19 10:13:34',3),(8,2,'Детская HERCULES','4.jpg',NULL,4),(9,2,'Холл JUPITER','5.jpg',NULL,5),(10,2,'Кухня MOON','6.jpg',NULL,6),(11,2,'Спальня NEMESIS','7.jpg',NULL,7),(12,2,'Ванная PHOENIX','8.jpg',NULL,8),(13,3,'Гостиная ALAN','1.jpg',NULL,1),(14,3,'Спальня CRAIG','2.jpg',NULL,2),(15,3,'Кухня HANS','3.jpg',NULL,3),(16,3,'Холл HOWARD','4.jpg',NULL,4),(17,3,'Детская JAMES','5.jpg',NULL,5),(18,3,'Ванная JOEL','6.jpg',NULL,6),(19,3,'Гостиная KEVIN','7.jpg',NULL,7),(20,5,'Гостиная NORDIC, STUDIO','1.jpg',NULL,1),(21,5,'Гостиная ACOUSTIC, TRIBUTE','2.jpg',NULL,2),(22,5,'Кухня CHILL, JAFFA, LORENZO','3.jpg',NULL,3),(23,5,'Гостиная LORENZO, NORDIC','4.jpg',NULL,4),(24,5,'Гостиная BALI, CHARANGO, HENRY, SERGE','5.jpg',NULL,5),(25,5,'Гостиная BUDDHA, RELAX','6.jpg',NULL,6),(26,5,'Гостиная SIMPLE, WOODY','7.jpg',NULL,7),(27,6,'Магазин, дизайн CRYSTAL','3325942157.jpg','2019-05-27 17:04:35',1),(28,6,'Магазин, дизайн OPAL','7603838938.jpg','2019-05-27 17:04:35',2),(29,6,'Ресторан, дизайн Aquamarine','8140788814.jpg','2019-05-27 17:04:35',3),(30,6,'Гостиная DIAMOND','7794278318.jpg','2019-05-27 17:04:35',4),(31,6,'Ресторан, дизайн DIAMOND','3447473869.jpg','2019-05-27 17:04:35',5),(32,6,'Ресторан, дизайн RUBY','8653142024.jpg','2019-05-27 17:04:35',6),(34,7,'Кухня SERENITY, SPACE, VOLO','1.jpg',NULL,1),(35,7,'Гостиная AMENO, GRAVITY, MISTY','2.jpg',NULL,2),(36,7,'Гостиная AMENO, ENIGMA, EXOTIC','3.jpg',NULL,3),(37,7,'Кухня SERENITY, SPACE, VOLO','4.jpg',NULL,4),(38,7,'Гостиная ENIGMA, ORTO','9.jpg',NULL,5),(39,7,'Гостиная MISTY, ORIENT, SENSE','5.jpg',NULL,6),(40,7,'Гостиная AMENO, GRAVITY, MISTY','6.jpg',NULL,7),(41,7,'Гостиная AMBIENT, ELYSIUM, VOLO','7.jpg',NULL,8),(42,8,'Гостинная Lost','1.jpg',NULL,1),(43,8,'Спальня Trance','2.jpg',NULL,2),(44,8,'Ванна Timeless','3.jpg',NULL,3),(45,8,'Спальня Атлантис','4.jpg',NULL,4),(46,8,'Гостинная Groove','5.jpg',NULL,5),(47,8,'Гостинная Jasper','6.jpg',NULL,6),(48,8,'Гостинная Luige','7.jpg',NULL,7),(49,8,'Гостинная Anel','8.jpg',NULL,8),(50,4,'Гостиная','8840036004.jpg','2019-05-22 18:07:59',1),(51,4,'Кухня','7528226981.jpg','2019-05-22 18:07:59',2),(52,4,'Холл','7744555758.jpg','2019-05-22 18:07:59',3),(53,9,'Гостиная Steve','7526914328.jpg','2019-05-17 13:06:43',1),(54,9,'Ванная Paolo','3235423321.jpg','2019-05-17 13:06:43',2),(55,9,'Гостиная Max','9893804574.jpg','2019-05-17 13:06:43',3),(56,9,'Кухня Michael','3109549512.jpg','2019-05-17 13:06:43',4);
/*!40000 ALTER TABLE `card_collection_interior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_layout`
--

DROP TABLE IF EXISTS `card_collection_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_layout` (
                                          `id` int(11) NOT NULL AUTO_INCREMENT,
                                          `collection_id` int(11) DEFAULT NULL,
                                          `position` int(11) DEFAULT NULL,
                                          `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `updated` datetime DEFAULT NULL,
                                          PRIMARY KEY (`id`),
                                          KEY `IDX_CB3A16C0514956FD` (`collection_id`),
                                          CONSTRAINT `FK_CB3A16C0514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_layout`
--

LOCK TABLES `card_collection_layout` WRITE;
/*!40000 ALTER TABLE `card_collection_layout` DISABLE KEYS */;
INSERT INTO `card_collection_layout` VALUES (1,1,1,'3537265006.gif','2019-06-04 13:45:23'),(2,1,2,'2516078140.gif','2019-06-04 13:45:23'),(3,1,3,'9410384980.gif','2019-06-04 13:45:23'),(4,1,4,'6991542164.gif','2019-06-04 13:45:23'),(5,1,5,'7757201482.gif','2019-06-04 13:45:23'),(6,1,6,'1870946643.gif','2019-06-04 13:45:23'),(7,1,7,'2051188204.gif','2019-06-04 13:45:23'),(8,2,1,'6608900028.gif','2019-06-04 13:39:00'),(9,2,2,'4719714997.gif','2019-06-04 13:39:00'),(10,8,1,'3134185859.png','2019-06-04 14:18:47'),(11,8,2,'6055287079.png','2019-06-04 14:18:47'),(12,8,3,'1089911499.png','2019-06-04 14:18:47'),(13,8,4,'2039754781.png','2019-06-04 14:18:47'),(14,8,5,'2052381361.png','2019-06-04 14:18:47'),(15,7,1,'1192342656.gif','2019-06-04 13:25:02'),(17,7,2,'9138008623.gif','2019-06-04 13:30:28'),(18,7,3,'9378839661.gif','2019-06-04 13:27:07'),(20,7,4,'4032063502.gif','2019-06-04 13:27:07'),(21,7,5,'5871064918.gif','2019-06-04 13:27:59'),(22,7,6,'8778163898.gif','2019-06-04 13:27:59'),(23,7,7,'3278546092.gif','2019-06-04 13:27:59'),(24,7,8,'9520171970.gif','2019-06-04 13:27:59'),(25,7,9,'9879708343.gif','2019-06-04 13:30:28'),(26,7,10,'8630075216.gif','2019-06-04 13:30:28'),(27,7,11,'1080807120.gif','2019-06-04 13:30:28'),(28,7,12,'3320158900.gif','2019-06-04 13:30:28'),(29,7,13,'8766046581.gif','2019-06-04 13:30:28'),(30,7,14,'4622750655.gif','2019-06-04 13:30:28'),(31,7,15,'2542450171.gif','2019-06-04 13:30:28'),(32,7,16,'6069328894.gif','2019-06-04 13:30:28'),(33,7,17,'3788562472.gif','2019-06-04 13:31:47'),(34,7,18,'8194629196.gif','2019-06-04 13:31:47'),(35,7,19,'8274451738.gif','2019-06-04 13:31:47'),(36,7,20,'2618632007.gif','2019-06-04 13:31:47'),(37,7,21,'9427419240.gif','2019-06-04 13:31:47'),(38,7,22,'1712517253.gif','2019-06-04 13:31:47'),(39,7,23,'1772526673.gif','2019-06-04 13:33:16'),(40,7,24,'1817973875.gif','2019-06-04 13:33:16'),(41,7,25,'9898557193.gif','2019-06-04 13:33:16'),(42,7,26,'4379393411.gif','2019-06-04 13:33:16'),(43,7,27,'1592458406.gif','2019-06-04 13:33:16'),(44,7,28,'2136667766.gif','2019-06-04 13:33:16'),(45,7,29,'6763398071.gif','2019-06-04 13:33:16'),(46,7,30,'8726711905.gif','2019-06-04 13:33:16'),(47,5,1,'4034519763.gif','2019-06-04 13:36:20'),(48,5,2,'3573123133.gif','2019-06-04 13:36:20'),(49,5,3,'5957176037.gif','2019-06-04 13:36:20'),(50,5,4,'4321080979.gif','2019-06-04 13:36:37'),(51,2,3,'6195194889.gif','2019-06-04 13:39:00'),(52,2,4,'5965484410.gif','2019-06-04 13:39:00'),(53,2,5,'6901966786.gif','2019-06-04 13:42:38'),(54,2,6,'8197845937.gif','2019-06-04 13:42:38'),(55,2,7,'7216948448.gif','2019-06-04 13:42:38'),(56,2,8,'4063173623.gif','2019-06-04 13:42:38'),(57,2,9,'7216612254.gif','2019-06-04 13:42:38'),(58,2,10,'3664133231.gif','2019-06-04 13:42:38'),(59,2,11,'7694049127.gif','2019-06-04 13:42:38'),(60,2,12,'2093405709.gif','2019-06-04 13:42:38'),(61,1,8,'9042861392.gif','2019-06-04 13:45:23'),(62,1,9,'6533261292.gif','2019-06-04 13:45:23'),(63,1,10,'2925795414.gif','2019-06-04 13:45:23'),(64,1,11,'1157973952.gif','2019-06-04 13:51:17'),(65,1,12,'1100247995.gif','2019-06-04 13:51:17'),(66,1,13,'9151754947.gif','2019-06-04 13:51:17'),(67,1,14,'1493310007.gif','2019-06-04 13:51:17'),(68,1,15,'9321414655.gif','2019-06-04 13:51:17'),(69,1,16,'2082869615.gif','2019-06-04 13:51:17'),(70,1,17,'7301504373.gif','2019-06-04 13:51:17'),(71,1,18,'6130509690.gif','2019-06-04 13:51:17'),(72,1,19,'2673960646.gif','2019-06-04 13:51:17'),(73,1,20,'3722886140.gif','2019-06-04 13:51:17'),(74,1,21,'4472416919.gif','2019-06-04 13:51:17'),(75,1,22,'8556969531.gif','2019-06-04 13:51:17'),(76,1,23,'4373634936.gif','2019-06-04 13:51:17'),(77,1,24,'9064918291.gif','2019-06-04 13:51:17'),(78,1,25,'3667731319.gif','2019-06-04 13:53:22'),(79,1,26,'3048777412.gif','2019-06-04 13:53:22'),(80,1,27,'1684470187.gif','2019-06-04 13:53:22'),(81,1,28,'1820852456.gif','2019-06-04 13:53:22'),(82,1,29,'3501248697.gif','2019-06-04 13:53:22'),(83,1,30,'1039184982.gif','2019-06-04 13:53:22'),(84,1,31,'7272554368.gif','2019-06-04 13:53:22'),(85,1,32,'3651682410.gif','2019-06-04 13:53:22'),(92,3,1,'4307105089.gif','2019-06-04 13:57:14'),(93,3,2,'3074877364.gif','2019-06-04 13:57:14'),(94,3,3,'5010756026.gif','2019-06-04 13:57:14'),(98,3,4,'3801481282.gif','2019-06-04 13:58:27'),(99,3,5,'9997085018.gif','2019-06-04 13:58:42'),(100,3,6,'2492819899.gif','2019-06-04 13:58:55'),(101,3,7,'3399077232.gif','2019-06-04 13:59:17'),(102,3,8,'5809802238.gif','2019-06-04 14:00:50'),(103,3,9,'2292746756.gif','2019-06-04 14:01:47'),(104,3,10,'5101571788.gif','2019-06-04 14:02:10'),(105,3,11,'1861389069.gif','2019-06-04 14:02:47'),(106,3,12,'2585408833.gif','2019-06-04 14:03:38'),(107,9,1,'9657616313.png','2019-06-04 14:19:58'),(108,9,2,'4113324488.png','2019-06-04 14:19:58'),(109,9,3,'3384543721.png','2019-06-04 14:19:58'),(110,9,4,'9442824069.png','2019-06-04 14:19:58'),(111,9,5,'2243188686.png','2019-06-04 14:19:58');
/*!40000 ALTER TABLE `card_collection_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_media_document`
--

DROP TABLE IF EXISTS `card_collection_media_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_media_document` (
                                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `collection_id` int(11) DEFAULT NULL,
                                                  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                                  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                                  `size` int(11) DEFAULT NULL,
                                                  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                                  `updated` datetime DEFAULT NULL,
                                                  `position` int(11) DEFAULT NULL,
                                                  PRIMARY KEY (`id`),
                                                  KEY `IDX_94B009E9514956FD` (`collection_id`),
                                                  CONSTRAINT `FK_94B009E9514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_media_document`
--

LOCK TABLES `card_collection_media_document` WRITE;
/*!40000 ALTER TABLE `card_collection_media_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `card_collection_media_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_media_youtube`
--

DROP TABLE IF EXISTS `card_collection_media_youtube`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_media_youtube` (
                                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                                 `collection_id` int(11) DEFAULT NULL,
                                                 `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                                 `href` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                                 `position` int(11) DEFAULT NULL,
                                                 PRIMARY KEY (`id`),
                                                 KEY `IDX_40EB5815514956FD` (`collection_id`),
                                                 CONSTRAINT `FK_40EB5815514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_media_youtube`
--

LOCK TABLES `card_collection_media_youtube` WRITE;
/*!40000 ALTER TABLE `card_collection_media_youtube` DISABLE KEYS */;
INSERT INTO `card_collection_media_youtube` VALUES (2,1,'Что такое Art Vinyl?','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(3,1,'Новое поколение напольный покрытий','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',2),(4,1,'Подготовка основания','https://www.youtube.com/watch?v=CweBwNjrnog&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=4',3),(5,1,'Укладка Art Vinyl','https://www.youtube.com/embed/k8soJlPUig8',4),(6,2,'Что такое Art Vinyl?','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(7,2,'Новое поколение напольный покрытий','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',2),(8,2,'Подготовка основания','https://www.youtube.com/watch?v=CweBwNjrnog&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=4',3),(9,2,'Укладка Art Vinyl','https://www.youtube.com/embed/k8soJlPUig8',4),(10,3,'Что такое Art Vinyl?','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(11,3,'Новое поколение напольный покрытий','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',2),(12,3,'Подготовка основания','https://www.youtube.com/watch?v=CweBwNjrnog&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=4',3),(13,3,'Укладка Art Vinyl','https://www.youtube.com/embed/k8soJlPUig8',4),(14,4,'Что такое Art Vinyl?','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(15,4,'Новое поколение напольный покрытий','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',2),(16,4,'Подготовка основания','https://www.youtube.com/watch?v=CweBwNjrnog&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=4',3),(17,4,'Укладка Art Vinyl','https://www.youtube.com/embed/k8soJlPUig8',4),(18,5,'Что такое Art Vinyl?','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(19,5,'Новое поколение напольный покрытий','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',2),(20,5,'Подготовка основания','https://www.youtube.com/watch?v=CweBwNjrnog&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=4',3),(21,5,'Укладка Art Vinyl','https://www.youtube.com/embed/k8soJlPUig8',4),(22,6,'Что такое Art Vinyl?','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(23,6,'Новое поколение напольный покрытий','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',2),(24,6,'Подготовка основания','https://www.youtube.com/watch?v=CweBwNjrnog&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=4',3),(25,6,'Укладка Art Vinyl','https://www.youtube.com/embed/k8soJlPUig8',4),(26,7,'Что такое Art Vinyl?','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(27,7,'Новое поколение напольный покрытий','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',2),(28,7,'Подготовка основания','https://www.youtube.com/watch?v=CweBwNjrnog&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=4',3),(29,7,'Укладка Art Vinyl','https://www.youtube.com/embed/k8soJlPUig8',4),(30,8,'Что такое Art Vinyl?','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(31,8,'Новое поколение напольный покрытий','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',2),(33,8,'Укладка Art Vinyl','https://www.youtube.com/embed/Xyky7AyO-RU',3),(34,9,'Что такое Art Vinyl?','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(35,9,'Новое поколение напольный покрытий','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',2),(37,9,'Укладка Art Vinyl','https://www.youtube.com/embed/Xyky7AyO-RU',3);
/*!40000 ALTER TABLE `card_collection_media_youtube` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_prop`
--

DROP TABLE IF EXISTS `card_collection_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_prop` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `updated` datetime DEFAULT NULL,
                                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_prop`
--

LOCK TABLES `card_collection_prop` WRITE;
/*!40000 ALTER TABLE `card_collection_prop` DISABLE KEYS */;
INSERT INTO `card_collection_prop` VALUES (1,'100% влагостойкость','9575721258.svg','2019-05-17 10:38:04'),(2,'Долговечность и износостойкость','5219885148.svg','2019-05-28 13:10:25'),(3,'Комфорт при эксплуатации','7630936204.svg','2019-05-28 13:10:13'),(4,'Экологичность и безопасность','1924762822.svg','2019-05-17 14:59:57'),(5,'Удобство укладки и ухода','9991730622.svg','2019-05-28 13:11:12'),(6,'Удобная транспортировка и хранение','7825983405.svg','2019-05-17 10:15:06'),(7,'Дизайнерские возможности','9015615684.svg','2019-05-17 14:43:31'),(8,'Возможность использовать вместе с системами подогрева пола','9478536534.svg','2019-05-17 10:28:55'),(9,'Класс пожарной опасности (ФЗ-123) КМ2','8230059574.svg','2019-05-17 10:29:05'),(10,'Два формата планок','3823849713.svg','2019-05-17 10:29:24'),(11,'Замковое соединение - легкая укладка','8587910196.svg','2019-05-17 10:29:38'),(14,'4-х сторонняя фаска','2939199021.svg','2019-05-24 14:50:25');
/*!40000 ALTER TABLE `card_collection_prop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_prop_rels`
--

DROP TABLE IF EXISTS `card_collection_prop_rels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_prop_rels` (
                                             `id` int(11) NOT NULL AUTO_INCREMENT,
                                             `collection_id` int(11) DEFAULT NULL,
                                             `prop_id` int(11) DEFAULT NULL,
                                             `position` int(11) DEFAULT NULL,
                                             PRIMARY KEY (`id`),
                                             KEY `IDX_1A76D874514956FD` (`collection_id`),
                                             KEY `IDX_1A76D874DEB3FFBD` (`prop_id`),
                                             CONSTRAINT `FK_1A76D874514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`),
                                             CONSTRAINT `FK_1A76D874DEB3FFBD` FOREIGN KEY (`prop_id`) REFERENCES `card_collection_prop` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_prop_rels`
--

LOCK TABLES `card_collection_prop_rels` WRITE;
/*!40000 ALTER TABLE `card_collection_prop_rels` DISABLE KEYS */;
INSERT INTO `card_collection_prop_rels` VALUES (1,1,1,1),(2,1,4,2),(3,1,8,3),(4,1,9,4),(5,2,1,1),(6,2,2,2),(7,2,8,3),(8,2,9,4),(9,3,1,1),(10,3,4,2),(11,3,8,3),(12,3,9,4),(13,4,1,1),(14,4,7,2),(15,4,4,3),(16,4,8,4),(17,5,1,1),(18,5,2,2),(19,5,4,3),(20,5,8,4),(21,5,9,5),(22,6,1,1),(23,6,2,2),(24,6,4,3),(25,6,7,4),(26,6,8,5),(27,6,9,6),(28,7,1,1),(29,7,4,2),(30,7,8,3),(31,7,10,4),(32,8,1,1),(33,8,2,2),(34,8,5,3),(35,8,11,4),(36,9,1,1),(37,9,2,2),(38,9,5,4),(39,9,3,5),(40,5,10,6),(41,1,14,5),(42,2,14,5),(43,3,14,5),(44,5,14,7),(45,8,14,5),(46,9,14,6),(47,9,4,3),(48,9,11,7);
/*!40000 ALTER TABLE `card_collection_prop_rels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_target`
--

DROP TABLE IF EXISTS `card_collection_target`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_target` (
                                          `id` int(11) NOT NULL AUTO_INCREMENT,
                                          `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `updated` datetime DEFAULT NULL,
                                          `gicon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_target`
--

LOCK TABLES `card_collection_target` WRITE;
/*!40000 ALTER TABLE `card_collection_target` DISABLE KEYS */;
INSERT INTO `card_collection_target` VALUES (1,'Офис','1.svg','2019-05-24 14:15:28','5855218308.svg'),(2,'Магазин','8077173013.svg','2019-05-24 14:17:01','4373480293.svg'),(3,'Учебные заведения','1469497268.svg','2019-05-24 14:17:10','1706922497.svg'),(4,'Гостиница','7018295398.svg','2019-05-24 14:17:21','6174577548.svg'),(5,'Детская','5.svg','2019-05-24 14:17:27','3757456321.svg'),(6,'Гостиная','6.svg','2019-05-24 14:17:34','7916043469.svg'),(7,'Прихожая','7.svg','2019-05-24 14:17:43','5222705340.svg'),(8,'Спальня','8.svg','2019-05-24 14:17:49','9891175321.svg'),(9,'Кухня','9.svg','2019-05-24 14:17:56','8948413641.svg');
/*!40000 ALTER TABLE `card_collection_target` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_target_rels`
--

DROP TABLE IF EXISTS `card_collection_target_rels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_target_rels` (
                                               `id` int(11) NOT NULL AUTO_INCREMENT,
                                               `collection_id` int(11) DEFAULT NULL,
                                               `target_id` int(11) DEFAULT NULL,
                                               `position` int(11) DEFAULT NULL,
                                               PRIMARY KEY (`id`),
                                               KEY `IDX_4CC17885514956FD` (`collection_id`),
                                               KEY `IDX_4CC17885158E0B66` (`target_id`),
                                               CONSTRAINT `FK_4CC17885158E0B66` FOREIGN KEY (`target_id`) REFERENCES `card_collection_target` (`id`),
                                               CONSTRAINT `FK_4CC17885514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_target_rels`
--

LOCK TABLES `card_collection_target_rels` WRITE;
/*!40000 ALTER TABLE `card_collection_target_rels` DISABLE KEYS */;
INSERT INTO `card_collection_target_rels` VALUES (3,1,2,2),(4,1,3,3),(5,1,4,4),(7,1,5,5),(8,1,6,6),(9,3,1,1),(10,3,2,2),(11,3,3,3),(12,3,4,4),(13,3,9,9),(14,3,5,5),(15,3,6,6),(16,3,7,7),(17,3,8,8),(22,4,9,5),(23,4,5,1),(24,4,6,2),(25,4,7,3),(26,4,8,4),(27,5,1,1),(28,5,2,2),(29,5,3,3),(30,5,4,4),(31,5,5,5),(32,5,6,6),(33,5,7,7),(34,5,8,8),(35,5,9,9),(36,6,1,1),(37,6,2,2),(38,6,3,3),(39,6,4,4),(40,6,5,5),(41,6,6,6),(42,6,7,7),(43,6,8,8),(44,6,9,9),(45,7,8,5),(46,7,5,2),(47,7,6,3),(48,7,7,4),(49,7,9,6),(50,8,8,5),(51,8,5,2),(52,8,6,3),(53,8,7,4),(54,8,9,6),(55,8,1,1),(56,9,5,1),(57,9,6,2),(58,9,7,3),(59,9,8,4),(60,9,9,5),(65,1,7,7),(66,2,1,1),(67,2,5,2),(68,2,6,3),(69,2,7,4),(70,1,8,8),(71,1,9,9),(73,7,1,1),(77,1,1,1);
/*!40000 ALTER TABLE `card_collection_target_rels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_collection_youtube`
--

DROP TABLE IF EXISTS `card_collection_youtube`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_collection_youtube` (
                                           `id` int(11) NOT NULL AUTO_INCREMENT,
                                           `collection_id` int(11) DEFAULT NULL,
                                           `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                           `href` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                           `position` int(11) DEFAULT NULL,
                                           PRIMARY KEY (`id`),
                                           KEY `IDX_25E9D8AD514956FD` (`collection_id`),
                                           CONSTRAINT `FK_25E9D8AD514956FD` FOREIGN KEY (`collection_id`) REFERENCES `card_collection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_collection_youtube`
--

LOCK TABLES `card_collection_youtube` WRITE;
/*!40000 ALTER TABLE `card_collection_youtube` DISABLE KEYS */;
INSERT INTO `card_collection_youtube` VALUES (1,NULL,'1','1',NULL),(2,NULL,'2','2',NULL);
/*!40000 ALTER TABLE `card_collection_youtube` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_image`
--

DROP TABLE IF EXISTS `card_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_image` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `card_id` int(11) DEFAULT NULL,
                              `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `updated` datetime DEFAULT NULL,
                              `position` int(11) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `IDX_FD09F5994ACC9A20` (`card_id`),
                              CONSTRAINT `FK_FD09F5994ACC9A20` FOREIGN KEY (`card_id`) REFERENCES `card` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_image`
--

LOCK TABLES `card_image` WRITE;
/*!40000 ALTER TABLE `card_image` DISABLE KEYS */;
INSERT INTO `card_image` VALUES (1,1,'3367106026.jpg','2019-06-19 18:13:23',1),(2,1,'2.jpg',NULL,2),(3,2,'3650283258.jpg','2019-06-19 18:13:31',1),(4,2,'2.jpg',NULL,2),(5,3,'2826015832.jpg','2019-06-19 18:13:36',1),(6,3,'2.jpg',NULL,2),(7,4,'6681532432.jpg','2019-06-19 18:13:41',1),(8,5,'4253465826.jpg','2019-06-19 18:13:47',1),(9,6,'3283645879.jpg','2019-06-19 18:13:55',1),(10,6,'2.jpg',NULL,2),(11,7,'8088914890.jpg','2019-06-19 18:14:01',1),(12,8,'9166449135.jpg','2019-06-19 18:14:07',1),(13,8,'2.jpg',NULL,2),(14,9,'8007711017.jpg','2019-06-19 18:14:15',1),(15,9,'5744583415.jpg','2019-04-19 08:47:47',2),(16,10,'1907111956.jpg','2019-06-19 18:14:21',1),(17,10,'4312260142.jpg','2019-04-19 08:50:14',2),(18,11,'6688653091.jpg','2019-06-19 18:12:48',1),(19,11,'2.jpg',NULL,2),(20,12,'6450632826.jpg','2019-06-19 18:12:39',1),(21,12,'2.jpg',NULL,2),(22,13,'8223051656.jpg','2019-06-19 18:12:31',1),(23,13,'2.jpg',NULL,2),(24,14,'1.jpg',NULL,1),(25,14,'2.jpg',NULL,2),(26,15,'1.jpg',NULL,NULL),(27,15,'2.jpg',NULL,NULL),(28,16,'6980260810.jpg','2019-04-18 17:06:43',NULL),(29,16,'2.jpg',NULL,NULL),(30,17,'2170195096.jpg','2019-04-18 17:07:57',NULL),(31,17,'2.jpg',NULL,NULL),(32,18,'5537745368.jpg','2019-04-18 17:08:50',NULL),(33,18,'2.jpg',NULL,NULL),(34,19,'5450808747.jpg','2019-04-18 17:10:03',NULL),(35,19,'2.jpg',NULL,NULL),(36,20,'2496194299.jpg','2019-04-18 17:11:51',NULL),(37,20,'2.jpg',NULL,NULL),(38,21,'2693180288.jpg','2019-04-18 17:12:34',NULL),(39,21,'2.jpg',NULL,NULL),(40,22,'1733695242.jpg','2019-04-18 17:14:28',NULL),(41,22,'2.jpg',NULL,NULL),(42,23,'7306350162.jpg','2019-04-18 17:15:25',NULL),(43,23,'5087213980.jpg','2019-04-19 10:24:34',NULL),(44,24,'1.jpg',NULL,NULL),(45,24,'2.jpg',NULL,NULL),(46,25,'1.jpg',NULL,NULL),(47,25,'2.jpg',NULL,NULL),(48,26,'1.jpg',NULL,NULL),(49,26,'2.jpg',NULL,NULL),(50,27,'1.jpg',NULL,NULL),(51,27,'2.jpg',NULL,NULL),(52,28,'1.jpg',NULL,NULL),(53,28,'2.jpg',NULL,NULL),(54,29,'1.jpg',NULL,NULL),(55,29,'2.jpg',NULL,NULL),(56,30,'1.jpg',NULL,NULL),(57,30,'2.jpg',NULL,NULL),(58,31,'1.jpg',NULL,NULL),(59,31,'2.jpg',NULL,NULL),(60,32,'1.jpg',NULL,NULL),(61,32,'2.jpg',NULL,NULL),(62,33,'1.jpg',NULL,NULL),(63,33,'2.jpg',NULL,NULL),(64,34,'1.jpg',NULL,NULL),(65,35,'1.jpg',NULL,NULL),(66,36,'1.jpg',NULL,NULL),(67,37,'1.jpg',NULL,NULL),(68,38,'1.jpg',NULL,NULL),(69,39,'1.jpg',NULL,NULL),(70,40,'1.jpg',NULL,NULL),(71,41,'1.jpg',NULL,NULL),(72,41,'8500379795.jpg','2019-04-19 11:40:50',NULL),(73,42,'1.jpg',NULL,NULL),(74,42,'2.jpg',NULL,NULL),(75,43,'1.jpg',NULL,NULL),(76,43,'2.jpg',NULL,NULL),(77,44,'1.jpg',NULL,NULL),(78,44,'2.jpg',NULL,NULL),(79,45,'1.jpg',NULL,NULL),(80,45,'7695784019.jpg','2019-04-19 11:42:51',NULL),(81,46,'1.jpg',NULL,NULL),(82,46,'2.jpg',NULL,NULL),(83,47,'1.jpg',NULL,NULL),(84,47,'2.jpg',NULL,NULL),(85,48,'1.jpg',NULL,NULL),(86,48,'2.jpg',NULL,NULL),(87,49,'1.jpg',NULL,NULL),(88,49,'2.jpg',NULL,NULL),(89,50,'1.jpg',NULL,NULL),(90,50,'6867584598.jpg','2019-04-19 11:58:33',NULL),(91,51,'1.jpg',NULL,NULL),(92,51,'2.jpg',NULL,NULL),(93,52,'1.jpg',NULL,NULL),(94,52,'2.jpg',NULL,NULL),(95,53,'1.jpg',NULL,NULL),(96,53,'2.jpg',NULL,NULL),(97,54,'1.jpg',NULL,NULL),(98,54,'2.jpg',NULL,NULL),(99,55,'1.jpg',NULL,NULL),(100,55,'2.jpg',NULL,NULL),(101,56,'1.jpg',NULL,NULL),(102,56,'2.jpg',NULL,NULL),(103,57,'1.jpg',NULL,NULL),(104,57,'2.jpg',NULL,NULL),(105,58,'1.jpg',NULL,NULL),(106,58,'2.jpg',NULL,NULL),(107,59,'1.jpg',NULL,NULL),(108,59,'5324919368.jpg','2019-04-19 12:10:53',NULL),(109,60,'1.jpg',NULL,NULL),(110,60,'4662896240.jpg','2019-04-19 12:15:27',NULL),(111,61,'1.jpg',NULL,NULL),(112,61,'2.jpg',NULL,NULL),(113,62,'1.jpg',NULL,NULL),(114,62,'2.jpg',NULL,NULL),(115,63,'1.jpg',NULL,NULL),(116,63,'1210005127.jpg','2019-04-19 14:10:20',NULL),(117,64,'1.jpg',NULL,NULL),(118,64,'2.jpg',NULL,NULL),(119,65,'1.jpg',NULL,NULL),(120,65,'2.jpg',NULL,NULL),(121,75,'1.jpg',NULL,NULL),(122,75,'2.jpg',NULL,NULL),(123,76,'1.jpg',NULL,NULL),(124,76,'2.jpg',NULL,NULL),(125,77,'1.jpg',NULL,NULL),(126,77,'2.jpg',NULL,NULL),(127,78,'1.jpg',NULL,NULL),(128,78,'2.jpg',NULL,NULL),(129,79,'1.jpg',NULL,NULL),(130,79,'2.jpg',NULL,NULL),(133,81,'1.jpg',NULL,NULL),(134,81,'2.jpg',NULL,NULL),(137,83,'1.jpg',NULL,NULL),(138,83,'2.jpg',NULL,NULL),(139,84,'1.jpg',NULL,NULL),(140,84,'2.jpg',NULL,NULL),(143,86,'1.jpg',NULL,NULL),(144,86,'2.jpg',NULL,NULL),(145,87,'1.jpg',NULL,NULL),(146,87,'2.jpg',NULL,NULL),(147,88,'1.jpg',NULL,NULL),(148,88,'2.jpg',NULL,NULL),(149,89,'1.jpg',NULL,NULL),(150,89,'2.jpg',NULL,NULL),(151,90,'1.jpg',NULL,NULL),(152,90,'2.jpg',NULL,NULL),(155,92,'1.jpg',NULL,NULL),(156,92,'2.jpg',NULL,NULL),(157,93,'9461421827.jpg','2019-06-19 18:22:18',1),(158,93,'2.jpg',NULL,2),(159,94,'4488712060.jpg','2019-06-19 18:28:00',1),(161,95,'8379048123.jpg','2019-06-19 18:30:49',1),(163,96,'7645942586.jpg','2019-06-19 18:33:48',1),(164,96,'2.jpg',NULL,2),(165,97,'8261667681.jpg','2019-06-19 18:34:27',1),(166,97,'2.jpg',NULL,2),(167,98,'5595962399.jpg','2019-06-19 18:34:58',1),(168,98,'3583671544.jpg','2019-06-19 18:35:45',2),(169,99,'6823588065.jpg','2019-06-19 18:38:35',1),(170,99,'2.jpg',NULL,2),(171,100,'4699662811.jpg','2019-06-19 18:39:24',1),(172,101,'3082337114.jpg','2019-06-19 18:41:17',1),(173,102,'5383557012.jpg','2019-06-20 09:54:23',1),(174,102,'2.jpg',NULL,2),(175,103,'3596854845.jpg','2019-06-20 09:56:04',1),(176,103,'2.jpg',NULL,2),(177,4,'3903497419.jpg','2019-04-19 08:22:42',2),(178,5,'2495642240.jpg','2019-04-19 08:42:06',2),(179,7,'3586015687.jpg','2019-04-19 08:44:28',2),(180,66,'1759010871.png','2019-04-19 14:32:21',NULL),(181,66,'9278010618.jpg','2019-04-19 14:31:39',NULL),(182,67,'3587649742.jpg','2019-04-19 14:33:23',NULL),(183,67,'6803895417.jpg','2019-04-19 14:33:40',NULL),(184,68,'4585055715.jpg','2019-04-19 14:34:29',NULL),(185,68,'4884922109.jpg','2019-04-19 14:34:38',NULL),(186,69,'1797587372.jpg','2019-04-19 14:35:17',NULL),(187,69,'5972362672.jpg','2019-04-19 14:35:25',NULL),(188,70,'4570034558.jpg','2019-04-19 14:36:18',NULL),(189,70,'5183112175.jpg','2019-04-19 14:36:27',NULL),(190,71,'2730172458.jpg','2019-04-19 14:36:46',NULL),(191,71,'9182044763.jpg','2019-04-19 14:36:55',NULL),(192,72,'9979498275.jpg','2019-04-19 14:37:18',NULL),(193,72,'5984577950.jpg','2019-04-19 14:37:27',NULL),(194,73,'8920572999.jpg','2019-04-19 14:37:50',NULL),(195,73,'4248754422.jpg','2019-04-19 14:37:59',NULL),(196,74,'3312050475.jpg','2019-04-23 14:05:21',NULL),(197,74,'2811006377.jpg','2019-04-19 14:38:27',NULL),(198,100,'6104005258.jpg','2019-06-19 18:39:24',2),(199,101,'8157970024.jpg','2019-06-19 18:41:17',2),(200,104,'4348261985.jpg','2019-06-20 10:21:04',1),(201,104,'8039466740.jpg','2019-05-17 16:10:07',2),(202,105,'7409247837.jpg','2019-06-20 10:22:45',1),(203,105,'2007097765.jpg','2019-05-17 16:10:47',2),(204,106,'1719494567.jpg','2019-05-17 16:11:40',2),(205,106,'2012060869.jpg','2019-06-20 10:23:53',1),(206,107,'2828524969.jpg','2019-06-20 10:24:47',1),(207,107,'3779597363.jpg','2019-05-17 16:13:04',2),(208,108,'3798346312.jpg','2019-06-20 10:25:40',1),(209,108,'1628208359.jpg','2019-05-17 16:14:04',2),(210,109,'1527857960.jpg','2019-06-20 10:26:30',1),(211,109,'4094105917.jpg','2019-05-17 16:15:09',2),(212,110,'8686823319.jpg','2019-06-20 10:27:55',1),(213,110,'6950486051.jpg','2019-05-17 16:15:54',2),(214,111,'4077377404.jpg','2019-06-20 10:28:45',1),(215,111,'5840272705.jpg','2019-05-17 16:17:20',2),(216,112,'7693885509.jpg','2019-06-20 10:29:50',1),(217,112,'3609227044.jpg','2019-05-17 16:18:10',2),(218,113,'3155833119.jpg','2019-06-20 10:30:40',1),(219,113,'5329952311.jpg','2019-05-17 16:19:28',2),(220,114,'2645604034.jpg','2019-06-20 10:31:31',1),(221,114,'7197907436.jpg','2019-05-17 16:22:19',2),(222,115,'4186638520.jpg','2019-06-20 10:32:21',1),(223,115,'6692875268.jpg','2019-05-17 16:23:24',2),(224,116,'3349156396.jpg','2019-06-20 10:33:15',1),(225,116,'3265675188.jpg','2019-05-17 16:25:07',2),(226,117,'3718353350.jpg','2019-06-20 10:34:02',1),(227,117,'1991498097.jpg','2019-05-17 16:26:00',2),(229,34,'7613238520.jpg','2019-05-17 14:15:26',NULL),(230,35,'2227253138.jpg','2019-05-17 14:15:34',NULL),(231,36,'4986859453.jpg','2019-05-17 14:15:37',NULL),(232,37,'4920905693.jpg','2019-05-17 14:15:43',NULL),(233,118,'1164549766.jpg','2019-05-17 14:13:31',NULL),(234,118,'2921509139.jpg','2019-05-17 14:13:31',NULL),(235,119,'9044223533.jpg','2019-05-17 14:14:11',NULL),(236,119,'2277137737.jpg','2019-05-17 14:14:11',NULL),(237,120,'6128860845.jpg','2019-05-17 14:09:09',NULL),(238,120,'8328811656.jpg','2019-05-17 14:09:09',NULL),(239,38,'4611176456.jpg','2019-05-17 14:15:49',NULL),(240,39,'4024290754.jpg','2019-05-17 14:15:54',NULL),(241,40,'2798567329.jpg','2019-05-17 14:15:59',NULL),(242,121,'8237079184.jpg','2019-05-17 14:00:42',NULL),(243,121,'9101169746.jpg','2019-05-17 14:00:42',NULL),(248,124,'6067777346.jpg','2019-05-16 18:46:35',NULL),(249,124,'2099344139.jpg','2019-05-16 18:45:05',NULL),(250,126,'7553682221.jpg','2019-05-25 07:01:54',NULL),(251,126,'1196929763.jpg','2019-05-25 07:01:54',NULL),(252,127,'7941842170.jpg','2019-05-25 07:09:12',NULL),(253,127,'7594734240.jpg','2019-05-25 07:09:12',NULL),(254,128,'2488989211.jpg','2019-05-25 07:17:23',NULL),(255,128,'1405149580.jpg','2019-05-25 07:17:23',NULL),(257,130,'6477181512.jpg','2019-05-25 07:40:47',NULL),(258,130,'3233893231.jpg','2019-05-25 07:40:47',NULL),(259,131,'2678697895.jpg','2019-05-25 07:48:17',NULL),(260,131,'1344109421.jpg','2019-05-25 07:48:17',NULL),(261,132,'9068914108.jpg','2019-05-25 08:08:47',NULL),(262,132,'2760015447.jpg','2019-05-25 08:08:47',NULL),(263,95,'5655359098.jpg','2019-06-19 18:32:56',2),(264,133,'1808845046.jpg','2019-06-20 10:00:54',1);
/*!40000 ALTER TABLE `card_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_type`
--

DROP TABLE IF EXISTS `card_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_type` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_type`
--

LOCK TABLES `card_type` WRITE;
/*!40000 ALTER TABLE `card_type` DISABLE KEYS */;
INSERT INTO `card_type` VALUES (1,'плитка'),(2,'планка');
/*!40000 ALTER TABLE `card_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_value`
--

DROP TABLE IF EXISTS `card_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_value` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `char_id` int(11) DEFAULT NULL,
                              `card_id` int(11) DEFAULT NULL,
                              `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `position` int(11) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `IDX_2543A9F23FF12EF9` (`char_id`),
                              KEY `IDX_2543A9F24ACC9A20` (`card_id`),
                              CONSTRAINT `FK_2543A9F23FF12EF9` FOREIGN KEY (`char_id`) REFERENCES `card_char` (`id`),
                              CONSTRAINT `FK_2543A9F24ACC9A20` FOREIGN KEY (`card_id`) REFERENCES `card` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1403 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_value`
--

LOCK TABLES `card_value` WRITE;
/*!40000 ALTER TABLE `card_value` DISABLE KEYS */;
INSERT INTO `card_value` VALUES (1,1,1,'Плитка',1),(2,2,1,'457 мм *457 мм',2),(3,3,1,'10 шт',6),(4,4,1,'10 кг',7),(5,5,1,'Extreme Protection',8),(6,6,1,'3 мм',3),(7,7,1,'0,7 мм',4),(8,8,1,'На клей',9),(9,9,1,'KM2',10),(10,10,1,'23/34',11),(11,1,2,'Плитка',1),(12,2,2,'457 мм*457 мм',2),(13,3,2,'10 шт',5),(14,4,2,'10 кг',7),(15,5,2,'Extreme Protection',8),(16,6,2,'3 мм',4),(17,7,2,'0,7 мм',3),(18,8,2,'На клей',9),(19,9,2,'KM2',10),(20,10,2,'23/34',11),(21,1,3,'Плитка',1),(22,2,3,'457мм*457мм',2),(23,3,3,'10 шт',6),(24,4,3,'10 кг',7),(25,5,3,'Extreme Protection',8),(26,6,3,'3 мм',3),(27,7,3,'0,7 мм',4),(28,8,3,'На клей',9),(29,9,3,'KM2',10),(30,10,3,'23/34',11),(31,1,4,'Плитка',1),(32,2,4,'457мм*457мм',2),(33,3,4,'10 шт',6),(34,4,4,'10 кг.',7),(35,5,4,'Extreme Protection',8),(36,6,4,'3 мм',3),(37,7,4,'0,7 мм',4),(38,8,4,'На клей',9),(39,9,4,'KM2',10),(40,10,4,'23/34',11),(41,1,5,'Плитка',1),(42,2,5,'457мм*457мм',2),(43,3,5,'10',6),(44,4,5,'10',7),(45,5,5,'Extreme Protection',8),(46,6,5,'3 мм',4),(47,7,5,'0,7 мм',3),(48,8,5,'На клей',9),(49,9,5,'KM2',10),(50,10,5,'23/34',11),(51,1,6,'Плитка',1),(52,2,6,'457мм*457мм',2),(53,3,6,'10',6),(54,4,6,'10',7),(55,5,6,'Extreme Protection',8),(56,6,6,'3 мм',3),(57,7,6,'0,7 мм',4),(58,8,6,'На клей',9),(59,9,6,'KM2',10),(60,10,6,'23/34',11),(61,1,7,'Плитка',1),(62,2,7,'457мм*457мм',2),(63,3,7,'10 шт',6),(64,4,7,'10 кг.',7),(65,5,7,'Extreme Protection',8),(66,6,7,'3 мм',3),(67,7,7,'0,7 мм',4),(68,8,7,'На клей',9),(69,9,7,'KM2',10),(70,10,7,'23/34',11),(71,1,8,'Плитка',1),(72,2,8,'457мм*457мм',2),(73,3,8,'10 шт',6),(74,4,8,'10 кг.',7),(75,5,8,'Extreme Protection',8),(76,6,8,'3 мм',3),(77,7,8,'0,7 мм',4),(78,8,8,'На клей',9),(79,9,8,'KM2',10),(80,10,8,'23/34',11),(81,1,9,'Плитка',1),(82,2,9,'457мм*457мм',2),(83,3,9,'10 шт',6),(84,4,9,'10 кг.',7),(85,5,9,'Extreme Protection',8),(86,6,9,'3 мм',3),(87,7,9,'0,7 мм',4),(88,8,9,'На клей',9),(89,9,9,'KM2',10),(90,10,9,'23/34',11),(91,1,10,'Плитка',1),(92,2,10,'457мм*457мм',2),(93,3,10,'10 шт',6),(94,4,10,'10 кг.',7),(95,5,10,'Extreme Protection',8),(96,6,10,'3 мм',3),(97,7,10,'0,7 мм',4),(98,8,10,'На клей',9),(99,9,10,'KM2',10),(100,10,10,'23/34',11),(101,1,11,'Планка',1),(102,2,11,'152мм*914мм',2),(103,3,11,'15 шт.',6),(104,4,11,'10 кг.',7),(105,5,11,'Extreme Protection',8),(106,6,11,'3 мм',3),(107,7,11,'0,7 мм',4),(108,8,11,'На клей',9),(109,9,11,'KM2',10),(110,10,11,'23/34',11),(111,1,12,'Планка',1),(112,2,12,'152мм*914мм',4),(113,3,12,'15 шт.',6),(114,4,12,'10 кг.',7),(115,5,12,'Extreme Protection',8),(116,6,12,'3 мм',2),(117,7,12,'0,7 мм',3),(118,8,12,'На клей',9),(119,9,12,'KM2',10),(120,10,12,'23/34',11),(121,1,13,'Планка',1),(122,2,13,'152мм*914мм',2),(123,3,13,'15 шт.',6),(124,4,13,'10 кг.',7),(125,5,13,'Extreme Protection',8),(126,6,13,'3 мм',3),(127,7,13,'0,7 мм',4),(128,8,13,'На клей',9),(129,9,13,'KM2',10),(130,10,13,'23/34',11),(131,1,14,'Планка',1),(132,2,14,'152*914',2),(133,3,14,'15',3),(134,4,14,'10',4),(135,5,14,'R.Max',5),(136,6,14,'3',6),(137,7,14,'0,7',7),(138,8,14,'На клей',8),(139,9,14,'KM2',9),(140,10,14,'23/34/43',10),(141,1,15,'Планка',NULL),(142,2,15,'152*914',NULL),(143,3,15,'15',NULL),(144,4,15,'10',NULL),(145,5,15,'R.Max',NULL),(146,6,15,'3',NULL),(147,7,15,'0,7',NULL),(148,8,15,'На клей',NULL),(149,9,15,'KM2',NULL),(150,10,15,'23/34/43',NULL),(151,1,16,'Планка',NULL),(152,2,16,'152*914',NULL),(153,3,16,'15',NULL),(154,4,16,'10',NULL),(155,5,16,'R.Max',NULL),(156,6,16,'3',NULL),(157,7,16,'0,7',NULL),(158,8,16,'На клей',NULL),(159,9,16,'KM2',NULL),(160,10,16,'23/34/43',NULL),(161,1,17,'Планка',NULL),(162,2,17,'152*914',NULL),(163,3,17,'15',NULL),(164,4,17,'10',NULL),(165,5,17,'R.Max',NULL),(166,6,17,'3',NULL),(167,7,17,'0,7',NULL),(168,8,17,'На клей',NULL),(169,9,17,'KM2',NULL),(170,10,17,'23/34/43',NULL),(171,1,18,'Планка',NULL),(172,2,18,'152*914',NULL),(173,3,18,'15',NULL),(174,4,18,'10',NULL),(175,5,18,'R.Max',NULL),(176,6,18,'3',NULL),(177,7,18,'0,7',NULL),(178,8,18,'На клей',NULL),(179,9,18,'KM2',NULL),(180,10,18,'23/34/43',NULL),(181,1,19,'Планка',NULL),(182,2,19,'152*914',NULL),(183,3,19,'15',NULL),(184,4,19,'10',NULL),(185,5,19,'R.Max',NULL),(186,6,19,'3',NULL),(187,7,19,'0,7',NULL),(188,8,19,'На клей',NULL),(189,9,19,'KM2',NULL),(190,10,19,'23/34/43',NULL),(191,1,20,'Планка',NULL),(192,2,20,'152*914',NULL),(193,3,20,'15',NULL),(194,4,20,'10',NULL),(195,5,20,'R.Max',NULL),(196,6,20,'3',NULL),(197,7,20,'0,7',NULL),(198,8,20,'На клей',NULL),(199,9,20,'KM2',NULL),(200,10,20,'23/34/43',NULL),(201,1,21,'Планка',NULL),(202,2,21,'152*914',NULL),(203,3,21,'15',NULL),(204,4,21,'10',NULL),(205,5,21,'R.Max',NULL),(206,6,21,'3',NULL),(207,7,21,'0,7',NULL),(208,8,21,'На клей',NULL),(209,9,21,'KM2',NULL),(210,10,21,'23/34/43',NULL),(211,1,22,'Планка',NULL),(212,2,22,'152*914',NULL),(213,3,22,'15',NULL),(214,4,22,'10',NULL),(215,5,22,'R.Max',NULL),(216,6,22,'3',NULL),(217,7,22,'0,7',NULL),(218,8,22,'На клей',NULL),(219,9,22,'KM2',NULL),(220,10,22,'23/34/43',NULL),(221,1,23,'Планка',NULL),(222,2,23,'152*914',NULL),(223,3,23,'15',NULL),(224,4,23,'10',NULL),(225,5,23,'R.Max',NULL),(226,6,23,'3',NULL),(227,7,23,'0,7',NULL),(228,8,23,'На клей',NULL),(229,9,23,'KM2',NULL),(230,10,23,'23/34/43',NULL),(231,1,24,'Планка',NULL),(232,2,24,'152*914',NULL),(233,3,24,'15',NULL),(234,4,24,'8,8',NULL),(235,5,24,'Extreme Protection',NULL),(236,6,24,'2,7',NULL),(237,7,24,'0,55',NULL),(238,8,24,'На клей',NULL),(239,9,24,'KM2',NULL),(240,10,24,'23/33/42',NULL),(241,1,25,'Планка',NULL),(242,2,25,'152*914',NULL),(243,3,25,'15',NULL),(244,4,25,'8,8',NULL),(245,5,25,'Extreme Protection',NULL),(246,6,25,'2,7',NULL),(247,7,25,'0,55',NULL),(248,8,25,'На клей',NULL),(249,9,25,'KM2',NULL),(250,10,25,'23/33/42',NULL),(251,1,26,'Планка',NULL),(252,2,26,'152*914',NULL),(253,3,26,'15',NULL),(254,4,26,'8,8',NULL),(255,5,26,'Extreme Protection',NULL),(256,6,26,'2,7',NULL),(257,7,26,'0,55',NULL),(258,8,26,'На клей',NULL),(259,9,26,'KM2',NULL),(260,10,26,'23/33/42',NULL),(261,1,27,'Планка',NULL),(262,2,27,'152*914',NULL),(263,3,27,'15',NULL),(264,4,27,'8,8',NULL),(265,5,27,'Extreme Protection',NULL),(266,6,27,'2,7',NULL),(267,7,27,'0,55',NULL),(268,8,27,'На клей',NULL),(269,9,27,'KM2',NULL),(270,10,27,'23/33/42',NULL),(271,1,28,'Планка',NULL),(272,2,28,'152*914',NULL),(273,3,28,'15',NULL),(274,4,28,'8,8',NULL),(275,5,28,'Extreme Protection',NULL),(276,6,28,'2,7',NULL),(277,7,28,'0,55',NULL),(278,8,28,'На клей',NULL),(279,9,28,'KM2',NULL),(280,10,28,'23/33/42',NULL),(281,1,29,'Планка',NULL),(282,2,29,'152*914',NULL),(283,3,29,'15',NULL),(284,4,29,'8,8',NULL),(285,5,29,'Extreme Protection',NULL),(286,6,29,'2,7',NULL),(287,7,29,'0,55',NULL),(288,8,29,'На клей',NULL),(289,9,29,'KM2',NULL),(290,10,29,'23/33/42',NULL),(291,1,30,'Планка',NULL),(292,2,30,'152*914',NULL),(293,3,30,'15',NULL),(294,4,30,'8,8',NULL),(295,5,30,'Extreme Protection',NULL),(296,6,30,'2,7',NULL),(297,7,30,'0,55',NULL),(298,8,30,'На клей',NULL),(299,9,30,'KM2',NULL),(300,10,30,'23/33/42',NULL),(301,1,31,'Планка',NULL),(302,2,31,'152*914',NULL),(303,3,31,'15',NULL),(304,4,31,'8,8',NULL),(305,5,31,'Extreme Protection',NULL),(306,6,31,'2,7',NULL),(307,7,31,'0,55',NULL),(308,8,31,'На клей',NULL),(309,9,31,'KM2',NULL),(310,10,31,'23/33/42',NULL),(311,1,32,'Планка',NULL),(312,2,32,'152*914',NULL),(313,3,32,'15',NULL),(314,4,32,'8,8',NULL),(315,5,32,'Extreme Protection',NULL),(316,6,32,'2,7',NULL),(317,7,32,'0,55',NULL),(318,8,32,'На клей',NULL),(319,9,32,'KM2',NULL),(320,10,32,'23/33/42',NULL),(321,1,33,'Планка',NULL),(322,2,33,'152*914',NULL),(323,3,33,'15',NULL),(324,4,33,'8,8',NULL),(325,5,33,'Extreme Protection',NULL),(326,6,33,'2,7',NULL),(327,7,33,'0,55',NULL),(328,8,33,'На клей',NULL),(329,9,33,'KM2',NULL),(330,10,33,'23/33/42',NULL),(331,1,34,'Планка',NULL),(332,2,34,'152*914',NULL),(333,3,34,'18',NULL),(334,4,34,'9',NULL),(335,5,34,'Extreme Protection',NULL),(336,6,34,'2,1',NULL),(337,7,34,'0,4',NULL),(338,8,34,'На клей',NULL),(339,9,34,'KM5',NULL),(340,10,34,'23/32/41',NULL),(341,1,35,'Планка',NULL),(342,2,35,'152*914',NULL),(343,3,35,'18',NULL),(344,4,35,'9',NULL),(345,5,35,'Extreme Protection',NULL),(346,6,35,'2,1',NULL),(347,7,35,'0,4',NULL),(348,8,35,'На клей',NULL),(349,9,35,'KM5',NULL),(350,10,35,'23/32/41',NULL),(351,1,36,'Планка',NULL),(352,2,36,'152*914',NULL),(353,3,36,'18',NULL),(354,4,36,'9',NULL),(355,5,36,'Extreme Protection',NULL),(356,6,36,'2,1',NULL),(357,7,36,'0,4',NULL),(358,8,36,'На клей',NULL),(359,9,36,'KM5',NULL),(360,10,36,'23/32/41',NULL),(361,1,37,'Планка',NULL),(362,2,37,'152*914',NULL),(363,3,37,'18',NULL),(364,4,37,'9',NULL),(365,5,37,'Extreme Protection',NULL),(366,6,37,'2,1',NULL),(367,7,37,'0,4',NULL),(368,8,37,'На клей',NULL),(369,9,37,'KM5',NULL),(370,10,37,'23/32/41',NULL),(371,1,38,'Планка',NULL),(372,2,38,'152*914',NULL),(373,3,38,'18',NULL),(374,4,38,'9',NULL),(375,5,38,'Extreme Protection',NULL),(376,6,38,'2,1',NULL),(377,7,38,'0,4',NULL),(378,8,38,'На клей',NULL),(379,9,38,'KM5',NULL),(380,10,38,'23/32/41',NULL),(381,1,39,'Планка',NULL),(382,2,39,'152*914',NULL),(383,3,39,'18',NULL),(384,4,39,'9',NULL),(385,5,39,'Extreme Protection',NULL),(386,6,39,'2,1',NULL),(387,7,39,'0,4',NULL),(388,8,39,'На клей',NULL),(389,9,39,'KM5',NULL),(390,10,39,'23/32/41',NULL),(391,1,40,'Планка',NULL),(392,2,40,'152*914',NULL),(393,3,40,'18',NULL),(394,4,40,'9',NULL),(395,5,40,'Extreme Protection',NULL),(396,6,40,'2,1',NULL),(397,7,40,'0,4',NULL),(398,8,40,'На клей',NULL),(399,9,40,'KM5',NULL),(400,10,40,'23/32/41',NULL),(401,1,41,'Плитка',NULL),(402,2,41,'457*457',NULL),(403,3,41,'10',NULL),(404,4,41,'10',NULL),(405,5,41,'Extreme Protection',NULL),(406,6,41,'3',NULL),(407,7,41,'0,7',NULL),(408,8,41,'На клей',NULL),(409,9,41,'KM2',NULL),(410,10,41,'23/34/43',NULL),(411,1,42,'Плитка',NULL),(412,2,42,'457*457',NULL),(413,3,42,'10',NULL),(414,4,42,'10',NULL),(415,5,42,'Extreme Protection',NULL),(416,6,42,'3',NULL),(417,7,42,'0,7',NULL),(418,8,42,'На клей',NULL),(419,9,42,'KM2',NULL),(420,10,42,'23/34/43',NULL),(421,1,43,'Плитка',NULL),(422,2,43,'457*457',NULL),(423,3,43,'10',NULL),(424,4,43,'10',NULL),(425,5,43,'Extreme Protection',NULL),(426,6,43,'3',NULL),(427,7,43,'0,7',NULL),(428,8,43,'На клей',NULL),(429,9,43,'KM2',NULL),(430,10,43,'23/34/43',NULL),(431,1,44,'Плитка',NULL),(432,2,44,'457*457',NULL),(433,3,44,'10',NULL),(434,4,44,'10',NULL),(435,5,44,'Extreme Protection',NULL),(436,6,44,'3',NULL),(437,7,44,'0,7',NULL),(438,8,44,'На клей',NULL),(439,9,44,'KM2',NULL),(440,10,44,'23/34/43',NULL),(441,1,45,'Плитка',NULL),(442,2,45,'457*457',NULL),(443,3,45,'10',NULL),(444,4,45,'10',NULL),(445,5,45,'Extreme Protection',NULL),(446,6,45,'3',NULL),(447,7,45,'0,7',NULL),(448,8,45,'На клей',NULL),(449,9,45,'KM2',NULL),(450,10,45,'23/34/43',NULL),(451,1,46,'Плитка',NULL),(452,2,46,'457*457',NULL),(453,3,46,'10',NULL),(454,4,46,'10',NULL),(455,5,46,'Extreme Protection',NULL),(456,6,46,'3',NULL),(457,7,46,'0,7',NULL),(458,8,46,'На клей',NULL),(459,9,46,'KM2',NULL),(460,10,46,'23/34/43',NULL),(461,1,47,'Плитка',NULL),(462,2,47,'457*457',NULL),(463,3,47,'10',NULL),(464,4,47,'10',NULL),(465,5,47,'Extreme Protection',NULL),(466,6,47,'3',NULL),(467,7,47,'0,7',NULL),(468,8,47,'На клей',NULL),(469,9,47,'KM2',NULL),(470,10,47,'23/34/43',NULL),(471,1,48,'Плитка',NULL),(472,2,48,'457*457',NULL),(473,3,48,'10',NULL),(474,4,48,'10',NULL),(475,5,48,'Extreme Protection',NULL),(476,6,48,'3',NULL),(477,7,48,'0,7',NULL),(478,8,48,'На клей',NULL),(479,9,48,'KM2',NULL),(480,10,48,'23/34/43',NULL),(481,1,49,'Планка',NULL),(482,2,49,'152*914',NULL),(483,3,49,'15',NULL),(484,4,49,'10',NULL),(485,5,49,'Extreme Protection',NULL),(486,6,49,'3',NULL),(487,7,49,'0,7',NULL),(488,8,49,'На клей',NULL),(489,9,49,'KM2',NULL),(490,10,49,'23/34/43',NULL),(491,1,50,'Планка',NULL),(492,2,50,'152*914',NULL),(493,3,50,'15',NULL),(494,4,50,'10',NULL),(495,5,50,'Extreme Protection',NULL),(496,6,50,'3',NULL),(497,7,50,'0,7',NULL),(498,8,50,'На клей',NULL),(499,9,50,'KM2',NULL),(500,10,50,'23/34/43',NULL),(501,1,51,'Планка',NULL),(502,2,51,'152*914',NULL),(503,3,51,'15',NULL),(504,4,51,'10',NULL),(505,5,51,'Extreme Protection',NULL),(506,6,51,'3',NULL),(507,7,51,'0,7',NULL),(508,8,51,'На клей',NULL),(509,9,51,'KM2',NULL),(510,10,51,'23/34/43',NULL),(511,1,52,'Планка',NULL),(512,2,52,'152*914',NULL),(513,3,52,'15',NULL),(514,4,52,'10',NULL),(515,5,52,'Extreme Protection',NULL),(516,6,52,'3',NULL),(517,7,52,'0,7',NULL),(518,8,52,'На клей',NULL),(519,9,52,'KM2',NULL),(520,10,52,'23/34/43',NULL),(521,1,53,'Планка',NULL),(522,2,53,'152*914',NULL),(523,3,53,'15',NULL),(524,4,53,'10',NULL),(525,5,53,'Extreme Protection',NULL),(526,6,53,'3',NULL),(527,7,53,'0,7',NULL),(528,8,53,'На клей',NULL),(529,9,53,'KM2',NULL),(530,10,53,'23/34/43',NULL),(531,1,54,'Планка',NULL),(532,2,54,'152*914',NULL),(533,3,54,'15',NULL),(534,4,54,'10',NULL),(535,5,54,'Extreme Protection',NULL),(536,6,54,'3',NULL),(537,7,54,'0,7',NULL),(538,8,54,'На клей',NULL),(539,9,54,'KM2',NULL),(540,10,54,'23/34/43',NULL),(541,1,55,'Планка',NULL),(542,2,55,'152*914',NULL),(543,3,55,'15',NULL),(544,4,55,'10',NULL),(545,5,55,'Extreme Protection',NULL),(546,6,55,'3',NULL),(547,7,55,'0,7',NULL),(548,8,55,'На клей',NULL),(549,9,55,'KM2',NULL),(550,10,55,'23/34/43',NULL),(551,1,56,'Планка',NULL),(552,2,56,'152*914',NULL),(553,3,56,'15',NULL),(554,4,56,'10',NULL),(555,5,56,'Extreme Protection',NULL),(556,6,56,'3',NULL),(557,7,56,'0,7',NULL),(558,8,56,'На клей',NULL),(559,9,56,'KM2',NULL),(560,10,56,'23/34/43',NULL),(561,1,57,'Планка',NULL),(562,2,57,'152*914',NULL),(563,3,57,'15',NULL),(564,4,57,'10',NULL),(565,5,57,'Extreme Protection',NULL),(566,6,57,'3',NULL),(567,7,57,'0,7',NULL),(568,8,57,'На клей',NULL),(569,9,57,'KM2',NULL),(570,10,57,'23/34/43',NULL),(571,1,58,'Планка',NULL),(572,2,58,'152*914',NULL),(573,3,58,'15',NULL),(574,4,58,'10',NULL),(575,5,58,'Extreme Protection',NULL),(576,6,58,'3',NULL),(577,7,58,'0,7',NULL),(578,8,58,'На клей',NULL),(579,9,58,'KM2',NULL),(580,10,58,'23/34/43',NULL),(581,1,59,'Планка',NULL),(582,2,59,'152*914',NULL),(583,3,59,'15',NULL),(584,4,59,'10',NULL),(585,5,59,'Extreme Protection',NULL),(586,6,59,'3',NULL),(587,7,59,'0,7',NULL),(588,8,59,'На клей',NULL),(589,9,59,'KM2',NULL),(590,10,59,'23/34/43',NULL),(591,1,60,'Планка',NULL),(592,2,60,'152*914',NULL),(593,3,60,'15',NULL),(594,4,60,'10',NULL),(595,5,60,'Extreme Protection',NULL),(596,6,60,'3',NULL),(597,7,60,'0,7',NULL),(598,8,60,'На клей',NULL),(599,9,60,'KM2',NULL),(600,10,60,'23/34/43',NULL),(601,1,61,'Планка',NULL),(602,2,61,'152*914',NULL),(603,3,61,'15',NULL),(604,4,61,'10',NULL),(605,5,61,'Extreme Protection',NULL),(606,6,61,'3',NULL),(607,7,61,'0,7',NULL),(608,8,61,'На клей',NULL),(609,9,61,'KM2',NULL),(610,10,61,'23/34/43',NULL),(611,1,62,'Планка',NULL),(612,2,62,'152*914',NULL),(613,3,62,'15',NULL),(614,4,62,'10',NULL),(615,5,62,'Extreme Protection',NULL),(616,6,62,'3',NULL),(617,7,62,'0,7',NULL),(618,8,62,'На клей',NULL),(619,9,62,'KM2',NULL),(620,10,62,'23/34/43',NULL),(621,1,63,'Планка',NULL),(622,2,63,'152*914',NULL),(623,3,63,'15',NULL),(624,4,63,'10',NULL),(625,5,63,'Extreme Protection',NULL),(626,6,63,'3',NULL),(627,7,63,'0,7',NULL),(628,8,63,'На клей',NULL),(629,9,63,'KM2',NULL),(630,10,63,'23/34/43',NULL),(631,1,64,'Планка',NULL),(632,2,64,'152*914',NULL),(633,3,64,'15',NULL),(634,4,64,'10',NULL),(635,5,64,'Extreme Protection',NULL),(636,6,64,'3',NULL),(637,7,64,'0,7',NULL),(638,8,64,'На клей',NULL),(639,9,64,'KM2',NULL),(640,10,64,'23/34/43',NULL),(641,1,65,'Планка',NULL),(642,2,65,'152*914',NULL),(643,3,65,'15',NULL),(644,4,65,'10',NULL),(645,5,65,'Extreme Protection',NULL),(646,6,65,'3',NULL),(647,7,65,'0,7',NULL),(648,8,65,'На клей',NULL),(649,9,65,'KM2',NULL),(650,10,65,'23/34/43',NULL),(651,1,66,'Плитка',NULL),(652,2,66,'457*457',NULL),(653,3,66,'10',NULL),(654,4,66,'10',NULL),(655,5,66,'Extreme Protection',NULL),(656,6,66,'3',NULL),(657,7,66,'0,7',NULL),(658,8,66,'На клей',NULL),(659,9,66,'KM2',NULL),(660,10,66,'23/34/43',NULL),(661,1,67,'Плитка',NULL),(662,2,67,'457*457',NULL),(663,3,67,'10',NULL),(664,4,67,'10',NULL),(665,5,67,'Extreme Protection',NULL),(666,6,67,'3',NULL),(667,7,67,'0,7',NULL),(668,8,67,'На клей',NULL),(669,9,67,'KM2',NULL),(670,10,67,'23/34/43',NULL),(671,1,68,'Плитка',NULL),(672,2,68,'457*457',NULL),(673,3,68,'10',NULL),(674,4,68,'10',NULL),(675,5,68,'Extreme Protection',NULL),(676,6,68,'3',NULL),(677,7,68,'0,7',NULL),(678,8,68,'На клей',NULL),(679,9,68,'KM2',NULL),(680,10,68,'23/34/43',NULL),(681,1,69,'Плитка',NULL),(682,2,69,'457*457',NULL),(683,3,69,'10',NULL),(684,4,69,'10',NULL),(685,5,69,'Extreme Protection',NULL),(686,6,69,'3',NULL),(687,7,69,'0,7',NULL),(688,8,69,'На клей',NULL),(689,9,69,'KM2',NULL),(690,10,69,'23/34/43',NULL),(691,1,70,'Плитка',NULL),(692,2,70,'457*457',NULL),(693,3,70,'10',NULL),(694,4,70,'10',NULL),(695,5,70,'Extreme Protection',NULL),(696,6,70,'3',NULL),(697,7,70,'0,7',NULL),(698,8,70,'На клей',NULL),(699,9,70,'KM2',NULL),(700,10,70,'23/34/43',NULL),(701,1,71,'Плитка',NULL),(702,2,71,'457*457',NULL),(703,3,71,'10',NULL),(704,4,71,'10',NULL),(705,5,71,'Extreme Protection',NULL),(706,6,71,'3',NULL),(707,7,71,'0,7',NULL),(708,8,71,'На клей',NULL),(709,9,71,'KM2',NULL),(710,10,71,'23/34/43',NULL),(711,1,72,'Плитка',NULL),(712,2,72,'457*457',NULL),(713,3,72,'10',NULL),(714,4,72,'10',NULL),(715,5,72,'Extreme Protection',NULL),(716,6,72,'3',NULL),(717,7,72,'0,7',NULL),(718,8,72,'На клей',NULL),(719,9,72,'KM2',NULL),(720,10,72,'23/34/43',NULL),(721,1,73,'Плитка',NULL),(722,2,73,'457*457',NULL),(723,3,73,'10',NULL),(724,4,73,'10',NULL),(725,5,73,'Extreme Protection',NULL),(726,6,73,'3',NULL),(727,7,73,'0,7',NULL),(728,8,73,'На клей',NULL),(729,9,73,'KM2',NULL),(730,10,73,'23/34/43',NULL),(731,1,74,'Плитка',NULL),(732,2,74,'457*457',NULL),(733,3,74,'10',NULL),(734,4,74,'10',NULL),(735,5,74,'Extreme Protection',NULL),(736,6,74,'3',NULL),(737,7,74,'0,7',NULL),(738,8,74,'На клей',NULL),(739,9,74,'KM2',NULL),(740,10,74,'23/34/43',NULL),(741,1,75,'Планка',NULL),(742,2,75,'152*914',NULL),(743,3,75,'18',NULL),(744,4,75,'9',NULL),(745,5,75,'Extreme Protection',NULL),(746,6,75,'2,1',NULL),(747,7,75,'0,4',NULL),(748,8,75,'На клей',NULL),(749,9,75,'KM5',NULL),(750,10,75,'23/32/41',NULL),(751,1,76,'Планка',NULL),(752,2,76,'152*914',NULL),(753,3,76,'18',NULL),(754,4,76,'9',NULL),(755,5,76,'Extreme Protection',NULL),(756,6,76,'2,1',NULL),(757,7,76,'0,4',NULL),(758,8,76,'На клей',NULL),(759,9,76,'KM5',NULL),(760,10,76,'23/32/41',NULL),(761,1,77,'Планка',NULL),(762,2,77,'152*914',NULL),(763,3,77,'18',NULL),(764,4,77,'9',NULL),(765,5,77,'Extreme Protection',NULL),(766,6,77,'2,1',NULL),(767,7,77,'0,4',NULL),(768,8,77,'На клей',NULL),(769,9,77,'KM5',NULL),(770,10,77,'23/32/41',NULL),(771,1,78,'Планка',NULL),(772,2,78,'152*914',NULL),(773,3,78,'18',NULL),(774,4,78,'9',NULL),(775,5,78,'Extreme Protection',NULL),(776,6,78,'2,1',NULL),(777,7,78,'0,4',NULL),(778,8,78,'На клей',NULL),(779,9,78,'KM5',NULL),(780,10,78,'23/32/41',NULL),(781,1,79,'Планка',NULL),(782,2,79,'152*914',NULL),(783,3,79,'18',NULL),(784,4,79,'9',NULL),(785,5,79,'Extreme Protection',NULL),(786,6,79,'2,1',NULL),(787,7,79,'0,4',NULL),(788,8,79,'На клей',NULL),(789,9,79,'KM5',NULL),(790,10,79,'23/32/41',NULL),(801,1,81,'Планка',NULL),(802,2,81,'152*914',NULL),(803,3,81,'18',NULL),(804,4,81,'9',NULL),(805,5,81,'Extreme Protection',NULL),(806,6,81,'2,1',NULL),(807,7,81,'0,4',NULL),(808,8,81,'На клей',NULL),(809,9,81,'KM5',NULL),(810,10,81,'23/32/41',NULL),(821,1,83,'Планка',NULL),(822,2,83,'152*914',NULL),(823,3,83,'18',NULL),(824,4,83,'9',NULL),(825,5,83,'Extreme Protection',NULL),(826,6,83,'2,1',NULL),(827,7,83,'0,4',NULL),(828,8,83,'На клей',NULL),(829,9,83,'KM5',NULL),(830,10,83,'23/32/41',NULL),(831,1,84,'Планка',NULL),(832,2,84,'152*914',NULL),(833,3,84,'18',NULL),(834,4,84,'9',NULL),(835,5,84,'Extreme Protection',NULL),(836,6,84,'2,1',NULL),(837,7,84,'0,4',NULL),(838,8,84,'На клей',NULL),(839,9,84,'KM5',NULL),(840,10,84,'23/32/41',NULL),(851,1,86,'Планка',NULL),(852,2,86,'152*914',NULL),(853,3,86,'18',NULL),(854,4,86,'9',NULL),(855,5,86,'Extreme Protection',NULL),(856,6,86,'2,1',NULL),(857,7,86,'0,4',NULL),(858,8,86,'На клей',NULL),(859,9,86,'KM5',NULL),(860,10,86,'23/32/41',NULL),(861,1,87,'Планка',NULL),(862,2,87,'152*914',NULL),(863,3,87,'18',NULL),(864,4,87,'9',NULL),(865,5,87,'Extreme Protection',NULL),(866,6,87,'2,1',NULL),(867,7,87,'0,4',NULL),(868,8,87,'На клей',NULL),(869,9,87,'KM5',NULL),(870,10,87,'23/32/41',NULL),(871,1,88,'Планка',NULL),(872,2,88,'152*914',NULL),(873,3,88,'18',NULL),(874,4,88,'9',NULL),(875,5,88,'Extreme Protection',NULL),(876,6,88,'2,1',NULL),(877,7,88,'0,4',NULL),(878,8,88,'На клей',NULL),(879,9,88,'KM5',NULL),(880,10,88,'23/32/41',NULL),(881,1,89,'Планка',NULL),(882,2,89,'152*914',NULL),(883,3,89,'18',NULL),(884,4,89,'9',NULL),(885,5,89,'Extreme Protection',NULL),(886,6,89,'2,1',NULL),(887,7,89,'0,4',NULL),(888,8,89,'На клей',NULL),(889,9,89,'KM5',NULL),(890,10,89,'23/32/41',NULL),(891,1,90,'Планка',NULL),(892,2,90,'152*914',NULL),(893,3,90,'18',NULL),(894,4,90,'9',NULL),(895,5,90,'Extreme Protection',NULL),(896,6,90,'2,1',NULL),(897,7,90,'0,4',NULL),(898,8,90,'На клей',NULL),(899,9,90,'KM5',NULL),(900,10,90,'23/32/41',NULL),(911,1,92,'Планка',NULL),(912,2,92,'152*914',NULL),(913,3,92,'18',NULL),(914,4,92,'9',NULL),(915,5,92,'Extreme Protection',NULL),(916,6,92,'2,1',NULL),(917,7,92,'0,4',NULL),(918,8,92,'На клей',NULL),(919,9,92,'KM5',NULL),(920,10,92,'23/32/41',NULL),(921,1,93,'Планка',1),(922,2,93,'183*1220мм',2),(923,3,93,'9 шт',4),(924,4,93,'15 кг',5),(925,5,93,'PUR',6),(926,6,93,'4 мм',7),(927,7,93,'0,3 мм',8),(928,8,93,'Замковый',9),(929,9,93,'KM5',10),(930,10,93,'21/31',11),(931,1,94,'Планка',1),(932,2,94,'183*1220мм',2),(933,3,94,'9 шт',4),(934,4,94,'15 кг',5),(935,5,94,'PUR',6),(936,6,94,'4 мм',7),(937,7,94,'0,3 мм',8),(938,8,94,'Замковый',9),(939,9,94,'KMS',10),(940,10,94,'21/31',11),(941,1,95,'Планка',2),(942,2,95,'183*1220мм',3),(943,3,95,'9 шт',4),(944,4,95,'15 кг',5),(945,5,95,'PUR',6),(946,6,95,'4 мм',7),(947,7,95,'0,3 мм',8),(948,8,95,'Замковый',9),(949,9,95,'KMS',10),(950,10,95,'21/31',11),(951,1,96,'Планка',2),(952,2,96,'183*1220мм',3),(953,3,96,'9 шт',4),(954,4,96,'15 кг',5),(955,5,96,'PUR',6),(956,6,96,'4 мм',7),(957,7,96,'0,3 мм',8),(958,8,96,'Замковый',9),(959,9,96,'KM5',10),(960,10,96,'21/31',11),(961,1,97,'Планка',2),(962,2,97,'183*1220мм',3),(963,3,97,'9 шт',4),(964,4,97,'15 кг',5),(965,5,97,'PUR',6),(966,6,97,'4 мм',7),(967,7,97,'0,3 мм',8),(968,8,97,'Замковый',9),(969,9,97,'KMS',10),(970,10,97,'21/31',11),(971,1,98,'Планка',2),(972,2,98,'183*1220мм',3),(973,3,98,'9 шт',4),(974,4,98,'15 кг',5),(975,5,98,'PUR',6),(976,6,98,'4 мм',7),(977,7,98,'0,3 мм',8),(978,8,98,'Замковый',9),(979,9,98,'KMS',10),(980,10,98,'21/31',11),(981,1,99,'Планка',1),(982,2,99,'183*1220мм',2),(983,3,99,'9 шт',4),(984,4,99,'15 кг',5),(985,5,99,'PUR',6),(986,6,99,'4 мм',7),(987,7,99,'0,3 мм',8),(988,8,99,'Замковый',9),(989,9,99,'KMS',10),(990,10,99,'21/31',11),(991,1,100,'Планка',1),(992,2,100,'183*1220мм',2),(993,3,100,'9 шт',4),(994,4,100,'15 кг',5),(995,5,100,'PUR',6),(996,6,100,'4 мм',7),(997,7,100,'0,3 мм',8),(998,8,100,'Замковый',9),(999,9,100,'KMS',10),(1000,10,100,'21/31',11),(1001,1,101,'Планка',1),(1002,2,101,'183*1220мм',2),(1003,3,101,'9 шт',4),(1004,4,101,'15 кг',5),(1005,5,101,'PUR',6),(1006,6,101,'4 мм',7),(1007,7,101,'0,3 мм',8),(1008,8,101,'Замковый',9),(1009,9,101,'KMS',10),(1010,10,101,'21/31',11),(1011,1,102,'Планка',1),(1012,2,102,'183*1220мм',2),(1013,3,102,'9 шт',4),(1014,4,102,'15 кг',5),(1015,5,102,'PUR',8),(1016,6,102,'4 мм',6),(1017,7,102,'0,3 мм',7),(1018,8,102,'Замковый',9),(1019,9,102,'KMS',10),(1020,10,102,'21/31',11),(1021,1,103,'Планка',1),(1022,2,103,'183*1220мм',2),(1023,3,103,'9 шт',4),(1024,4,103,'15 кг',5),(1025,5,103,'PUR',10),(1026,6,103,'4 мм',6),(1027,7,103,'0,3мм',7),(1028,8,103,'Замковый',8),(1029,9,103,'KM5',9),(1030,10,103,'21/31',11),(1031,1,104,'Планка',1),(1032,2,104,'200,8мм*1220мм',2),(1033,3,104,'8 шт',6),(1034,4,104,'17 кг',7),(1035,5,104,'Extreme protection',8),(1036,6,104,'4,4 мм',3),(1037,7,104,'0,3 мм',4),(1038,8,104,'Клик',9),(1039,9,104,'KM5',10),(1040,10,104,'23/31',11),(1041,1,105,'Планка',1),(1042,2,105,'200,8мм*1220мм',2),(1043,3,105,'8 шт',6),(1044,4,105,'17 кг',7),(1045,5,105,'Extreme protection',8),(1046,6,105,'4,4 мм',3),(1047,7,105,'0,3 мм',4),(1048,8,105,'Клик',9),(1049,9,105,'KM5',10),(1050,10,105,'23/31',11),(1051,1,106,'Планка',1),(1052,2,106,'200,8мм*1220мм',2),(1053,3,106,'8 шт',3),(1054,4,106,'17 кг',7),(1055,5,106,'Extreme protection',8),(1056,6,106,'4,4 мм',4),(1057,7,106,'0,3 мм',5),(1058,8,106,'Клик',9),(1059,9,106,'KM5',10),(1060,10,106,'23/31',11),(1061,1,107,'Планка',1),(1062,2,107,'200,8мм*1220мм',2),(1063,3,107,'8 шт',6),(1064,4,107,'17 кг',7),(1065,5,107,'Extreme protection',8),(1066,6,107,'4,4',3),(1067,7,107,'0,3',4),(1068,8,107,'Клик',9),(1069,9,107,'KM5',10),(1070,10,107,'23/31',11),(1071,1,108,'Планка',1),(1072,2,108,'200,8мм*1220мм',2),(1073,3,108,'8 шт',6),(1074,4,108,'17 кг',7),(1075,5,108,'Extreme protection',8),(1076,6,108,'4,4 мм',3),(1077,7,108,'0,3 мм',4),(1078,8,108,'Клик',9),(1079,9,108,'KM5',10),(1080,10,108,'23/31',11),(1081,1,109,'Планка',1),(1082,2,109,'200,8мм*1220мм',2),(1083,3,109,'8 шт',6),(1084,4,109,'17 кг',7),(1085,5,109,'Extreme protection',8),(1086,6,109,'4,4 мм',3),(1087,7,109,'0,3 мм',4),(1088,8,109,'Клик',9),(1089,9,109,'KMS',10),(1090,10,109,'23/31',11),(1091,1,110,'Планка',1),(1092,2,110,'200,8мм*1220мм',2),(1093,3,110,'8 шт',6),(1094,4,110,'17 кг',7),(1095,5,110,'Extreme protection',8),(1096,6,110,'4,4 мм',3),(1097,7,110,'0,3 мм',4),(1098,8,110,'Клик',9),(1099,9,110,'KM5',10),(1100,10,110,'23/31',11),(1101,1,111,'Планка',1),(1102,2,111,'200,8мм*1220мм',2),(1103,3,111,'8 шт',6),(1104,4,111,'17 кг',7),(1105,5,111,'Extreme protection',8),(1106,6,111,'4,4 мм',3),(1107,7,111,'0,3 мм',4),(1108,8,111,'Клик',9),(1109,9,111,'KM5',10),(1110,10,111,'23/31',11),(1111,1,112,'Планка',1),(1112,2,112,'200,8мм*1220мм',2),(1113,3,112,'8 шт',6),(1114,4,112,'17 кг',7),(1115,5,112,'Extreme protection',8),(1116,6,112,'4,4 мм',3),(1117,7,112,'0,3 мм',4),(1118,8,112,'Клик',9),(1119,9,112,'KM5',10),(1120,10,112,'23/31',11),(1121,1,113,'Планка',1),(1122,2,113,'200,8мм*1220мм',2),(1123,3,113,'8 шт',6),(1124,4,113,'17 кг',7),(1125,5,113,'Extreme protection',8),(1126,6,113,'4,4 мм',3),(1127,7,113,'0,3 мм',4),(1128,8,113,'Клик',9),(1129,9,113,'KM5',10),(1130,10,113,'23/31',11),(1131,1,114,'Планка',1),(1132,2,114,'200,8мм*1220мм',2),(1133,3,114,'8 шт',6),(1134,4,114,'17 кг',7),(1135,5,114,'Extreme protection',8),(1136,6,114,'4,4 мм',3),(1137,7,114,'0,3 мм',4),(1138,8,114,'Клик',9),(1139,9,114,'KM5',10),(1140,10,114,'23/31',11),(1141,1,115,'Планка',1),(1142,2,115,'200,8мм*1220мм',2),(1143,3,115,'8 шт',6),(1144,4,115,'17 кг',7),(1145,5,115,'Extreme protection',8),(1146,6,115,'4,4 мм',3),(1147,7,115,'0,3 мм',4),(1148,8,115,'Клик',9),(1149,9,115,'KM5',10),(1150,10,115,'23/31',11),(1151,1,116,'Планка',1),(1152,2,116,'200,8мм*1220мм',2),(1153,3,116,'8 шт',6),(1154,4,116,'17 кг',7),(1155,5,116,'Extreme protection',8),(1156,6,116,'4,4 мм',3),(1157,7,116,'0,3 мм',4),(1158,8,116,'Клик',9),(1159,9,116,'KM5',10),(1160,10,116,'23/31',11),(1161,1,117,'Планка',1),(1162,2,117,'200,8мм*1220мм',2),(1163,3,117,'8 шт',6),(1164,4,117,'17 кг',7),(1165,5,117,'Extreme protection',8),(1166,6,117,'4,4 мм',3),(1167,7,117,'0,3 мм',4),(1168,8,117,'Клик',9),(1169,9,117,'KM5',10),(1170,10,117,'23/31',11),(1171,11,41,'4-сторонняя',NULL),(1172,1,118,'Планка',NULL),(1173,2,118,'152*914',NULL),(1174,3,118,'18',NULL),(1175,4,118,'9',NULL),(1176,5,118,'Extreme Protection',NULL),(1177,6,118,'2,1',NULL),(1178,7,118,'0,4',NULL),(1179,8,118,'На клей',NULL),(1180,9,118,'KM5',NULL),(1181,10,118,'23/32/41',NULL),(1182,1,119,'Планка',NULL),(1183,2,119,'152*914',NULL),(1184,3,119,'18',NULL),(1185,4,119,'9',NULL),(1186,5,119,'Extreme Protection',NULL),(1187,6,119,'2,1',NULL),(1188,7,119,'0,4',NULL),(1189,8,119,'На клей',NULL),(1190,9,119,'KM5',NULL),(1191,10,119,'23/32/41',NULL),(1192,1,120,'Планка',NULL),(1193,2,120,'152*914',NULL),(1194,3,120,'18',NULL),(1195,4,120,'9',NULL),(1196,5,120,'Extreme Protection',NULL),(1197,6,120,'2,1',NULL),(1198,7,120,'0,4',NULL),(1199,8,120,'На клей',NULL),(1200,9,120,'KM5',NULL),(1201,10,120,'23/32/41',NULL),(1202,1,121,'Плитка',NULL),(1203,2,121,'457*457',NULL),(1204,3,121,'10',NULL),(1205,4,121,'10',NULL),(1206,5,121,'Extreme Protection',NULL),(1207,6,121,'3',NULL),(1208,7,121,'0,7',NULL),(1209,8,121,'На клей',NULL),(1210,9,121,'KM2',NULL),(1211,10,121,'23/34/43',NULL),(1232,1,124,'Планка',NULL),(1233,2,124,'152*914',NULL),(1234,3,124,'18',NULL),(1235,4,124,'9',NULL),(1236,5,124,'Extreme Protection',NULL),(1237,6,124,'2,1',NULL),(1238,7,124,'0,4',NULL),(1239,8,124,'На клей',NULL),(1240,9,124,'KM5',NULL),(1241,10,124,'23/32/41',NULL),(1242,1,126,'Плитка',NULL),(1243,2,126,'457*457',NULL),(1244,3,126,'12',NULL),(1245,4,126,'9',NULL),(1246,5,126,'Extreme Protection',NULL),(1247,6,126,'2,1',NULL),(1248,7,126,'0,4',NULL),(1249,8,126,'На клей',NULL),(1250,9,126,'KM5',NULL),(1251,10,126,'23/32/41',NULL),(1252,11,126,'Нет',NULL),(1253,1,127,'Плитка',NULL),(1254,2,127,'457*457',NULL),(1255,3,127,'12',NULL),(1256,4,127,'9',NULL),(1257,5,127,'Extreme Protection',NULL),(1258,6,127,'2,1',NULL),(1259,7,127,'0,4',NULL),(1260,8,127,'На клей',NULL),(1261,9,127,'KM5',NULL),(1262,10,127,'23/32/41',NULL),(1263,11,127,'Нет',NULL),(1264,1,128,'Плитка',NULL),(1265,2,128,'457*457',NULL),(1266,3,128,'12',NULL),(1267,4,128,'9',NULL),(1268,5,128,'Extreme Protection',NULL),(1269,6,128,'2,1',NULL),(1270,7,128,'0,4',NULL),(1271,8,128,'На клей',NULL),(1272,9,128,'KM5',NULL),(1273,10,128,'23/32/41',NULL),(1274,11,128,'Нет',NULL),(1275,1,130,'Плитка',NULL),(1276,2,130,'457*457',NULL),(1277,3,130,'12',NULL),(1278,4,130,'9',NULL),(1279,5,130,'Extreme Protection',NULL),(1280,6,130,'2,1',NULL),(1281,7,130,'0,4',NULL),(1282,8,130,'На клей',NULL),(1283,9,130,'KM5',NULL),(1284,10,130,'23/32/41',NULL),(1285,11,130,'Нет',NULL),(1286,1,131,'Плитка',NULL),(1287,2,131,'457*457',NULL),(1288,3,131,'12',NULL),(1289,4,131,'9',NULL),(1290,5,131,'Extreme Protection',NULL),(1291,6,131,'2,1',NULL),(1292,7,131,'0,4',NULL),(1293,8,131,'На клей',NULL),(1294,9,131,'KM5',NULL),(1295,10,131,'23/32/41',NULL),(1296,11,131,'Нет',NULL),(1297,1,132,'Плитка',NULL),(1298,2,132,'457*457',NULL),(1299,3,132,'12',NULL),(1300,4,132,'9',NULL),(1301,5,132,'Extreme Protection',NULL),(1302,6,132,'2,1',NULL),(1303,7,132,'0,4',NULL),(1304,8,132,'На клей',NULL),(1305,9,132,'KM5',NULL),(1306,10,132,'23/32/41',NULL),(1307,11,132,'Нет',NULL),(1308,11,75,'Нет',NULL),(1309,11,1,'4-х сторонняя фаска',5),(1310,11,2,'4-х сторонняя фаска',6),(1311,11,3,'4-х сторонняя фаска',5),(1312,11,4,'4-х сторонняя фаска',5),(1313,11,5,'4-х сторонняя фаска',5),(1314,11,6,'4-х сторонняя фаска',5),(1315,11,7,'4-х сторонняя фаска',5),(1316,11,8,'4-х сторонняя фаска',5),(1317,11,9,'4-х сторонняя фаска',5),(1318,11,10,'4-х сторонняя фаска',5),(1319,11,11,'4-х сторонняя фаска',5),(1320,11,12,'4-х сторонняя фаска',5),(1321,11,13,'4-х сторонняя фаска',5),(1322,11,14,'4-х сторонняя фаска',11),(1323,11,15,'4-х сторонняя фаска',NULL),(1324,11,16,'4-х сторонняя фаска',NULL),(1325,11,17,'4-х сторонняя фаска',NULL),(1326,11,18,'4-х сторонняя фаска',NULL),(1327,11,19,'4-х сторонняя фаска',NULL),(1328,11,20,'4-х сторонняя фаска',NULL),(1329,11,21,'4-х сторонняя фаска',NULL),(1330,11,22,'4-х сторонняя фаска',NULL),(1331,11,23,'4-х сторонняя фаска',NULL),(1332,11,24,'4-х сторонняя фаска',NULL),(1333,11,25,'4-х сторонняя фаска',NULL),(1334,11,26,'4-х сторонняя фаска',NULL),(1335,11,27,'4-х сторонняя фаска',NULL),(1336,11,28,'4-х сторонняя фаска',NULL),(1337,11,29,'4-х сторонняя фаска',NULL),(1338,11,30,'4-х сторонняя фаска',NULL),(1339,11,31,'4-х сторонняя фаска',NULL),(1340,11,32,'4-х сторонняя фаска',NULL),(1341,11,33,'4-х сторонняя фаска',NULL),(1342,11,42,'4-х сторонняя',NULL),(1343,11,43,'4-х сторонняя',NULL),(1344,11,44,'4-х сторонняя',NULL),(1345,11,45,'4-х сторонняя',NULL),(1346,11,46,'4-х сторонняя',NULL),(1347,11,47,'4-х сторонняя',NULL),(1348,11,48,'4-х сторонняя',NULL),(1349,11,121,'4-х сторонняя',NULL),(1350,11,49,'4-х сторонняя',NULL),(1351,11,50,'4-х сторонняя',NULL),(1352,11,51,'4-х сторонняя',NULL),(1353,11,52,'4-х сторонняя',NULL),(1354,11,53,'4-х сторонняя',NULL),(1355,11,54,'4-х сторонняя',NULL),(1356,11,55,'4-х сторонняя',NULL),(1357,11,56,'4-х сторонняя',NULL),(1358,11,57,'4-х сторонняя',NULL),(1359,11,58,'4-х сторонняя',NULL),(1360,11,59,'4-х сторонняя',NULL),(1361,11,60,'4-х сторонняя',NULL),(1362,11,61,'4-х сторонняя',NULL),(1363,11,62,'4-х сторонняя',NULL),(1364,11,63,'4-х сторонняя',NULL),(1365,11,64,'4-х сторонняя',NULL),(1366,11,65,'4-х сторонняя',NULL),(1367,11,93,'4-х сторонняя фаска',3),(1368,11,94,'4-х сторонняя фаска',3),(1369,11,95,'4-х сторонняя фаска',1),(1370,11,96,'4-х сторонняя фаска',1),(1371,11,97,'4-х сторонняя фаска',1),(1372,11,98,'4-х сторонняя фаска',1),(1373,11,99,'4-х сторонняя фаска',3),(1374,11,100,'4-х сторонняя фаска',3),(1375,11,101,'4-х сторонняя фаска',3),(1376,11,102,'4-х сторонняя фаска',3),(1377,11,103,'4-х сторонняя фаска',3),(1378,11,104,'4-х сторонняя фаска',5),(1379,11,105,'4-х сторонняя фаска',5),(1380,11,106,'4-х сторонняя фаска',6),(1381,11,107,'4-х сторонняя фаска',5),(1382,11,108,'4-х сторонняя фаска',5),(1383,11,109,'4-х сторонняя фаска',5),(1384,11,110,'4-х сторонняя фаска',5),(1385,11,111,'4-х сторонняя фаска',5),(1386,11,112,'4-х сторонняя фаска',5),(1387,11,113,'4-х сторонняя фаска',5),(1388,11,114,'4-х сторонняя фаска',5),(1389,11,115,'4-х сторонняя фаска',5),(1390,11,116,'4-х сторонняя фаска',5),(1391,11,117,'4-х сторонняя фаска',5),(1392,1,133,'Планка',1),(1393,2,133,'183*1220мм',2),(1394,6,133,'4 мм',6),(1395,7,133,'0,3мм',7),(1396,11,133,'4-х сторонняя фаска',3),(1397,3,133,'9 шт',4),(1398,4,133,'10 кг',5),(1399,5,133,'PUR',8),(1400,8,133,'замковый',9),(1401,9,133,'KM5',10),(1402,10,133,'21/31',11);
/*!40000 ALTER TABLE `card_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_page_cities`
--

DROP TABLE IF EXISTS `contacts_page_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_page_cities` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_page_cities`
--

LOCK TABLES `contacts_page_cities` WRITE;
/*!40000 ALTER TABLE `contacts_page_cities` DISABLE KEYS */;
INSERT INTO `contacts_page_cities` VALUES (1,'Абакан'),(2,'Абинск'),(3,'Адлер'),(4,'Азов'),(5,'Аксай'),(6,'Алексеевка'),(7,'Анапа'),(8,'Ангарск'),(9,'Арзамас'),(10,'Армавир'),(11,'Артем'),(12,'Астрахань'),(13,'Ачинск'),(14,'Балашиха'),(15,'Барнаул'),(16,'Батайск'),(17,'Белая Калитва'),(18,'Белгород'),(19,'Белоозёрский п.г.т.'),(20,'Бердск'),(21,'Бийск'),(22,'Благовещенск'),(23,'Благодарный'),(24,'Борисоглебск'),(25,'Братск'),(26,'Брянск'),(27,'Буденновск'),(28,'Владивосток'),(29,'Владикавказ'),(30,'Владимир'),(31,'Волгоград'),(32,'Волгодонск'),(33,'Волжский'),(34,'Вологда'),(35,'Воронеж'),(36,'Геленджик'),(37,'Грязи'),(38,'Губкин'),(39,'Гуково'),(40,'Гулькевичи'),(41,'Дальнереченск'),(42,'Дербент'),(43,'Десногорск'),(44,'Дзержинск'),(45,'Дмитров'),(46,'Дубна'),(47,'Евпатория'),(48,'Егорьевск'),(49,'Ейск'),(50,'Ессентуки'),(51,'Железногорск'),(52,'Жуковский'),(53,'Зарайск'),(54,'Зеленоград'),(55,'Ипатово'),(56,'Иркутск'),(57,'Калуга'),(58,'Камышин'),(59,'Кемерово'),(60,'Керчь'),(61,'Киров'),(62,'Кисловодск'),(63,'Ковров'),(64,'Коломна'),(65,'Комсомольск-на-Амуре'),(66,'Кострома'),(67,'Краснодар'),(68,'Красноярск'),(69,'Кропоткин'),(70,'Крымск'),(71,'Курганинск'),(72,'Куровское'),(73,'Курск'),(74,'Лабинск'),(75,'Липецк'),(76,'Луховицы'),(77,'Люберцы'),(78,'Магас'),(79,'Махачкала'),(80,'Москва'),(81,'Муром'),(82,'Мытищи'),(83,'Назрань'),(84,'Нальчик'),(85,'Невинномысск'),(86,'Нижний Новгород'),(87,'Новокузнецк'),(88,'Новомосковск'),(89,'Новороссийск'),(90,'Новосибирск'),(91,'Новочеркасск'),(92,'Новошахтинск'),(93,'Ногинск'),(94,'Обнинск'),(95,'Омск'),(96,'Орел'),(97,'Орехово-Зуево'),(98,'Павлово'),(99,'Переславль Залесский'),(100,'Петропавловск-Камчатский'),(101,'Приморско-Ахтарск'),(102,'Пушкино'),(103,'Пятигорск'),(104,'Раменское'),(105,'Реутов'),(106,'Ржев'),(107,'Россошь'),(108,'Ростов-на-Дону'),(109,'Рыбинск'),(110,'Рязань'),(111,'Сальск'),(112,'Саранск'),(113,'Севастополь'),(114,'Сергиев Посад'),(115,'Симферополь'),(116,'Симфирополь'),(117,'Смоленск'),(118,'Сочи'),(119,'Спасск-Дальний'),(120,'Ставрополь'),(121,'Старый Оскол'),(122,'Таганрог'),(123,'Тамбов'),(124,'Тверь'),(125,'Темрюк'),(126,'Тимашевск'),(127,'Тихорецк'),(128,'Томск'),(129,'Торжок'),(130,'Тула'),(131,'Удомля'),(132,'Уссурийск'),(133,'Феодосия'),(134,'Фролово'),(135,'Хабаровск'),(136,'Химки'),(137,'Череповец'),(138,'Черкесск'),(139,'Шахты'),(140,'Элиста'),(141,'Южно Сахалинск'),(142,'Ялта'),(143,'Ярославль'),(144,'пос. Дубовое'),(145,'с.Хомутово');
/*!40000 ALTER TABLE `contacts_page_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_page_commons`
--

DROP TABLE IF EXISTS `contacts_page_commons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_page_commons` (
                                         `id` int(11) NOT NULL AUTO_INCREMENT,
                                         `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `metatitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                         `metakeys` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                         `metadesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_page_commons`
--

LOCK TABLES `contacts_page_commons` WRITE;
/*!40000 ALTER TABLE `contacts_page_commons` DISABLE KEYS */;
INSERT INTO `contacts_page_commons` VALUES (1,'+7 495 775-3737','Купить Tarkett Art Vinyl в твоем городе','магазины, арт винил, модульное покрытие, таркетт','Для покупки Таркетт АртВинил в вашем городе воспользуйтесь интерактивной картой на территории России.');
/*!40000 ALTER TABLE `contacts_page_commons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_page_locations`
--

DROP TABLE IF EXISTS `contacts_page_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_page_locations` (
                                           `id` int(11) NOT NULL AUTO_INCREMENT,
                                           `city_id` int(11) DEFAULT NULL,
                                           `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                           `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                           `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                           `lng` double NOT NULL,
                                           `lat` double NOT NULL,
                                           PRIMARY KEY (`id`),
                                           KEY `IDX_DB7B3F718BAC62AF` (`city_id`),
                                           CONSTRAINT `FK_DB7B3F718BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `contacts_page_cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=510 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_page_locations`
--

LOCK TABLES `contacts_page_locations` WRITE;
/*!40000 ALTER TABLE `contacts_page_locations` DISABLE KEYS */;
INSERT INTO `contacts_page_locations` VALUES (1,135,'Баярд','Павла Морозова, 58','+74212455545',135.099163,48.480946),(2,135,'Столичный двор','Краснореченская, 92Б','+74212457785',135.11208,48.426993),(3,135,'Напольные покрытия','улица Монтажная, 38а','+74212777088',135.097878,48.380039),(4,65,'LA`vina пола','улица Вокзальная, 32/1','+74217510025',136.998605,50.553071),(5,65,'СтокЦентр','Комсомольское шоссе, 7','+79294196010',137.028277,50.559372),(6,65,'Уровень','ул. Вокзальная, 91/2','8-800-250-45-54 доб. 5000, 8 (4217) 52-15-15 доб. 5000',136.969482,50.543352),(7,65,'Уровень','ул. Аллея Труда, 59/5','8-800-250-45-54 доб. 4100, 8 (4217) 52-15-15 доб. 4100',137.002585,50.530368),(8,65,'Уровень','улица Кирова, 46/6','8-800-250-45-54 доб. 2130, 8 (4217) 52-15-15 доб. 2130',137.03019,50.55275),(9,65,'Уровень','улица Севастопольская, 53 к2','8-800-250-45-54 доб. 3113, 8 (4217) 52-15-15 доб. 3113',137.007355,50.554324),(10,65,'Уровень','пр-т. Победы, 57','8-800-250-45-54 доб. 1176, 8 (4217) 52-15-15 доб. 1176',137.058191,50.588375),(11,100,'Дом Люксор','улица Ленинградская, 70Б','+74 152 423 470',158.654032,53.034711),(12,100,'Все для дома','улица Чубарова, 16','+74152257700',158.613221,53.07603),(13,100,'Строймаркет на Кавказской','улицаКавказская, 41','+74152259000',158.618485,53.0705),(14,135,'GoodPol','Проспект 60 лет октября, д. 152 (второй этаж)','+74212600117',135.112745,48.479327),(15,135,'NetPol','Карла Маркса, 166 лит В','+74212203048',135.124019,48.505283),(16,135,'FloorShop','60 лет Октября проспект, 158/1','+74212600117',135.110508,48.482642),(17,135,'АСТЕРЛИН','улица Ленинградская, д. 73Б','+74212515160',135.076301,48.494955),(18,135,'Практик','улица Промышленная, д.3, офис/кв.101','+74212784444',135.115512,48.486518),(19,135,'Свиф','улица краснореченская, д. 97а','+74212 45 95 59',135.102909,48.406865),(20,135,'WoodLin','Краснореченская, 101','+74 212 672 068',135.103915,48.408103),(21,135,'Технология пола','Карла Маркса, 101/1','+74 212 672 068',135.101022,48.501063),(22,135,'WoodLin','Ленинградская, 28 лит Н','+79 244 044 645',135.091707,48.486351),(23,135,'Ковчег','Гаражный переулок, 8а','+74 212 271 786',135.115018,48.480826),(24,135,'Залог','Некрасова, 44','+74212454300',135.068629,48.493743),(25,135,'Баярд','60 лет Октября проспект, 178','+74 212 455 545',135.105199,48.488667),(26,135,'Свиф','Ленинградская, 62в лит Д','+7 4212 47 46 45',135.066859,48.497671),(27,135,'ЗАЛОГ','проспект 60-я Октября, д. 170','+73 212 911 888',135.106331,48.486792),(28,135,'Столичный двор','Тихоокеанская, 204 к3','+74212760606',135.04857,48.554882),(29,22,'Строймаркет','Калинина, 116','+74162990615',127.524748,50.282934),(30,22,'Строймода','Ленина, 173','+74162225555',127.52075,50.259432),(31,22,'Строймастер','Студенческая, 14','+74162995555',127.502694,50.291964),(32,22,'Центр Напольных Покрытий','Текстильная, 116','+79145771111',127.534791,50.298438),(33,22,'Амурская Строительная Ярмарка','Мухина, 110а Ася','+74162770711',127.512189,50.281287),(34,22,'Мир Ковров','Текстильная, 49/а','+74162343333',127.53604,50.301792),(35,119,'Галант','улица Советская, д. 112','+74235223823',132.807507,44.589181),(36,28,'АСТЕРЛИН','улица Бородинская ,д.46, корп./стр.50','+74232300700',131.933473,43.166813),(37,28,'АСТЕРЛИН','улица Хабаровская,д.36','+74 232 300 700',131.898367,43.133881),(38,28,'Бернаучис','улица Бородинская, д.46, корп./стр.50','+74232697919',131.933473,43.166813),(39,28,'ЗАЛОГ','улица Бородинская, д. 46, корп./стр.50','+74232242312',131.933473,43.166813),(40,28,'ЗАЛОГ','Народный пр-т, д.11 в','+74232600600',131.922917,43.123992),(41,28,'ЗАЛОГ','улица Светланская, д. 150','+74232220664',131.934748,43.11216),(42,28,'ТОПТЫГИН','улица Бородинская, 46 стр 50','+74232684468',131.935629,43.167326),(43,28,'Солнышко','улица Гоголя, д. 30','+74232744342',131.904772,43.122656),(44,28,'ТОПТЫГИН','улица Светланская, д. 150 ст2','+74 232 209 604',131.934371,43.112535),(45,41,'Универмаг на Театральной','улица Владивостокская, д.2','+74 235 629 577',133.81459,45.943303),(46,132,'Солнышко Профи','улица Советская, д. 114','+74234334020',131.946247,43.804657),(47,132,'Солнышко','улица Тургенева, д. 73/б ст3','+74234264027',131.973008,43.809934),(48,28,'Горазд','Русская 65 ст 3','+74232756508',131.937129,43.168234),(49,11,'Каскад','Рабочая 1-я 16г','+74233742755',132.084462,43.328591),(50,11,'Помощник','Рабочая 1-я, 16','+74232996767',132.085962,43.328873),(51,11,'Помощник','Кирова, 9/1 ст5','+74232523333',132.177967,43.356318),(52,28,'Помощник','Адмирала Юмашева 19','+74232582828',131.94931,43.12719),(53,132,'С Маркет','Чичерина 139а','+74234269966',131.972325,43.793785),(54,141,'Галерея','Ленина 254/1','+74242724428',142.729093,46.950722),(55,56,'Арт Декор','улица Октябрьской Революции, 1/3 пав. 328','+7(9500) 77 14 59',104.294836,52.295278),(56,56,'Дисэр','улица Красноказачья-1-я, д.110, оф.1','+7 (3952) 50 57 17',104.331649,52.266027),(57,56,'Капитель','улица Старокузьмихинская, дом 41/2','+7 (3952) 42 10 16',104.282744,52.242823),(58,56,'Стильный Дом','Байкальский тракт, дом 11, пав. 95','+7 (3952) 60 79 27',104.415291,52.249632),(59,56,'Идея Ремонта','Култукский тракт , дом 17','+7 (9526) 20 34 53',104.097853,52.207433),(60,56,'FESTIVAL','Улица Воронежская л 2а','+7 (3952) 48 00 07',104.225719,52.272965),(61,56,'Decoro','Сергеева 3Б /1, пав 18','+7 (3952) 74 45 92',104.223033,52.266721),(62,25,'Контраст','Улица Гагарина д 17а','+7 (3953) 44 04 33',101.601372,56.168521),(63,56,'Строй Арсенал','улица Мира дом 2а','+7 (3952) 37 62 22',104.233777,52.338702),(64,8,'Пилигрим','микрорайон 18-й, дом 19','+7 (3955) 57 12 33',103.843154,52.503017),(65,8,'Toskana','Московский тракт, дом 1/2 пав 24','+7 (9021) 74 37 20',103.908506,52.546565),(66,56,'Студия отделочных материалов ДекАрт','улица Октябрьской Революции, д.1, корп. 3/3','+7 (3952) 71 05 00',104.294836,52.295278),(67,56,'Студия отделочных материалов ДекАрт','улица Сергеева, д.3Б/1','+7 (3952) 71 05 00',104.223033,52.266721),(68,56,'Студия отделочных материалов ДекАрт','улица Академическая, д.31','+7 (3952) 71 05 00',104.302417,52.233695),(69,56,'ГК Десятка','улица Поленова, д.18','+7 (3952) 25 23 29',104.317895,52.28447),(70,56,'ГК Десятка','улица Трактовая, д. 18Б','+7 (3952) 25 23 29',104.235394,52.315498),(71,56,'ГК Десятка','улица Октябрьской Революции, д.1 к3, павильон 86, 88','+7 (3952) 25 23 29',104.302956,52.289885),(72,56,'Метр','улица Трактовая, д.18, корп. Б','+7 (3952) 78 04 80',104.235394,52.315498),(73,56,'Метр','улица Байкальская, д.244а, корп./стр.1','+7 (3952) 78 04 80',104.29639,52.281274),(74,56,'Метр','улица Сергеева, д.3Б, корп./стр.1','+7 (3952) 78 04 80',104.208184,52.269466),(75,145,'Метр','улица Колхозная, д. 135/3','+7 (3952) 28 82 00',104.361302,52.463941),(76,56,'Кавалер','улица Октябрьской Революции, д. 1, к.2','+7 (3952) 50 44 33',104.294539,52.294336),(77,56,'ЛинТрейд','улица Лыткина, д.75, корп./стр.a','+7 (3952) 22 95 20',104.305849,52.262185),(78,56,'Роща','улица Берёзовая Роща, д.5а','+7 (3952) 76 31 76',104.231846,52.264682),(79,15,'Аксиома','тракт Павловский, д. 166','+7 (3852) 27 18 88',83.654766,53.347231),(80,15,'Алтай пол','улица Балтийская, д. 88','+7 (3852) 25 95 55',83.662663,53.337237),(81,15,'Знак','проспект Строителей, д. 92','+7 (3852) 36 40 80',83.745963,53.34171),(82,21,'Марка Пола','Коммунарский переулок 8 к2','+7 (960) 960 30 90',85.224554,52.538509),(83,15,'Гауди','проспект Красноармейский, д. 15','+7 (3852) 50 42 99',83.784923,53.327364),(84,15,'Гауди','улица Свободы площадь, д.6','+7 (3852 )50 12 99',83.788993,53.330634),(85,15,'Гауди','проспект Космонавтов, д. 6, к. \"Г\"','+7 (3852) 27 15 49',83.73504,53.390729),(86,15,'Домикс','улица Павловский тракт, д. 27','+7 (3852) 27 18 88',83.729623,53.339554),(87,15,'Формула М2','улица Космонавтов проспек, д.6, к. \"Г\"','+7 (3852) 27 18 88',83.73504,53.390729),(88,15,'Астера','улица Ярных, д. 2','+7 (3852) 69 63 27',83.760606,53.368056),(89,90,'ПОЛБЕРРИ','улица Семьи-Шамшиных, д.24, корп./стр.2','+7 (383) 367 02 36',82.935602,55.026711),(90,90,'ПОЛБЕРРИ','улица Светлановская, д. 50, ТЦ \"Красный Мамонт\", вход №3 справа','+7 (383) 367 02 36',82.912488,55.071968),(91,90,'ПОЛБЕРРИ','улица Светлановская, д.50, ТЦ \"Большая Медведица\"','+7 (383) 367 02 36',82.932871,55.081725),(92,90,'ПОЛБЕРРИ','улица площадь Карла Маркса, д. 6, к. 1 \"ТЦ \"Калейдоскоп\"','+7 (383) 367 02 36',82.92043,55.030199),(93,90,'ПОЛБЕРРИ','улица Фабричная, д.55, корп./стр.5','+7 (383) 367 02 36',82.899059,55.029193),(94,90,'Мир Линолеума','улица Светлановская, д. 50, ТЦ \"Большая Медведица\"','+7 (383) 230 39 71',82.932871,55.081725),(95,90,'Пол что надо','улица Светлановская, д. 50, ТВК \"Большая Медведица\"','+7 (800) 234 87 07',82.932871,55.081725),(96,90,'Пол со склада','улица Линейная, 114, к.2','+7 (383) 233 33 11',82.921076,55.051334),(97,90,'ДОМ ПАРКЕТА','улица Ельцовская, д. 20','+7 (383) 236 35 35',82.896454,55.050597),(98,90,'КОЛОРЛОН','улица Богдана Хмельницкого, д. 100','+7 (383) 303 11 88',82.981075,55.0971),(99,90,'КОЛОРЛОН','улица Толмачевская, д. 19А, корп./стр. 1','+7 (383) 303 12 65',82.827858,54.965471),(100,90,'КОЛОРЛОН','улица Барнаульская, д. 19','+7 (383) 303 10 65',82.92043,55.030199),(101,90,'Аксиома','улица Кутателадзе, 4 к. 1','+7 800 775 90 00',83.105141,54.858652),(102,90,'Фарт Плюс','улица Станционная, д. 80/1','+7 (383) 341 30 92',82.789033,54.997977),(103,20,'Мастер пол','улица Первомайская, д. 5, к. 6','+7 913 474 14 09',83.100236,54.767389),(104,90,'ПолДизайн','улица Сибиряков-Гвардейцев, д.47, к. 2, ТЦ \"Н54\"','+7 (383) 230 43 87',82.893651,54.953121),(105,90,'ПолДизайн','улица Карла Маркса площадь, д. 6, к. 1, ТВК \"Калейдоскоп\"','+7 (383) 255 01 55',82.92043,55.030199),(106,90,'ПолДизайн','улица Светлановская, д. 50, ТВК \"Большая Медведица\"','+7 (383) 380 12 02',82.932871,55.081725),(107,90,'Линолеум от склада','улица Светлановская, д.50, ТЦ \"Большая Медведица\"','+7 (383) 255 87 55',82.932871,55.081725),(108,90,'ФОРМУЛА М2','улица Планетная, д. 30, к.1','+7 (383) 319 14 26',82.959955,55.049937),(109,90,'Разрез','улица Светлановская, д. 50/2','+7 (383) 303 44 36',82.928937,55.079653),(110,90,'Разрез','улица Зыряновская, д. 63','+7 (383) 383 24 41',82.94207,55.009967),(111,90,'Декорация Регион','улица Карла Маркса площадь, д.6, корп./стр.1','+7 (383) 335 42 44',82.897451,54.981371),(112,59,'Стильный Дом','улица Ленина, д. 14','+7 (3842) 36 91 48',86.102181,55.521639),(113,59,'Азбука Пола','улица Кирова, д. 18','+7 (3842) 36 82 60',86.077298,55.358525),(114,59,'ДОМИНГО','улица Тухачевского, д. 40/2','+7 (3842) 48 00 77',86.134359,55.326543),(115,59,'ДОМИНГО','проспект Шахтеров 89 а','+7 (3842) 48 00 77',86.116222,55.402674),(116,59,'СТРОЙБАТ','улица Терешковой, д. 39 д','+79511764664',86.146244,55.327608),(117,59,'Диона-Строй','проспект Кузнецкий, д. 234','+7 (3842) 37 77 53',86.044123,55.317445),(118,87,'ДОМИНГО','улица Зорге, д. 15, к. \"А\"','+7 (3843) 92 12 19',87.279675,53.782533),(119,87,'ДОМИНГО','проспект Архитекторов, д. 14, к. \"А\"','+7 (3843) 92 02 09',87.114331,53.892697),(120,87,'Ламинат-НК','проспект Строителей, 55 а','+7 (3843) 46 07 92',87.11911,53.77121),(121,87,'ДОМИНГО','улица Куйбышева, д. 22','+7 (3843) 79 49 47',87.093311,53.757313),(122,87,'ПЕРВОМАСТЕР','шоссе Кондомское, д. 3','+7 (3843) 92 02 09',87.160702,53.742826),(123,87,'Дом Паркета','проспект Николая Ермакова, д. 9','+7 (3843) 20 00 15',87.133223,53.767123),(124,87,'ТОВАРИЩ','проспект Строителей, д. 91','+7 (3843) 45 88 02',87.127887,53.778335),(125,87,'Европол','улица Кирова 72','+7 (3843) 77 77 97',87.154261,53.756371),(126,87,'Европол','улица Белана, д. 18','+7 (3843) 46 58 29',87.127096,53.770157),(127,128,'СТРОЙСЯ','улица Кирова проспект, д.51, корп.а стр.5','+7 (3822) 90 80 02',84.979557,56.464097),(128,128,'СТРОЙСЯ','улица Ленина, д.174','+7 (3822) 90 80 27',84.947658,56.505738),(129,128,'СТРОЙСЯ','Северск(ЗАТО Северск городской округ) улица Коммунистический проспект, д.46','+7 (3822) 90 80 02',84.884371,56.600569),(130,128,'СТРОЙПАРК','улица Вершинина, д.76','+7 (3822) 41 37 07',84.96566,56.450712),(131,128,'СТРОЙПАРК-ГИПЕР','улица Пушкина, д.59/1','+7 (3822) 90 85 55',84.984659,56.49614),(132,128,'ПолБерри','улица Комсомольский проспект, д.44','+7 (3822) 99 42 09',84.982431,56.480023),(133,128,'Материа','улица Обруб, д.10/1','8 (3822) 90 65 60',84.953802,56.488015),(134,128,'Дельта','улица Гоголя, д.15','+7 (3822) 46 96 71',84.961528,56.478173),(135,128,'Альтериери','улица Красноармейская, д. 96','+7 (3822) 27 42 78',84.965894,56.467235),(136,95,'Евростиль','улица Кемеровская, д. 26','+7 (3812) 99 09 50',73.358636,55.001463),(137,95,'Евродизайн','улица Масленникова, д. 64','+7 (3812) 30 46 16',73.397668,54.973099),(138,95,'Альферац','улица Октябрьская, д.127, к. \"А\"','+7 (3812) 38 35 44',73.37857,54.999924),(139,95,'Стильный пол','улица 10 лет Октября, д.182 к. 3, ТК \"Кит Интерьер\"','+7 (3812) 72 92 02',73.458753,54.988804),(140,95,'Стильный пол','улица 70 лет Октября, д.25, к. 4','+7 (3812) 92 54 02',73.325794,54.975233),(141,95,'Стильный пол','улица Учебная, д. 83','+7 (3812) 30 46 76',73.391398,54.97684),(142,95,'Марка пола','улица 70 лет Октября, д.25, к. 4','+7 (3812) 92 50 64',73.325794,54.975233),(143,95,'Меандр','улица Жукова, д. 76','+7 (3812) 53 44 75',73.388712,54.97207),(144,95,'Центр пол','улица Инструментальная, д.2а','+7 (3812) 48 68 97',73.437059,54.918405),(145,68,'Азбука Пола','улица Семафорная, д.289','+7 (391) 269-58-57',92.933043,55.995095),(146,68,'Азбука пола','Вавилова, д.1, корп./стр.39','+7 (391) 232 80 30',92.939807,55.999646),(147,68,'Азбука Пола','улица Молокова, д.60','+7 (391) 200 00 60',92.899814,56.039625),(148,68,'Азбука Пола','улица Калинина, д.8','+7 (391) 200-09-08',92.821274,56.03439),(149,68,'АНТАРЕС','улица Молокова, д.46','+7 (391) 277 08 41',92.902743,56.041627),(150,68,'Арт Паркет','улица Телевизорная, д.1, корп./стр.100','+7 (391) 21 515 52',92.79384,56.028566),(151,68,'АртСтрит','улица Авиаторов, д.39','+7 (391) 288 23 62',92.913181,56.047414),(152,68,'Город мастеров','улица Семафорная, д.271, корп./стр.4','+7 (391) 249 15 91',92.850488,55.981681),(153,68,'Дедал','улица 78-й Добровольческой Бригады, д.1','+7 (391) 277 56 46',92.917646,56.037413),(154,68,'Дедал','улица Красномосковская, д1, к. А','+7 (391) 298 03 98',92.818831,56.01872),(155,68,'Дедал','улица Водопьянова, д.11','+7 (391) 253 42 25',92.909911,56.059695),(156,68,'Дедал','улица Воронова, д.24','+7 (391) 212 88 55',92.944559,56.058147),(157,68,'Каспер','улица 78 Добровольческой бригады, д.28','+7 (391) 241 44 57',92.904908,56.0466),(158,68,'Ламинат Паркетыч','улица Линейная, д.97','+7 (391) 271 68 78',92.879297,56.034405),(159,68,'ЛАМИНАТ-Т-Т','улица 78 Добровольческой бригады, д.10','+7 (391) 277 33 01',92.914843,56.040445),(160,68,'ЛАМИНАТ-Т-Т','улица Робеспьера, д.25','+7 (391) 279 02 02',92.841684,56.007941),(161,68,'Линолеум','улица Северная, д.10','+7 (391) 223 23 90',92.834596,56.025467),(162,68,'Линолеум','улица Гладкова, д.6/1','+7 (391) 258 68 55',92.879539,55.989048),(163,68,'Мир Линолеума','улица Академика Вавилова, д.1, к. 39','+7 (391) 274 28 71',92.910253,55.991752),(164,68,'Мир Линолеума','улица Батурина, д.40, корп./стр.а','+7 (391) 272 72 44',92.904773,56.039645),(165,68,'ПаркетПол','улица 9 мая, д.79','+7 (391) 240 83 50',92.897443,56.048706),(166,68,'ПАРТНЕР-СИБИРЬ','улица Семафорная, д.289','+7 (391) 277 55 00',92.933043,55.995095),(167,68,'ПИЛОН','улица Юшкова, д. 8','+7 (391) 290 60 60',92.771059,56.031589),(168,68,'ПИЛОН','улица Вавилова, д. 1, стр. 17','+7 (391) 217 90 80',92.916774,55.991496),(169,68,'Студия пола','улица Академика Вавилова, д.1 стр.48','+7 (391) 2 695 605',92.909758,55.992905),(170,68,'Ламинат-Эксперт','улица Авиаторов, д. 5','+7 (391) 293 80 19',92.922461,56.044493),(171,68,'МегаПолис','улица Академика Вавилова, д.1, ст. 10','+7 (391) 215 14 75',92.915076,55.992216),(172,68,'Ардон Торговы Дом','улица Телевизорная, 1. стр.9','+7 (391) 231 73 97',92.794154,56.021623),(173,68,'Студия напольных покрытий и дверей','улица Ладо Кецховели, д.22, корп./стр.а','+7 (391) 209 97 00',92.814528,56.003356),(174,1,'Авангард','улица Пушкина, д.221','+7 (3902) 28 55 44',91.405673,53.712786),(175,1,'КОМФОРТ','улица Жукова, д.22','+7 (3902) 34 33 75',91.461656,53.725068),(176,1,'Ламинат19.ru','Кравченко, д.11л, корп./стр.1','+7 (913) 440 37 70',91.411369,53.715717),(177,1,'Ламинат плюс','Некрасова, д. 8','+7 (3902) 26 80 62',91.426173,53.718163),(178,1,'Склад Линолеум','Кирова, д.251, корп./стр.1','+7 (3902) 321 325',91.467504,53.729814),(179,1,'Новый Дом','улица Аскизская, д.150','+7 (3902) 35 34 94',91.415213,53.698262),(180,1,'Овалтун,','улица Советская, д.182 з','+7 (3902) 204 204',91.374241,53.715035),(181,1,'Паркет19.ru','Лермонтова, д.21','+7 (3902) 32 44 22',91.418061,53.74019),(182,1,'Основа Дома','Советская, д.209м','+7 (3902) 30 53 13',91.41277,53.717816),(183,1,'Основа Дома','улица Аскизская, д.150, корп./стр.В','+7 (3902) 35 26 03',91.446609,53.706161),(184,13,'Decoroff','6 мкр, д.16','+7 (39151) 3 02 12',90.498222,56.264232),(185,13,'Door&Floor','мкрн 1, д.42 а','+7 (39151) 5 78 75',90.493389,56.263337),(186,13,'Мастер С','улица Калинина, д.11','+7 (39151) 4 64 64',90.466943,56.248116),(187,13,'Формика','25 квартал, д.6','+7 (39151) 7 02 11',90.475108,56.248196),(188,15,'Алтай пол','улица Балтийская, д. 88','+7 (3852) 538–478',83.662663,53.337237),(189,15,'Гауди','Красноармейский проспект, 15','+7 (3852) 50–42–99',83.784923,53.327364),(190,15,'Гауди','проспект Космонавтов, д. 6, к. \"Г\"','+7 (3852) 50–52–99',83.73504,53.390729),(191,15,'Гауди','ТЦ Поместье, Свободы площадь, 6','+7 (3852) 501–299',83.788993,53.330634),(192,15,'Аксиома','Павловский тракт 166','+7 (3852) 271–888',83.654766,53.347231),(193,15,'Знак','проспект Строителей д. 92','+7 (3852) 36–40–80',83.745963,53.34171),(194,15,'ПолБерри','Павловский тракт д.27','+7 (3852) 50–54–37',83.729623,53.339554),(195,21,'Марка пола','улица Октябрьская, д.31','+7 (3854) 555–666',85.226162,52.539128),(196,57,'Белый Дом','Грабцевское шоссе, д.4, корп./стр.Б','+79807164230',36.284472,54.521178),(197,96,'Лесоторговая','Городская, д.98','+7 486 271-48-01',36.068706,52.943995),(198,96,'ЛАиС','улица Машиностроительная, д.6','+7 486 244-24-02',35.995152,52.918874),(199,96,'Паркет Маркет','улица Карачевская, д.68, корп./стр.А','+7 486 244-53-84',36.056983,52.954603),(200,96,'Судия Плитки','Наугорское шоссе, д.5','+7 486 244-00-01',36.049231,52.980354),(201,96,'Добрострой','Кромское Шоссе, д.23','+7 486 263-24-56',36.010289,52.918098),(202,110,'Стройка','Окружная дорога, 185 км, корп./стр.6 (Гипермаркет \"Стройка\")','+7 491 230-03-00',39.637461,54.644614),(203,117,'Remont Doma','Кутузова, д.11','+7 481 262-00-00',32.046499,54.815773),(204,117,'Планета','улица Нахимова, д.23','+7 481 264-03-30',32.01354,54.779452),(205,117,'Планета','Кирова 23','+7 481 265-11-52',32.032863,54.769762),(206,117,'Мир Ламината','улица Кашена, д.8','+7 481 240-84-53',32.045152,54.795499),(207,117,'Строймаркет','улица Смольянинова, д.2','+7 481 262-00-00',32.089592,54.78358),(208,130,'РазноПолье','улица Мосина, д.2, корп./стр.а (ТЦ СТРОЙ ДВОР)','+7 906 621 56 52',37.608275,54.201739),(209,130,'ИНТЕРДЕКОР','Красноармейский проспект, д.44','+7 487 270-13-13',37.582826,54.199901),(210,130,'ИНТЕРДЕКОР','Проезд Энергетиков, д.1','+7 487 270-13-13',37.588404,54.142763),(211,130,'Квадратный Метр','улица Староникитская, д.37','+7 487 238-44-08',37.626564,54.187617),(212,130,'Чипак','улица Скуратовская, д.109','+7 487 270-50-50',37.590785,54.131058),(213,94,'Керамо Рум','Киевское шоссе, д.59 (СГ \"Северный\" Торговая Галерея, секция 3, 1 этаж)','+7 (910) 5235022',36.643897,55.119528),(214,94,'Домашний Мастер','улица Ф. Энгельса, д.10, офис/кв.18','+7 (484) 395-49-02',36.614945,55.119636),(215,94,'Домашний Мастер','Киевское шоссе, д.33 (ЯРМАРКА на 101 км. Павильон М-1-А)','+7 484 399-36-52',36.640672,55.110965),(216,94,'ТЦ Михалыч','улица Красных зорь, д.18, корп./стр.А (ТЦ МИХАЛЫЧ)','+7 484 396-47-00',36.617119,55.092566),(217,43,'Спектр','4 ый микрорайон','+7 481 533-30-45',33.291322,54.148411),(218,88,'ИНТЕРДЕКОР','улица Генерала Белова, д.2, корп./стр.А','+7 487 627-06-19',38.318833,54.026937),(219,35,'Мелодия пола','улица Холмистая 68, копрус 1, павильон 145','+7 (473) 229 08 05',39.122906,51.672305),(220,26,'ООО \"Брянскспецстрой\"','улица Кромская. 50 склад 707','+79308200185',34.289817,53.315864),(221,18,'Мегадекор','улица Супруновскя 25 А','+7 (472) 240-26-61 +7 (952) 438-83-55',36.581581,50.588129),(222,73,'ООО ТК \"СтройСнаб\"','Проспект В.Клыкова 1А','+7 (4712) 22 20 20',36.134499,51.721881),(223,73,'ООО ТК \"СтройСнаб\"','Магистральный проезд, д. 34б','+7 (4712) 200070',36.172947,51.658406),(224,35,'Идеальный пол','улица Средне-Московская, 31','+7 (473) 261 04 72',39.195131,51.668914),(225,121,'Салон Мир плитки и ламината','микрорайон Рождественский, д.6','+7(4725)415-115',37.898458,51.320979),(226,73,'ТД \"Олимпия\"','улица Литовская 6В','+7 (4712) 35 06 79',36.177582,51.705386),(227,73,'ТД \"Олимпия\"','2-ой Литовский переулок 4Б','+7 (4712) 35 06 79',36.177735,51.704331),(228,51,'ТД \"Олимпия\"','Алексеевский проезд 1А','+7 (47148) 4-32-73',35.338879,52.346064),(229,18,'Новопол','улица Магистральная, дом 2а','+7 (4722) 777 236',36.54078,50.573933),(230,18,'Двери под ключ','улица Магистральная, д.4, корп./стр.Д (ТЦ \"Спутник Дом\" мод 98)','+7950-718-18-88',36.53618,50.571736),(231,18,'Евро дом','улица Костюкова, д.14','+7(4722) 554594',36.574961,50.579338),(232,18,'КАРУЦЯК/КРАСНОДЕРЕВЩИК','улица Восточная, д.71','+7 (4722) 372815',36.544355,50.575506),(233,18,'КАРУЦЯК/ИДЕЯ ПАРКЕТА','Улица Магистральная, д.4, корп./стр.Д (ТЦ Спутник Дом)','+7 (4722) 231013',36.53618,50.571736),(234,18,'Наполка 31','улица Студенческая улица, д.38','+7(920)5660335',36.5656,50.619555),(235,18,'Наполка 31','улица Серафимовича, д.66, корп./стр.А','+7(910)3650699',36.623128,50.585899),(236,121,'KERAMIX','микрорайон Надежда, д.10','+79194320889',37.9086,51.318007),(237,38,'БУРЦЕВА/ГОРИЗОНТ','улица Преображенская, д.7','+7(952)4270781',37.499965,51.277841),(238,51,'Квадратный метр','Улица Ленина, д.65','+7 (47148) 77276',35.343802,52.351125),(239,73,'ЛАМИНАТ 54','переулок 2ой Литовский, д.8','+7 (4712) 326227',36.171905,51.701284),(240,35,'ЛИДЕР','улица Пеше-Стрелецкая, д.56','+7 (473) 2330077',39.158345,51.663697),(241,121,'Мастер/Володин А.Н','микрорайон Приборостроитель, д.53','+7 (4725) 436062',37.823035,51.289289),(242,26,'МОРОЗОВ/МЕТР КВАДРАТНЫЙ','улица Сталилетейная, д.14','+7 (4832) 422899',34.244623,53.329246),(243,18,'Новопол/Магистральная','Улица Магистральная, д.2, корп./стр.а, офис/кв.100','+7 (4722) 510072',36.53618,50.571736),(244,6,'Спутник Новосел','улица Революционная, д.32','+7 (47234) 44248',38.685831,50.617594),(245,144,'Столица','Звездная улица, д.9к2','+7(920)5888858',40.308,52.383547),(246,35,'СтройМикс','улица Ворошилова, д.16','+7 (473) 2615000',39.177452,51.655556),(247,121,'Терем','улица Юбилейная, д.1, корп./стр.А','+7 (4725) 454444',37.869801,51.309179),(248,18,'Товарищ','улица Студенческая, д.17, корп./стр.B','+7 (4722) 358826',36.591067,50.622864),(249,26,'ХЛЕСТОВ/Современный Дом','проспект Московский, д.2, корп./стр.б','+7 (980) 3075514',34.367099,53.220616),(250,35,'Виниловый Пол, Воронеж','Ленинский проспект, д.156, корп./стр.А (на первом этаже ТЦ \"Перестройка\")','+7 (473) 2405334',39.231755,51.629332),(251,35,'ИДЕЯ ПАРКЕТА','улица Урицкого д.70 (цокольный этаж)','+7 (473) 207-16-57',39.199631,51.682949),(252,35,'КВАРТАЛ','улица Димитрова, д.120','+7 (473) 247-55-57',39.287037,51.656193),(253,35,'КВАРТАЛ','улица Пешестрелецкая, д.28, корп./стр.А','+7 (473) 260-51-33',39.169529,51.667026),(254,35,'Мир Паркета','Ленинский проспект, д.26, корп./стр.3','+7 (473) 240-13-47',39.232851,51.635023),(255,35,'Мозайка Интерьера','Хользунова, д.38 (С торца жилого дома (ЖК Острова))','+7 (473) 228-99-25',39.165738,51.701987),(256,35,'Style Floor','40 лет Октября, д.12','+7 (473) 2027814',39.181414,51.670177),(257,35,'San Floor','Шишкова, д.146','+7 (473) 2400686',39.185905,51.713975),(258,75,'Добрострой','Лебедянское шоссе, д.2, корп./стр.Б','+7 (474) 220-30-97',39.528037,52.630312),(259,75,'Ёлки Палки','проспект Победы, д.70, корп./стр.Б','+7(474) 271-00-40',39.572378,52.602132),(260,75,'ИДЕЯ ПАРКЕТА','улица Космонавтов, д.92','+7(4742) 71-31-61',39.548043,52.609859),(261,75,'КРАСНОДЕРЕВЩИК','улица Зегеля, д.30','+7(4742) 39-26-41',39.598205,52.614075),(262,75,'КРАСНОДЕРЕВЩИК','улица Меркулова, д.10','+7(4742) 71-09-08',39.543821,52.590158),(263,75,'ИДЕЯ ПАРКЕТА','улица Катукова, д.23','+7(4742) 71-31-51',39.520563,52.584741),(264,123,'КемиСтрой','улица Кавказкая, д.1, корп./стр.А','+7 (475) 244-02-49',41.390479,52.729624),(265,24,'Олимп','улица Пролетарская, д.42','+7 (47354) 5-30-84',42.087975,51.362237),(266,37,'НОВЫЙ ДОМ','улица Осоавиахима, д.13','+7 (47461) 2-13-04',39.941783,52.498768),(267,107,'Стройбат','переулок Песочный, д.2, корп./стр.Г','+7 (47396) 5-14-21',39.588422,50.184773),(268,48,'Строй Двор/(Фурсов С.А.)','д. Заболотье, д.82','+7 (496) 404‑00-57',39.070849,55.392219),(269,72,'Умелый','улица Ленина, д. 1А','+7 (915) 013-73-73',38.913221,55.57008),(270,48,'Зимина И.Е.','Рязанская улица, д.101 (ТД ОТДЕЛОЧНЫЕ МАТЕРИАЛЫ)','+7 (496) 404‑14-64',39.064264,55.384047),(271,64,'Паркет Стиль/(Бойко А.В.)','Октябрьская улица, д.88, стр.1 (ТК СТРОЙЛЕНД, 2 этаж, пав. 39)','+7 (985) 581-53-33',38.85961,55.059497),(272,104,'Дворцов Ю.А.','Донинское шоссе, д.1 (С/Р РАДУГА, пав. 5А-2)','+7 (926) 544-59-89',38.235505,55.573042),(273,97,'ДОКА/(Уланов П.А.)','улица Урицкого, д. 92 (ТЦ БАРРИКАДА, 1 этаж)','+7 (496) 422-36-40',38.966474,55.822453),(274,64,'Лагун А.Ю.','улица Октябрьской революции, д.387 (ТК КОЛОМЕНСКИЙ СТРОИТЕЛЬ, пав. 3)','+7 (910) 414-35-67',38.815296,55.08106),(275,80,'\"Маляров\" ООО','улица Борисовские пруды, д.1, корп./стр.72','+7 (495) 980-01-62',37.738387,55.635858),(276,97,'От Винта','улица Северная, д.30, стр.2','+7 (936) 000‑48-48',38.97764,55.831848),(277,105,'Паниш А.Ф.','Носовихинское шоссе, д.4 (С/Р НИКОЛЬСКИЙ, линия 4, пав. 37)','+7 (926) 719-03-65',37.856165,55.745357),(278,105,'Триумф','улица Академика Чаломея, д.12 (ТЦ АЛЛАДИН, 1 этаж, правое крыло)','+7 (499) 409-09-15',37.878506,55.749005),(279,52,'Материалы для отделки','улица Театральная, д.3 (С/Я НА ТЕАТРАЛЬНОЙ, пав. 252)','+7 (926) 219-07-80',38.077249,55.610097),(280,76,'Стройка','улица Куйбышева, д.2, стр.Б','+7 (496) 630‑12-12',39.004338,54.975972),(281,19,'Огирин Ю. Л.','улица Коммунальная, д. 53/54, Т/К БЕЛООЗЕРСКИЙ, пав. Г-1','+7 (926) 858-47-19',38.441444,55.461038),(282,80,'Бутов В.А.','Тихорецкий Бульвар, д.1 (ТЦ ЛЮБЛИНСКОЕ ПОЛЕ, пав. Е-129)','+7 (915) 055-23-55',37.779404,55.6779),(283,80,'Кутепов А.А.','Тихорецкий бульвар, д.1 (ТК ЛЮБЛИНСКОЕ ПОЛЕ, пав. Б-134)','+7 (495) 995-79-67',37.779404,55.6779),(284,52,'Попов П.В.','улица Театральная, д.3 (С/Я на ТЕАТРАЛЬНОЙ, пав. 236)','+7 (965) 404-55-48',38.077249,55.610097),(285,77,'Гончеренко К.С.','улица Инициативная, д.8 (ТК ЭСТАКАДА, И-9,10,11)','+7 (963) 766-66-37',37.896356,55.683518),(286,53,'Зарайский кооператор','улица К. Маркса, д.27, -1 этаж','+7 (929) 546-93-11',38.878843,54.762054),(287,80,'Дюс-М','Москва, МКАД 71 км. (ТЦ КУХНИ ПАРК)','+7(495)1234601',37.39762,55.858655),(288,82,'ОстМаркет','Трудовая, д.37','+7(495) 648-64-07',37.711087,55.891542),(289,114,'Коваленко А.А.','проспект Красной Армии, д.212, корп./стр.Б (Строй Континент)','+79663400504',38.128085,56.293202),(290,45,'Уют','улица Пушкинская, д.2а','+7(496)2241800',37.535403,56.35117),(291,80,'ВМ-Профиль','Новорижское шоссе (9км. МКАД, \"ТК ПЕТРОВСКИЙ\" , пав -9)','+79268441750',37.301204,55.803843),(292,114,'Строй Центр','Центральная, д.1, корп./стр.Б','+7(496)5475858',38.1518,56.291948),(293,54,'Макшинов М.В.','Пятницкое ш. (С/Р БРЁХОВО, пав 25,61)','+79168249951',37.214382,55.991893),(294,102,'Назаров А.Н.','улица Институтская, д.27 (Магазин \"О`ПУШКИНО\")','+79265459615',37.860953,56.01861),(295,93,'Усадьба','ул. Ильича, д. 21','+79269095986',38.479075,55.880445),(296,14,'Ремонтник','Текстильщиков, д.18 (СР \"МАСТЕРОК\", пав. Ж-6)','+79263884925',37.960163,55.80916),(297,80,'Чижиков С.В.','улица Пришвина, д.26А (ТЦ МИЛЛИОН МЕЛОЧЕЙ, А-08)','+7(909) 9067276',37.603954,55.884575),(298,46,'Колорит','Проспект Боголюбова, д.45','+7(496)2181918',37.1416,56.72839),(299,136,'Шамшурин С.А.','Ленинградское шоссе 29 км (СТ ЛЕНИНГРАДКА, пав. 35)','+7(495)2081011',37.339032,55.958139),(300,80,'Линолеум-Склад','1 км. Киевского шоссе (С/Р СТРОЙМАСТЕР, пав. 182-183)','+7 (495) 191 20 56',37.566458,55.744384),(301,80,'Изолюкс','улица Молодогвардейская, д.61','+7 (495) 274 00 43',37.391071,55.733574),(302,80,'Euro-Floor','улица 2я Карпатская, д.4 (район Солнцево)','+7 (495) 983 58 57',37.37209,55.654909),(303,80,'Территория Ремонта','41 км. МКАД (С/Р СЛАВЯНСКИЙ МИР, пав. А-15/1)','+7 (495) 221 05 05',37.49329,55.609823),(304,80,'Торгово-Архитектурная компания','22 км Киевское шоссе, д.4 (БП РУМЯНЦЕВО, пав. Г-151)','+7 (495) 969 07 90',37.441026,55.634151),(305,80,'Власова Л.А','41 км. МКАД (С/Р СЛАВЯНСКИЙ МИР, пав. Г 6/9)','+7 (925) 011 89 88',37.49329,55.609823),(306,80,'Магия Паркета','Каширское шоссе, д.19, корп./стр.1 (ТК КАШИРСКИЙ ДВОР-1, пав. 2Д-119)','+7 (926) 599 55 68',37.62864,55.674438),(307,80,'Vintage','улица Амбулаторная, д.50 (ТЦ МОЖАЙСКИЙ ДВОР, пав В-27)','+7 (909) 900 25 95',37.37527,55.712431),(308,80,'Vintage','МКАД 25 км. ( ТК КОНСТРУКТОР, пав.1.9)','+7 (495) 021 06 24',37.722199,55.588888),(309,80,'Мальцев А.В.','32 км. МКАД (ТК КАШИРСКИЙ ДВОР-3, пав. 10)','+7 (968) 602 77 71',37.61733,55.574711),(310,80,'Паркет Для Вас','Каширское шоссе, д.19, корп./стр.1 (ТК КАШИРСКИЙ ДВОР-1, пав. 2А-48)','+7 (968) 532 28 22',37.62864,55.674438),(311,80,'СоюзТорг-М','41 км. МКАД (С/Р СЛАВЯНСКИЙ МИР, пав. Пассаж 16)','+7 (495) 664 98 02',37.49329,55.609823),(312,80,'ЛегкоПол','Варшавское шоссе, д.69 к.1','+7 (495) 225 08 31',37.620177,55.66178),(313,9,'Планета ремонта','улица Пландина, д.10','+7(83147) 9 58 80',43.808716,55.402587),(314,9,'Новосел','улица Пландина, д.15','+7(83147) 9 66 77',43.814088,55.405363),(315,112,'Южный','улица Попова, д.53','+7(8342) 32 00 19',45.142894,54.169511),(316,112,'ЮЖНЫЙ/Строй дом','Александровское шоссе, д.8, корп./стр.а','+7(8342) 35 12 88',45.20354,54.217942),(317,81,'ВМС','улица Ленина, д.28','+7(49234) 2 27 60',42.05401,55.577571),(318,81,'ТЦ Витязь','улица Советская, д.10 (ТЦ ВИТЯЗЬ)','+7(49234) 9 99 94',42.051791,55.577231),(319,86,'Интерьер+','улица Генкиной, д.25, корп./стр.а','+7(831) 413 67 78',44.003435,56.309629),(320,86,'КОЧЕРГИН/Vitality','улица Касьянова, корп./стр.4','+7(987) 741 17 68',44.069776,56.292008),(321,86,'МИР ПАРКЕТА','улица Бекетова, д.13','+7(831) 283 11 80',43.99818,56.290649),(322,86,'Полы-Двери','улица Ижорская, д.18','+7(831) 416 75 76',44.021437,56.315495),(323,86,'НАПОЛЬНЫЙ ДВОРЪ','улица Бекетова, д.13','+7(831) 4 166 533',43.99818,56.290649),(324,86,'Линолеум Города','улица Бекетова, д.32','+7(831) 280 82 83',44.002923,56.291883),(325,98,'АРХИТЕКТОР','улица Сенная, д.7','+7 (83171) 2-23-22',43.068783,55.965431),(326,98,'Дом и сад','улица Вокзальная, д.1','+7(83171) 3 13 29',43.099299,55.973914),(327,30,'ДОМИКС','улица Куйбышева, д.28, корп./стр.а','+7(9506) 25 48 48',40.381024,56.160035),(328,30,'Калашников','улица Куйбышева, д.26, корп./стр.А (с/р ТАНДЕМ, блок \"Юг\")','+7(4922) 35 42 29',40.381024,56.160035),(329,30,'Сантехстройсервис','м-н Юрьевец улица Странционная, д.2','+7(4922) 47 40 49',40.263416,56.096423),(330,63,'Аладдин','улица Шмидта, д.14, корп./стр.6','+7(49232) 9 06 00',41.297494,56.362132),(331,30,'Паркетный двор','улица Куйбышева, д.26, корп./стр.а (блок \"Юг\")','+7(910) 177 77 87',40.381024,56.160035),(332,143,'Напольная ярмарка','улица Всполинское Поле, д.1, корп./стр.б','+7(4852) 28 22 24',39.845583,57.6181),(333,143,'СВКОМ/ДЕКОР ПРЕСТИЖ','улица Всполинское поле, д.11, корп./стр.Б','+7(4852) 67 11 37',39.845583,57.6181),(334,99,'Настоящие двери','ул.Октябрьская, д.35','+7(48535) 6 00 07',38.882122,56.745878),(335,109,'Строймаркет','улица Плеханова, д.20','+7(4855) 24 35 05',38.833532,58.044153),(336,34,'Лидер','улица Преображенская, д.28','+7(8172) 52 05 20',39.891523,59.220496),(337,34,'ЦЕНТР СМ/ИДЕИ ВАШЕГО ДОМА','Окружное шоссе, д.11, корп./стр.а','+7(8172) 23 93 39',39.81151,59.21999),(338,137,'ПОЛMASTER','улица Гоголя, д.54','+7(8202) 28 19 58',37.961564,59.127604),(339,66,'Линолеум','ул.Галичская, д.108','+74942) 48 00 80',40.967686,57.779432),(340,124,'Stone Step','ул.Спартака, д.19','+7(4822) 63 05 05',35.883411,56.850289),(341,124,'Настил.ru','ул.Склизкова, д.36','+7(960) 717-97-97',35.910944,56.841492),(342,131,'Твой Дом','ул.Энтузиастов, д.1, корп./стр.г','+7(915) 743 63 47',35.015333,57.865473),(343,129,'Студия обоев','ул.Дзержинского, д.23','+7(980) 641 16 63',34.969384,57.039448),(344,106,'Дельтастрой','Шоссе Ленинградское, д.40, корп./стр.а','+7(48232) 6 01 09',34.308008,56.269971),(345,61,'LEROY MERLIN','улица Луганская, д.53, корп./стр.1','+7 (8332) 20‑57-99',49.612255,58.63571),(346,61,'Алтай Сервис','улица Производственная, д.28, корп./стр.в','+7 (8332) 34‑01-10',49.638387,58.502931),(347,61,'Ламинат Маркет','улица Производственная, д.28, корп./стр.б','+7 (8332) 44-80-55',49.638387,58.502931),(348,61,'Динтера','улица Труда, д.71','+7 (8332) 21-08-62',49.664079,58.608804),(349,61,'Ламинат паркет пробка','улица Московская, д.102, корп./стр.В (пав 11з)','+7 (8332) 78-26-26',49.689645,58.603755),(350,61,'СТРОЙ РЕМО','улица Труда, д.71','+7 (8332) 71-55-77',49.664079,58.608804),(351,61,'СТРОЙ РЕМО','проспект Октябрьский, д.107','+7 (8332) 35-71-50',49.656156,58.608537),(352,61,'ХИМТОРГ','улица Комсомольская, д.8','+7 (8332) 37-32-43',49.669739,58.584827),(353,61,'ЕВРОСТРОЙ','улица Базовая, д.8/2','+7 (8332) 21‑72-16',49.604916,58.565111),(354,86,'Vitality','улица базарная, д.8','+7 (986) 767-12-32',43.86028,56.352856),(355,86,'ЛАМИНАТ ЛИНОЛЕУМ','улица Моторный переулок, д.2, корп./стр.3','+7 (8312) 97-21-08',43.865068,56.255277),(356,86,'М2','улица Горловская, д.12','+7 (8312) 78-65-10',44.008663,56.293886),(357,86,'Новосел','улица Культуры, д.110, корп./стр.Б','+7 (8312) 23-70-17',43.864385,56.348791),(358,44,'ДОМИКС','улица Петрищева, д.2, корп./стр.А','+7(8313) 31‑44-22',43.433607,56.235806),(359,103,'Монарх','Георгиевсое шоссе,1й км','+7(928) 3407928',43.135618,44.032492),(360,62,'Домас','улица 40 Лет Октября, д.37 (мясокомбинат)','+7+7(928) 637730',42.700528,43.91672),(361,62,'Магазин Напольных Покрытий','улица Железнодорожная, д.20','+7(961) 4844202',42.726255,43.926616),(362,50,'Строймаг','улица Луначарского, д.61','+7(928) 3546601',42.856062,44.039685),(363,103,'Уют, Пятигорск','Кисловодское шоссе, д.2','+7(906) 4734682',42.988096,44.054704),(364,103,'Арсенал','улица Ермолова, д.272','+7(928) 2673161',43.032311,44.04115),(365,103,'ВОТЕР ЛЕНД','Черкесское шоссе (2 км)','+7(928) 3102525',42.988851,44.058092),(366,103,'Гермес','улица Ермолова, д.43','+7(928) 3790008',43.033506,44.039264),(367,103,'Держава','улица Почтовая (База Левада)','+7(928) 3033572',43.11713,44.037638),(368,103,'Стильный Ламинат','Кисловодское шоссе, д.2','+7(928) 8273034',42.988096,44.054704),(369,84,'Стройдвор','улица Калюжного, д.4','+7(928) 0818022',43.602966,43.505865),(370,84,'Строймаркет','улица Кабардинская, д.143','+7(928) 0380080',43.623942,43.489824),(371,29,'Мегадом','улица Дзержинского, д.72а','+7(915)4062104',44.662143,43.020576),(372,29,'СОМ','улица Московская, д.3','+7(928) 4870695',44.653717,43.057944),(373,78,'ИП Келигов','Трасса Назрань-Магас (Рынок)','+7(928) 7254404',44.81624,43.171501),(374,83,'ТЦ Строймаркет','улица Картоева, д.148а','+7(962) 6485034',44.751525,43.225504),(375,79,'ИП Гаджиев','улица Амедхана Султана, д.33','+7(928) 5701000',47.504682,42.98306),(376,79,'Милано','улица Ирчи Казака, д.33в','+7(928) 5007003',47.508868,42.963548),(377,79,'Примо','улица Батырая, д.167','+7(967) 4246661',47.519774,42.972856),(378,42,'Паркет Стиль','улица Агасиева (на мосту)','+7(967) 7777610',48.277988,42.065468),(379,10,'ТЦ Держава','улица Железнодорожная, д.76','+7 (86137) 73787',41.134216,44.991275),(380,10,'Интерьер','улица Мира, д.42','+7 (86137) 27744',41.13418,44.99239),(381,10,'Наполкин 23','улица Мира, д.81','+7+7 (989) 800444',41.134692,44.993059),(382,10,'Двери и К','улица Ефремова, д.190','+7 (86137) 22477',41.104967,44.9948),(383,10,'Двери и К','улица Тургенева, д.113','+79180866713',41.126581,44.99202),(384,69,'Двери и К','ул. Красная 217-А','+78613877696',40.548586,45.431227),(385,69,'ТЦ Ваш Дом','улица Красная, д.217','+7 (86138) 70-8-03',40.548586,45.431227),(386,71,'Двери и К','улица Матросова, д.203, корп./стр.1','+79183509045',40.573434,44.878131),(387,74,'Евродизайн','улица Победы, д.268','+7 (918) 1587886',40.734187,44.647272),(388,74,'Двери и К','улица Победы, д.324','+79884702183',40.737951,44.640589),(389,127,'Двери и К','улица Октябрьская, д.65','+78619670878',40.13334,45.860637),(390,127,'Шанс','улица Подвойского, д.73, корп./стр.1','+7 (86196) 72074',40.125264,45.848352),(391,27,'Стройторг',', проспект Чехова, д.316','+7 (86559) 41222',44.165464,44.79223),(392,55,'Спектр','улица Гагарина, д.18','+7 (903) 4144150',42.913887,45.703656),(393,85,'Все для Пола','улица, 3-го Интернационала, д.128','+79054107886',41.952653,44.633098),(394,85,'Двери и К','улица Калинина, д.48','+79880867071',41.950084,44.622648),(395,85,'Тц Мегаполис','улица Полевая, д.1','+7 (928) 3239649',41.969389,44.636305),(396,85,'Тц Мегаполис','улица Калинина, д.153 (Район ПРП)','+7 (86554) 55777',41.97318,44.62402),(397,120,'Европол','улица Кулакова, д.6А','+7 (8652) 720153',41.911097,45.046947),(398,120,'Домкс','улица Пирогова, д.27','+7 (8652) 330033',41.91302,45.010114),(399,120,'Домикс','улица Южный обход, д.4','+7 (8652) 330033',41.928273,44.991249),(400,120,'Ламинатный 26','улица Пирогова, д.15, корп./стр.2','+7 (962) 4035542',41.916092,45.020016),(401,120,'Центр Паркета','улица Пирогова, д.15, корп./стр.1','+7 (928) 3570001',41.911942,45.020698),(402,120,'Leroy Merlin','трасса Невинномысск-Ставрополь 2 км','+78652302100',41.969083,45.044521),(403,138,'Строительный Двор','улица Кавказская, д.147','+7 (8782) 267027',42.070539,44.230396),(404,138,'Стройоптторг','улица Октябрьская, д.301','+7 (8782) 273264',42.058098,44.201963),(405,138,'Держава','переулок Пролетарский, д.1','+7 (8782) 273264',42.042925,44.238056),(406,140,'Строй Дом','улица Хомутникова, д.2','+7 (84722) 41517',44.253158,46.312069),(407,40,'Триумф','улица Красноармейская (Центральный рынок, с обратной стороны)','+7 (86160) 32789',40.691661,45.363736),(408,120,'Держава','улица Буйнакского, д.3','+7 (8652) 386111',41.929665,45.067131),(409,23,'Двери и напоные покрытия','улица Фрунзе, д.1','+7+7962442858',43.428648,45.112273),(410,49,'ДВЕРИ И К','ул. Мичурина, д.9','+7 (86132) 50506',38.27663,46.685937),(411,49,'ЕвроЛюкс','ул. Мира, д.171','+7 (86132) 367 65',38.285721,46.702397),(412,49,'Стройстандарт','ул. Мичурина, д.25, корп./стр.2','+7 (86132) 681 11',38.274483,46.687587),(413,4,'EURO Мастер','ул. Мира, д.30, корп./стр.111','+7 (86342) 4-20-04',39.437038,47.095335),(414,4,'СТРОЙ!ЭКОНОМ','ул. Ленина, д.83','+786342 7 05 80',39.428001,47.104818),(415,5,'LEROY MERLIN','Аксайский проспект, д.23','+7 (863) 204-03-60',39.846535,47.290051),(416,5,'М2 Держава','Новочеркасское шоссе, д.30 (Колхозный рынок)','+7 (928) 1017654',39.853066,47.291017),(417,16,'ДВЕРИ И К','ул.Куйбышева, д.63','+7 (863) 5458507',39.738665,47.147172),(418,17,'Каскад','Белая Калитва, ул. Калинина, д.8','+7 (86383) 25800',40.804552,48.17336),(419,32,'ДВЕРИ И К','ул. Кошевого, д.25, корп./стр.а','+7 8639 24 39 24',42.208098,47.518017),(420,32,'Квадратный Метр','ул. Степная, д.100, корп./стр.Б','+7 8639 22 77 36',42.139072,47.514396),(421,32,'Колорит','ул. Пионерская, д.138','+7 (8639) 27 17 71',42.156957,47.505236),(422,39,'Эврика','ул. Мира, д.14, корп./стр.Б','+7 (909) 4375989',40.042314,48.031636),(423,91,'ДВЕРИ И К','ул. Гагарина, д.107, корп./стр.19','+7 (86352) 65085',40.100165,47.43845),(424,92,'Строймаркет','ул. Трамвайная, д.3','+7 (86369) 2-33-33',39.908052,47.788403),(425,108,'LEROY MERLIN','Обсерваторная, д.15','+7+7 (863) 20 06 46',39.7267,47.309453),(426,108,'ДВЕРИ И К','пр.Стачки, д.155','+7 (863) 269-69-25',39.65152,47.211606),(427,108,'ДВЕРИ И К','ул. Текучева, д.109','+7 (863) 291-36-26',39.692932,47.232297),(428,108,'ДВЕРИ И К','ул. Фурмановская, д.108','+7 (863) 235-23-24',39.720268,47.270819),(429,108,'М2 Метр Квадратный','Рынок Атлант ряд 4 магазин 1','+7 (928) 167 60 69',41.903497,47.601747),(430,108,'Мегаполис Групп','пр. 40 - летия Победы, д.334, корп./стр.ж','+7 (863) 226 13 88',39.789133,47.233509),(431,108,'Мир Напольных Покрытий','ул. Зорге, д.17','+7 863 300 76 87',39.630823,47.221301),(432,108,'Напольные покрытия','ул. Вавилова, д.57','+7 (863) 273 25 51',39.683383,47.268178),(433,108,'ПОЛЫ.сом','ул. Малиновского, д.48, корп./стр.в','+7 (928) 270-92-63',39.61441,47.199374),(434,111,'ДВЕРИ И К','ул.Дмитрова, д.5','+7 (86372) 50005',41.541665,46.475332),(435,111,'Мангазея','ул. Школьная площадь, д.34, корп./стр.Б','+7 (8637) 258556',41.516153,46.470206),(436,122,'ЛЕМАКС','ул. Маршала Жукова, д.53, корп./стр.В','+7 (8634) 3222223',38.927298,47.274469),(437,122,'ЛЕМАКС','шоссе Поляковское, д.11/2','+7 (8634) 649777',38.878564,47.214721),(438,122,'ТОПЛИНГ','Таганрог, ул.Нестерова, д.21, корп./стр.1','+7 (8634) 477 877',38.846144,47.224538),(439,122,'ТОПЛИНГ','ул. Чехова, д.120, корп./стр.5','+7 (8634) 311110',38.943288,47.203531),(440,139,'Макси','ул. Маяковского, д.224, корп./стр.В','+7 (8636) 237751',40.207308,47.69888),(441,139,'Стройцентр Стайер','пер. Комиссаровский, д.132','+7 (8636) 259242',40.212554,47.699729),(442,5,'Мир Ремонта','пр. Аксайский, д.24','+7 (863) 300-83-00',39.835701,47.276854),(443,108,'Салон Декор','ул. Красноармейская, д.87, корп./стр.50','+7 (863) 279 47 29',39.699041,47.224728),(444,116,'Каиса','ул.Глинки,64','+79787541224',34.139951,44.971415),(445,115,'Уютное решение','Проспект Победы, 209','+79787065093',34.138954,44.975668),(446,115,'Хата ламината','Проспект Победы, 256а','+79788457708',34.138667,44.976726),(447,115,'Новацентр','Проспект Победы, 245','8(3652) 66-76-26',34.150219,44.982025),(448,113,'Хата ламината','ул. Хрусталева, 111','+79787344769',33.5213,44.561918),(449,113,'Новацентр','ул. Отрадная, 15','+7(8692) 47-76-42',33.486705,44.580642),(450,113,'Новацентр','ул. Генерала Мельника, 150','+7(8692) 47-75-47',33.568299,44.57331),(451,113,'Новастрой','ул. Льва Толстого, 64','+79781288815',33.508058,44.584905),(452,113,'Элит пол','ул. Гидрографическая 1а','+79789335550',33.508184,44.583955),(453,113,'ЭлитДекор','Камышовое шоссе, 27','+79782160310',33.486086,44.5601),(454,47,'Погонный метр','ул. Строителей, 2А','+79787652470',33.352326,45.211149),(455,47,'Ламинат паркет','Проспект Победы, 4','+79787652470',33.350314,45.189632),(456,142,'ОРЕГОН','пер. Дарсановский, 8в','+79787282645',34.149186,44.504062),(457,133,'СуперМастер','ул. Володарского, 37-В','+79780911618',35.366331,45.041209),(458,133,'ХАТА','Керчиенское Шоссе. 11','+79788494241',35.423276,45.085061),(459,60,'Торговый дом веллис','ул. Курсантов, 20','+79788001805',36.464728,45.358354),(460,60,'Ламель','ул. Мирошника, 28/30','+79788615488',36.467693,45.368897),(461,36,'ДВЕРИ И К','Геленджик, улица Луначарского, д.346','+7 (86141) 3-03-50,3-03-70',38.048997,44.592371),(462,36,'ЭКО ПОЛ','Геленджик, улица Новороссийская, д.163','+79184336923',38.095341,44.566959),(463,67,'Cалон Индиго','Краснодар, Калинина, д.327','+7 (988) 2462953',38.966662,45.038712),(464,67,'XL','Краснодар, Тургеньевское шоссе, д.13','+7 (918)1723222',38.942875,45.025635),(465,67,'БАУЦЕНТР','Краснодар, Ростовское шоссе, д.28, корп./стр.7','+7 (861) 228-88-00',38.985653,45.068983),(466,67,'БАУЦЕНТР','Краснодар, Селезнева, д.4','+7 (861) 210-60-35',39.020543,45.027553),(467,67,'ЕВРООТДЕЛКА','Краснодар, улица, Фрунзе, д.186/2','+7 (918) 0653535',38.9673,45.040585),(468,67,'Много Полов','Краснодар, улица Василия Мачуги, д.5','+7 900 243‑24-96',39.063869,45.013574),(469,67,'КРОНА','Краснодар, улица Генерала Шифрина, д.5','+7(918)141-43-47',38.912691,45.041846),(470,67,'Премиум климат','Краснодар, улица Промышленная, д.90','+7 861 247‑00-90',38.993899,45.039865),(471,67,'Рулон','Краснодар, улица Московская, д.44, корп./стр.1','+7(861)252-37-77',39.002173,45.050239),(472,67,'Рулон','Краснодар, улица Онежская, д.33','+7(861)266-26-52',39.066321,45.031235),(473,67,'Ламинариум','Краснодар, Уральская, д.106','+7 (861) 246-79-99',39.054032,45.032923),(474,67,'ОПТОВИК','Краснодар, 40 лет победы, д.168','+7(938) 429-16-30',39.025349,45.055544),(475,67,'ОПТОВИК','Краснодар, Красных партизан, д.235','+7(938) 429-16-30',38.930334,45.057352),(476,67,'ОПТОВИК','Краснодар, Новороссийская, д.216, корп./стр.2','+7(938) 429-16-30',39.001715,45.022227),(477,67,'ОПТОВИК','Краснодар, Осокина, д.7, корп./стр.5','+7(938) 429-16-30',38.975313,45.03547),(478,67,'ОПТОВИК','Краснодар, Российская, д.414','+7(938) 429-16-30',39.014911,45.084672),(479,67,'ОПТОВИК','Краснодар, Уральская, д.212, корп./стр.13','+7(938) 429-16-30',39.031934,45.031477),(480,67,'Quban,','Краснодар, улица Российская, д.400','+7 (918) 077-99-12',39.01536,45.082929),(481,67,'ЭкоСтройМатериалы','Краснодар, улица Северная, д.312','+7 (918) 961-77-76',38.966258,45.041145),(482,89,'ДВЕРИ И К','Новороссийск, улица Ленина, д.139','+7 (918) 664-28-80',37.721121,44.761454),(483,89,'Трофимчук','Новороссийск, п. Цемдолина, улица Золотая рыбка, д.5, корп./стр.г','+7 (8617) 607002',37.699894,44.768449),(484,89,'БАУЦЕНТР','Новороссийск, улица, Золотая рыбка, д.23','+7 (861) 730-83-08',37.693902,44.77527),(485,89,'ОПТОВИК','Новороссийск, Золотая рыбка, д.9','+7(938) 429-16-30',37.698924,44.769902),(486,89,'Центр напольных покрвтий ВОРС','Новороссийск, улица, Куникова, д.49, корп./стр.Б','+7 (988) 245-15-55',37.786483,44.693977),(487,101,'ШАРИКОВ Домино','Приморско-Ахтарск, улица Первомайская, д.25, корп./стр.1','+7 (918) 4626883',38.167242,46.041413),(488,118,'Фаворит, Сочи','улица Донская, д.28','+79528660023',39.728919,43.6122),(489,118,'Флэкси, Сочи','Улица Пластунская, д.75','+7(862)286-16-96 доб 104',39.73509,43.603773),(490,118,'Ламинат/ИП Локтев','Сочи, Донская, д.28','+7 (988) 233-09-28',39.728919,43.6122),(491,118,'Панга-Панга','Сочи, Гагарина, д.54, корп./стр.Г','8-800-250-18-28',39.721373,43.595469),(492,126,'ДВЕРИ И К','Тимашевск, улица Пролетарская, д.73','+7 (86141) 3-03-50,3-03-70',38.93506,45.618386),(493,7,'Неско /ИП Нестеренко','Анапа, проезд Солдатских матерей, д.10','+7 (861) 335-97-09',37.340568,44.898183),(494,125,'ДВЕРИ И К','Темрюк, улица Розы Люксембург, д.92','+7 (86141) 3-03-50,3-03-70',37.375306,45.280167),(495,125,'Новый мир','Темрюк, улица Морозова, д.37','8 (800)22-22-342',37.384019,45.325234),(496,2,'Модный пол','Абинск, улица, Советов, д.41, корп./стр.А','+7-918-041-4777',38.182137,44.860928),(497,70,'Модный пол','Крымск, Синева, д.13','+7-918-041-4777',37.987472,44.929843),(498,3,'Центр напольных покрытий','Адлер, Энергетиков, д.11, корп./стр.6','+7(965)4827087',39.941361,43.448331),(499,3,'Панга-Панга','Адлер, Авиационная, д.19, корп./стр.А','8-800-250-18-28',39.941361,43.448331),(500,31,'АРСЕНАЛ СТРОЙ','Волгоград, Бахтурова, д.1','+7 (8442) 239053',44.567407,48.526321),(501,31,'ДОМИКС','Волгоград, улица 25 Летия Октября, д.1К, корп./стр.ТЦ Тулак','+7 (919) 7973616',44.467559,48.669829),(502,31,'Интер Пол','Волгоград, пр. Героев сталинграда, д.51а','+79275223023',44.533046,48.515806),(503,31,'Стройотряд','Волгоград, ул. 25 лет Октября, д.1 строение 3, корп./стр.Новострой','+7 (8442) 475986',44.469921,48.672584),(504,33,'ЭЛКО  Добрострой','Волжский, пр. Ленина, д. 308 М, д.308, корп./стр.М','88005557346',44.732211,48.809207),(505,58,'Лучшее для пола','Камышин, улица Пролетарская, д.2','+7 (84457) 46882',45.415003,50.091298),(506,134,'21 век','Фролово, улица Ковалева, д.1, корп./стр.а','+7 906 169 34 11',43.650406,49.768456),(507,12,'ДОМИКС','Астрахань, Минусинская, д.8, корп./стр.Вход №20','+7 (8512) 43-43-20',48.073324,46.358692),(508,12,'Паркетная мастерская','Астрахань, улица Куйбышева, д.66','+7 (8512) 54-78-77',48.040455,46.36373),(509,12,'ЭЛКО  ДоброСтрой','Астрахань, улица Боевая, д.134','88005557346',48.021887,46.3136);
/*!40000 ALTER TABLE `contacts_page_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqitem_glue`
--

DROP TABLE IF EXISTS `faqitem_glue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqitem_glue` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                `position` int(11) DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqitem_glue`
--

LOCK TABLES `faqitem_glue` WRITE;
/*!40000 ALTER TABLE `faqitem_glue` DISABLE KEYS */;
INSERT INTO `faqitem_glue` VALUES (1,'Тест клеевое','<p>Тест соединение</p>',NULL);
/*!40000 ALTER TABLE `faqitem_glue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqitem_lock`
--

DROP TABLE IF EXISTS `faqitem_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqitem_lock` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                `position` int(11) DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqitem_lock`
--

LOCK TABLES `faqitem_lock` WRITE;
/*!40000 ALTER TABLE `faqitem_lock` DISABLE KEYS */;
INSERT INTO `faqitem_lock` VALUES (1,'Замковое','<p>Соединение</p>',NULL);
/*!40000 ALTER TABLE `faqitem_lock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_group`
--

DROP TABLE IF EXISTS `fos_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_group` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
                                  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_group`
--

LOCK TABLES `fos_user_group` WRITE;
/*!40000 ALTER TABLE `fos_user_group` DISABLE KEYS */;
INSERT INTO `fos_user_group` VALUES (1,'Ð ÐµÐ´Ð°ÐºÑ‚Ð¾Ñ€','a:2:{i:0;s:6:\"SONATA\";i:1;s:32:\"ROLE_SONATA_PAGE_ADMIN_PAGE_EDIT\";}');
/*!40000 ALTER TABLE `fos_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_user`
--

DROP TABLE IF EXISTS `fos_user_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_user` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `enabled` tinyint(1) NOT NULL,
                                 `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `last_login` datetime DEFAULT NULL,
                                 `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `password_requested_at` datetime DEFAULT NULL,
                                 `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
                                 `created_at` datetime NOT NULL,
                                 `updated_at` datetime NOT NULL,
                                 `date_of_birth` datetime DEFAULT NULL,
                                 `firstname` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `lastname` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `website` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `biography` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `gender` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `locale` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `timezone` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `phone` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `facebook_uid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `facebook_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `facebook_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
                                 `twitter_uid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `twitter_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `twitter_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
                                 `gplus_uid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `gplus_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `gplus_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
                                 `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `two_step_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
                                 UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`),
                                 UNIQUE KEY `UNIQ_C560D761C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_user`
--

LOCK TABLES `fos_user_user` WRITE;
/*!40000 ALTER TABLE `fos_user_user` DISABLE KEYS */;
INSERT INTO `fos_user_user` VALUES (1,'admin','admin','denis.mironov@tarkett.com','denis.mironov@tarkett.com',1,'GW3dr8LUFFDJHs9T46Y9lbVvcTMVgizSfBZWY9CeCwU','wUz4J2MUCDMmjcL44jm0KYJrl8wWlbQSKdrKWQb6WIvBw/T5wWkTMaD/5MeDiRiIlLGjQIZUt2dAPcAlk08w3A==','2019-12-25 13:17:09',NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','2019-05-27 06:26:43','2019-12-25 13:17:09',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'tarkett','tarkett','webmaster@tarkett.com','webmaster@tarkett.com',1,'/3slgnb1KrAVBoiTT3Uy7k5v8dOKKm7Kt81hhmZKzW4','DZGXsbg7Dk3AQNdJsWAq26lgkSiJC2MGfQPdYipgy5m6iCW95GTnGCzZWyjI65kH5yA2riGn57ozbylYDqgtqw==',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','2019-05-27 10:35:03','2019-05-28 10:23:39',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'editor','editor','test@tarkett.com','test@tarkett.com',1,'i0BfVh0WOWtL1Vpyf2e5JflYUZNgO6zudtriZwuR1jg','bosahbiMD0M3tQ+wpoZldAL7Of9Bn69+YVmojVdVh7cH7MQmbn3RFLKjHpCfB0zNp7+EuMrP0Eimxq9fUWYJPg==','2019-11-14 18:38:22',NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','2019-11-12 14:24:05','2019-11-14 18:38:22',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'ivan','ivan','iasumbaev@gmail.com','iasumbaev@gmail.com',1,'02qItKZrHPivn5oxlIcKbIPeCHb779jifpxgPA6KRa0','pNz2l9cHdvPwbgVlpHqJ3/7QlAMoCXt3tu75gpRf/cnyFkvyUFvnlqqL4cxBysSnbrnzFt1i3IuTYy7zWAV+qQ==','2020-04-28 10:16:48',NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','2020-03-12 05:35:41','2020-04-28 10:16:48',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `fos_user_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_user_group`
--

DROP TABLE IF EXISTS `fos_user_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_user_group` (
                                       `user_id` int(11) NOT NULL,
                                       `group_id` int(11) NOT NULL,
                                       PRIMARY KEY (`user_id`,`group_id`),
                                       KEY `IDX_B3C77447A76ED395` (`user_id`),
                                       KEY `IDX_B3C77447FE54D947` (`group_id`),
                                       CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`) ON DELETE CASCADE,
                                       CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_user_group`
--

LOCK TABLES `fos_user_user_group` WRITE;
/*!40000 ALTER TABLE `fos_user_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `fos_user_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page`
--

DROP TABLE IF EXISTS `install_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `activeicon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `updated` datetime DEFAULT NULL,
                                `reqcaption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `needcaption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `compcaption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `stagecaption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `designcaption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `designtext` longtext COLLATE utf8mb4_unicode_ci,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page`
--

LOCK TABLES `install_page` WRITE;
/*!40000 ALTER TABLE `install_page` DISABLE KEYS */;
INSERT INTO `install_page` VALUES (1,'ÐšÐ»ÐµÐµÐ²Ð¾Ð¹','1.svg','1-active.svg','3382698037.jpg','2019-06-04 15:54:11','Ð¢Ñ€ÐµÐ±Ð¾Ð²Ð°Ð½Ð¸Ñ Ðº Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸ÑŽ','ÐÐµÐ»ÑŒÐ·Ñ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Art Vinyl','Ð§Ñ‚Ð¾ Ð¿Ð¾Ð½Ð°Ð´Ð¾Ð±Ð¸Ñ‚ÑÑ Ð´Ð»Ñ ÑƒÐºÐ»Ð°Ð´ÐºÐ¸?','Ð­Ñ‚Ð°Ð¿Ñ‹ ÑƒÐºÐ»Ð°Ð´ÐºÐ¸','Ð’Ð°Ñ€Ð¸Ð°Ð½Ñ‚Ñ‹ Ñ€Ð°ÑÐºÐ»Ð°Ð´Ð¾Ðº Ð¸ Ð¼Ð¾Ð´ÐµÐ»Ð¸Ñ€Ð¾Ð²Ð°Ð½Ð¸Ðµ Ð¸Ð½Ñ‚ÐµÑ€ÑŒÐµÑ€Ð¾Ð²','<p>Ð’ <a href=\"http://artvinyl.ru/designer/\">ArtDesigner</a> Ð²Ñ‹ Ð¼Ð¾Ð¶ÐµÑ‚Ðµ Ð¿Ð¾ÑÐ¼Ð¾Ñ‚Ñ€ÐµÑ‚ÑŒ Ð²ÑÐµ Ð´Ð¸Ð·Ð°Ð¹Ð½Ñ‹ Ð¸ Ð²Ð°Ñ€Ð¸Ð°Ð½Ñ‚Ñ‹ Ñ€Ð°ÑÐºÐ»Ð°Ð´Ð¾Ðº, Ð¿Ñ€Ð¸Ð¼ÐµÑ€Ð¸Ñ‚ÑŒ Ð´Ð¸Ð·Ð°Ð¹Ð½ Ð² Ð²Ð°ÑˆÐµÐ¼ Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸Ð¸, Ð° Ñ‚Ð°ÐºÐ¶Ðµ Ñ€Ð°ÑÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾Ðµ ÐºÐ¾Ð»Ð¸Ñ‡ÐµÑÑ‚Ð²Ð¾ Ð¼Ð°Ñ‚ÐµÑ€Ð¸Ð°Ð»Ð¾Ð² Ð´Ð»Ñ Ð¿Ñ€Ð¾ÐµÐºÑ‚Ð°</p>'),(2,'Ð—Ð°Ð¼ÐºÐ¾Ð²Ñ‹Ð¹','2.svg','2-active.svg','1045921016.jpg','2019-06-06 15:37:53','Ð¢Ñ€ÐµÐ±Ð¾Ð²Ð°Ð½Ð¸Ñ Ðº Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸ÑŽ','ÐÐµÐ»ÑŒÐ·Ñ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Art Vinyl','Ð§Ñ‚Ð¾ Ð¿Ð¾Ð½Ð°Ð´Ð¾Ð±Ð¸Ñ‚ÑÑ Ð´Ð»Ñ ÑƒÐºÐ»Ð°Ð´ÐºÐ¸?','Ð­Ñ‚Ð°Ð¿Ñ‹ ÑƒÐºÐ»Ð°Ð´ÐºÐ¸','Ð’Ð°Ñ€Ð¸Ð°Ð½Ñ‚Ñ‹ Ñ€Ð°ÑÐºÐ»Ð°Ð´Ð¾Ðº Ð¸ Ð´Ð¸Ð·Ð°Ð¹Ð½Ñ‹','<p>Ð’ <a href=\"http://artvinyl.ru/designer/\">ArtDesigner</a> Ð²Ñ‹ Ð¼Ð¾Ð¶ÐµÑ‚Ðµ Ð¿Ð¾ÑÐ¼Ð¾Ñ‚Ñ€ÐµÑ‚ÑŒ Ð²ÑÐµ Ð´Ð¸Ð·Ð°Ð¹Ð½Ñ‹ Ð¸ Ð²Ð°Ñ€Ð¸Ð°Ð½Ñ‚Ñ‹ Ñ€Ð°ÑÐºÐ»Ð°Ð´Ð¾Ðº, Ð¿Ñ€Ð¸Ð¼ÐµÑ€Ð¸Ñ‚ÑŒ Ð´Ð¸Ð·Ð°Ð¹Ð½ Ð² Ð²Ð°ÑˆÐµÐ¼ Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸Ð¸, Ð° Ñ‚Ð°ÐºÐ¶Ðµ Ñ€Ð°ÑÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾Ðµ ÐºÐ¾Ð»Ð¸Ñ‡ÐµÑÑ‚Ð²Ð¾ Ð¼Ð°Ñ‚ÐµÑ€Ð¸Ð°Ð»Ð¾Ð²</p>');
/*!40000 ALTER TABLE `install_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page_commons`
--

DROP TABLE IF EXISTS `install_page_commons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page_commons` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `subcaption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `metatitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                        `metakeys` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                        `metadesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page_commons`
--

LOCK TABLES `install_page_commons` WRITE;
/*!40000 ALTER TABLE `install_page_commons` DISABLE KEYS */;
INSERT INTO `install_page_commons` VALUES (1,'Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ','ÐšÐ°ÐºÐ¾Ð¹ Ñƒ Ð²Ð°Ñ Ð¿Ñ€Ð¾Ð´ÑƒÐºÑ‚?','Tarkett Art Vinyl ÑƒÐºÐ»Ð°Ð´ÐºÐ° Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ð¾Ð³Ð¾ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ','Ð¿Ð¾Ð´Ð³Ð¾Ñ‚Ð¾Ð²ÐºÐ° Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ñ, ÑƒÐºÐ»Ð°Ð´ÐºÐ°, Ð½Ð° ÐºÐ»ÐµÐ¹, Ð¿Ð»Ð°Ð²Ð°ÑŽÑ‰Ð¸Ð¹ ÑÐ¿Ð¾ÑÐ¾Ð±, Ñ€ÐµÐºÐ¾Ð¼Ð¼ÐµÐ½Ð´Ð°Ñ†Ð¸Ð¸, Ð¸Ð½ÑÑ‚Ñ€ÑƒÐ¼ÐµÐ½Ñ‚Ñ‹','Ð§Ñ‚Ð¾ Ð¿Ð¾Ð½Ð°Ð´Ð¾Ð±Ð¸Ñ‚ÑÑ Ð´Ð»Ñ ÑƒÐºÐ»Ð°Ð´ÐºÐ¸? ÐžÑÐ½Ð¾Ð²Ð½Ñ‹Ðµ Ð¿Ñ€Ð°Ð²Ð¸Ð»Ð° ÑƒÐºÐ»Ð°Ð´ÐºÐ¸ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ, ÑÑ‚Ð°Ð¿Ñ‹ Ð¿Ð¾Ð´Ð³Ð¾Ñ‚Ð¾Ð²ÐºÐ¸ Ð¸ ÑƒÐºÐ»Ð°Ð´ÐºÐ¸ Ð¿Ð¾Ð»Ð°.');
/*!40000 ALTER TABLE `install_page_commons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page_comps`
--

DROP TABLE IF EXISTS `install_page_comps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page_comps` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `install_id` int(11) DEFAULT NULL,
                                      `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `updated` datetime DEFAULT NULL,
                                      `position` int(11) DEFAULT NULL,
                                      PRIMARY KEY (`id`),
                                      KEY `IDX_2BDB86AB17BB6D9B` (`install_id`),
                                      CONSTRAINT `FK_2BDB86AB17BB6D9B` FOREIGN KEY (`install_id`) REFERENCES `install_page` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page_comps`
--

LOCK TABLES `install_page_comps` WRITE;
/*!40000 ALTER TABLE `install_page_comps` DISABLE KEYS */;
INSERT INTO `install_page_comps` VALUES (1,1,'Ð ÑƒÐ»ÐµÑ‚ÐºÐ° Ð¸ ÐºÐ°Ñ€Ð°Ð½Ð´Ð°Ñˆ Ð´Ð»Ñ Ñ€Ð°Ð·Ð¼ÐµÑ‚ÐºÐ¸','1_1.svg',NULL,1),(2,1,'ÐÐ¾Ð¶ Ð´Ð»Ñ Ñ€ÐµÐ·ÐºÐ¸ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ñ‹Ñ… Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ð¹','1_2.svg',NULL,2),(3,1,'Ð—ÑƒÐ±Ñ‡Ð°Ñ‚Ñ‹Ð¹ ÑˆÐ¿Ð°Ñ‚ÐµÐ»ÑŒ A1 Ð¸Ð»Ð¸ A2','1_3.svg',NULL,3),(4,1,'ÐŸÑ€Ð¸Ñ‚Ð¸Ñ€Ð¾Ñ‡Ð½Ð°Ñ Ð´Ð¾ÑÐºÐ°','1_4.svg',NULL,4),(5,1,'Ð’Ð¾Ð´Ð½Ð¾-Ð´Ð¸ÑÐ¿ÐµÑ€ÑÐ¸Ð¾Ð½Ð½Ñ‹Ð¹ ÐºÐ»ÐµÐ¹ Ð´Ð»Ñ Ð¼Ð¾Ð´ÑƒÐ»ÑŒÐ½Ñ‹Ñ… ÐŸÐ’Ð¥-Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ð¹','1_5.svg',NULL,5),(6,2,'Ð ÑƒÐ»ÐµÑ‚ÐºÐ° Ð¸ ÐºÐ°Ñ€Ð°Ð½Ð´Ð°Ñˆ Ð´Ð»Ñ Ñ€Ð°Ð·Ð¼ÐµÑ‚ÐºÐ¸','2_1.svg',NULL,1),(7,2,'ÐÐ¾Ð¶ Ð´Ð»Ñ Ñ€ÐµÐ·ÐºÐ¸ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ñ‹Ñ… Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ð¹','2_2.svg',NULL,2),(8,2,'Ð›Ð¸Ð½ÐµÐ¹ÐºÐ°','2813532419.svg','2019-05-16 15:31:03',3),(10,2,'ÐŸÐµÑ€Ñ‡Ð°Ñ‚ÐºÐ¸','8696535104.svg','2019-05-16 15:31:03',4);
/*!40000 ALTER TABLE `install_page_comps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page_doc`
--

DROP TABLE IF EXISTS `install_page_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page_doc` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `section_id` int(11) DEFAULT NULL,
                                    `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `size` int(11) DEFAULT NULL,
                                    `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `updated` datetime DEFAULT NULL,
                                    `position` int(11) DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `IDX_EB5A714BD823E37A` (`section_id`),
                                    CONSTRAINT `FK_EB5A714BD823E37A` FOREIGN KEY (`section_id`) REFERENCES `install_page_docs_section` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page_doc`
--

LOCK TABLES `install_page_doc` WRITE;
/*!40000 ALTER TABLE `install_page_doc` DISABLE KEYS */;
INSERT INTO `install_page_doc` VALUES (1,1,'ÐŸÐ¾Ð´Ð³Ð¾Ñ‚Ð¾Ð²ÐºÐ° Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ñ','2906798545.pdf',16363732,'PDF','2019-05-22 10:25:33',NULL),(2,1,'Ð˜Ð½ÑÑ‚Ñ€ÑƒÐºÑ†Ð¸Ñ Ð¿Ð¾ ÑƒÐºÐ»Ð°Ð´ÐºÐµ','3157708399.pdf',188688,'PDF','2019-05-22 10:25:33',NULL),(3,1,'Ð˜Ð½ÑÑ‚Ñ€ÑƒÐºÑ†Ð¸Ñ Ð¿Ð¾ ÑƒÑ…Ð¾Ð´Ñƒ','8919661000.pdf',130078,'PDF','2019-05-22 10:25:33',NULL),(4,2,'ÐŸÐ¾Ð´Ð³Ð¾Ñ‚Ð¾Ð²ÐºÐ° Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ñ','8910732083.pdf',16363732,'PDF','2019-05-25 11:32:16',NULL),(5,2,'Ð˜Ð½ÑÑ‚Ñ€ÑƒÐºÑ†Ð¸Ñ Ð¿Ð¾ ÑƒÐºÐ»Ð°Ð´ÐºÐµ','2669540486.pdf',407691,'PDF','2019-06-04 15:01:55',NULL),(6,2,'Ð˜Ð½ÑÑ‚Ñ€ÑƒÐºÑ†Ð¸Ñ Ð¿Ð¾ ÑƒÑ…Ð¾Ð´Ñƒ','3239607660.pdf',407691,'PDF','2019-06-04 15:01:55',NULL);
/*!40000 ALTER TABLE `install_page_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page_docs_section`
--

DROP TABLE IF EXISTS `install_page_docs_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page_docs_section` (
                                             `id` int(11) NOT NULL AUTO_INCREMENT,
                                             `install_id` int(11) DEFAULT NULL,
                                             `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                             `position` int(11) DEFAULT NULL,
                                             PRIMARY KEY (`id`),
                                             KEY `IDX_D425FE2917BB6D9B` (`install_id`),
                                             CONSTRAINT `FK_D425FE2917BB6D9B` FOREIGN KEY (`install_id`) REFERENCES `install_page` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page_docs_section`
--

LOCK TABLES `install_page_docs_section` WRITE;
/*!40000 ALTER TABLE `install_page_docs_section` DISABLE KEYS */;
INSERT INTO `install_page_docs_section` VALUES (1,1,'ÐŸÐ¾Ð´Ñ€Ð¾Ð±Ð½Ñ‹Ðµ Ð¸Ð½ÑÑ‚Ñ€ÑƒÐºÑ†Ð¸Ð¸',NULL),(2,2,'ÐŸÐ¾Ð´Ñ€Ð¾Ð±Ð½Ñ‹Ðµ Ð¸Ð½ÑÑ‚Ñ€ÑƒÐºÑ†Ð¸Ð¸',NULL);
/*!40000 ALTER TABLE `install_page_docs_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page_faq`
--

DROP TABLE IF EXISTS `install_page_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page_faq` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `install_id` int(11) DEFAULT NULL,
                                    `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `position` int(11) DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `IDX_85E4F9E317BB6D9B` (`install_id`),
                                    CONSTRAINT `FK_85E4F9E317BB6D9B` FOREIGN KEY (`install_id`) REFERENCES `install_page` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page_faq`
--

LOCK TABLES `install_page_faq` WRITE;
/*!40000 ALTER TABLE `install_page_faq` DISABLE KEYS */;
INSERT INTO `install_page_faq` VALUES (1,1,'тест','<p>тест тест</p>',1),(2,1,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Art Vinyl Ð½Ð° Ð½ÐµÑ€Ð¾Ð²Ð½Ñ‹Ðµ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ?','<p>ÐžÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ &nbsp;Ð´Ð»Ñ Art Vinyl Ð´Ð¾Ð»Ð¶Ð½Ð¾ Ð±Ñ‹Ñ‚ÑŒ Ð°Ð±ÑÐ¾Ð»ÑŽÑ‚Ð½Ð¾ Ñ€Ð¾Ð²Ð½Ñ‹Ð¼.</p>',2),(3,1,'ÐšÐ°Ðº Ð¿Ñ€Ð°Ð²Ð¸Ð»ÑŒÐ½Ð¾ Ð²Ñ‹Ð±Ñ€Ð°Ñ‚ÑŒ ÐºÐ»ÐµÐ¹?','<p>Ð”Ð»Ñ Art Vinyl Ð¿Ñ€Ð¸Ð¼ÐµÐ½ÑÐµÑ‚ÑÑ Ð°ÐºÑ€Ð¸Ð»Ð¾Ð²Ñ‹Ð¹ Ð²Ð¾Ð´Ð½Ð¾-Ð´Ð¸ÑÐ¿ÐµÑ€ÑÐ¸Ð¾Ð½Ð½Ñ‹Ð¹ ÐºÐ»ÐµÐ¹, Ð¿Ñ€ÐµÐ´Ð½Ð°Ð·Ð½Ð°Ñ‡ÐµÐ½Ð½Ñ‹Ð¹ Ð´Ð»Ñ ÐŸÐ’Ð¥ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ð¹. Ð ÐµÐºÐ¾Ð¼ÐµÐ½Ð´ÑƒÐµÐ¼Ñ‹Ðµ Ð¼Ð°Ñ€ÐºÐ¸ ÐºÐ»ÐµÐµÐ²1: Uzin ÐšÐ• 2000 S (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Uzin Utz AG), Homakoll 164 prof (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Homa Chemical Engineering), Ultrabond Eco 375 (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Mapei S.p.A.), Ultrabond V4SP (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Mapei S.p.A.), EUROCOL Eurostar Allround 528 (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Forbo).</p>',3),(4,1,'Ð“Ð´Ðµ Ñ Ð¼Ð¾Ð³Ñƒ Ð¿Ð¾ÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ Ñ‚Ð¾Ñ‡Ð½Ð¾Ðµ ÐºÐ¾Ð»Ð¸Ñ‡ÐµÑÑ‚Ð²Ð¾ Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾Ð¹ Ð¿Ð»Ð¸Ñ‚ÐºÐ¸?','<p>ÐŸÐ¾ÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ ÐºÐ¾Ð»Ð¸Ñ‡ÐµÑÑ‚Ð²Ð¾ Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾Ð¹ Ð¿Ð»Ð¸Ñ‚ÐºÐ¸ Ð¿Ñ€Ð¾ÑÑ‚Ð¾: ÐÑƒÐ¶Ð½Ð¾ Ñ€Ð°ÑÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ Ð¿Ð»Ð¾Ñ‰Ð°Ð´ÑŒ Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸Ñ Ð¸ Ð¿Ñ€Ð¸Ð±Ð°Ð²Ð¸Ñ‚ÑŒ +10%. ÐŸÑ€Ð¸ ÑÐ»Ð¾Ð¶Ð½Ñ‹Ñ… ÑƒÐºÐ»Ð°Ð´ÐºÐ°Ñ… Ð¸Ð· Ð½ÐµÑÐºÐ¾Ð»ÑŒÐºÐ¸Ñ… Ñ„Ð¾Ñ€Ð¼Ð°Ñ‚Ð¾Ð² Ð¸/Ð¸Ð»Ð¸ Ð´Ð¸Ð·Ð°Ð¹Ð½Ð¾Ð² &nbsp;Ð¼Ð¾Ð¶Ð½Ð¾ Ð²Ð¾ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÑŒÑÑ Ñ„ÑƒÐ½ÐºÑ†Ð¸ÐµÐ¹ ÐºÐ°Ð»ÑŒÐºÑƒÐ»ÑÑ‚Ð¾Ñ€ Ð² Ð¿Ñ€Ð¾Ð³Ñ€Ð°Ð¼Ð¼Ðµ <a href=\"http://new.artvinyl.ru/designer/\">Art Designer</a></p>',4),(5,1,'ÐšÐ°Ðº ÑƒÐ»Ð¾Ð¶Ð¸Ñ‚ÑŒ Art Vinyl Ð½Ð° Ð¿Ð¾Ñ‚Ð¾Ð»Ð¾Ðº?','<p>Ð”ÐµÐ¹ÑÑ‚Ð²Ð¸Ñ‚ÐµÐ»ÑŒÐ½Ð¾ Ð¼Ð½Ð¾Ð³Ð¸Ðµ Ð´Ð¸Ð·Ð°Ð¹Ð½ÐµÑ€Ñ‹ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·ÑƒÑŽÑ‚ Art Vinyl Ð´Ð»Ñ ÑÑ‚ÐµÐ½ Ð¸ Ð¿Ð¾Ñ‚Ð¾Ð»ÐºÐ¾Ð². ÐœÑ‹ Ð½Ðµ Ð´Ð°ÐµÐ¼ Ñ€ÐµÐºÐ¾Ð¼ÐµÐ½Ð´Ð°Ñ†Ð¸Ð¹, Ñ‚Ð°Ðº ÐºÐ°Ðº Ð´Ð»Ñ Ð½Ð°Ñ ÑÑ‚Ð¾ Ð¿Ñ€ÐµÐ¶Ð´Ðµ Ð²ÑÐµÐ³Ð¾ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ð¾Ðµ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ðµ.</p>',5),(6,1,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ð½Ð° Ñ‚ÐµÐ¿Ð»Ñ‹Ð¹ Ð¿Ð¾Ð»?','<p>Ð”Ð° Ð¼Ð¾Ð¶Ð½Ð¾ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÑŒ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ðµ Ñ Ñ‚ÐµÐ¿Ð»Ñ‹Ð¼ Ð¿Ð¾Ð»Ð¾Ð¼ Ð´Ð¾ 27&deg;C.</p>',6),(7,1,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ð½Ð° Ñ„Ð°Ð½ÐµÑ€Ñƒ?','<p>ÐÐµÑ‚, Ð½ÑƒÐ¶Ð½Ð¾ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ñ‚Ð¾Ð»ÑŒÐºÐ¾ Ð½Ð° Ñ€Ð¾Ð²Ð½Ð¾Ðµ Ð±ÐµÑ‚Ð¾Ð½Ð½Ð¾Ðµ Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ.</p>',7),(8,2,'Ð§Ñ‚Ð¾ Ð´ÐµÐ»Ð°Ñ‚ÑŒ ÐµÑÐ»Ð¸ Ð¸ÑÐ¿Ð¾Ñ€Ñ‚Ð¸Ð»Ð°ÑÑŒ Ð¾Ð´Ð½Ð° Ð¿Ð»Ð¸Ñ‚ÐºÐ°?','<p>Ð¡ Ð¿Ð¾Ð¼Ð¾Ñ‰ÑŒÑŽ Ð½Ð¾Ð¶Ð° Ð²Ñ‹Ñ€ÐµÐ¶ÑŒÑ‚Ðµ Ð¿Ð¾ ÑÑ‚Ñ‹ÐºÐ°Ð¼ Ð¸ ÑƒÐ´Ð°Ð»Ð¸Ñ‚Ðµ Ð¿Ð¾Ð²Ñ€ÐµÐ¶Ð´ÐµÐ½Ð½ÑƒÑŽ Ð¿Ð»Ð°Ð½ÐºÑƒ. Ð’Ð¾Ð·ÑŒÐ¼Ð¸Ñ‚Ðµ Ð½Ð¾Ð²ÑƒÑŽ Ð¿Ð»Ð°Ð½ÐºÑƒ Ð¸ Ð¿Ð¾Ð´Ñ€ÐµÐ¶ÑŒÑ‚Ðµ Ð·Ð°Ð¼ÐºÐ¸.</p>\r\n\r\n<p>ÐŸÐµÑ€ÐµÐ´ Ð¼Ð¾Ð½Ñ‚Ð°Ð¶Ð¾Ð¼ Ð¿Ð»Ð°Ð½ÐºÐ¸ ÑƒÐ±ÐµÐ´Ð¸Ñ‚ÐµÑÑŒ, Ñ‡Ñ‚Ð¾ ÑÐ¾Ð±Ð»ÑŽÐ´Ð°ÐµÑ‚ÑÑ Ð½Ð°Ð¿Ñ€Ð°Ð²Ð»ÐµÐ½Ð¸Ðµ Ñ€Ð¸ÑÑƒÐ½ÐºÐ°. Ð—Ð°ÐºÑ€ÐµÐ¿Ð¸Ñ‚Ðµ Ð½Ð¾Ð²ÑƒÑŽ Ð¿Ð»Ð°Ð½ÐºÑƒ Ð¸ Ð¾ÐºÑ€ÑƒÐ¶Ð°ÑŽÑ‰Ð¸Ðµ ÐµÐµ Ð¿Ð»Ð°Ð½ÐºÐ¸ Ð¿Ñ€Ð¸ Ð¿Ð¾Ð¼Ð¾Ñ‰Ð¸ Ð´Ð²ÑƒÑÑ‚Ð¾Ñ€Ð¾Ð½Ð½ÐµÐ¹ ÐºÐ»ÐµÐ¹ÐºÐ¾Ð¹ Ð»ÐµÐ½Ñ‚Ñ‹. Ð¢Ñ‰Ð°Ñ‚ÐµÐ»ÑŒÐ½Ð¾ Ð¿Ñ€Ð¾ÐºÐ°Ñ‚Ð°Ð¹Ñ‚Ðµ Ð¸ Ð¿Ñ€Ð¸Ð´Ð°Ð²Ð¸Ñ‚Ðµ Ð²ÑÐµ Ð¿Ð»Ð°Ð½ÐºÐ¸, Ñ‡Ñ‚Ð¾Ð±Ñ‹ Ð½Ð°Ð´Ñ‘Ð¶Ð½Ð¾ Ð¿Ñ€Ð¸ÐºÑ€ÐµÐ¿Ð¸Ñ‚ÑŒ Ð¸Ñ… Ðº Ð¿Ð¾Ð»Ñƒ.</p>',1),(9,2,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Art Vinyl Ð½Ð° Ð½ÐµÑ€Ð¾Ð²Ð½Ñ‹Ðµ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ?','<p>ÐžÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ &nbsp;Ð´Ð»Ñ Art Vinyl Ð´Ð¾Ð»Ð¶Ð½Ð¾ Ð±Ñ‹Ñ‚ÑŒ Ð°Ð±ÑÐ¾Ð»ÑŽÑ‚Ð½Ð¾ Ñ€Ð¾Ð²Ð½Ñ‹Ð¼.</p>',2),(10,2,'ÐšÐ°Ðº Ð¿Ñ€Ð°Ð²Ð¸Ð»ÑŒÐ½Ð¾ Ð²Ñ‹Ð±Ñ€Ð°Ñ‚ÑŒ ÐºÐ»ÐµÐ¹?','<p>Ð”Ð»Ñ Art Vinyl Ð¿Ñ€Ð¸Ð¼ÐµÐ½ÑÐµÑ‚ÑÑ Ð°ÐºÑ€Ð¸Ð»Ð¾Ð²Ñ‹Ð¹ Ð²Ð¾Ð´Ð½Ð¾-Ð´Ð¸ÑÐ¿ÐµÑ€ÑÐ¸Ð¾Ð½Ð½Ñ‹Ð¹ ÐºÐ»ÐµÐ¹, Ð¿Ñ€ÐµÐ´Ð½Ð°Ð·Ð½Ð°Ñ‡ÐµÐ½Ð½Ñ‹Ð¹ Ð´Ð»Ñ ÐŸÐ’Ð¥ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ð¹. Ð ÐµÐºÐ¾Ð¼ÐµÐ½Ð´ÑƒÐµÐ¼Ñ‹Ðµ Ð¼Ð°Ñ€ÐºÐ¸ ÐºÐ»ÐµÐµÐ²1: Uzin ÐšÐ• 2000 S (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Uzin Utz AG), Homakoll 164 prof (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Homa Chemical Engineering), Ultrabond Eco 375 (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Mapei S.p.A.), Ultrabond V4SP (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Mapei S.p.A.), EUROCOL Eurostar Allround 528 (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Forbo).</p>',3),(11,2,'Ð“Ð´Ðµ Ñ Ð¼Ð¾Ð³Ñƒ Ð¿Ð¾ÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ Ñ‚Ð¾Ñ‡Ð½Ð¾Ðµ ÐºÐ¾Ð»Ð¸Ñ‡ÐµÑÑ‚Ð²Ð¾ Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾Ð¹ Ð¿Ð»Ð¸Ñ‚ÐºÐ¸?','<p>ÐŸÐ¾ÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ ÐºÐ¾Ð»Ð¸Ñ‡ÐµÑÑ‚Ð²Ð¾ Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾Ð¹ Ð¿Ð»Ð¸Ñ‚ÐºÐ¸ Ð¿Ñ€Ð¾ÑÑ‚Ð¾: ÐÑƒÐ¶Ð½Ð¾ Ñ€Ð°ÑÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ Ð¿Ð»Ð¾Ñ‰Ð°Ð´ÑŒ Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸Ñ Ð¸ Ð¿Ñ€Ð¸Ð±Ð°Ð²Ð¸Ñ‚ÑŒ +10%. ÐŸÑ€Ð¸ ÑÐ»Ð¾Ð¶Ð½Ñ‹Ñ… ÑƒÐºÐ»Ð°Ð´ÐºÐ°Ñ… Ð¸Ð· Ð½ÐµÑÐºÐ¾Ð»ÑŒÐºÐ¸Ñ… Ñ„Ð¾Ñ€Ð¼Ð°Ñ‚Ð¾Ð² Ð¸/Ð¸Ð»Ð¸ Ð´Ð¸Ð·Ð°Ð¹Ð½Ð¾Ð² &nbsp;Ð¼Ð¾Ð¶Ð½Ð¾ Ð²Ð¾ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÑŒÑÑ Ñ„ÑƒÐ½ÐºÑ†Ð¸ÐµÐ¹ ÐºÐ°Ð»ÑŒÐºÑƒÐ»ÑÑ‚Ð¾Ñ€ Ð² Ð¿Ñ€Ð¾Ð³Ñ€Ð°Ð¼Ð¼Ðµ <a href=\"http://new.artvinyl.ru/designer/\">Art Designer</a></p>',4),(12,2,'ÐšÐ°Ðº ÑƒÐ»Ð¾Ð¶Ð¸Ñ‚ÑŒ Art Vinyl Ð½Ð° Ð¿Ð¾Ñ‚Ð¾Ð»Ð¾Ðº?','<p>Ð”ÐµÐ¹ÑÑ‚Ð²Ð¸Ñ‚ÐµÐ»ÑŒÐ½Ð¾ Ð¼Ð½Ð¾Ð³Ð¸Ðµ Ð´Ð¸Ð·Ð°Ð¹Ð½ÐµÑ€Ñ‹ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·ÑƒÑŽÑ‚ Art Vinyl Ð´Ð»Ñ ÑÑ‚ÐµÐ½ Ð¸ Ð¿Ð¾Ñ‚Ð¾Ð»ÐºÐ¾Ð². ÐœÑ‹ Ð½Ðµ Ð´Ð°ÐµÐ¼ Ñ€ÐµÐºÐ¾Ð¼ÐµÐ½Ð´Ð°Ñ†Ð¸Ð¹, Ñ‚Ð°Ðº ÐºÐ°Ðº Ð´Ð»Ñ Ð½Ð°Ñ ÑÑ‚Ð¾ Ð¿Ñ€ÐµÐ¶Ð´Ðµ Ð²ÑÐµÐ³Ð¾ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ð¾Ðµ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ðµ.</p>',5),(14,2,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ð½Ð° Ñ‚ÐµÐ¿Ð»Ñ‹Ð¹ Ð¿Ð¾Ð»?','<p>&nbsp;Ð”Ð° Ð¼Ð¾Ð¶Ð½Ð¾ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÑŒ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ðµ Ñ Ñ‚ÐµÐ¿Ð»Ñ‹Ð¼ Ð¿Ð¾Ð»Ð¾Ð¼ Ð´Ð¾ 27&deg;Ð¡.</p>',6),(15,1,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ð½Ð° Ð±Ð°Ð»ÐºÐ¾Ð½Ðµ?','<p>ÐÐµÑ‚, Ð¼Ð¾Ð¶Ð½Ð¾ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ñ‚Ð¾Ð»ÑŒÐºÐ¾ Ð² Ð¾Ñ‚Ð°Ð¿Ð»Ð¸Ð²Ð°ÐµÐ¼Ñ‹Ñ… Ð¾ÑÑ‚ÐµÐºÐ»ÐµÐ½Ð½Ñ‹Ñ… Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸ÑÑ….</p>',8),(16,1,'ÐÐ°ÑÐºÐ¾Ð»ÑŒÐºÐ¾ Ð¿Ñ€Ð¾Ð´ÑƒÐºÑ‚ Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐµÐ½?','<p>ÐŸÑ€Ð¾Ð´ÑƒÐºÑ‚ Ð°Ð±ÑÐ¾Ð»ÑŽÑ‚Ð½Ð¾ Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐµÐ½. ÐžÐ½ Ð¸Ð¼ÐµÐµÑ‚ ÑÐºÐ¾Ð»Ð¾Ð³Ð¸Ñ‡ÐµÑÐºÐ¸Ð¹ ÑÐµÑ€Ñ‚Ð¸Ñ„Ð¸ÐºÐ°Ñ‚ &laquo;Ð›Ð¸ÑÑ‚Ð¾Ðº Ð–Ð¸Ð·Ð½Ð¸&raquo;.</p>',9),(17,2,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ð½Ð° Ñ„Ð°Ð½ÐµÑ€Ñƒ?','<p>ÐÐµÑ‚, Ð½ÑƒÐ¶Ð½Ð¾ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ñ‚Ð¾Ð»ÑŒÐºÐ¾ Ð½Ð° Ñ€Ð¾Ð²Ð½Ð¾Ðµ Ð±ÐµÑ‚Ð¾Ð½Ð½Ð¾Ðµ Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ.</p>',7),(18,2,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ð½Ð° Ð±Ð°Ð»ÐºÐ¾Ð½Ðµ?','<p>&nbsp;ÐÐµÑ‚, Ð¼Ð¾Ð¶Ð½Ð¾ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ñ‚Ð¾Ð»ÑŒÐºÐ¾ Ð² Ð¾Ñ‚Ð°Ð¿Ð»Ð¸Ð²Ð°ÐµÐ¼Ñ‹Ñ… Ð¾ÑÑ‚ÐµÐºÐ»ÐµÐ½Ð½Ñ‹Ñ… Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸ÑÑ….</p>',8),(19,2,'ÐÐ°ÑÐºÐ¾Ð»ÑŒÐºÐ¾ Ð¿Ñ€Ð¾Ð´ÑƒÐºÑ‚ Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐµÐ½?','<p>ÐŸÑ€Ð¾Ð´ÑƒÐºÑ‚ Ð°Ð±ÑÐ¾Ð»ÑŽÑ‚Ð½Ð¾ Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐµÐ½. ÐžÐ½ Ð¸Ð¼ÐµÐµÑ‚ ÑÐºÐ¾Ð»Ð¾Ð³Ð¸Ñ‡ÐµÑÐºÐ¸Ð¹ ÑÐµÑ€Ñ‚Ð¸Ñ„Ð¸ÐºÐ°Ñ‚ &laquo;Ð›Ð¸ÑÑ‚Ð¾Ðº Ð–Ð¸Ð·Ð½Ð¸&raquo;.</p>',9),(20,NULL,'ТЕст','<p>ТЕСТ</p>',NULL),(21,NULL,'Тест 2','<p>Настя</p>',NULL);
/*!40000 ALTER TABLE `install_page_faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page_needs`
--

DROP TABLE IF EXISTS `install_page_needs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page_needs` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `install_id` int(11) DEFAULT NULL,
                                      `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `position` int(11) DEFAULT NULL,
                                      PRIMARY KEY (`id`),
                                      KEY `IDX_9C4B249317BB6D9B` (`install_id`),
                                      CONSTRAINT `FK_9C4B249317BB6D9B` FOREIGN KEY (`install_id`) REFERENCES `install_page` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page_needs`
--

LOCK TABLES `install_page_needs` WRITE;
/*!40000 ALTER TABLE `install_page_needs` DISABLE KEYS */;
INSERT INTO `install_page_needs` VALUES (1,1,'ÐÐ° ÑÑ‚Ð°Ñ€Ð¾Ðµ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ðµ',1),(2,1,'ÐÐ° Ð°ÑÑ„Ð°Ð»ÑŒÑ‚Ð¾Ð²Ð¾Ðµ Ð¸ Ð±Ð¸Ñ‚ÑƒÐ¼Ð½Ð¾Ðµ Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ',2),(3,1,'ÐÐ° Ð´ÐµÑ€ÐµÐ²ÑÐ½Ð½Ñ‹Ðµ Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ñ, Ñ„Ð°Ð½ÐµÑ€Ñƒ, Ð”Ð¡ÐŸ, ÐžÐ¡Ð‘',3),(4,1,'ÐÐ° Ð¾ÐºÑ€Ð°ÑˆÐµÐ½Ð½Ñ‹Ðµ Ð¼Ð°Ð»ÑÐ½Ð¾Ð¹ ÐºÑ€Ð°ÑÐºÐ¾Ð¹ Ð¾ÑÐ½Ð¾Ð²Ñ‹',4),(5,2,'ÐÐ° Ð³Ð¾Ñ‚Ð¾Ð²Ð¾Ðµ Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ Ñ Ð½Ð°Ð»Ð¸Ñ‡Ð¸ÐµÐ¼ Ñ‚Ñ€ÐµÑ‰Ð¸Ð½, Ñ€Ð°ÐºÐ¾Ð²Ð¸Ð½, Ð±ÑƒÐ³Ð¾Ñ€ÐºÐ¾Ð²',1),(6,2,'ÐÐ° Ð°ÑÑ„Ð°Ð»ÑŒÑ‚Ð¾Ð²Ð¾Ðµ Ð¸ Ð±Ð¸Ñ‚ÑƒÐ¼Ð½Ð¾Ðµ Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ',2),(7,2,'ÐÐ° Ð´ÐµÑ€ÐµÐ²ÑÐ½Ð½Ñ‹Ðµ Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ñ, Ñ„Ð°Ð½ÐµÑ€Ñƒ, Ð”Ð¡ÐŸ, ÐžÐ¡Ð‘',3),(8,2,'ÐÐ° Ð¾ÐºÑ€Ð°ÑˆÐµÐ½Ð½Ñ‹Ðµ Ð¼Ð°Ð»ÑÐ½Ð¾Ð¹ ÐºÑ€Ð°ÑÐºÐ¾Ð¹ Ð¾ÑÐ½Ð¾Ð²Ñ‹',4);
/*!40000 ALTER TABLE `install_page_needs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page_reqs`
--

DROP TABLE IF EXISTS `install_page_reqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page_reqs` (
                                     `id` int(11) NOT NULL AUTO_INCREMENT,
                                     `install_id` int(11) DEFAULT NULL,
                                     `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                     `position` int(11) DEFAULT NULL,
                                     PRIMARY KEY (`id`),
                                     KEY `IDX_FAFAF02417BB6D9B` (`install_id`),
                                     CONSTRAINT `FK_FAFAF02417BB6D9B` FOREIGN KEY (`install_id`) REFERENCES `install_page` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page_reqs`
--

LOCK TABLES `install_page_reqs` WRITE;
/*!40000 ALTER TABLE `install_page_reqs` DISABLE KEYS */;
INSERT INTO `install_page_reqs` VALUES (1,1,'Ð—Ð°Ð²ÐµÑ€ÑˆÐµÐ½Ð½Ñ‹Ðµ Ð¾Ñ‚Ð´ÐµÐ»Ð¾Ñ‡Ð½Ñ‹Ðµ Ñ€Ð°Ð±Ð¾Ñ‚Ñ‹',1),(2,1,'ÐžÑ‚Ð½Ð¾ÑÐ¸Ñ‚ÐµÐ»ÑŒÐ½Ð°Ñ Ð²Ð»Ð°Ð¶Ð½Ð¾ÑÑ‚ÑŒ Ð²Ð¾Ð·Ð´ÑƒÑ…Ð° Ð½Ðµ Ð±Ð¾Ð»ÐµÐµ 60%',2),(3,1,'Ð¢ÐµÐ¼Ð¿ÐµÑ€Ð°Ñ‚ÑƒÑ€Ð° Ð½Ðµ Ð¼ÐµÐ½ÐµÐµ +20Â°C',3),(4,1,'ÐžÑ‚ÑÑƒÑ‚ÑÑ‚Ð²Ð¸Ðµ ÑÐºÐ²Ð¾Ð·Ð½ÑÐºÐ¾Ð²',4),(5,2,'Ð¢ÐµÐ¼Ð¿ÐµÑ€Ð°Ñ‚ÑƒÑ€Ð° Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸Ñ Ð½Ðµ Ð´Ð¾Ð»Ð¶Ð½Ð° Ð±Ñ‹Ñ‚ÑŒ Ð½Ð¸Ð¶Ðµ 15Â°Ð¡ Ð¸Ð»Ð¸ Ð²Ñ‹ÑˆÐµ 28Â°Ð¡.',1);
/*!40000 ALTER TABLE `install_page_reqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page_stages`
--

DROP TABLE IF EXISTS `install_page_stages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page_stages` (
                                       `id` int(11) NOT NULL AUTO_INCREMENT,
                                       `install_id` int(11) DEFAULT NULL,
                                       `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `position` int(11) DEFAULT NULL,
                                       `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `updated` datetime DEFAULT NULL,
                                       PRIMARY KEY (`id`),
                                       KEY `IDX_1E0757F17BB6D9B` (`install_id`),
                                       CONSTRAINT `FK_1E0757F17BB6D9B` FOREIGN KEY (`install_id`) REFERENCES `install_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page_stages`
--

LOCK TABLES `install_page_stages` WRITE;
/*!40000 ALTER TABLE `install_page_stages` DISABLE KEYS */;
/*!40000 ALTER TABLE `install_page_stages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install_page_youtube`
--

DROP TABLE IF EXISTS `install_page_youtube`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install_page_youtube` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `section_id` int(11) DEFAULT NULL,
                                        `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `href` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `position` int(11) DEFAULT NULL,
                                        PRIMARY KEY (`id`),
                                        KEY `IDX_7A3312C7D823E37A` (`section_id`),
                                        CONSTRAINT `FK_7A3312C7D823E37A` FOREIGN KEY (`section_id`) REFERENCES `install_page_docs_section` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install_page_youtube`
--

LOCK TABLES `install_page_youtube` WRITE;
/*!40000 ALTER TABLE `install_page_youtube` DISABLE KEYS */;
INSERT INTO `install_page_youtube` VALUES (1,1,'Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ Ð¾Ñ‚ Ð Ð´Ð¾ Ð¯','https://www.youtube.com/watch?v=k8soJlPUig8',NULL),(2,2,'Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ Ð¾Ñ‚ Ð Ð´Ð¾ Ð¯','https://www.youtube.com/watch?v=Xyky7AyO-RU',NULL);
/*!40000 ALTER TABLE `install_page_youtube` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
                                      `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
                                      PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20190524070615','2019-05-24 07:06:19'),('20190524071414','2019-05-24 07:14:18'),('20190524075247','2019-05-24 07:52:50'),('20190524080235','2019-05-24 08:02:38'),('20190524084807','2019-05-24 08:48:11'),('20190524094949','2019-05-24 09:49:53'),('20190524100844','2019-05-24 10:08:48'),('20190524101140','2019-05-24 10:11:43'),('20190524101426','2019-05-24 10:14:28'),('20190524101752','2019-05-24 10:17:54'),('20190524102039','2019-05-24 10:20:42'),('20190524102348','2019-05-24 10:23:50'),('20190527032445','2019-05-27 03:25:17'),('20190604072807','2019-06-04 07:28:22'),('20200316054659','2020-03-16 06:05:29'),('20200316060413','2020-03-16 06:05:30'),('20200316074523','2020-03-16 07:46:43'),('20200317042835','2020-03-17 04:29:30'),('20200317043105','2020-03-17 04:31:39'),('20200317065639','2020-03-17 06:57:44'),('20200318071437','2020-03-18 07:15:19'),('20200319054938','2020-03-19 05:50:32');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_page_commons`
--

DROP TABLE IF EXISTS `product_page_commons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_page_commons` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `updated` datetime DEFAULT NULL,
                                        `metatitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                        `metakeys` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                        `metadesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_page_commons`
--

LOCK TABLES `product_page_commons` WRITE;
/*!40000 ALTER TABLE `product_page_commons` DISABLE KEYS */;
INSERT INTO `product_page_commons` VALUES (1,'Art Vinyl â€” ÐºÑ€ÐµÐ°Ñ‚Ð¸Ð²Ð½Ñ‹Ð¹ Ð¿Ð¾Ð»','ÐšÑ€Ð°ÑÐ¾Ñ‚Ð° Ð¿Ð°Ñ€ÐºÐµÑ‚Ð°, Ð¿Ñ€Ð°ÐºÑ‚Ð¸Ñ‡Ð½Ð¾ÑÑ‚ÑŒ Ð»Ð¸Ð½Ð¾Ð»ÐµÑƒÐ¼Ð°, Ð¼Ð¾Ð´ÑƒÐ»ÑŒÐ½Ð¾ÑÑ‚ÑŒ Ð¿Ð»Ð¸Ñ‚ÐºÐ¸','https://www.youtube.com/embed/V-RkDxfGA94',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `product_page_commons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_page_doc`
--

DROP TABLE IF EXISTS `product_page_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_page_doc` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `section_id` int(11) DEFAULT NULL,
                                    `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `size` int(11) DEFAULT NULL,
                                    `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `updated` datetime DEFAULT NULL,
                                    `position` int(11) DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `IDX_814E9F62D823E37A` (`section_id`),
                                    CONSTRAINT `FK_814E9F62D823E37A` FOREIGN KEY (`section_id`) REFERENCES `product_page_docs_section` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_page_doc`
--

LOCK TABLES `product_page_doc` WRITE;
/*!40000 ALTER TABLE `product_page_doc` DISABLE KEYS */;
INSERT INTO `product_page_doc` VALUES (4,8,'ÐŸÐ¾Ð´Ð³Ð¾Ñ‚Ð¾Ð²ÐºÐ° Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ñ','1520238136.pdf',16363732,'PDF','2019-05-21 12:53:39',NULL),(5,8,'Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ð¹ Ð½Ð° ÐºÐ»ÐµÐ¹','8265914832.pdf',188688,'PDF','2019-05-21 12:53:39',NULL),(6,8,'Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð·Ð°Ð¼ÐºÐ¾Ð²Ñ‹Ñ… Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ð¹','6021171138.pdf',407691,'PDF','2019-05-21 12:53:39',NULL),(7,8,'Ð£Ñ…Ð¾Ð´ Ð·Ð° Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ðµ','7862030395.pdf',130078,'PDF','2019-05-21 13:00:05',NULL),(12,4,'ÐšÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ BLUES','4415862287.pdf',8284000,'PDF','2019-05-24 14:20:03',NULL),(13,4,'ÐšÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ Cosmic','9304271360.pdf',24560548,'PDF','2019-05-24 14:20:03',NULL),(14,4,'ÐšÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ EPIC','7211183623.pdf',9045927,'PDF','2019-05-24 14:20:03',NULL),(15,4,'ÐšÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ Jazz','5995371306.pdf',24560548,'PDF','2019-05-24 14:20:03',NULL),(16,4,'ÐšÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ Lounge','4844751109.pdf',12858447,'PDF','2019-05-24 14:20:03',NULL),(17,5,'ÐŸÑ€Ð¸Ð¼ÐµÑ€Ñ‹ ÑƒÐºÐ»Ð°Ð´Ð¾Ðº ÐºÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ð¸ LOUNGE','9256314812.pdf',4678755,'PDF','2019-05-24 14:26:49',NULL),(18,5,'ÐŸÑ€Ð¸Ð¼ÐµÑ€Ñ‹ ÑƒÐºÐ»Ð°Ð´Ð¾Ðº ÐºÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ð¸ NEW AGE','5956665441.pdf',15618509,'PDF','2019-05-24 14:26:49',NULL),(19,4,'ÐšÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ Murano','1069072656.pdf',24560548,'PDF','2019-05-24 14:20:03',NULL),(20,4,'ÐšÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ NEW AGE','1510256840.pdf',7178570,'PDF','2019-05-24 14:20:29',NULL),(21,4,'ÐšÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ  DREAM HOUSE','6777818189.pdf',8088353,'PDF','2019-05-24 14:24:10',NULL),(22,4,'ÐšÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ PROGRESSIVE HOUSE','3253502634.pdf',14480418,'PDF','2019-05-24 14:24:10',NULL);
/*!40000 ALTER TABLE `product_page_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_page_docs_section`
--

DROP TABLE IF EXISTS `product_page_docs_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_page_docs_section` (
                                             `id` int(11) NOT NULL AUTO_INCREMENT,
                                             `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                             `position` int(11) DEFAULT NULL,
                                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_page_docs_section`
--

LOCK TABLES `product_page_docs_section` WRITE;
/*!40000 ALTER TABLE `product_page_docs_section` DISABLE KEYS */;
INSERT INTO `product_page_docs_section` VALUES (4,'Ð‘Ñ€Ð¾ÑˆÑŽÑ€Ñ‹ Ð¿Ð¾ ÐºÐ¾Ð»ÐµÐºÑ†Ð¸ÑÐ¼',1),(5,'ÐŸÑ€Ð¸Ð¼ÐµÑ€Ñ‹ ÑƒÐºÐ»Ð°Ð´Ð¾Ðº',2),(6,'Ð’Ð¸Ð´ÐµÐ¾-Ð¼Ð°Ñ‚ÐµÑ€Ð¸Ð°Ð»Ñ‹ Ð¿Ð¾ ÐºÐ¾Ð»Ð»ÐµÐºÑ†Ð¸ÑÐ¼',3),(7,'Ð’Ð¸Ð´ÐµÐ¾-Ð¸Ð½ÑÑ‚Ñ€ÑƒÐºÑ†Ð¸Ñ Ð¿Ð¾ ÑƒÐºÐ»Ð°Ð´ÐºÐµ',4),(8,'Ð˜Ð½ÑÑ‚Ñ€ÑƒÐºÑ†Ð¸Ð¸ Ð¿Ð¾ ÑƒÐºÐ»Ð°Ð´ÐºÐµ',5),(9,'Ð‘Ð»Ð¾Ð³ÐµÑ€Ñ‹ Ð¿Ñ€Ð¾ Art Vinyl',NULL);
/*!40000 ALTER TABLE `product_page_docs_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_page_faq`
--

DROP TABLE IF EXISTS `product_page_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_page_faq` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `position` int(11) DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_page_faq`
--

LOCK TABLES `product_page_faq` WRITE;
/*!40000 ALTER TABLE `product_page_faq` DISABLE KEYS */;
INSERT INTO `product_page_faq` VALUES (1,'Ð§Ñ‚Ð¾ Ð´ÐµÐ»Ð°Ñ‚ÑŒ ÐµÑÐ»Ð¸ Ð¸ÑÐ¿Ð¾Ñ€Ñ‚Ð¸Ð»Ð°ÑÑŒ Ð¾Ð´Ð½Ð° Ð¿Ð»Ð¸Ñ‚ÐºÐ°?','<p>Ð¡&nbsp;Ð¿Ð¾Ð¼Ð¾Ñ‰ÑŒÑŽ Ð½Ð¾Ð¶Ð° Ð²Ñ‹Ñ€ÐµÐ¶ÑŒÑ‚Ðµ Ð¿Ð¾&nbsp;ÑÑ‚Ñ‹ÐºÐ°Ð¼ Ð¸&nbsp;ÑƒÐ´Ð°Ð»Ð¸Ñ‚Ðµ Ð¿Ð¾Ð²Ñ€ÐµÐ¶Ð´ÐµÐ½Ð½ÑƒÑŽ Ð¿Ð»Ð°Ð½ÐºÑƒ. Ð’Ð¾Ð·ÑŒÐ¼Ð¸Ñ‚Ðµ Ð½Ð¾Ð²ÑƒÑŽ Ð¿Ð»Ð°Ð½ÐºÑƒ Ð¸&nbsp;Ð¿Ð¾Ð´Ñ€ÐµÐ¶ÑŒÑ‚Ðµ Ð·Ð°Ð¼ÐºÐ¸.<br />\r\nÐŸÐµÑ€ÐµÐ´&nbsp;Ð¼Ð¾Ð½Ñ‚Ð°Ð¶Ð¾Ð¼ Ð¿Ð»Ð°Ð½ÐºÐ¸ ÑƒÐ±ÐµÐ´Ð¸Ñ‚ÐµÑÑŒ, Ñ‡Ñ‚Ð¾&nbsp;ÑÐ¾Ð±Ð»ÑŽÐ´Ð°ÐµÑ‚ÑÑ Ð½Ð°Ð¿Ñ€Ð°Ð²Ð»ÐµÐ½Ð¸Ðµ Ñ€Ð¸ÑÑƒÐ½ÐºÐ°. Ð—Ð°ÐºÑ€ÐµÐ¿Ð¸Ñ‚Ðµ Ð½Ð¾Ð²ÑƒÑŽ Ð¿Ð»Ð°Ð½ÐºÑƒ Ð¸&nbsp;Ð¾ÐºÑ€ÑƒÐ¶Ð°ÑŽÑ‰Ð¸Ðµ ÐµÐµ&nbsp;Ð¿Ð»Ð°Ð½ÐºÐ¸<br />\r\nÐ¿Ñ€Ð¸&nbsp;Ð¿Ð¾Ð¼Ð¾Ñ‰Ð¸ Ð´Ð²ÑƒÑÑ‚Ð¾Ñ€Ð¾Ð½Ð½ÐµÐ¹ ÐºÐ»ÐµÐ¹ÐºÐ¾Ð¹ Ð»ÐµÐ½Ñ‚Ñ‹. Ð¢Ñ‰Ð°Ñ‚ÐµÐ»ÑŒÐ½Ð¾ Ð¿Ñ€Ð¾ÐºÐ°Ñ‚Ð°Ð¹Ñ‚Ðµ Ð¸&nbsp;Ð¿Ñ€Ð¸Ð´Ð°Ð²Ð¸Ñ‚Ðµ Ð²ÑÐµ&nbsp;Ð¿Ð»Ð°Ð½ÐºÐ¸, Ñ‡Ñ‚Ð¾Ð±Ñ‹&nbsp;Ð½Ð°Ð´Ñ‘Ð¶Ð½Ð¾ Ð¿Ñ€Ð¸ÐºÑ€ÐµÐ¿Ð¸Ñ‚ÑŒ Ð¸Ñ…&nbsp;Ðº&nbsp;Ð¿Ð¾Ð»Ñƒ.</p>',2),(2,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Art Vinyl Ð½Ð° Ð½ÐµÑ€Ð¾Ð²Ð½Ñ‹Ðµ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ?','<p>ÐžÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ &nbsp;Ð´Ð»Ñ Art Vinyl Ð´Ð¾Ð»Ð¶Ð½Ð¾ Ð±Ñ‹Ñ‚ÑŒ Ð°Ð±ÑÐ¾Ð»ÑŽÑ‚Ð½Ð¾ Ñ€Ð¾Ð²Ð½Ñ‹Ð¼.</p>',3),(3,'ÐšÐ°Ðº Ð¿Ñ€Ð°Ð²Ð¸Ð»ÑŒÐ½Ð¾ Ð²Ñ‹Ð±Ñ€Ð°Ñ‚ÑŒ ÐºÐ»ÐµÐ¹?','<p>Ð”Ð»Ñ Art Vinyl Ð¿Ñ€Ð¸Ð¼ÐµÐ½ÑÐµÑ‚ÑÑ Ð°ÐºÑ€Ð¸Ð»Ð¾Ð²Ñ‹Ð¹ Ð²Ð¾Ð´Ð½Ð¾-Ð´Ð¸ÑÐ¿ÐµÑ€ÑÐ¸Ð¾Ð½Ð½Ñ‹Ð¹ ÐºÐ»ÐµÐ¹, Ð¿Ñ€ÐµÐ´Ð½Ð°Ð·Ð½Ð°Ñ‡ÐµÐ½Ð½Ñ‹Ð¹ Ð´Ð»Ñ ÐŸÐ’Ð¥ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ð¹. Ð ÐµÐºÐ¾Ð¼ÐµÐ½Ð´ÑƒÐµÐ¼Ñ‹Ðµ Ð¼Ð°Ñ€ÐºÐ¸ ÐºÐ»ÐµÐµÐ²1: Uzin ÐšÐ• 2000 S (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Uzin Utz AG), Homakoll 164 prof (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Homa Chemical Engineering), Ultrabond Eco 375 (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Mapei S.p.A.), Ultrabond V4SP (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Mapei S.p.A.), EUROCOL Eurostar Allround 528 (Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒ Forbo).</p>',5),(4,'Ð“Ð´Ðµ Ñ Ð¼Ð¾Ð³Ñƒ Ð¿Ð¾ÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ Ñ‚Ð¾Ñ‡Ð½Ð¾Ðµ ÐºÐ¾Ð»Ð¸Ñ‡ÐµÑÑ‚Ð²Ð¾ Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾Ð¹ Ð¿Ð»Ð¸Ñ‚ÐºÐ¸?','<p>ÐŸÐ¾ÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ ÐºÐ¾Ð»Ð¸Ñ‡ÐµÑÑ‚Ð²Ð¾ Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾Ð¹ Ð¿Ð»Ð¸Ñ‚ÐºÐ¸ Ð¿Ñ€Ð¾ÑÑ‚Ð¾: ÐÑƒÐ¶Ð½Ð¾ Ñ€Ð°ÑÑÑ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ Ð¿Ð»Ð¾Ñ‰Ð°Ð´ÑŒ Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸Ñ Ð¸ Ð¿Ñ€Ð¸Ð±Ð°Ð²Ð¸Ñ‚ÑŒ +10%. ÐŸÑ€Ð¸ ÑÐ»Ð¾Ð¶Ð½Ñ‹Ñ… ÑƒÐºÐ»Ð°Ð´ÐºÐ°Ñ… Ð¸Ð· Ð½ÐµÑÐºÐ¾Ð»ÑŒÐºÐ¸Ñ… Ñ„Ð¾Ñ€Ð¼Ð°Ñ‚Ð¾Ð² Ð¸/Ð¸Ð»Ð¸ Ð´Ð¸Ð·Ð°Ð¹Ð½Ð¾Ð² &nbsp;Ð¼Ð¾Ð¶Ð½Ð¾ Ð²Ð¾ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÑŒÑÑ Ñ„ÑƒÐ½ÐºÑ†Ð¸ÐµÐ¹ ÐºÐ°Ð»ÑŒÐºÑƒÐ»ÑÑ‚Ð¾Ñ€ Ð² Ð¿Ñ€Ð¾Ð³Ñ€Ð°Ð¼Ð¼Ðµ <a href=\"http://new.artvinyl.ru/designer/\">Art Designer</a></p>',6),(5,'ÐšÐ°Ðº ÑƒÐ»Ð¾Ð¶Ð¸Ñ‚ÑŒ Art Vinyl Ð½Ð° Ð¿Ð¾Ñ‚Ð¾Ð»Ð¾Ðº?','<p>Ð”ÐµÐ¹ÑÑ‚Ð²Ð¸Ñ‚ÐµÐ»ÑŒÐ½Ð¾ Ð¼Ð½Ð¾Ð³Ð¸Ðµ Ð´Ð¸Ð·Ð°Ð¹Ð½ÐµÑ€Ñ‹ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·ÑƒÑŽÑ‚ Art Vinyl Ð´Ð»Ñ ÑÑ‚ÐµÐ½ Ð¸ Ð¿Ð¾Ñ‚Ð¾Ð»ÐºÐ¾Ð². ÐœÑ‹ Ð½Ðµ Ð´Ð°ÐµÐ¼ Ñ€ÐµÐºÐ¾Ð¼ÐµÐ½Ð´Ð°Ñ†Ð¸Ð¹, Ñ‚Ð°Ðº ÐºÐ°Ðº Ð´Ð»Ñ Ð½Ð°Ñ ÑÑ‚Ð¾ Ð¿Ñ€ÐµÐ¶Ð´Ðµ Ð²ÑÐµÐ³Ð¾ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ð¾Ðµ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ðµ.</p>',8),(6,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ð½Ð° Ñ‚ÐµÐ¿Ð»Ñ‹Ð¹ Ð¿Ð¾Ð»?','<p>Ð”Ð° Ð¼Ð¾Ð¶Ð½Ð¾ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÑŒ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ðµ Ñ Ñ‚ÐµÐ¿Ð»Ñ‹Ð¼ Ð¿Ð¾Ð»Ð¾Ð¼ Ð´Ð¾ 27â„ƒ.</p>',9),(7,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ð½Ð° Ñ„Ð°Ð½ÐµÑ€Ñƒ?','<p>ÐÐµÑ‚, Ð½ÑƒÐ¶Ð½Ð¾ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ñ‚Ð¾Ð»ÑŒÐºÐ¾ Ð½Ð° Ñ€Ð¾Ð²Ð½Ð¾Ðµ Ð±ÐµÑ‚Ð¾Ð½Ð½Ð¾Ðµ Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ.</p>',10),(8,'ÐœÐ¾Ð¶Ð½Ð¾ Ð»Ð¸ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ð½Ð° Ð±Ð°Ð»ÐºÐ¾Ð½Ðµ?','<p>ÐÐµÑ‚, Ð¼Ð¾Ð¶Ð½Ð¾ ÑƒÐºÐ»Ð°Ð´Ñ‹Ð²Ð°Ñ‚ÑŒ Ñ‚Ð¾Ð»ÑŒÐºÐ¾ Ð² Ð¾Ñ‚Ð°Ð¿Ð»Ð¸Ð²Ð°ÐµÐ¼Ñ‹Ñ… Ð¾ÑÑ‚ÐµÐºÐ»ÐµÐ½Ð½Ñ‹Ñ… Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸ÑÑ….</p>',7),(9,'ÐÐ°ÑÐºÐ¾Ð»ÑŒÐºÐ¾ Ð¿Ñ€Ð¾Ð´ÑƒÐºÑ‚ Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐµÐ½?','<p>ÐŸÑ€Ð¾Ð´ÑƒÐºÑ‚ Ð°Ð±ÑÐ¾Ð»ÑŽÑ‚Ð½Ð¾ Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐµÐ½. ÐžÐ½ Ð¸Ð¼ÐµÐµÑ‚ ÑÐºÐ¾Ð»Ð¾Ð³Ð¸Ñ‡ÐµÑÐºÐ¸Ð¹ ÑÐµÑ€Ñ‚Ð¸Ñ„Ð¸ÐºÐ°Ñ‚ &laquo;Ð›Ð¸ÑÑ‚Ð¾Ðº Ð–Ð¸Ð·Ð½Ð¸&raquo;.</p>',4),(10,'Страница продукта?','<p>Где она&nbsp;</p>',1),(11,'привет','<p>привет</p>',11);
/*!40000 ALTER TABLE `product_page_faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_page_icon`
--

DROP TABLE IF EXISTS `product_page_icon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_page_icon` (
                                     `id` int(11) NOT NULL AUTO_INCREMENT,
                                     `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                     `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                     `updated` datetime DEFAULT NULL,
                                     `position` int(11) DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_page_icon`
--

LOCK TABLES `product_page_icon` WRITE;
/*!40000 ALTER TABLE `product_page_icon` DISABLE KEYS */;
INSERT INTO `product_page_icon` VALUES (1,'100% Ð²Ð»Ð°Ð³Ð¾ÑÑ‚Ð¾Ð¹ÐºÐ¾ÑÑ‚ÑŒ','8921269261.svg','2019-05-17 10:05:34',1),(2,'Ð”Ð¾Ð»Ð³Ð¾Ð²ÐµÑ‡Ð½Ð¾ÑÑ‚ÑŒ Ð¸ Ð¸Ð·Ð½Ð¾ÑÐ¾ÑÑ‚Ð¾Ð¹ÐºÐ¾ÑÑ‚ÑŒ','1105774746.svg','2019-05-29 09:55:29',2),(3,'ÐšÐ¾Ð¼Ñ„Ð¾Ñ€Ñ‚ Ð¿Ñ€Ð¸ ÑÐºÑÐ¿Ð»ÑƒÐ°Ñ‚Ð°Ñ†Ð¸Ð¸','9371464514.svg','2019-05-29 09:55:43',3),(4,'Ð­ÐºÐ¾Ð»Ð¾Ð³Ð¸Ñ‡Ð½Ð¾ÑÑ‚ÑŒ Ð¸ Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐ½Ð¾ÑÑ‚ÑŒ','3814782319.svg','2019-05-29 11:01:18',4),(5,'Ð£Ð´Ð¾Ð±ÑÑ‚Ð²Ð¾ ÑƒÐºÐ»Ð°Ð´ÐºÐ¸ Ð¸ ÑƒÑ…Ð¾Ð´Ð°','3602626622.svg','2019-05-29 09:55:53',5),(6,'Ð£Ð´Ð¾Ð±Ð½Ð°Ñ Ñ‚Ñ€Ð°Ð½ÑÐ¿Ð¾Ñ€Ñ‚Ð¸Ñ€Ð¾Ð²ÐºÐ° Ð¸ Ñ…Ñ€Ð°Ð½ÐµÐ½Ð¸Ðµ','4882205507.svg','2019-05-29 11:01:29',6),(7,'Ð”Ð¸Ð·Ð°Ð¹Ð½ÐµÑ€ÑÐºÐ¸Ðµ Ð²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ‚Ð¸','9552063145.svg','2019-05-17 10:06:37',7);
/*!40000 ALTER TABLE `product_page_icon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_page_slider`
--

DROP TABLE IF EXISTS `product_page_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_page_slider` (
                                       `id` int(11) NOT NULL AUTO_INCREMENT,
                                       `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `updated` datetime DEFAULT NULL,
                                       `position` int(11) DEFAULT NULL,
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_page_slider`
--

LOCK TABLES `product_page_slider` WRITE;
/*!40000 ALTER TABLE `product_page_slider` DISABLE KEYS */;
INSERT INTO `product_page_slider` VALUES (8,'100% Ð²Ð»Ð°Ð³Ð¾ÑÑ‚Ð¾Ð¹ÐºÐ¸Ð¹ Ð¼Ð°Ñ‚ÐµÑ€Ð¸Ð°Ð»','4530534342.jpg','2019-05-16 14:04:45',1),(9,'Ð”Ð¾Ð»Ð³Ð¾Ð²ÐµÑ‡Ð½Ð¾ÑÑ‚ÑŒ Ð¸ Ð¸Ð·Ð½Ð¾ÑÐ¾ÑÑ‚Ð¾Ð¹ÐºÐ¾ÑÑ‚ÑŒ','1522826442.jpg','2019-05-16 14:06:05',2),(10,'ÐšÐ¾Ð¼Ñ„Ð¾Ñ€Ñ‚ Ð¿Ñ€Ð¸ ÑÐºÑÐ¿Ð»ÑƒÐ°Ñ‚Ð°Ñ†Ð¸Ð¸','3401183294.jpg','2019-05-16 14:07:16',3),(11,'Ð­ÐºÐ¾Ð»Ð¾Ð³Ð¸Ñ‡Ð½Ð¾ÑÑ‚ÑŒ Ð¸ Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐ½Ð¾ÑÑ‚ÑŒ','9101734940.jpg','2019-05-16 14:09:47',4),(12,'Ð£Ð´Ð¾Ð±ÑÑ‚Ð²Ð¾ ÑƒÐºÐ»Ð°Ð´ÐºÐ¸ Ð¸ ÑƒÑ…Ð¾Ð´Ð°','7482922349.jpg','2019-05-16 14:10:57',5),(13,'Ð£Ð´Ð¾Ð±Ð½Ð°Ñ Ñ‚Ñ€Ð°Ð½ÑÐ¿Ð¾Ñ€Ñ‚Ð¸Ñ€Ð¾Ð²ÐºÐ° Ð¸ Ñ…Ñ€Ð°Ð½ÐµÐ½Ð¸Ðµ','7706273486.jpg','2019-05-16 14:11:41',6),(14,'Ð”Ð¸Ð·Ð°Ð¹Ð½ÐµÑ€ÑÐºÐ¸Ðµ Ð²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ‚Ð¸','4826835912.jpg','2019-05-16 14:13:01',7);
/*!40000 ALTER TABLE `product_page_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_page_slider_info`
--

DROP TABLE IF EXISTS `product_page_slider_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_page_slider_info` (
                                            `id` int(11) NOT NULL AUTO_INCREMENT,
                                            `slider_id` int(11) DEFAULT NULL,
                                            `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                            `position` int(11) DEFAULT NULL,
                                            PRIMARY KEY (`id`),
                                            KEY `IDX_3527BB092CCC9638` (`slider_id`),
                                            CONSTRAINT `FK_3527BB092CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `product_page_slider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_page_slider_info`
--

LOCK TABLES `product_page_slider_info` WRITE;
/*!40000 ALTER TABLE `product_page_slider_info` DISABLE KEYS */;
INSERT INTO `product_page_slider_info` VALUES (34,8,'ÐœÐ¾Ð¶Ð½Ð¾ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÑŒ Ð½Ð° ÐºÑƒÑ…Ð½Ðµ',1),(35,8,'Ð’Ð»Ð°Ð³Ð° Ð½Ðµ Ð¿Ð¾Ð¿Ð°Ð´Ð°ÐµÑ‚  Ð¼ÐµÐ¶Ð´Ñƒ ÑÑ‚Ñ‹ÐºÐ°Ð¼Ð¸ Ð±Ð»Ð°Ð³Ð¾Ð´Ð°Ñ€Ñ ÐºÐ¾Ð½ÑÑ‚Ñ€ÑƒÐºÑ†Ð¸Ð¸ Ð¸ ÑÐ¾ÑÑ‚Ð°Ð²Ñƒ Ð¼Ð°Ñ‚ÐµÑ€Ð¸Ð°Ð»Ð°',2),(36,8,'Ð¢Ð¾Ñ€Ñ†Ñ‹ Ð½Ðµ Ð¿Ð¾Ð´Ð²ÐµÑ€Ð¶ÐµÐ½Ñ‹ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸ÑÐ¼ Ð¾Ñ‚ Ð²Ð»Ð°Ð³Ð¸',3),(37,9,'Ð’Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ‚ÑŒ Ð¿Ñ€Ð¸Ð¼ÐµÐ½ÐµÐ½Ð¸Ñ Ð² Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸ÑÑ… Ñ Ð²Ñ‹ÑÐ¾ÐºÐ¾Ð¹ Ð¿Ñ€Ð¾Ñ…Ð¾Ð´Ð¸Ð¼Ð¾ÑÑ‚ÑŒÑŽ, Ð½Ð°Ð¿Ñ€Ð¸Ð¼ÐµÑ€ Ð² ÐºÐ¾Ñ€Ð¸Ð´Ð¾Ñ€Ð°Ñ… Ð¸Ð»Ð¸ Ð¿Ñ€Ð¸Ñ…Ð¾Ð¶Ð¸Ñ…',NULL),(38,9,'ÐÐµ Ð´ÐµÑ„Ð¾Ñ€Ð¼Ð¸Ñ€ÑƒÐµÑ‚ÑÑ - Ð½Ðµ Ð±Ð¾Ð¸Ñ‚ÑÑ ÑÐ»ÐµÐ´Ð¾Ð² Ð¾Ñ‚ ÐºÑ€ÐµÑÐµÐ» Ð¸ ÐºÐ°Ð±Ð»ÑƒÐºÐ¾Ð²',NULL),(39,9,'ÐÐµ Ñ…Ñ€ÑƒÐ¿ÐºÐ¸Ð¹ â€“ Ð½Ðµ Ð±Ð¾Ð¸Ñ‚ÑÑ Ð¿Ð°Ð´ÐµÐ½Ð¸Ñ Ñ‚ÑÐ¶ÐµÐ»Ñ‹Ñ… Ð¿Ñ€ÐµÐ´Ð¼ÐµÑ‚Ð¾Ð²',NULL),(40,9,'ÐŸÑ€Ð¾Ñ‡ÐµÐ½, Ð¼Ð°Ð»Ð¾ Ð¸Ð·Ð½Ð°ÑˆÐ¸Ð²Ð°ÐµÑ‚ÑÑ - ÑƒÑÑ‚Ð¾Ð¹Ñ‡Ð¸Ð² Ðº Ñ†Ð°Ñ€Ð°Ð¿Ð¸Ð½Ð°Ð¼ Ð¸ Ñ€Ð°Ð·Ð»Ð¸Ñ‡Ð½Ñ‹Ð¼ Ð·Ð°Ð³Ñ€ÑÐ·Ð½ÐµÐ½Ð¸ÑÐ¼',NULL),(41,9,'ÐÐµ Ð±Ð¾Ð¸Ñ‚ÑÑ ÑÑ‹Ñ€Ð¾ÑÑ‚Ð¸, Ð½Ðµ Ð³Ð½Ð¸ÐµÑ‚',NULL),(42,10,'Ð£ÑÑ‚Ð¾Ð¹Ñ‡Ð¸Ð²Ð¾ÑÑ‚ÑŒ Ðº ÑÐºÐ¾Ð»ÑŒÐ¶ÐµÐ½Ð¸ÑŽ, Ñ‡Ñ‚Ð¾ Ð¾ÑÐ¾Ð±ÐµÐ½Ð½Ð¾ Ð°ÐºÑ‚ÑƒÐ°Ð»ÑŒÐ½Ð¾, ÐµÑÐ»Ð¸ Ð´Ð¾Ð¼Ð° Ð´ÐµÑ‚Ð¸ Ð¸Ð»Ð¸ Ð¿Ð¾Ð¶Ð¸Ð»Ñ‹Ðµ Ð»ÑŽÐ´Ð¸',NULL),(43,10,'ÐÐºÑƒÑÑ‚Ð¸Ñ‡ÐµÑÐºÐ¸Ð¹ ÐºÐ¾Ð¼Ñ„Ð¾Ñ€Ñ‚ â€“ Ð¼ÐµÐ½ÑŒÑˆÐµ ÑˆÑƒÐ¼Ð° ÐºÐ°Ðº Ð² ÑÐ°Ð¼Ð¾Ð¼ Ð¿Ð¾Ð¼ÐµÑ‰ÐµÐ½Ð¸Ð¸, Ñ‚Ð°Ðº Ð´Ð»Ñ ÑÐ¾ÑÐµÐ´ÐµÐ¹ ÑÐ½Ð¸Ð·Ñƒ',NULL),(44,10,'ÐÐ½Ñ‚Ð¸ÑÑ‚Ð°Ñ‚Ð¸Ñ‡ÐµÑÐºÐ¸Ðµ ÑÐ²Ð¾Ð¹ÑÑ‚Ð²Ð° â€“ Ð½Ðµ Â«Ð±ÑŒÐµÑ‚ Ñ‚Ð¾ÐºÐ¾Ð¼Â» Ð¿Ñ€Ð¸ Ð¿Ñ€Ð¸ÐºÐ¾ÑÐ½Ð¾Ð²ÐµÐ½Ð¸Ð¸ Ðº Ð´Ð²ÐµÑ€Ð½Ñ‹Ð¼ Ñ€ÑƒÑ‡ÐºÐ°Ð¼',NULL),(45,10,'Ð¢Ð°ÐºÑ‚Ð¸Ð»ÑŒÐ½Ð¾ Ñ‚ÐµÐ¿Ð»Ñ‹Ð¹ Ð¼Ð°Ñ‚ÐµÑ€Ð¸Ð°Ð», Ð´Ð°Ð¶Ðµ ÐµÑÐ»Ð¸ Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ðµ Ñ…Ð¾Ð»Ð¾Ð´Ð½Ð¾Ðµ â€“ Ð¼Ð¾Ð¶Ð½Ð¾ Ñ…Ð¾Ð´Ð¸Ñ‚ÑŒ Ð±Ð¾ÑÐ¸ÐºÐ¾Ð¼, ÐºÐ¾Ð¼Ñ„Ð¾Ñ€Ñ‚Ð½Ð¾ ÑÐ¸Ð´ÐµÑ‚ÑŒ Ð¸ Ð»ÐµÐ¶Ð°Ñ‚ÑŒ Ð½Ð° Ð¿Ð¾Ð»Ñƒ',NULL),(46,10,'Ð’Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ‚ÑŒ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÑŒ Ð¿Ð¾Ð²ÐµÑ€Ñ… Ñ‚ÐµÐ¿Ð»Ñ‹Ñ… Ð¿Ð¾Ð»Ð¾Ð²',NULL),(47,11,'Ð˜Ð¼ÐµÐµÑ‚ ÑÐºÐ¾Ð»Ð¾Ð³Ð¸Ñ‡ÐµÑÐºÑƒÑŽ Ð¼Ð°Ñ€ÐºÐ¸Ñ€Ð¾Ð²ÐºÑƒ Â«Ð›Ð¸ÑÑ‚Ð¾Ðº Ð¶Ð¸Ð·Ð½Ð¸Â» Ð¼ÐµÐ¶Ð´ÑƒÐ½Ð°Ñ€Ð¾Ð´Ð½Ð¾Ð³Ð¾ ÑƒÑ€Ð¾Ð²Ð½Ñ',NULL),(48,11,'ÐÐµ Ð²Ñ‹Ð´ÐµÐ»ÑÐµÑ‚ Ð²Ñ€ÐµÐ´Ð½Ñ‹Ñ… Ð²ÐµÑ‰ÐµÑÑ‚Ð², Ð½Ðµ ÑÐ¾Ð´ÐµÑ€Ð¶Ð¸Ñ‚ Ð¸ Ð½Ðµ Ð²Ñ‹Ð´ÐµÐ»ÑÐµÑ‚ Ñ„Ð¾Ñ€Ð¼Ð°Ð»ÑŒÐ´ÐµÐ³Ð¸Ð´',NULL),(50,11,'Ð’ Ð¿Ñ€Ð¾Ð¸Ð·Ð²Ð¾Ð´ÑÑ‚Ð²Ðµ Ð½Ðµ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·ÑƒÑŽÑ‚ÑÑ ÑÐ´Ð¾Ð²Ð¸Ñ‚Ñ‹Ðµ Ð¾Ñ€Ð³Ð°Ð½Ð¸Ñ‡ÐµÑÐºÐ¸Ðµ Ñ€Ð°ÑÑ‚Ð²Ð¾Ñ€Ð¸Ñ‚ÐµÐ»Ð¸',NULL),(51,12,'Ð ÐµÐ¼Ð¾Ð½Ñ‚Ð¾Ð¿Ñ€Ð¸Ð³Ð¾Ð´Ð½Ð¾ÑÑ‚ÑŒ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ â€“ Ð¼Ð¾Ð¶Ð½Ð¾ Ð·Ð°Ð¼ÐµÐ½Ð¸Ñ‚ÑŒ Ð¿Ð¾Ð²Ñ€ÐµÐ¶Ð´ÐµÐ½Ð½ÑƒÑŽ Ð¿Ð»Ð°Ð½ÐºÑƒ',NULL),(52,12,'ÐœÐ°Ñ‚ÐµÑ€Ð¸Ð°Ð» Ð¼Ð¾Ð¶Ð½Ð¾ Ñ€ÐµÐ·Ð°Ñ‚ÑŒ Ð¾Ð±Ñ‹Ñ‡Ð½Ñ‹Ð¼ Ñ‚ÐµÑ…Ð½Ð¸Ñ‡ÐµÑÐºÐ¸Ð¼ Ð½Ð¾Ð¶Ð¾Ð¼',NULL),(53,12,'Ð’Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ‚ÑŒ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ð½Ð¸Ñ Ð½Ð° Ð±Ð¾Ð»ÑŒÑˆÐ¸Ñ… Ð¿Ð»Ð¾Ñ‰Ð°Ð´ÑÑ… Ð±ÐµÐ· Ð¿ÐµÑ€ÐµÑ…Ð¾Ð´Ð½Ñ‹Ñ… Ð¿Ð¾Ñ€Ð¾Ð¶ÐºÐ¾Ð² â€“ Ð½ÐµÑ‚ Ð´Ð¾Ð¿Ð¾Ð»Ð½Ð¸Ñ‚ÐµÐ»ÑŒÐ½Ñ‹Ñ… Ð·Ð°Ñ‚Ñ€Ð°Ñ‚ Ð½Ð° Ð¿Ð¾Ñ€Ð¾Ð¶ÐºÐ¸, Ð½Ðµ ÑÐºÐ°Ð¿Ð»Ð¸Ð²Ð°ÐµÑ‚ÑÑ Ð³Ñ€ÑÐ·ÑŒ',NULL),(54,12,'Ð›ÐµÐ³ÐºÐ¾ ÑƒÑ…Ð°Ð¶Ð¸Ð²Ð°Ñ‚ÑŒ Ð·Ð° Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸ÐµÐ¼ â€“ Ð¿Ñ€Ð¾ÑÑ‚Ð°Ñ ÑÑƒÑ…Ð°Ñ Ð¸, ÐµÑÐ»Ð¸ Ð½ÐµÐ¾Ð±Ñ…Ð¾Ð´Ð¸Ð¼Ð¾, Ð²Ð»Ð°Ð¶Ð½Ð°Ñ ÑƒÐ±Ð¾Ñ€ÐºÐ°',NULL),(55,13,'Ð’ÐµÑ ÑƒÐ¿Ð°ÐºÐ¾Ð²ÐºÐ¸ Ð½Ðµ Ð¿Ñ€ÐµÐ²Ñ‹ÑˆÐ°ÐµÑ‚ 10 ÐºÐ³ â€“ Ð½Ðµ Ñ‚Ñ€ÐµÐ±ÑƒÑŽÑ‚ÑÑ ÑƒÑÐ»ÑƒÐ³Ð¸ Ð³Ñ€ÑƒÐ·Ñ‡Ð¸ÐºÐ°',NULL),(56,13,'ÐœÐ°ÐºÑÐ¸Ð¼Ð°Ð»ÑŒÐ½Ð°Ñ Ð´Ð»Ð¸Ð½Ð° Ð¿Ð°Ñ‡ÐºÐ¸ 92 ÑÐ¼ - Ð¿Ð¾Ð¼ÐµÑ‰Ð°ÐµÑ‚ÑÑ Ð´Ð°Ð¶Ðµ Ð² Ð¿Ð°ÑÑÐ°Ð¶Ð¸Ñ€ÑÐºÐ¾Ð¼ Ð»Ð¸Ñ„Ñ‚Ðµ Ð¸Ð»Ð¸ Ð² Ð±Ð°Ð³Ð°Ð¶Ð½Ð¸ÐºÐµ Ð»ÑŽÐ±Ð¾Ð³Ð¾ Ð°Ð²Ñ‚Ð¾Ð¼Ð¾Ð±Ð¸Ð»Ñ',NULL),(57,13,'Ð›ÐµÐ³ÐºÐ¾ Ð¸ ÑƒÐ´Ð¾Ð±Ð½Ð¾ ÑÐºÐ»Ð°Ð´Ð¸Ñ€Ð¾Ð²Ð°Ñ‚ÑŒ Ð²Ð¾ Ð²Ñ€ÐµÐ¼Ñ Ñ€ÐµÐ¼Ð¾Ð½Ñ‚Ð°',NULL),(58,14,'Ð‘Ð¾Ð³Ð°Ñ‚Ñ‹Ð¹ Ð°ÑÑÐ¾Ñ€Ñ‚Ð¸Ð¼ÐµÐ½Ñ‚ Ñ€Ð°ÑÑ†Ð²ÐµÑ‚Ð¾Ðº Ð¸ Ñ„Ð°ÐºÑ‚ÑƒÑ€ Ð¿Ð»Ð°Ð½Ð¾Ðº Ð´Ð»Ñ Ð»ÑŽÐ±Ð¾Ð³Ð¾ ÑÑ‚Ð¸Ð»Ñ Ð¸Ð½Ñ‚ÐµÑ€ÑŒÐµÑ€Ð°',NULL),(59,14,'Ð’Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ‚ÑŒ Ñ€Ð°Ð·Ñ€Ð°Ð±Ð¾Ñ‚ÐºÐ¸ ÑÐ¾Ð±ÑÑ‚Ð²ÐµÐ½Ð½Ð¾Ð³Ð¾ Ð´Ð¸Ð·Ð°Ð¹Ð½-Ð¿Ñ€Ð¾ÐµÐºÑ‚Ð°',NULL),(60,14,'ÐœÐ½Ð¾Ð³Ð¾Ð¾Ð±Ñ€Ð°Ð·Ð¸Ðµ ÑÑ…ÐµÐ¼ ÑƒÐºÐ»Ð°Ð´Ð¾Ðº â€” Ð’Ð°Ñˆ Ð½ÐµÐ¿Ð¾Ð²Ñ‚Ð¾Ñ€Ð¸Ð¼Ñ‹Ð¹ Ð¿Ð¾Ð»',NULL),(61,14,'ÐœÐ¾Ð´ÑƒÐ»Ð¸ Ð¸Ð¼ÐµÑŽÑ‚ Ñ‚Ð¾Ñ‡Ð½Ñ‹Ðµ Ñ€Ð°Ð·Ð¼ÐµÑ€Ñ‹ Ð¸ Ñ„Ð¾Ñ€Ð¼Ñƒ, Ð¿Ð¾Ð» Ð²Ñ‹Ð³Ð»ÑÐ´Ð¸Ñ‚ ÐµÐ´Ð¸Ð½Ñ‹Ð¼ Ð±ÐµÑÑˆÐ¾Ð²Ð½Ñ‹Ð¼ Ð¼Ð°ÑÑÐ¸Ð²Ð¾Ð¼',NULL),(62,14,'Ð¡Ð¾Ð·Ð´Ð°Ð½Ð¸Ðµ Â«ÑÑ„Ñ„ÐµÐºÑ‚Ð° Ð¾Ð±ÑŠÐµÐ¼Ð°Â» Ð·Ð° ÑÑ‡ÐµÑ‚ Ñ„Ð°ÑÐºÐ¸',NULL);
/*!40000 ALTER TABLE `product_page_slider_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_page_youtube`
--

DROP TABLE IF EXISTS `product_page_youtube`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_page_youtube` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `section_id` int(11) DEFAULT NULL,
                                        `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `href` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `position` int(11) DEFAULT NULL,
                                        PRIMARY KEY (`id`),
                                        KEY `IDX_87BC50CAD823E37A` (`section_id`),
                                        CONSTRAINT `FK_87BC50CAD823E37A` FOREIGN KEY (`section_id`) REFERENCES `product_page_docs_section` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_page_youtube`
--

LOCK TABLES `product_page_youtube` WRITE;
/*!40000 ALTER TABLE `product_page_youtube` DISABLE KEYS */;
INSERT INTO `product_page_youtube` VALUES (4,4,'Ð§Ñ‚Ð¾ Ñ‚Ð°ÐºÐ¾Ðµ Art Vinyl?','https://youtu.be/V-RkDxfGA94',NULL),(5,6,'Art Vinyl Tarkett - Ð±ÐµÐ·Ð³Ñ€Ð°Ð½Ð¸Ñ‡Ð½Ñ‹Ðµ Ð´Ð¸Ð·Ð°Ð¹Ð½ÐµÑ€ÑÐºÐ¸Ðµ Ð²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ‚Ð¸','https://www.youtube.com/watch?v=V-RkDxfGA94&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=2&t=0s',1),(6,6,'Art Vinyl Murano','https://www.youtube.com/watch?v=F8rvJEyAf6E&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=3&t=0s',2),(7,6,'Art Vinyl Tarkett ÐºÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ NEW AGE','https://www.youtube.com/watch?v=zhQ9xsTIIA4&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=4&t=0s',3),(8,6,'Art Vinyl Tarkett ÐºÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ñ LOUNGE','https://www.youtube.com/watch?v=2zNjMokowXU&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=6&t=1s',4),(10,6,'Art Vinyl - Ð½Ð¾Ð²Ð¾Ðµ Ð¿Ð¾ÐºÐ¾Ð»ÐµÐ½Ð¸Ðµ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ñ‹Ð¹ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ð¹ Ð¾Ñ‚ Tarkett','https://www.youtube.com/watch?v=xvof4HBs9WI&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=7&t=0s',5),(11,7,'Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð¼Ð¾Ð´ÑƒÐ»ÑŒÐ½Ð¾Ð³Ð¾ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ð¾Ð³Ð¾ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ Tarkett Art Vinyl NEW AGE','https://www.youtube.com/watch?v=CweBwNjrnog&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=5&t=29s',2),(12,7,'Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð¼Ð¾Ð´ÑƒÐ»ÑŒÐ½Ð¾Ð³Ð¾ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ð¾Ð³Ð¾ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ Tarkett Art Vinyl Ð¿Ð¾Ð´Ð³Ð¾Ñ‚Ð¾Ð²ÐºÐ° Ð¾ÑÐ½Ð¾Ð²Ð°Ð½Ð¸Ñ','https://www.youtube.com/watch?v=k8soJlPUig8&list=PL8TDAZmMQt_9mB63PD1_feeTaBAX7qq9C&index=10&t=0s',1),(14,7,'Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð¼Ð¾Ð´ÑƒÐ»ÑŒÐ½Ð¾Ð³Ð¾ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ð¾Ð³Ð¾ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ Ð½Ð° ÐºÐ»ÐµÐ¹','https://www.youtube.com/watch?v=k8soJlPUig8',3),(15,7,'Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð¼Ð¾Ð´ÑƒÐ»ÑŒÐ½Ð¾Ð³Ð¾ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ð¾Ð³Ð¾ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ Ñ Ð·Ð°Ð¼ÐºÐ¾Ð¼','https://www.youtube.com/watch?v=Xyky7AyO-RU',4),(16,9,'ÐŸÐ°Ð²ÐµÐ» Ð¡Ð¸Ð´Ð¾Ñ€Ð¸Ðº. Ð£ÐºÐ»Ð°Ð´ÐºÐ° Ð½Ð° ÑÑ‚ÐµÐ½Ñƒ','https://www.youtube.com/watch?v=Ub-ta0nW598',1),(17,9,'ÐÐ½Ð´Ñ€ÐµÐ¹ Ð¨Ð°Ð¹Ñ‚ÐµÑ€. ÐŸÐ’Ð¥ Ð¿Ð»Ð¸Ñ‚ÐºÐ°','https://www.youtube.com/watch?v=0r7fozdIf10&t',2),(18,9,'ÐÐ½Ð´Ñ€ÐµÐ¹ Ð¨Ð°Ð¹Ñ‚ÐµÑ€. ÐÐµÑÑ‚Ð°Ð½Ð´Ð°Ñ€Ñ‚Ð½Ð°Ñ ÑƒÐºÐ»Ð°Ð´ÐºÐ° Ð¸ Ð½Ðµ Ñ‚Ð¾Ð»ÑŒÐºÐ¾','https://www.youtube.com/watch?v=PEihei8gQAw',3);
/*!40000 ALTER TABLE `product_page_youtube` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `start_page_commons`
--

DROP TABLE IF EXISTS `start_page_commons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `start_page_commons` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `captionaside` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `updated` datetime DEFAULT NULL,
                                      `metatitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `metakeys` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `metadesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `designcaption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `designtext` longtext COLLATE utf8mb4_unicode_ci,
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `start_page_commons`
--

LOCK TABLES `start_page_commons` WRITE;
/*!40000 ALTER TABLE `start_page_commons` DISABLE KEYS */;
INSERT INTO `start_page_commons` VALUES (1,'ART VINYL','ÐšÑ€Ð°ÑÐ¾Ñ‚Ð° Ð¿Ð°Ñ€ÐºÐµÑ‚Ð°, Ð¿Ñ€Ð°ÐºÑ‚Ð¸Ñ‡Ð½Ð¾ÑÑ‚ÑŒ Ð»Ð¸Ð½Ð¾Ð»ÐµÑƒÐ¼Ð°, Ð¼Ð¾Ð´ÑƒÐ»ÑŒÐ½Ð¾ÑÑ‚ÑŒ Ð¿Ð»Ð¸Ñ‚ÐºÐ¸',NULL,'Tarkett Art Vinyl - ÐºÑ€ÐµÐ°Ñ‚Ð¸Ð²Ð½Ñ‹Ð¹ Ð¿Ð¾Ð»','art vinyl, Ð°Ñ€Ñ‚ Ð²Ð¸Ð½Ð¸Ð», Ð¼Ð¾Ð´ÑƒÐ»ÑŒÐ½Ñ‹Ð¹ Ð¿Ð¾Ð», Ð²Ð¸Ð½Ð¸Ð», ÐºÑ€ÐµÐ°Ñ‚Ð¸Ð²Ð½Ñ‹Ð¹ Ð¿Ð¾Ð»','Tarkett Art Vinyl - ÐºÑ€ÐµÐ°Ñ‚Ð¸Ð²Ð½Ñ‹Ð¹ Ð¿Ð¾Ð»: ÐºÑ€Ð°ÑÐ¾Ñ‚Ð° Ð¿Ð°Ñ€ÐºÐµÑ‚Ð°, Ð¿Ñ€Ð°ÐºÑ‚Ð¸Ñ‡Ð½Ð¾ÑÑ‚ÑŒ Ð»Ð¸Ð½Ð¾Ð»ÐµÑƒÐ¼Ð°, Ð¼Ð¾Ð´ÑƒÐ»ÑŒÐ½Ð¾ÑÑ‚ÑŒ Ð¿Ð»Ð¸Ñ‚ÐºÐ¸','Ð¡Ð¾Ð·Ð´Ð°Ð¹ ÑÐ²Ð¾Ð¹ ÑƒÐ½Ð¸ÐºÐ°Ð»ÑŒÐ½Ñ‹Ð¹ Ð¿Ð¾Ð»','Ð’Ñ‹ Ð¼Ð¾Ð¶ÐµÑ‚Ðµ ÑÐ¾Ð·Ð´Ð°Ñ‚ÑŒ ÑÐ²Ð¾Ð¹ Ð¸Ð½Ð´Ð¸Ð²Ð¸Ð´ÑƒÐ°Ð»ÑŒÐ½Ñ‹Ð¹ Ð´Ð¸Ð·Ð°Ð¹Ð½ Ð½Ð°Ð¿Ð¾Ð»ÑŒÐ½Ð¾Ð³Ð¾ Ð¿Ð¾ÐºÑ€Ñ‹Ñ‚Ð¸Ñ Ð² Ñ€Ð°Ð¼ÐºÐ°Ñ… Ð¾Ð´Ð½Ð¾Ð¹ ÐºÐ¾Ð»Ð»ÐµÐºÑ†Ð¸Ð¸ Ð¸Ð· Ð´Ð²ÑƒÑ… Ñ‚Ð¸Ð¿Ð¾Ð² ÑÐ¾ÐµÐ´Ð¸Ð½ÐµÐ½Ð¸Ð¹ ÐºÐ»ÐµÐµÐ²Ð¾Ð¹ Ð¸Ð»Ð¸ Ð·Ð°Ð¼ÐºÐ¾Ð²Ñ‹Ð¹');
/*!40000 ALTER TABLE `start_page_commons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `start_page_facts`
--

DROP TABLE IF EXISTS `start_page_facts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `start_page_facts` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `updated` datetime DEFAULT NULL,
                                    `position` int(11) DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `start_page_facts`
--

LOCK TABLES `start_page_facts` WRITE;
/*!40000 ALTER TABLE `start_page_facts` DISABLE KEYS */;
INSERT INTO `start_page_facts` VALUES (1,'100% Ð²Ð»Ð°Ð³Ð¾ÑÑ‚Ð¾Ð¹ÐºÐ¾ÑÑ‚Ð¸','9762386602.svg','2019-05-17 09:59:58',1),(2,'Ð”Ð¾Ð»Ð³Ð¾Ð²ÐµÑ‡Ð½Ð¾ÑÑ‚ÑŒ Ð¸ Ð¸Ð·Ð½Ð¾ÑÐ¾ÑÑ‚Ð¾Ð¹ÐºÐ¾ÑÑ‚ÑŒ','9928073277.svg','2019-05-24 14:37:54',2),(3,'ÐšÐ¾Ð¼Ñ„Ð¾Ñ€Ñ‚ Ð¿Ñ€Ð¸ ÑÐºÑÐ¿Ð»ÑƒÐ°Ñ‚Ð°Ñ†Ð¸Ð¸','8109633310.svg','2019-05-22 13:22:10',3),(4,'Ð­ÐºÐ¾Ð»Ð¾Ð³Ð¸Ñ‡Ð½Ð¾ÑÑ‚ÑŒ Ð¸ Ð±ÐµÐ·Ð¾Ð¿Ð°ÑÐ½Ð¾ÑÑ‚ÑŒ','9929654992.svg','2019-05-22 13:22:22',4),(5,'Ð£Ð´Ð¾Ð±ÑÑ‚Ð²Ð¾ ÑƒÑ…Ð¾Ð´Ð°','9854044740.svg','2019-05-24 14:37:17',5),(6,'Ð£Ð´Ð¾Ð±Ð½Ð°Ñ Ñ‚Ñ€Ð°Ð½ÑÐ¿Ð¾Ñ€Ñ‚Ð¸Ñ€Ð¾Ð²ÐºÐ° Ð¸ Ñ…Ñ€Ð°Ð½ÐµÐ½Ð¸Ðµ','5872166033.svg','2019-05-22 13:22:36',6),(7,'Ð”Ð¸Ð·Ð°Ð¹Ð½ÐµÑ€ÑÐºÐ¸Ðµ Ð²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ‚Ð¸','9828952655.svg','2019-05-17 10:00:08',7);
/*!40000 ALTER TABLE `start_page_facts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `start_page_facts_info`
--

DROP TABLE IF EXISTS `start_page_facts_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `start_page_facts_info` (
                                         `id` int(11) NOT NULL AUTO_INCREMENT,
                                         `fact_id` int(11) DEFAULT NULL,
                                         `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `position` int(11) DEFAULT NULL,
                                         PRIMARY KEY (`id`),
                                         KEY `IDX_81060F00156D5C2A` (`fact_id`),
                                         CONSTRAINT `FK_81060F00156D5C2A` FOREIGN KEY (`fact_id`) REFERENCES `start_page_facts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `start_page_facts_info`
--

LOCK TABLES `start_page_facts_info` WRITE;
/*!40000 ALTER TABLE `start_page_facts_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `start_page_facts_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `start_page_slider`
--

DROP TABLE IF EXISTS `start_page_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `start_page_slider` (
                                     `id` int(11) NOT NULL AUTO_INCREMENT,
                                     `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                     `text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                     `updated` datetime DEFAULT NULL,
                                     `position` int(11) DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `start_page_slider`
--

LOCK TABLES `start_page_slider` WRITE;
/*!40000 ALTER TABLE `start_page_slider` DISABLE KEYS */;
INSERT INTO `start_page_slider` VALUES (1,'Ð‘Ð¾Ð³Ð°Ñ‚Ñ‹Ð¹ Ð°ÑÑÐ¾Ñ€Ñ‚Ð¸Ð¼ÐµÐ½Ñ‚','Ñ€Ð°ÑÑ†Ð²ÐµÑ‚Ð¾Ðº Ð¸ Ñ„Ð°ÐºÑ‚ÑƒÑ€ Ð¿Ð»Ð°Ð½Ð¾Ðº Ð´Ð»Ñ Ð»ÑŽÐ±Ð¾Ð³Ð¾ ÑÑ‚Ð¸Ð»Ñ Ð¸Ð½Ñ‚ÐµÑ€ÑŒÐµÑ€Ð°','2212965817.jpg','2019-05-24 17:09:58',1),(2,'ÐœÐ½Ð¾Ð³Ð¾Ð¾Ð±Ñ€Ð°Ð·Ð¸Ðµ ÑÑ…ÐµÐ¼ ÑƒÐºÐ»Ð°Ð´Ð¾Ðº','Ð¡Ð¾Ð·Ð´Ð°Ð¹Ñ‚Ðµ ÑƒÐ½Ð¸ÐºÐ°Ð»ÑŒÐ½Ñ‹Ð¹ Ð¸ Ð½ÐµÐ¿Ð¾Ð²Ñ‚Ð¾Ñ€Ð¸Ð¼Ñ‹Ð¹ Ð¿Ð¾Ð»','9779032546.jpg','2019-05-24 17:09:51',2),(3,'Ð’Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ‚ÑŒ Ñ‚Ð²Ð¾Ñ€Ñ‡ÐµÑÑ‚Ð²Ð°','Ð´Ð»Ñ Ñ€Ð°Ð·Ñ€Ð°Ð±Ð¾Ñ‚ÐºÐ¸ ÑÐ¾Ð±ÑÑ‚Ð²ÐµÐ½Ð½Ð¾Ð³Ð¾ Ð´Ð¸Ð·Ð°Ð¹Ð½-Ð¿Ñ€Ð¾ÐµÐºÑ‚Ð° Ð¸ ÑÐ¾Ð·Ð´Ð°Ð½Ð¸Ñ ÑƒÐ½Ð¸ÐºÐ°Ð»ÑŒÐ½Ð¾Ð³Ð¾ Ð¿Ñ€Ð¾ÑÑ‚Ñ€Ð°Ð½ÑÑ‚Ð²Ð°','1856993927.jpg','2019-05-24 17:09:11',3),(4,'ÐœÐ¾Ð´ÑƒÐ»ÑŒÐ½Ð¾ÑÑ‚ÑŒ','ÑÐ»ÐµÐ¼ÐµÐ½Ñ‚Ñ‹ Ð¿Ð¾Ð»Ð° Ð¸Ð¼ÐµÑŽÑ‚ Ñ‚Ð¾Ñ‡Ð½Ñ‹Ðµ Ñ€Ð°Ð·Ð¼ÐµÑ€Ñ‹ Ð¸ Ñ„Ð¾Ñ€Ð¼Ñƒ, Ð¾Ñ‰ÑƒÑ‰ÐµÐ½Ð¸Ðµ ÐµÐ´Ð¸Ð½Ð¾Ð³Ð¾ Ð±ÐµÑÑˆÐ¾Ð²Ð½Ð¾Ð³Ð¾ Ð¼Ð°ÑÑÐ¸Ð²Ð°','1718588585.jpg','2019-05-24 17:09:11',4),(5,'Ð¡Ð¾Ð·Ð´Ð°Ð½Ð¸Ðµ Ð¿Ñ€Ð¾ÑÑ‚Ñ€Ð°Ð½ÑÑ‚Ð²Ð°','Â«ÑÑ„Ñ„ÐµÐºÑ‚Ð° Ð¾Ð±ÑŠÐµÐ¼Ð°Â» Ð·Ð° ÑÑ‡ÐµÑ‚ Ñ‡ÐµÑ‚Ñ‹Ñ€ÐµÑ…ÑÑ‚Ð¾Ñ€Ð¾Ð½Ð½ÐµÐ¹ Ñ„Ð°ÑÐºÐ¸','3158663027.jpg','2019-05-24 17:09:11',5);
/*!40000 ALTER TABLE `start_page_slider` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-07 10:37:22
